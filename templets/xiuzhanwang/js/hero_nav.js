$(function(){
	var $heroNav = $("#hero_nav");
	if($heroNav[0]){
		var $heroNavH3 = $heroNav.find("h3:first-child"), $heroNavSpan = $heroNav.find(".up"),$heroNavUl = $heroNav.find("ul");
		if ($heroNavH3.length > 0) {
			$heroNavH3.on("click",function(){
				if($heroNavSpan.hasClass("down")){
				   $heroNavSpan.removeClass("down");
				   $heroNavUl.hide();
				}else{
					$heroNavSpan.addClass("down");
					$heroNavUl.css('display', 'table');
				}
			});
		}

		var resize_flag = false;
		$(window).resize(function() {
			if (!resize_flag) {
				resize_flag = true;
				if(window.matchMedia && window.matchMedia("(min-width:998px)").matches) {
					$heroNavUl.css('display', 'table');
				} else {
					$heroNavSpan.removeClass("down");
					$heroNavUl.hide();
				}
			}
			setTimeout(function() {resize_flag = false}, 100);
		});
	}

	//游戏封面切换
	var $gameCover = $("#game-cover"),$covers = $("#game-covers a");
	if($covers[1] && $gameCover){
		$covers.on("click",function(){
			var $this = $(this);
			$this.addClass("cur").siblings().removeClass("cur");
			$gameCover.attr("href",$this.attr("data-original"));
			$gameCover.find("img").attr("src",$this.attr("data-cover"))
		})
	}
})