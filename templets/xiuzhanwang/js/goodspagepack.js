var FavoriteControlStyleType = {FloatStyle: 1, ListStyle: 2, SCMStyle: 3};
var FavoriteCategoryControl = Class.create();
Object.extend(FavoriteCategoryControl.prototype, {
    FAVORITE_CATEGORY_NAME_DEFAULT_TXT: "新分类名称",
    FAVORITE_CATEGORY_NAME_MAX_LENGTH: 25,
    FAVORITE_CATEGORY_NAME_MAX_DESCRIPTION: "分类名不能超过{0}个字",
    closed: false,
    favoriteCategories: [],
    favoriteDialog: null,
    selectedCategoryBtn: null,
    favoriteCategoryListRegion: null,
    newCategoryTextId: null,
    newCategoryBtn: null,
    newCategoryControlBtn: null,
    newCategoryItemId: null,
    htmlElement: "li",
    isFloatStyle: true,
    server: {
        getFavoriteCategorys: function (h, g, f, e) {
            if (siteCommunityServiceUrl) {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetFavoriteCategorys2", [h, g, f], e, "/community.api?Ajax_CallBack=true", "post", "20000")
            }
        }, addToFavorate: function (k, h, f, j, g) {
            if (siteCommunityServiceUrl) {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "AddToFavorate102", [k, h, f, j], g, "/community.api?Ajax_CallBack=true", "post", "20000")
            }
        }, createCategory: function (c, d) {
            if (siteCommunityServiceUrl) {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "CreateFavoriteCategory2", [c], d, "/community.api?Ajax_CallBack=true", "post", "20000")
            }
        }
    },
    initialize: function (b) {
        this.favoriteCategoryListRegion = b.favoriteCategoryListRegionObj;
        this.favoriteDialog = b;
        this.load();
        this.server.getFavoriteCategorys(this.favoriteDialog.relatedObjType, this.favoriteDialog.relatedObjId, true, function () {
            var a = getFavoriteCategorysResult;
            if (a && a.value) {
                if (!a.value.isLogin) {
                    if (typeof UserLoginDialog != "undefined") {
                        var d = new UserLoginDialog({
                            callback: function () {
                                location.reload()
                            }
                        })
                    } else {
                        $loadJs(["/js/2014/UserLoginDialog.js"], function () {
                            var c = new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        })
                    }
                    return
                }
                this.favoriteCategories = a.value.categorys;
                this.favoriteCountWithoutCategory = a.value.favoriteCountWithoutCategory;
                this.favoriteDialog.isLogin = a.value.isLogin;
                this.favoriteDialog.hasFavorite = a.value.hasFavorited;
                if (this.favoriteDialog.hasFavorite) {
                    this.favoriteDialog.favoriteButtonText.innerHTML = "已收藏";
                    if (!this.favoriteDialog.favoriteButton.hasClassName("curron") && this.favoriteDialog.favoriteControlStyleType == FavoriteControlStyleType.FloatStyle) {
                        this.favoriteDialog.favoriteButton.addClassName("curron");
                        this.favoriteDialog.favoriteButton.title = "已收藏"
                    }
                    this.buildFavoriteCategoryListRegion();
                    this.initializeDOM();
                    this.initializeEvent();
                    if (a.value.isLogin) {
                        this.favoriteDialog.favoriteContainer.show();
                        this.favoriteDialog.favoriteButton.addClassName("curr")
                    }
                } else {
                    this.server.addToFavorate(this.favoriteDialog.relatedObjType, this.favoriteDialog.relatedObjId, 0, "", function () {
                        var c = addToFavorateResult;
                        if (c && c.value) {
                            if (!this.favoriteDialog.favoriteButton.hasClassName("curron") && this.favoriteDialog.favoriteControlStyleType == FavoriteControlStyleType.FloatStyle) {
                                this.favoriteDialog.favoriteButton.addClassName("curron");
                                this.favoriteDialog.favoriteButton.title = "已收藏"
                            }
                            this.favoriteDialog.hasFavorite = true;
                            this.favoriteDialog.favoriteButtonText.innerHTML = "已收藏"
                        }
                        this.buildFavoriteCategoryListRegion();
                        this.initializeDOM();
                        this.initializeEvent();
                        this.favoriteDialog.favoriteContainer.show();
                        this.favoriteDialog.favoriteButton.addClassName("curr")
                    }.bind(this))
                }
            }
        }.bind(this))
    },
    initializeDOM: function () {
        this.selectedCategoryBtn = this.favoriteDialog.selectedCategoryObj
    },
    destroyDOM: function () {
        this.selectedCategoryBtn = null;
        this.favoriteDialog = null;
        this.favoriteCategories = [];
        this.favoriteCategoryListRegion = null;
        this.newCategoryTextId = null;
        this.newCategoryBtn = null;
        this.newCategoryControlBtn = null;
        this.newCategoryItemId = null
    },
    initializeEvent: function () {
        this.onClickSelectedCategoryBtnHandler = this.onClickSelectedCategoryBtn.bindAsEventListener(this);
        Event.observe(this.selectedCategoryBtn, "click", this.onClickSelectedCategoryBtnHandler);
        this.onClickNewCategoryBtnHandler = this.onClickNewCategoryBtn.bindAsEventListener(this);
        Event.observe(this.newCategoryBtn, "click", this.onClickNewCategoryBtnHandler)
    },
    destroyEvent: function () {
        Event.stopObserving(this.onClickSelectedCategoryBtn, "click", this.onClickSelectedCategoryBtnHandler);
        Event.stopObserving(this.onClickNewCategoryBtn, "click", this.onClickNewCategoryBtnHandler);
        this.favoriteCategoryListRegion.getElementsBySelector(this.htmlElement).each(function (f) {
            var d = f.readAttribute("categoryId");
            if (typeof(d) != "undefined") {
                var e = Event.findElement(f, this.htmlElement);
                Event.stopObserving(e, "click", this.onClickAddFavoriteBtnHandler)
            }
        })
    },
    buildCategoryItem: function (g, h, f) {
        var e = Builder.node(this.htmlElement, {categoryId: g}, [Builder.node("a", {
            href: "#",
            method: "selectCategory"
        }, [h])]);
        this.onClickAddFavoriteBtnHandler = this.onClickAddFavoriteBtn.bindAsEventListener(this);
        Event.observe(e, "click", this.onClickAddFavoriteBtnHandler);
        return e
    },
    buildNewCategoryItem: function () {
        this.newCategoryControlBtn = Builder.node(this.htmlElement, {}, [Builder.node("a", {
            href: "#",
            method: "selectCategory"
        }, [Builder.node("i", {className: "ico_t_add"}), "新分类"])]);
        return this.newCategoryControlBtn
    },
    buildAddTextCategoryItem: function () {
        this.newCategoryTextId = Builder.node("input", {className: "colltxt", type: "text"});
        this.newCategoryBtn = Builder.node("input", {className: "btn", type: "button"});
        if (this.isFloatStyle) {
            this.newCategoryItemId = Builder.node("li", {}, [Builder.node("div", {className: "addcollsec clearfix"}, [this.newCategoryTextId, this.newCategoryBtn])])
        } else {
            this.newCategoryItemId = Builder.node("div", {className: "addcollsec clearfix"}, [this.newCategoryTextId, this.newCategoryBtn])
        }
        return this.newCategoryItemId
    },
    buildFavoriteCategoryListRegion: function () {
        this.favoriteCategoryListRegion.appendChild(this.buildCategoryItem(0, "未分类", this.favoriteCountWithoutCategory));
        this.favoriteCategories.each(function (b) {
            this.favoriteCategoryListRegion.appendChild(this.buildCategoryItem(b.Id, b.Name, b.Count))
        }.bind(this));
        this.favoriteCategoryListRegion.appendChild(this.buildAddTextCategoryItem())
    },
    onClickSelectedCategoryBtn: function (d) {
        htmlElement = "dl";
        if (!this.isFloatStyle) {
            htmlElement = "div"
        }
        var c = Event.findElement(d, htmlElement);
        if (c.hasClassName("curr")) {
            c.removeClassName("curr");
            if (this.favoriteDialog.categoryContrainerObj.visible()) {
                this.favoriteDialog.categoryContrainerObj.hide()
            }
        } else {
            c.addClassName("curr");
            if (!this.favoriteDialog.categoryContrainerObj.visible()) {
                this.favoriteDialog.categoryContrainerObj.show()
            }
        }
        Event.stop(d)
    },
    onClickNewCategoryControlBtn: function (b) {
        if (this.newCategoryItemId) {
            if (!this.newCategoryItemId.visible()) {
                this.newCategoryItemId.show()
            } else {
                this.newCategoryItemId.hide()
            }
        }
        Event.stop(b)
    },
    onClickNewCategoryBtn: function (d) {
        Event.stop(d);
        var c = this.newCategoryTextId.value.trim();
        if (c.length > 0) {
            if (c.bytelength() > 50) {
                $alert("分类名不要超过25个汉字！");
                return
            }
            this.server.createCategory(c, function () {
                var a = createFavoriteCategoryResult;
                if (a && a.value) {
                    this.createCategoryCallback(a.value)
                } else {
                    $alert("添加分类失败，请稍候重试")
                }
            }.bind(this))
        } else {
            $alert("分类名不能为空！")
        }
    },
    createCategoryCallback: function (g) {
        var e = g.id;
        var f = g.name;
        if (this.favoriteCategories !== null) {
            this.favoriteCategories.push({Id: e, Name: f});
            var h = this.buildCategoryItem(e, f, this.favoriteCategories.Count);
            this.favoriteCategoryListRegion.insertBefore(h, this.newCategoryItemId);
            this.newCategoryTextId.value = ""
        }
    },
    addToFavoriteCallback: function () {
        var b = addToFavorateResult;
        if (b && b.value) {
            this.favoriteDialog.hasFavorite = true;
            this.favoriteDialog.favoriteButtonText.innerHTML = "已收藏";
            this.favoriteDialog.favoriteContainer.hide();
            this.favoriteDialog.favoriteButton.removeClassName("curr")
        } else {
            $alert("添加收藏失败，请稍候重试")
        }
        Event.stop()
    },
    onClickAddFavoriteBtn: function (f) {
        var e = Event.findElement(f, this.htmlElement);
        var d = e.readAttribute("categoryId");
        this.server.addToFavorate(this.favoriteDialog.relatedObjType, this.favoriteDialog.relatedObjId, d, "", this.addToFavoriteCallback.bind(this));
        Event.stop(f)
    },
    load: function () {
        if (this.favoriteDialog.favoriteControlStyleType == FavoriteControlStyleType.FloatStyle) {
            this.htmlElement = "li"
        } else {
            if (this.favoriteDialog.favoriteControlStyleType == FavoriteControlStyleType.ListStyle) {
                this.htmlElement = "p";
                this.isFloatStyle = false
            }
        }
    },
    close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var FavoriteButtonControl = Class.create();
Object.extend(FavoriteButtonControl.prototype, {
    name: "FavoriteButtonControl",
    closed: false,
    relatedObjType: 0,
    relatedObjId: 0,
    hasFavorited: false,
    observer: null,
    favoriteButton: null,
    favoriteContainer: null,
    favoriteButtonText: null,
    favoriteCategoryControl: null,
    options: {
        isLogin: false,
        signInUrl: "",
        relatedObjId: 0,
        relatedObjType: 0,
        shareRelatedObjType: 0,
        hasFavorited: false,
        favoriteButton: "",
        onFavoriteOKCallback: null,
        favoriteControlStyleType: FavoriteControlStyleType.FloatStyle,
        callback: null,
        expandMovieId: 0
    },
    FavoriteTweet: function (f, e, d) {
        return Mtime.Component.Ajax.get("Mtime.Service.Pages.TwitterService", "FavoriteTweet", [f], d, "/Service/Twitter.msi", "get", "20000", e)
    },
    getFavoriteCategorys: function (h, g, f, e) {
        if (siteCommunityServiceUrl) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetFavoriteCategorys2", [h, g, f], e, "/community.api?Ajax_CallBack=true", "post", "20000")
        }
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initialize: function (b) {
        this.setOptions(b);
        if (this.signInUrl) {
            this.signInUrl = this.signInUrl + "/operation/?redirectUrl=" + encodeURIComponent(location.href)
        } else {
            this.signInUrl = siteMcUrl + "/member/signin/operation/?redirectUrl=" + encodeURIComponent(location.href)
        }
        if (this.shareRelatedObjType !== "9992" && this.shareRelatedObjType !== "6401") {
            this.initializeField();
            this.initializeDOM();
            this.initializeControl();
            this.initializeEvent();
            this.load();
            this.render()
        } else {
            this.getFavoriteCategorys(this.relatedObjType, this.relatedObjId, true, function () {
                var a = getFavoriteCategorysResult;
                if (a && a.value) {
                    if (!a.value.isLogin) {
                        if (typeof UserLoginDialog != "undefined") {
                            var d = new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        } else {
                            $loadJs(["/js/2014/UserLoginDialog.js"], function () {
                                var c = new UserLoginDialog({
                                    callback: function () {
                                        location.reload()
                                    }
                                })
                            })
                        }
                    } else {
                        this.isLogin = true;
                        this.FavoriteTweet(this.relatedObjId, this.favoriteButton, function (f) {
                            if (f && f.value) {
                                var c = [];
                                if (this.favoriteControlStyleType == FavoriteControlStyleType.FloatStyle) {
                                    c.push('<p><i class="ico_t_coll"></i><span>已收藏</span></p>')
                                } else {
                                    if (this.favoriteControlStyleType == FavoriteControlStyleType.ListStyle) {
                                        c.push('<p><i class="ico_t_coll"></i><span>已收藏</span></p>')
                                    }
                                }
                                this.favoriteContainer.innerHTML = c.join("");
                                this.favoriteContainer.show();
                                this.favoriteContainer.addClassName("onlycollect");
                                this.favoriteButton.addClassName("curr");
                                this.initializeEvent()
                            } else {
                                $alert("电影微评收藏失败")
                            }
                        }.bind(this))
                    }
                }
            }.bind(this))
        }
        Event.observe(window, "unload", this.close.bind(this))
    },
    initializeField: function () {
    },
    initializeDOM: function () {
        var c = [];
        this.favoriteCategoryListRegionObj;
        this.selectedCategoryObj;
        this.categoryContrainer;
        this.categoryContrainerSelectedObj;
        var d = Builder.node("p", {}, [Builder.node("i", {className: "ico_t_coll"}), Builder.node("span", {}, ["收 藏"])]);
        if (this.favoriteControlStyleType == FavoriteControlStyleType.FloatStyle) {
            this.favoriteCategoryListRegionObj = Builder.node("ul", {});
            this.selectedCategoryObj = Builder.node("dt", {}, [Builder.node("i", {className: "ico_t_collsec"}), Builder.node("span", {}, ["请选择分类"])]);
            this.categoryContrainerObj = Builder.node("dd", {}, [this.favoriteCategoryListRegionObj]);
            this.categoryContrainerSelectedObj = Builder.node("dl", {}, [this.selectedCategoryObj, this.categoryContrainerObj]);
            this.favoriteContainer.appendChild(d);
            this.favoriteContainer.appendChild(this.categoryContrainerSelectedObj)
        } else {
            if (this.favoriteControlStyleType == FavoriteControlStyleType.ListStyle) {
                this.favoriteCategoryListRegionObj = Builder.node("div", {className: "hidbox"});
                this.selectedCategoryObj = Builder.node("strong", {}, [Builder.node("i", {className: "ico_t_collsec"}), Builder.node("span", {}, ["请选择分类"])]);
                this.categoryContrainerObj = this.favoriteCategoryListRegionObj;
                this.categoryContrainerSelectedObj = Builder.node("div", {className: "showcoll"}, [this.selectedCategoryObj, this.favoriteCategoryListRegionObj]);
                this.favoriteContainer.appendChild(d);
                this.favoriteContainer.appendChild(this.categoryContrainerSelectedObj)
            } else {
                throw"favoriteControlStyleType does not implemented!"
            }
        }
        this.favoriteButtonText = this.favoriteContainer.getElementsBySelector("p span")[0]
    },
    destroyDOM: function () {
        this.favoriteButton = null;
        this.favoriteButtonText = null;
        this.favoriteContainer = null;
        this.favoriteCategories = [];
        this.favoriteCategoryListRegion = null
    },
    initializeEvent: function () {
        this.onClickFavoriteButtonHandler = this.onClickFavoriteButton.bindAsEventListener(this);
        this.favoriteButton.observe("click", this.onClickFavoriteButtonHandler)
    },
    destroyEvent: function () {
        this.favoriteButton.stopObserving("click", this.onClickFavoriteButtonHandler)
    },
    initializeControl: function () {
        this.favoriteCategoryControl = new FavoriteCategoryControl(this)
    },
    destroyControl: function () {
        this.favoriteCategoryControl = null
    },
    load: function () {
    },
    render: function () {
        if (this.hasFavorited) {
            this.favoriteButtonText.innerHTML = "已收藏";
            if (!this.favoriteButton.hasClassName("curron") && this.favoriteControlStyleType == FavoriteControlStyleType.FloatStyle) {
                this.favoriteButton.addClassName("curron");
                this.favoriteButton.title = "已收藏"
            }
        } else {
            this.favoriteButtonText.innerHTML = "收 藏"
        }
    },
    onClick: function (f) {
        var e = Event.findElement(f, "div");
        if (e && e.readAttribute) {
            var d = e.readAttribute("class");
            if (d == "addcollsec clearfix") {
                return
            }
        }
        if (this.favoriteButton) {
            this.favoriteButton.removeClassName("curr")
        }
        if (this.favoriteContainer) {
            this.favoriteContainer.hide()
        }
    },
    onClickFavoriteButton: function (f) {
        var e = Event.findElement(f, "div");
        var g = e.hasClassName("curr");
        if (this.callback) {
            this.callback(e)
        }
        if (!this.isLogin) {
            if (typeof UserLoginDialog != "undefined") {
                var h = new UserLoginDialog({
                    callback: function () {
                        location.reload()
                    }
                })
            } else {
                $loadJs(["/js/2014/UserLoginDialog.js"], function () {
                    var a = new UserLoginDialog({
                        callback: function () {
                            location.reload()
                        }
                    })
                })
            }
        } else {
            if (g) {
                e.removeClassName("curr");
                if (this.favoriteContainer) {
                    this.favoriteContainer.hide()
                }
            } else {
                if (e.hasClassName("notcurr")) {
                    e.removeClassName("notcurr")
                }
                e.addClassName("curr");
                if (this.categoryContrainerObj) {
                    this.categoryContrainerObj.hide();
                    if (this.categoryContrainerSelectedObj) {
                        if (this.categoryContrainerSelectedObj.hasClassName("curr")) {
                            this.categoryContrainerSelectedObj.removeClassName("curr")
                        }
                    }
                }
                if (this.favoriteContainer) {
                    this.favoriteContainer.show()
                }
            }
        }
        Event.stop(f)
    },
    onFavoriteControlDialogOKCallback: function (b) {
        this.hasFavorited = b.success;
        this.favoritedCount = b.favoritedCount;
        this.render();
        if (this.onFavoriteOKCallback) {
            this.onFavoriteOKCallback(b)
        }
    },
    close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyControl();
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var FavoriteButtonControlSCM = Class.create();
Object.extend(FavoriteButtonControlSCM.prototype, {
    name: "FavoriteButtonControlSCM",
    closed: false,
    hasFavorited: false,
    observer: null,
    favoriteButton: null,
    options: {
        goodsId: 0,
        isLogin: false,
        signInUrl: "",
        hasFavorited: false,
        favoriteButton: "",
        favoriteControlStyleType: FavoriteControlStyleType.SCMStyle,
        callback: null
    },
    hasIsLogin: function (f, e, d) {
        if (siteCommunityServiceUrl) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "HasIsLogin", [f, e], d, "/community.api?Ajax_CallBack=true", "post", "20000")
        }
    },
    addFavoritedSCM: function (d, c) {
        if (siteCommunityServiceUrl) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "AddFavoritedSCM", [d], c, "/community.api?Ajax_CallBack=true", "post", "20000")
        }
    },
    removeFavoritedSCM: function (d, c) {
        if (siteCommunityServiceUrl) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "RemoveFavoritedSCM", [d], c, "/community.api?Ajax_CallBack=true", "post", "20000")
        }
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    clickFavorite: function () {
        this.hasIsLogin(this.goodsId, true, function () {
            var c = getLoginAndFavoriteResult;
            if (c && c.value) {
                if (!c.value.isLogin) {
                    if (typeof UserLoginDialog != "undefined") {
                        var d = new UserLoginDialog({
                            callback: function () {
                                location.reload()
                            }
                        })
                    } else {
                        $loadJs(["/js/2014/UserLoginDialog.js"], function () {
                            var a = new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        })
                    }
                } else {
                    this.isLogin = true;
                    if (typeof c.value.hasFavorited != "undefined") {
                        if (c.value.hasFavorited) {
                            this.removeFavoritedSCM(this.goodsId, function () {
                                if (removeFavoriteSCMResult && removeFavoriteSCMResult.value && typeof removeFavoriteSCMResult.value.success != "undefined") {
                                    if (removeFavoriteSCMResult.value.success) {
                                        if (this.favoriteButton.hasClassName("on")) {
                                            this.favoriteButton.removeClassName("on")
                                        }
                                    } else {
                                        $alert("取消收藏失败")
                                    }
                                } else {
                                    $alert("取消收藏异常")
                                }
                            }.bind(this))
                        } else {
                            this.addFavoritedSCM(this.goodsId, function () {
                                if (addFavoriteSCMResult && addFavoriteSCMResult.value && typeof addFavoriteSCMResult.value.success != "undefined") {
                                    if (addFavoriteSCMResult.value.success) {
                                        if (!this.favoriteButton.hasClassName("on")) {
                                            this.favoriteButton.addClassName("on")
                                        }
                                    } else {
                                        $alert("添加收藏失败")
                                    }
                                } else {
                                    $alert("添加收藏异常")
                                }
                            }.bind(this))
                        }
                    }
                }
            }
        }.bind(this))
    },
    initialize: function (b) {
        this.setOptions(b);
        if (this.signInUrl) {
            this.signInUrl = this.signInUrl + "/operation/?redirectUrl=" + encodeURIComponent(location.href)
        } else {
            this.signInUrl = siteMcUrl + "/member/signin/operation/?redirectUrl=" + encodeURIComponent(location.href)
        }
        this.clickFavorite();
        Event.observe(window, "unload", this.close.bind(this))
    },
    initializeField: function () {
    },
    initializeDOM: function () {
    },
    destroyDOM: function () {
        this.favoriteButton = null
    },
    initializeEvent: function () {
    },
    destroyEvent: function () {
    },
    initializeControl: function () {
    },
    destroyControl: function () {
    },
    load: function () {
    },
    render: function () {
    },
    close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyControl();
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var ToolBannerShareControlManager;
var siteCommunityServiceUrl = siteCommunityServiceUrl || "http://service.community.mtime.com";
var RightFloatToolsControl = Class.create();
Object.extend(RightFloatToolsControl.prototype, {
    options: {
        editorComment: "",
        hasSearchButton: true,
        hasShareButton: true,
        hasFavoriteButton: true,
        hasMessageButton: true,
        hasCodeButton: true,
        hasTopButton: true,
        conversionUrl: "",
        shareRelatedObjType: "",
        relatedId: 0,
        expandId: 0,
        autoLoad: "",
        screenshotImageUrl: "",
        movieTitle: "",
        cityName: "",
        imageUrl: "",
        imagePath: "",
        link: "",
        videoId: 0,
        title: "",
        fullTitle: "",
        pic: "",
        desc: "",
        expandMovieId: 0,
        date: new Date(),
        isLogin: false,
        signInUrl: "",
        relatedObjId: 0,
        relatedObjType: 0,
        hasFavorited: false,
        codeTemplate: '<dl><dt><img src="#{codeUrl}" width="115" height="115"></dt><dd><p>扫描二维码</p><p>手机浏览页面</p></dd></dl>'
    }, server: {
        getFavoriteCategorys: function (h, g, f, e) {
            if (siteCommunityServiceUrl) {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetFavoriteCategorys2", [h, g, f], e, "/community.api?Ajax_CallBack=true", "post", "20000")
            }
        }
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeDom();
        this.initializeEvent();
        this.load()
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initializeDom: function () {
        this.scrolltop = 0;
        this.addDOM();
        this.rightFloatControls = this.rightFloatControl.immediateDescendants();
        this.topbar = $("topbar");
        this.isSearch = true
    }, initializeEvent: function () {
        if (this.rightFloatControl) {
            this.onClickRigtFloatHandler = this.onClickRigtFloat.bind(this);
            Event.observe(this.rightFloatControl, "click", this.onClickRigtFloatHandler)
        }
        this.onClickBodyHandler = this.onClickBody.bind(this);
        Event.observe(document.body, "click", this.onClickBodyHandler);
        Event.observe(window, "unload", this.close.bind(this));
        this.setIntervalTimer()
    }, load: function () {
        if (this.hasFavoriteButton) {
            this.server.getFavoriteCategorys(this.relatedObjType, this.relatedObjId, true, function (b) {
                if (b && b.value) {
                    if (b.value.isLogin && b.value.hasFavorited && this.favoritebar) {
                        this.favoritebar.addClassName("curron");
                        this.favoritebar.title = "已收藏"
                    }
                }
            }.bind(this))
        }
    }, addDOM: function () {
        this.showButtonMethod(this.rightFloatControls);
        this.rightFloatControl = Builder.node("div", {className: "mtimebar"}, [this.searchbar, this.sharebar, this.favoritebar, this.messagebar, this.codebar, this.tooltopbar, this.sharebox, this.favoritbox, this.codebox]);
        document.body.appendChild(this.rightFloatControl)
    }, showButtonMethod: function () {
        if (!this.hasSearchButton) {
            this.searchbar = $(Builder.node("div", {className: "searchbar", title: "搜索", style: "display:none"}))
        } else {
            this.searchbar = $(Builder.node("div", {
                className: "searchbar",
                title: "搜索",
                method: "search",
                style: "display:none"
            }, [Builder.node("b", {className: "baron", method: "search"}, ["搜索"])]))
        }
        if (!this.hasShareButton) {
            this.sharebar = $(Builder.node("div", {className: "sharebar", title: "分享", style: "display:none"}))
        } else {
            this.sharebar = $(Builder.node("div", {
                className: "sharebar",
                title: "分享",
                method: "share"
            }, [Builder.node("b", {className: "baron", method: "share"}, ["分享"])]))
        }
        if (!this.hasFavoriteButton) {
            this.favoritebar = $(Builder.node("div", {className: "collectbar", title: "收藏", style: "display:none"}))
        } else {
            this.favoritebar = $(Builder.node("div", {
                className: "collectbar",
                title: "收藏",
                method: "favorite"
            }, [Builder.node("b", {className: "baron", method: "favorite"}, ["收藏"])]))
        }
        if (!this.hasMessageButton) {
            this.messagebar = $(Builder.node("div", {className: "messagebar", title: "留言", style: "display:none"}))
        } else {
            this.messagebar = $(Builder.node("div", {
                className: "messagebar",
                title: "留言",
                method: "message"
            }, [Builder.node("b", {className: "baron", method: "message"}, ["留言"])]))
        }
        if (!this.hasCodeButton) {
            this.codebar = $(Builder.node("div", {className: "codebar", title: "二维码", style: "display:none"}))
        } else {
            this.codebar = $(Builder.node("div", {
                className: "codebar",
                title: "二维码",
                method: "code"
            }, [Builder.node("b", {className: "baron", method: "code"}, ["二维码"])]))
        }
        if (!this.hasTopButton) {
            this.tooltopbar = $(Builder.node("div", {className: "topbar", title: "返回顶部", style: "display:none"}))
        } else {
            this.tooltopbar = $(Builder.node("div", {
                className: "topbar",
                title: "返回顶部",
                method: "backtop",
                style: "visibility:hidden"
            }, [Builder.node("b", {className: "baron", method: "backtop"}, ["返回顶部"])]))
        }
        this.sharebox = $(Builder.node("div", {className: "sharebox", style: "display:none;"}));
        this.favoritbox = $(Builder.node("div", {className: "collectbox", style: "display:none;"}));
        this.codebox = $(Builder.node("div", {className: "codebox", style: "display:none;"}));
        nullDiv = null
    }, onClickBody: function (e) {
        var d = Event.element(e);
        var f = d.up(".mtimebar");
        if (f !== this.rightFloatControl) {
            this.removeClassName()
        }
    }, onClickRigtFloat: function (o) {
        this.clickElement = Event.element(o);
        var q = this.clickElement.readAttribute("method");
        if (!this.clickElement || this.clickElement === document.body) {
            return
        }
        switch (q) {
            case"search":
                this.removeClassName();
                if (this.topbar) {
                    if (this.isSearch) {
                        this.lockTopBar = true;
                        this.isSearch = false;
                        this.topbar.addClassName("scrolltop");
                        this.scrolltop = document.documentElement.scrollTop || document.body.scrollTop;
                        this.topbarHeight = this.topbar.getHeight();
                        var p = this.topbar.down(".menubg");
                        this.topbarHeight += p.getHeight();
                        this.topbar.style.top = -this.topbarHeight + "px";
                        var k = this.topbar.down(".bartop");
                        var l = 0;
                        if (k) {
                            l = k.getHeight()
                        }
                        var r = this.topbar.down(".topline");
                        if (!r) {
                            r = this.topbar.down(".bartop")
                        }
                        var s = r.getHeight() + l;
                        document.body.style.paddingTop = s + "px";
                        this.topbar.addClassName("transition6");
                        FunctionExt.defer(function () {
                            this.topbar.style.position = "fixed";
                            this.topbar.style.top = "0px";
                            p = r = s = k = l = null
                        }, 200, this)
                    } else {
                        this.topbarAnimate()
                    }
                }
                break;
            case"share":
                this.removeClassName();
                if (typeof ShareControlManager !== "undefined") {
                    this.instanceShareControl()
                } else {
                    $loadJs("/js/2014/share.js", function () {
                        this.instanceShareControl()
                    }.bind(this))
                }
                this.sharebar.addClassName("curr");
                this.sharebox.show();
                break;
            case"favorite":
                this.removeClassName();
                if (typeof FavoriteButtonControl !== "undefined") {
                    this.instanceCollectControl()
                } else {
                    $loadJs("/js/2014/favorite.js", function () {
                        this.instanceCollectControl()
                    }.bind(this))
                }
                break;
            case"message":
                this.removeClassName();
                if ($(this.editorComment)) {
                    var n = $(this.editorComment);
                    var m = $("commentRegion");
                    if (m) {
                        m.scrollTo()
                    } else {
                        n.scrollTo()
                    }
                    n.focus();
                    n = null
                }
                break;
            case"code":
                if (this.codebar.hasClassName("curr")) {
                    this.removeClassName()
                } else {
                    this.removeClassName();
                    this.setClassName();
                    this.builderCodeMethod()
                }
                break;
            case"backtop":
                this.removeClassName();
                document.body.scrollIntoView();
                break
        }
        q = null
    }, instanceShareControl: function () {
        if (typeof ToolBannerShareControlManager == "undefined") {
            ToolBannerShareControlManager = new ShareControlManager()
        }
        if (!this.shareControlManager) {
            this.shareControlManager = ToolBannerShareControlManager.initShareControl([{
                shareRelatedObjType: this.shareRelatedObjType,
                relatedId: this.relatedId,
                expandId: this.expandId,
                autoLoad: this.autoLoad,
                screenshotImageUrl: this.screenshotImageUrl,
                movieTitle: this.movieTitle,
                cityName: this.cityName,
                imageUrl: this.imageUrl,
                imagePath: this.imagePath,
                link: this.link,
                videoId: this.relatedId,
                title: this.title,
                fullTitle: this.fullTitle,
                pic: this.pic,
                date: this.date,
                desc: this.desc,
                shareButton: this.sharebar,
                expandMovieId: this.expandMovieId,
                shareContainer: this.sharebox,
                callback: this.removeClassName.bind(this)
            }])
        }
    }, instanceCollectControl: function () {
        if (!this.favoriteButtonControl) {
            this.favoriteButtonControl = new FavoriteButtonControl({
                shareRelatedObjType: this.shareRelatedObjType,
                isLogin: this.isLogin,
                signInUrl: this.signInUrl,
                relatedObjId: this.relatedObjId,
                relatedObjType: this.relatedObjType,
                hasFavorited: this.hasFavorited,
                favoriteButton: this.favoritebar,
                favoriteContainer: this.favoritbox,
                callback: this.removeClassName.bind(this)
            })
        }
    }, builderCodeMethod: function () {
        var h, j = {}, m = new Template(this.codeTemplate), g = new StringBuilder(), l = "";
        if (this.conversionUrl !== "") {
            l = encodeURIComponent(this.conversionUrl)
        } else {
            if (getNewValue) {
                l = encodeURIComponent(getNewValue(location.href))
            } else {
                l = encodeURIComponent(location.href)
            }
        }
        j.codeUrl = siteServiceUrl + "/GetQrCodeHandler.ashx?qrCodeStr=" + l;
        h = m.evaluate(j);
        var k = Builder.build(h);
        this.codebox.appendChild(k);
        this.codebox.show()
    }, setClassName: function () {
        for (var b = 0; b <= 5; b++) {
            if (this.rightFloatControls[b] !== this.clickElement) {
                this.rightFloatControls[b].addClassName("notcurr")
            } else {
                this.rightFloatControls[b].addClassName("curr")
            }
        }
    }, removeClassName: function (e) {
        var f, g = 9, h = 9;
        if (e) {
            if (e === this.sharebar) {
                g = 1;
                h = 6
            } else {
                if (e === this.favoritebar) {
                    g = 2;
                    h = 7
                }
            }
        }
        for (f = 0; f <= 5; f++) {
            if (g !== f) {
                if (this.rightFloatControls[f].hasClassName("notcurr")) {
                    this.rightFloatControls[f].removeClassName("notcurr")
                }
                if (this.rightFloatControls[f].hasClassName("curr")) {
                    this.rightFloatControls[f].removeClassName("curr")
                }
            }
        }
        for (f = 6; f <= 8; f++) {
            if (h !== f) {
                this.rightFloatControls[f].hide()
            }
        }
        this.codebox.innerHTML = "";
        f = g = h = null
    }, setIntervalTimer: function () {
        var c, d;
        d = document.documentElement.scrollTop || document.body.scrollTop;
        if (this.scrolltop !== d && this.topbar && this.lockTopBar) {
            this.topbarAnimate()
        }
        if (d < 200) {
            if (this.searchbar) {
                this.searchbar.hide()
            }
            if (this.tooltopbar) {
                this.tooltopbar.style.visibility = "hidden"
            }
        } else {
            if (this.searchbar) {
                this.searchbar.show()
            }
            if (this.tooltopbar) {
                this.tooltopbar.style.visibility = "visible"
            }
        }
        c = d = null;
        this.myTimer = setTimeout(function () {
            this.setIntervalTimer()
        }.bind(this), 2000)
    }, topbarAnimate: function () {
        this.topbar.style.top = -this.topbarHeight + "px";
        FunctionExt.defer(function () {
            this.topbar.removeClassName("transition6");
            this.topbar.removeClassName("scrolltop");
            this.topbar.style.top = "0px";
            this.topbar.style.position = "";
            document.body.style.paddingTop = "";
            this.lockTopBar = false;
            this.isSearch = true
        }, 600, this)
    }, close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    }, destroyField: function () {
    }, destroyEvent: function () {
        if (this.rightFloatControl) {
            Event.stopObserving(this.rightFloatControl, "click", this.onClickRigtFloatHandler)
        }
        Event.stopObserving(document.body, "click", this.onClickBodyHandler)
    }, destroyDOM: function () {
        this.rightFloatControl = null;
        this.myTimer && clearTimeout(this.myTimer);
        this.myTimer = null
    }
});
var CrosswiseTools = Class.create();
Object.extend(CrosswiseTools.prototype, RightFloatToolsControl.prototype);
Object.extend(CrosswiseTools.prototype, {
    options: {
        slidesRegion: "",
        slidesRegionEl: null,
        editorComment: "",
        editorUrl: "",
        hasShareButton: true,
        hasFavoriteButton: true,
        expandMovieId: 0,
        shareRelatedObjType: "",
        relatedId: 0,
        expandId: 0,
        autoLoad: "",
        screenshotImageUrl: "",
        movieTitle: "",
        cityName: "",
        imageUrl: "",
        imagePath: "",
        link: "",
        videoId: 0,
        title: "",
        fullTitle: "",
        pic: "",
        desc: "",
        shareSuccessCallBack: null,
        date: new Date(),
        isLogin: false,
        signInUrl: "",
        relatedObjId: 0,
        relatedObjType: 0,
        hasFavorited: false
    }, initializeEvent: function () {
        if (this.rightFloatControl) {
            this.onClickRigtFloatHandler = this.onClickRigtFloat.bind(this);
            Event.observe(this.rightFloatControl, "click", this.onClickRigtFloatHandler)
        }
        Event.observe(window, "unload", this.close.bind(this))
    }, load: function () {
    }, addDOM: function () {
        this.slidesRegion = $(this.slidesRegion);
        if (!this.slidesRegion && this.slidesRegionEl) {
            this.slidesRegion = this.slidesRegionEl
        }
        this.showButtonMethod(this.rightFloatControls);
        this.rightFloatControl = $(Builder.node("div", {className: "smtimebar"}, [this.sharebar, this.favoritebar, this.messagebar, this.sharebox, this.favoritbox]));
        if (this.slidesRegion) {
            this.slidesRegion.appendChild(this.rightFloatControl)
        }
    }, showButtonMethod: function () {
        var d = $(Builder.node("div", {style: "display:none"}));
        if (!this.hasShareButton) {
            this.sharebar = $(Builder.node("div", {className: "sharebar", title: "分享", style: "display:none"}))
        } else {
            this.sharebar = $(Builder.node("div", {
                className: "sharebar",
                title: "分享",
                method: "share"
            }, [Builder.node("b", {className: "baron", method: "share"}, ["分享"])]))
        }
        if (!this.hasFavoriteButton) {
            this.favoritebar = $(Builder.node("div", {className: "collectbar", title: "收藏", style: "display:none"}))
        } else {
            this.favoritebar = $(Builder.node("div", {
                className: "collectbar",
                title: "收藏",
                method: "favorite"
            }, [Builder.node("b", {className: "baron", method: "favorite"}, ["收藏"])]))
        }
        if (!this.editorUrl) {
            this.messagebar = $(Builder.node("div", {className: "messagebar", title: "留言", style: "display:none"}))
        } else {
            var c = this.editorComment === "" ? this.editorUrl : this.editorUrl + "#" + this.editorComment;
            this.messagebar = Builder.node("div", {
                className: "messagebar",
                title: "留言"
            }, [Builder.node("a", {className: "baron", href: c, target: "_blank"}, ["留言"])]);
            c = null
        }
        this.sharebox = $(Builder.node("div", {className: "sharebox", style: "display:none;"}));
        this.favoritbox = $(Builder.node("div", {className: "collectbox", style: "display:none;"}));
        d = null
    }, onClickRigtFloat: function (c) {
        this.clickElement = Event.element(c);
        if (!this.clickElement.readAttribute) {
            return
        }
        var d = this.clickElement.readAttribute("method");
        switch (d) {
            case"share":
                this.removeClassName();
                if (typeof ShareControlManager !== "undefined") {
                    this.instanceShareControl()
                } else {
                    $loadJs("/js/2014/share.js", function () {
                        this.instanceShareControl()
                    }.bind(this))
                }
                this.sharebar.addClassName("curr");
                this.sharebox.show();
                break;
            case"favorite":
                this.removeClassName();
                if (typeof FavoriteButtonControl !== "undefined") {
                    this.instanceCollectControl()
                } else {
                    $loadJs("/js/2014/favorite.js", function () {
                        this.instanceCollectControl()
                    }.bind(this))
                }
                break
        }
    }, instanceShareControl: function () {
        if (typeof ToolBannerShareControlManager == "undefined") {
            ToolBannerShareControlManager = new ShareControlManager()
        }
        if (!this.shareControlManager) {
            this.shareControlManager = ToolBannerShareControlManager.initShareControl([{
                shareRelatedObjType: this.shareRelatedObjType,
                expandId: this.expandId,
                relatedId: this.relatedId,
                autoLoad: this.autoLoad,
                screenshotImageUrl: this.screenshotImageUrl,
                movieTitle: this.movieTitle,
                cityName: this.cityName,
                imageUrl: this.imageUrl,
                imagePath: this.imagePath,
                link: this.link,
                videoId: this.relatedId,
                title: this.title,
                fullTitle: this.fullTitle,
                pic: this.pic,
                date: this.date,
                desc: this.desc,
                shareButton: this.sharebar,
                shareContainer: this.sharebox,
                expandMovieId: this.expandMovieId,
                shareControlStyleType: 2,
                callback: this.removeClassName.bind(this),
                shareSuccessCallBack: this.shareSuccessCallBack
            }])
        }
    }, instanceCollectControl: function () {
        if (!this.favoriteButtonControl) {
            this.favoriteButtonControl = new FavoriteButtonControl({
                shareRelatedObjType: this.shareRelatedObjType,
                isLogin: this.isLogin,
                signInUrl: this.signInUrl,
                relatedObjId: this.relatedObjId,
                relatedObjType: this.relatedObjType,
                hasFavorited: this.hasFavorited,
                favoriteButton: this.favoritebar,
                favoriteContainer: this.favoritbox,
                favoriteControlStyleType: 2,
                callback: this.removeClassName.bind(this)
            })
        }
    }, setClassName: function () {
    }, removeClassName: function (b) {
        num1 = 9, num2 = 9;
        if (b) {
            if (b === this.sharebar) {
                num1 = 0;
                num2 = 3
            } else {
                if (b === this.favoritebar) {
                    num1 = 1;
                    num2 = 4
                }
            }
        }
        for (i = 0; i < 3; i++) {
            if (num1 !== i) {
                if (this.rightFloatControls[i].hasClassName("notcurr")) {
                    this.rightFloatControls[i].removeClassName("notcurr")
                }
                if (this.rightFloatControls[i].hasClassName("curr")) {
                    this.rightFloatControls[i].removeClassName("curr")
                }
            }
        }
        for (i = 3; i < 5; i++) {
            if (num2 !== i) {
                this.rightFloatControls[i].hide()
            }
        }
    }
});
var ProductTools = Class.create();
Object.extend(ProductTools.prototype, CrosswiseTools.prototype);
Object.extend(ProductTools.prototype, {
    options: {
        slidesRegion: "",
        hasShareButton: true,
        hasFavoriteButton: true,
        hasCodeButton: true,
        slidesRegionEl: null,
        shareRelatedObjType: "",
        relatedId: 0,
        link: "",
        title: "",
        pic: "",
        goodsId: 0,
        pageName: "",
        sharePan: "",
        favoritePan: "",
        codePan: ""
    }, hasIsLogin: function (f, e, d) {
        if (siteCommunityServiceUrl) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "HasIsLogin", [f, e], d, "/community.api?Ajax_CallBack=true", "post", "20000")
        }
    }, initializeEvent: function () {
        if (this.rightFloatControl) {
            this.onClickRigtFloatHandler = this.onClickRigtFloat.bind(this);
            Event.observe(this.rightFloatControl, "click", this.onClickRigtFloatHandler)
        }
        Event.observe(window, "unload", this.close.bind(this));
        this.onClickBodyHandler = this.onClickBody.bind(this);
        Event.observe(document.body, "click", this.onClickBodyHandler)
    }, load: function () {
        if (this.hasFavoriteButton) {
            this.hasIsLogin(this.goodsId, true, function (b) {
                if (b && b.value) {
                    if (b.value.isLogin && b.value.hasFavorited && this.favoritebar) {
                        this.favoritebar.addClassName("on");
                        this.favoritebar.title = "已收藏"
                    }
                }
            }.bind(this))
        }
        if (typeof ShareControlManager !== "undefined") {
            this.instanceShareControl()
        } else {
            $loadJs("/js/2014/share.js", function () {
                this.instanceShareControl()
            }.bind(this))
        }
    }, onClickBody: function (b) {
        this.bodyClickRigtFloat(b)
    }, addDOM: function () {
        this.slidesRegion = $(this.slidesRegion);
        if (!this.slidesRegion && this.slidesRegionEl) {
            this.slidesRegion = this.slidesRegionEl
        }
        this.showButtonMethod();
        this.rightFloatControl = $(Builder.node("dl", {}, [this.codebar, this.favoritebar, this.sharebar, this.favoritbox]));
        if (this.slidesRegion) {
            this.slidesRegion.appendChild(this.rightFloatControl)
        }
    }, trackClick: function (b) {
        if (typeof tracker != "undefined") {
            tracker.trackClick(this.pageName, b)
        }
    }, showButtonMethod: function () {
        this.sharebox = $(Builder.node("div", {className: "sharebox", style: "display:none;"}));
        this.shareBtn = $(Builder.node("a", {className: "s_share", title: "分享"}, [Builder.node("b", {
            className: "baron",
            method: "share"
        }, ["分享"]), Builder.node("i")]));
        if (!this.hasShareButton) {
            this.sharebar = $(Builder.node("dd", {style: "display:none"}))
        } else {
            this.sharebar = $(Builder.node("dd", {method: "share"}, [this.shareBtn, this.sharebox]))
        }
        if (this.sharePan != "") {
            Event.observe(this.sharebar, "click", function () {
                this.trackClick(this.sharePan)
            }.bind(this))
        }
        if (!this.hasFavoriteButton) {
            this.favoritebar = $(Builder.node("dd", {className: "", title: "收藏", style: "display:none"}))
        } else {
            this.favoritebar = $(Builder.node("dd", {className: "", title: "收藏", method: "favorite"}));
            new Insertion.Bottom(this.favoritebar, '<a method="favorite" href="javascript:void(0)" class="s_fav" title="收藏"><i method="favorite"></i></a>')
        }
        if (this.favoritePan != "") {
            Event.observe(this.favoritebar, "click", function () {
                this.trackClick(this.favoritePan)
            }.bind(this))
        }
        if (!this.hasCodeButton) {
            this.codebar = $(Builder.node("dd", {title: "二维码", style: "display:none"}))
        } else {
            this.codebar = $(Builder.node("dd", {title: "二维码", method: "code"}));
            new Insertion.Bottom(this.codebar, '<a method="code" href="javascript:void(0)" class="s_code" title="二维码"><b method="code">二维码</b><i method="code"></i></a><div style="display: none" class="codebox"><p class="codeImgBox_"></p><p class="mt9">扫描二维码<br />手机浏览页面</p></div>')
        }
        if (this.codePan != "") {
            Event.observe(this.codebar, "click", function () {
                this.trackClick(this.codePan)
            }.bind(this))
        }
        this.favoritbox = $(Builder.node("div", {className: "collectbox", style: "display:none;"}))
    }, onClickRigtFloat: function (c) {
        this.clickElement = Event.element(c);
        if (!this.clickElement.readAttribute) {
            return
        }
        if (this.clickElement.previous("b") && this.clickElement.previous("b").hasAttribute("method")) {
            this.clickElement = this.clickElement.previous("b")
        }
        var d = this.clickElement.readAttribute("method");
        switch (d) {
            case"share":
                break;
            case"favorite":
                if (typeof FavoriteButtonControl !== "undefined") {
                    this.instanceCollectControl()
                } else {
                    $loadSubJs("/js/2014/favorite.js", function () {
                        this.instanceCollectControl()
                    }.bind(this))
                }
                break;
            case"code":
                if (this.codebar.hasClassName("on")) {
                    this.closeCode()
                } else {
                    this.codebar.addClassName("on");
                    this.builderCodeNode(this.codebar.down(".codeImgBox_"))
                }
                break
        }
    }, closeCode: function () {
        this.codebar.removeClassName("on");
        this.codebar.down(".codebox").hide()
    }, closeShare: function () {
        this.sharebar.removeClassName("on");
        this.sharebar.down(".sharebox").hide()
    }, bodyClickRigtFloat: function (c) {
        this.clickElement = Event.element(c);
        if (!this.clickElement.readAttribute) {
            return
        }
        var d = this.clickElement.readAttribute("method");
        switch (d) {
            case"share":
                this.closeCode();
                break;
            case"favorite":
                this.closeCode();
                this.closeShare();
                break;
            case"code":
                this.closeShare();
                break;
            default:
                this.closeCode();
                this.closeShare();
                break
        }
    }, instanceShareControl: function () {
        if (typeof ToolBannerShareControlManager == "undefined") {
            ToolBannerShareControlManager = new ShareControlManager()
        }
        if (!this.shareControlManager) {
            this.shareControlManager = ToolBannerShareControlManager.initShareControl([{
                shareRelatedObjType: this.shareRelatedObjType,
                expandId: this.expandId,
                relatedId: this.relatedId,
                autoLoad: this.autoLoad,
                screenshotImageUrl: this.screenshotImageUrl,
                movieTitle: this.movieTitle,
                cityName: this.cityName,
                imageUrl: this.imageUrl,
                imagePath: this.imagePath,
                link: this.link,
                videoId: this.relatedId,
                title: this.title,
                fullTitle: this.fullTitle,
                pic: this.pic,
                date: this.date,
                desc: this.desc,
                shareButton: this.shareBtn,
                shareContainer: this.sharebox,
                expandMovieId: this.expandMovieId,
                callback: this.removeClassName.bind(this),
                shareSuccessCallBack: this.shareSuccessCallBack
            }])
        }
    }, instanceCollectControl: function () {
        if (!this.favoriteButtonControl) {
            this.favoriteButtonControl = new FavoriteButtonControlSCM({
                goodsId: this.goodsId,
                favoriteButton: this.favoritebar
            })
        } else {
            this.favoriteButtonControl.clickFavorite()
        }
    }, builderCodeNode: function (e) {
        var d = "", f = "";
        this.conversionUrl = this.link;
        if (this.conversionUrl !== "") {
            f = encodeURIComponent(this.conversionUrl)
        } else {
            if (getNewValue) {
                f = encodeURIComponent(getNewValue(location.href))
            } else {
                f = encodeURIComponent(location.href)
            }
        }
        d = siteServiceUrl + "/GetQrCodeHandler.ashx?qrCodeStr=" + f;
        e.innerHTML = '<img class="codeImg" src="' + d + '" width="110" height="110" alt="二维码" />';
        this.codebar.down(".codebox").show()
    }, removeClassName: function (b) {
        this.closeCode()
    }
});
var IndexShareToolsControl = Class.create();
Object.extend(IndexShareToolsControl.prototype, {
    name: "IndexShareToolsControl", initialize: function () {
        this.initializeField();
        this.initializeDOM();
        this.initializeControl();
        this.initializeEvent();
        this.load()
    }, initializeField: function () {
    }, initializeDOM: function () {
        this.ShareContrainerList = $$(".i_share")
    }, destroyDOM: function () {
        this.ShareContrainerList = null
    }, initializeControl: function () {
        this.rightCtrl = new RightFloatToolsControl({
            hasShareButton: false,
            hasFavoriteButton: false,
            hasMessageButton: false,
            hasCodeButton: false
        })
    }, destroyControl: function () {
        if (this.ShareControlList) {
            this.ShareControlList.each(function (b) {
                b.close();
                b = null
            })
        }
    }, initializeEvent: function () {
    }, destroyEvent: function () {
    }, load: function () {
        if (this.ShareContrainerList) {
            var b = siteMcUrl + "/member/signin";
            this.ShareControlList = [];
            this.ShareContrainerList.each(function (z) {
                var w = 0;
                var D = z.readAttribute("method");
                switch (D) {
                    case"newsimgshare":
                        var F = z.readAttribute("newsid");
                        var I = z.readAttribute("newstitle");
                        var H = z.readAttribute("newslink");
                        var L = z.readAttribute("pic");
                        var x = "[今日时光网头条精选]：" + I;
                        var G = new CrosswiseTools({
                            shareRelatedObjType: 9981,
                            slidesRegionEl: z,
                            link: H,
                            title: I,
                            fullTitle: x,
                            pic: L,
                            isLogin: false,
                            signInUrl: b,
                            relatedId: F,
                            relatedObjId: F,
                            relatedObjType: 51,
                            hasFavorited: false
                        });
                        this.ShareControlList.push(G);
                        break;
                    case"hotplayimgshare":
                        var E = z.readAttribute("movieid");
                        var y = new CrosswiseTools({
                            slidesRegionEl: z,
                            shareRelatedObjType: 1111,
                            relatedId: E,
                            isLogin: false,
                            signInUrl: b,
                            relatedObjId: E,
                            relatedObjType: 1,
                            hasFavorited: false
                        });
                        this.ShareControlList.push(y);
                        break;
                    case"communityimgshare":
                        var K = z.readAttribute("objtype");
                        var J = z.readAttribute("objid");
                        var M;
                        var A = true;
                        switch (K) {
                            case"9991":
                                M = 6;
                                w = 1;
                                break;
                            case"7001":
                                M = 10;
                                break;
                            case"6001":
                                M = 6;
                                break;
                            case"2201":
                                M = 22;
                                A = false;
                                break;
                            default:
                                return
                        }
                        var v = new CrosswiseTools({
                            slidesRegionEl: z,
                            shareRelatedObjType: K,
                            relatedId: J,
                            isLogin: false,
                            signInUrl: b,
                            relatedObjId: J,
                            relatedObjType: M,
                            hasFavorited: false,
                            expandId: w,
                            hasFavoriteButton: A
                        });
                        this.ShareControlList.push(v);
                        break;
                    case"communitycommentshare":
                        var K = z.readAttribute("objtype");
                        var J = z.readAttribute("objid");
                        var C = "";
                        if (z.hasAttribute("title")) {
                            C = z.readAttribute("title")
                        }
                        var B = "";
                        if (z.hasAttribute("link")) {
                            B = z.readAttribute("link")
                        }
                        var M;
                        var A = true;
                        switch (K) {
                            case"9991":
                                M = 6;
                                w = 1;
                                break;
                            case"9992":
                                M = 78;
                                break;
                            case"9993":
                                M = 78;
                                break;
                            case"6001":
                                M = 6;
                                break;
                            case"7001":
                                M = 10;
                                break;
                            case"2201":
                                M = 22;
                                A = false;
                                break;
                            case"6401":
                                M = 78;
                            default:
                                return
                        }
                        var a = new CrosswiseTools({
                            slidesRegionEl: z,
                            link: B,
                            title: C,
                            shareRelatedObjType: K,
                            relatedId: J,
                            isLogin: false,
                            signInUrl: b,
                            relatedObjId: J,
                            relatedObjType: M,
                            hasFavorited: false,
                            expandId: w,
                            hasFavoriteButton: A
                        });
                        this.ShareControlList.push(a);
                        break
                }
            }.bind(this))
        }
    }, close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyControl();
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var movieDictionary = [{
    key: "http://movie.mtime.com/(\\d+)/reviews/(\\d+).[\\w\\W*?]",
    value: "http://m.mtime.cn/#!/review/detail/{1}/",
    isAppendGreedyValue: false
}, {
    key: "http://movie.mtime.com/(\\d+)/posters_and_images/(\\d+)([\\w\\W*?])",
    value: "http://m.mtime.cn/#!/movie/{0}/posters_and_images/{1}{2}/",
    isAppendGreedyValue: true
}, {
    key: "http://movie.mtime.com/(\\d+)/posters_and_images[\\w\\W*?]",
    value: "http://m.mtime.cn/#!/movie/{0}/posters_and_images/",
    isAppendGreedyValue: false
}, {
    key: "http://movie.mtime.com/(\\d+)([\\w\\W+?])",
    value: "http://m.mtime.cn/#!/movie/{0}{1}/",
    isAppendGreedyValue: true
}];
var personDictionary = [{
    key: "http://people.mtime.com/(\\d+)/photo_gallery/(\\d+)([\\w\\W*?])",
    value: "http://m.mtime.cn/#!/person/concreteImage/{0}/{1}{2}/",
    isAppendGreedyValue: true
}, {
    key: "http://people.mtime.com/(\\d+)/photo_gallery[\\w\\W*?]",
    value: "http://m.mtime.cn/#!/person/personImages/{0}/",
    isAppendGreedyValue: false
}, {
    key: "http://people.mtime.com/(\\d+)([\\w\\W*?])",
    value: "http://m.mtime.cn/#!/person/{0}{1}/",
    isAppendGreedyValue: true
}];
var newsDictionary = [{
    key: "http://news.mtime.com/(\\d+)/(\\d+)/(\\d+)/(\\d+).[\\w\\W*?]",
    value: "http://m.mtime.cn/#!/news/movie/{3}/",
    isAppendGreedyValue: false
}, {
    key: "http://news.mtime.com/pix/(\\d+)/(\\d+)/(\\d+)/(\\d+).[\\w\\W*?]",
    value: "http://m.mtime.cn/#!/news/gallery/{3}/",
    isAppendGreedyValue: false
}];
var otherDictionary = [{
    key: "http://i.mtime.com/(\\d+)/blog/(\\d+)([\\w\\W*?])",
    value: "http://m.mtime.cn/#!/review/detail/{1}{2}/",
    isAppendGreedyValue: true
}];
var goodsDictionary = [{
    key: "http://item.mall.mtime.com/(\\d+)/",
    value: "http://m.mtime.cn/#!/commerce/item/{0}/",
    isAppendGreedyValue: false
}];
var dicArray = [movieDictionary, personDictionary, newsDictionary, otherDictionary, goodsDictionary];
function stringFormat(f, k, j) {
    for (var h = 0; h < k.length; h++) {
        var g = "{" + h.toString() + "}";
        if (j && h == k.length - 1) {
            if (isNaN(Number(k[h]))) {
                k[h] = ""
            }
        }
        f = f.replace(g, k[h])
    }
    return f
}
function getNewValue(e) {
    var f = "";
    for (var d = 0; d < dicArray.length; d++) {
        f = dictionaryMatch(e, dicArray[d]);
        if (f !== "") {
            break
        }
    }
    return f === "" ? e : f
}
function dictionaryMatch(j, f) {
    for (var g = 0; g < f.length; g++) {
        var k = new RegExp(f[g].key);
        var h = k.exec(j);
        if (typeof h != "undefined" && h != null && h != "") {
            if (h.length != 0) {
                h.splice(0, 1);
                if (h.length != 0) {
                    return stringFormat(f[g].value, h, f[g].isAppendGreedyValue)
                }
            }
        }
        continue
    }
    return ""
}
var MtimeShareRelatedObjectTypes = {
    Movie: 1,
    Person: 2,
    UserPhoto: 5,
    Blog: 6,
    Group: 9,
    ForumTopic: 10,
    MoviePicture: 11,
    PersonPicture: 21,
    Activity: 22,
    Cinema: 33,
    Video: 41,
    List: 50,
    CMSNews: 51,
    CMSGalleryImage: 52,
    Twitter: 54,
    CMSSpecialTopic: 59,
    GameVote: 63,
    Link: 64,
    GameQuiz: 67,
    Festival: 82,
    FestivalEvent: 83,
    CommunityFeature: 84,
    TopList: 90,
    DETSeatOrder: 85,
    MediaReview: 336,
    SCMGoodsComment: 9911
};
var ShareRelatedObjectTypes = {
    MovieIsReleased: 1001,
    MovieUpcoming: 1002,
    MovieHasAwards: 1003,
    MovieGeneral: 1004,
    MovieContentPlot: 1005,
    MovieRoleDescInfo: 1006,
    MovieRoleDescAll: 1007,
    MovieBehindCurtain: 1008,
    MovieVerseLines: 1009,
    MovieBehindTemplate: 1010,
    MovieTelecinePage: 1011,
    PersonFavorableComment: 2001,
    News: 5101,
    ImageNews: 5102,
    MovieLongComment: 9991,
    MovieShortComment: 9992,
    MovieLeaveComment: 9993,
    VideoMovie: 4101,
    VideoPerson: 4102,
    PictureMovie: 11,
    PicturePerson: 21,
    Cinema: 33,
    IndexNewsTop: 9981,
    Movie: 1111,
    Blog: 6001,
    ForumTopic: 7001,
    Activity: 2201,
    MediaReview: 3361,
    Link: 6401,
    OffsiteLink: 64011,
    GoodsDetail: 99991,
    NewsComment: 51011,
    VideoMovieComment: 41011,
    VideoPersonComment: 41021,
    SCMGoodsDetail: 99992,
    TopList: 9000
};
var ShareControlStyleType = {FloatStyle: 1, ListStyle: 2, OnlyShare: 3, OnlyShareSCM: 4};
var ShareTabType = {
    shareToMtimeFriend: {name: "shareToMtimeFriend", text: "时光好友"},
    shareToEMail: {name: "shareToEMail", text: "邮件地址"},
    shareToWeb: {name: "shareToWeb", text: "其他网站"},
    showQrCode: {name: "showQrCode", text: "二维码"}
};
var GlobalUrlValue = "";
var ShareControlDialogBase = Class.create();
Object.extend(ShareControlDialogBase.prototype, {
    name: "ShareControlDialogBase",
    title: "分享到我的时光",
    closed: false,
    dialog: null,
    tabContentRegion: null,
    options: {shareControl: null},
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initialize: function (b) {
        this.setOptions(b);
        this.initializeField();
        this.initializeDOM();
        this.initializeControl();
        this.initializeEvent();
        this.load()
    },
    initializeField: function () {
    },
    initializeDOM: function () {
        this.tabContentRegion
    },
    destroyDOM: function () {
        this.shareControl = this.dialog = this.tabHeaderRegion = this.tabContentRegion = null
    },
    initializeControl: function () {
        this.dialog = new Dialog({
            title: this.title,
            content: this.tabContentRegion,
            windowClassName: "w600",
            closeCallback: this.close.bind(this)
        })
    },
    destroyControl: function () {
    },
    initializeEvent: function () {
    },
    destroyEvent: function () {
    },
    load: function () {
    },
    close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyControl();
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var ShareToMtimeFriendDialog = Class.create();
Object.extend(ShareToMtimeFriendDialog.prototype, ShareControlDialogBase.prototype);
Object.extend(ShareToMtimeFriendDialog.prototype, {
    defaultPrompt: "还可以输入210个字",
    reasonDefaultTxt: "分享理由",
    reasonMaxLength: 210,
    dialogTemplate: new Template('<div class="sharemid"><span class="load48" style="display:none;" _type="loading"></span><div class="editshare"><span _type="digit_feedback">#{defaultPrompt}</span> <textarea _type="txtreason" class="curr">#{defaultText}</textarea></div><div class="#{middleShareClassName} clearfix" _type="addition_region">#{middleShareHtml}</div></div>'),
    closed: false,
    selectFirstImage: false,
    content: null,
    txtReason: null,
    shareButton: null,
    onClickShareButtonHandler: null,
    getShareInfoAdditionalInfo: function (f, e, d) {
        if (typeof(siteCommunityServiceUrl) != "undefined") {
            return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetShareInfoAdditionalInfo", [f, e], d, "/community.api?Ajax_CallBack=true", "get", "20000")
        }
    },
    getShortenUrl: function (d, c) {
        return Mtime.Component.Ajax.get("Mtime.Service.Pages.TwitterService", "ShortenUrl", [d], c, "/Service/Twitter.msi", "get", "20000")
    },
    loadEditorJs: function (b) {
        if (typeof(Weibo) != "undefined" && Weibo.Face && Weibo.Image && Weibo.Sync && Weibo.Syntax && Weibo.Topic && Weibo.Video) {
            b && b()
        } else {
            if (self.debug) {
                $loadJs(["/js/2011/weibo/editor/editor.js", "/js/2011/weibo/editor/image.js", "/js/2011/weibo/editor/face.js", "/js/2011/weibo/editor/sync.js", "/js/2011/weibo/editor/syntax.js", "/js/2011/weibo/editor/video.js", "/js/2011/weibo/editor/topic.js"], function () {
                    b && b()
                })
            } else {
                $loadJs("/js/2011/weiboEditor.js", function () {
                    b && b()
                })
            }
        }
    },
    load: function () {
    },
    initializeDOM: function () {
        ShareControlDialogBase.prototype.initializeDOM.apply(this);
        var b = this.shareControl.shareInfo;
        this.textAreaContent = this.reasonDefaultTxt;
        if (this.shareControl.shareMtimeContent) {
            this.textAreaContent = this.shareControl.shareMtimeContent()
        }
        this.content = Builder.build(this.dialogTemplate.evaluate({
            defaultPrompt: this.defaultPrompt,
            defaultText: this.textAreaContent,
            middleShareClassName: this.shareControl.middleShareClassName,
            middleShareHtml: this.shareControl.getMiddleShareHtml()
        }));
        this.shareButton = $(Builder.node("a", {className: "sharelink"}, ["分 享"]));
        this.tabContentRegion = this.content
    },
    initializeControl: function () {
        ShareControlDialogBase.prototype.initializeControl.apply(this);
        if (this.dialog.buttonRegion) {
            this.dialog.buttonRegion.appendChild(this.shareButton);
            this.dialog.buttonRegion.addClassName("btnshare")
        }
        this.txtReason = this.content.down('[_type="txtreason"]');
        this.editorTextbox = new Textbox({
            text: this.txtReason,
            watermarkText: "",
            focusClassName: "",
            blurClassName: ""
        });
        this.editorTextbox.text.value = this.textAreaContent;
        var f = this;
        if (this.shareControl.relatedObjType === MtimeShareRelatedObjectTypes.CMSNews || this.shareControl.relatedObjType === MtimeShareRelatedObjectTypes.Cinema || this.shareControl.relatedObjType === MtimeShareRelatedObjectTypes.SCMGoodsComment) {
            if (this.shareControl.getShareObjectPicUrl) {
                this.shareImageUrl = this.shareControl.getShareObjectPicUrl()
            }
        }
        this.loadEditorJs(function () {
            f.editor = new Weibo.Editor({editor: f.txtReason});
            new Weibo.Feedback({
                editor: f.editor,
                digitFeedback: {
                    target: f.content.down('[_type="digit_feedback"]'),
                    normalTemplate: "还可以输入{2}个字",
                    warnTemplate: '<font color="red">字数超出{3}字</font>'
                }
            })
        });
        var e = this.shareControl.shareInfo;
        var d = this.content.down('[_type="loading"]');
        if (e.needAdditionalInfo) {
            d.show();
            this.getShareInfoAdditionalInfo(this.shareControl.relatedObjType, this.shareControl.getShareObjectId(), function (y) {
                if (y && y.value && (y.value.picUrls || y.value.videoUrls || y.value.extPicUrls)) {
                    var v = "", s = "", C = "";
                    var u = ['<ul class="infotab clearfix" _type="nav">'];
                    var a = y.value;
                    if ((a.picUrls && a.picUrls.length > 0) || (y.value.extPicUrls && y.value.extPicUrls.length > 0)) {
                        if (a.extPicUrls && a.extPicUrls.length > 0) {
                            if (a.picUrls) {
                                a.picUrls = a.extPicUrls.concat(a.picUrls)
                            } else {
                                a.picUrls = y.value.extPicUrls
                            }
                        }
                        u.push('<li _type="image" class="curr"><i class="i_share_pic">&nbsp;</i>选择一张图片<i class="i_share_select">&nbsp;</i></li>');
                        var c = ['<div class="infoslist" _type="images"><div class="infoscroll">'];
                        if (a.picUrls.length > 5) {
                            c.push('<a href="#" onclick="return false;" _type="turnleft" class="i_share_prev">&nbsp;</a><a href="#" class="i_share_next" _type="turnright" onclick="return false;">&nbsp;</a>')
                        }
                        c.push('<div _type="slidesregion" class="scrollmid">');
                        a.picUrls.each(function (h, g) {
                            if (g % 5 == 0) {
                                if (g / 5 != 0) {
                                    c.push("</ul>");
                                    c.push('<ul style="display:none;">')
                                } else {
                                    c.push("<ul>")
                                }
                            }
                            c.push('<li _imageurl="' + h + '" name="tab"><img src="' + h + '" width="90" /></li>')
                        });
                        c.push("</ul></div></div></div>");
                        s = c.join("")
                    }
                    if (a.videoUrls && a.videoUrls.length > 0) {
                        var B = [];
                        if (u.length == 1) {
                            u.push('<li _type="video" class="curr"><i class="i_share_play">&nbsp;</i>选择一个视频<i class="i_share_select">&nbsp;</i></li>');
                            B = ['<div class="infoslist" _type="videos"><div class="infoscroll">']
                        } else {
                            u.push('<li _type="video"><i class="i_share_play">&nbsp;</i>选择一个视频</li>');
                            B = ['<div class="infoslist" _type="videos" style="display:none"><div class="infoscroll">']
                        }
                        if (a.videoUrls.length > 5) {
                            B.push('<a href="#" onclick="return false;" _type="turnleft" class="i_share_prev">&nbsp;</a><a href="#" class="i_share_next" _type="turnright" onclick="return false;">&nbsp;</a>')
                        }
                        B.push('<div _type="slidesregion" class="scrollmid">');
                        a.videoCoverUrls.each(function (h, g) {
                            if (g % 5 == 0) {
                                if (g / 5 != 0) {
                                    B.push("</ul>");
                                    B.push('<ul style="display:none;">')
                                } else {
                                    B.push("<ul>")
                                }
                            }
                            B.push('<li _videourl="' + a.videoUrls[g].url + '" title="' + a.videoUrls[g].title + '" name="tab"><img src="' + h + '" width="90"></li>')
                        });
                        B.push("</ul></div></div></div>");
                        C = B.join("")
                    }
                    u.push("</ul>");
                    v = u.join("");
                    var b = f.content.down('[_type="addition_region"]');
                    v && b.appendChild(Builder.build(v));
                    s && b.appendChild(Builder.build(s));
                    C && b.appendChild(Builder.build(C));
                    f.navRegion = f.content.down('[_type="nav"]');
                    if (f.navRegion.childElements().length < 2) {
                        f.navRegion = null
                    }
                    f.imagesRegion = f.content.down('[_type="images"]');
                    f.videosRegion = f.content.down('[_type="videos"]');
                    if (f.selectFirstImage && f.imagesRegion) {
                        var t = f.imagesRegion.down("li[_imageurl]");
                        imgUrl = t.readAttribute("_imageurl");
                        f.shareImageUrl = imgUrl;
                        t.addClassName("curr");
                        var z = '<i class="i_sharecurr"></i>';
                        t.appendChild(Builder.build(z))
                    }
                    if (f.navRegion) {
                        f.navControlHandler = f.navControl.bind(f);
                        Event.observe(f.navRegion, "click", f.navControlHandler)
                    }
                    if (f.imagesRegion || f.videosRegion) {
                        f.selectImageOrVideoHandler = f.selectImageOrVideo.bind(f);
                        f.imagesRegion && Event.observe(f.imagesRegion, "click", f.selectImageOrVideoHandler);
                        f.videosRegion && Event.observe(f.videosRegion, "click", f.selectImageOrVideoHandler);
                        f.videosRegion && (f.videoCheckTimeout = setTimeout(f.videoCheck.bind(f), 500));
                        if (a.picUrls && a.picUrls.length > 5) {
                            var x = f.imagesRegion.down('[_type="turnleft"]');
                            var w = f.imagesRegion.down('[_type="turnright"]');
                            var A = f.imagesRegion.down('[_type="slidesregion"]');
                            f.buildSlidesControl(A, w, x)
                        }
                        if (a.videoUrls && a.videoUrls.length > 5) {
                            var x = f.videosRegion.down('[_type="turnleft"]');
                            var w = f.videosRegion.down('[_type="turnright"]');
                            var A = f.videosRegion.down('[_type="slidesregion"]');
                            f.buildSlidesControl(A, w, x)
                        }
                    }
                }
                d.hide()
            })
        }
    },
    buildSlidesControl: function (m, k, l) {
        var g = m.childElements();
        if (g.length > 1) {
            var h = 0;
            var j = g.length;
            Event.observe(k, "click", function () {
                $(g[h]).hide();
                if (h != j - 1) {
                    ++h
                } else {
                    h = 0
                }
                $(g[h]).show()
            });
            Event.observe(l, "click", function () {
                $(g[h]).hide();
                if (h != 0) {
                    h = 0
                } else {
                    h = j - 1
                }
                $(g[h]).show()
            })
        }
    },
    destroyDOM: function () {
        this.editor.destroy();
        ShareControlDialogBase.prototype.destroyDOM.apply(this);
        this.content = null;
        this.txtReason = null;
        this.shareButton = null;
        if (this.textAreaContent) {
            this.textAreaContent = null
        }
    },
    initializeEvent: function () {
        ShareControlDialogBase.prototype.initializeEvent.apply(this);
        this.onClickShareButtonHandler = this.onClickShareButton.bindAsEventListener(this);
        Event.observe(this.shareButton, "click", this.onClickShareButtonHandler)
    },
    destroyEvent: function () {
        this.navRegion && Event.stopObserving(this.navRegion, "click", this.navControlHandler);
        this.imagesRegion && Event.stopObserving(this.imagesRegion, "click", this.selectImageOrVideoHandler);
        this.videosRegion && Event.stopObserving(this.videosRegion, "click", this.selectImageOrVideoHandler);
        this.videoCheckTimeout && clearTimeout(this.videoCheckTimeout);
        Event.stopObserving(this.shareButton, "click", this.onClickShareButtonHandler);
        ShareControlDialogBase.prototype.destroyEvent.apply(this)
    },
    navControl: function (m) {
        var l = Event.findElement(m, "li");
        if (l != document && !l.hasClassName("curr")) {
            var n = l.up();
            var o = l.getAttribute("_type");
            var h = n.down(".curr");
            var k = h.getAttribute("_type");
            var j = h.down(".i_share_select");
            j && j.remove();
            h.removeClassName("curr");
            l.addClassName("curr");
            l.appendChild(Builder.build('<i class="i_share_select"></i>'));
            this.content.down('[_type="' + k + 's"]').hide();
            this.content.down('[_type="' + o + 's"]').show()
        }
    },
    videoCheck: function () {
        this.videoCheckTimeout && clearTimeout(this.videoCheckTimeout);
        this.videoCheckTimeout = null;
        var d = this.editorTextbox.getValue();
        if (this.shareVideoUrl && d.indexOf(this.shareVideoUrl) == -1) {
            var c = this.videosRegion.down('li[shortenurl="' + this.shareVideoUrl + '"]');
            if (c) {
                c.removeClassName("curr");
                this.shareVideoUrl = null
            }
        }
        this.videoCheckTimeout = setTimeout(this.videoCheck.bind(this), 500)
    },
    selectImageOrVideo: function (n) {
        var m = Event.findElement(n, "li");
        if (m != document && (m.hasAttribute("_imageurl") || m.hasAttribute("_videourl"))) {
            if (m.hasClassName("curr")) {
                if (m.hasAttribute("_imageurl")) {
                    this.shareImageUrl = null
                } else {
                    var t = this.shareVideoUrl;
                    if (t) {
                        var u = this.editorTextbox.getValue().replace(new RegExp(t, "ig"), "");
                        this.editor.editor.value = "";
                        this.editor.insertText(u)
                    }
                    this.shareVideoUrl = null
                }
                var q = m.down(".i_sharecurr");
                q && q.remove();
                m.removeClassName("curr")
            } else {
                if (m.hasAttribute("_imageurl")) {
                    this.shareImageUrl = m.getAttribute("_imageurl");
                    var o = m.up('[_type="images"]').down(".curr");
                    var p = o && o.down(".i_sharecurr");
                    o && o.removeClassName("curr");
                    p && p.remove();
                    m.addClassName("curr");
                    m.appendChild(Builder.build('<i class="i_sharecurr"></i>'))
                } else {
                    var t = this.shareVideoUrl;
                    var u = this.editorTextbox.getValue().replace(new RegExp(t, "ig"), "");
                    this.editor.editor.value = "";
                    this.editor.insertText(u);
                    t = m.getAttribute("_videourl");
                    var r = m.getAttribute("shortenurl");
                    var s = this;
                    var l = function () {
                        s.editor.insertText(s.shareVideoUrl + " ");
                        var a = m.up('[_type="videos"]').down(".curr");
                        var b = a && a.down(".i_sharecurr");
                        a && a.removeClassName("curr");
                        b && b.remove();
                        m.addClassName("curr");
                        m.appendChild(Builder.build('<i class="i_sharecurr"></i>'))
                    };
                    if (r) {
                        this.shareVideoUrl = r;
                        l()
                    } else {
                        this.getShortenUrl(t, function (a) {
                            if (a && a.value && a.value.errorCode != 1) {
                                r = a.value.shortenUrl;
                                m.setAttribute("shortenurl", r);
                                s.shareVideoUrl = r;
                                l()
                            }
                        })
                    }
                }
            }
        }
    },
    saveShareCallback: function (c) {
        if (c) {
            var d = c.value;
            switch (d) {
                case -2:
                    $alert("您在时光网发布的内容违反互联网管理规定，对此帐户予禁言处理。", null, null, 1);
                    this.dialog.close();
                    break;
                case -1:
                    $alert("转发理由含有敏感词，请修改后再试", null, null, 1);
                    this.dialog.close();
                    break;
                case 0:
                    $alert("转发失败，请稍后再试", null, null, 1);
                    this.dialog.close();
                    break;
                case 1:
                case 2:
                    $alert("转发成功", null, null, 1);
                    if (this.shareControl.shareSuccessCallBack) {
                        this.shareControl.shareSuccessCallBack()
                    }
                    this.dialog.close();
                    break;
                default:
                    this.dialog.close();
                    break
            }
        }
    },
    saveTweetShareCallback: function (d) {
        var c = null;
        switch (d.value) {
            case -5:
                c = "原作者微评正在审核中，如有不便敬请谅解!";
                break;
            case -9:
            case -4:
            case -3:
            case -2:
            case 0:
                c = "转发微评失败";
                break;
            case -1:
                c = "转发微评失败,内容过长";
                break;
            case -11:
                c = "转发微评失败,您被禁言";
                break;
            case -10:
                c = "转发微评失败,内容包含敏感词";
                break;
            case -20:
                c = "你发布的速度太快了，休息一下吧：）";
                break
        }
        if (c) {
            $alert(c, null, null, 1)
        } else {
            $alert("转发成功", null, null, 1);
            if (this.shareControl.shareSuccessCallBack) {
                this.shareControl.shareSuccessCallBack()
            }
        }
        this.dialog.close()
    },
    onClickShareButton: function (f) {
        var j = this.txtReason.value.trim();
        var h = this.editor.getLength();
        if (h > this.editor.lengthExp.max) {
            return
        } else {
            var g = this.shareImageUrl ? this.shareImageUrl : "";
            var k = "";
            this.shareControl.saveShare(j, this.saveShareCallback.bind(this), this.saveTweetShareCallback.bind(this), this.shareButton, g, k)
        }
    }
});
var ShareControlBase = Class.create();
Object.extend(ShareControlBase.prototype, {
    name: "ShareControlBase",
    EXCERPT_SIZE: 50,
    closed: false,
    MtimeShareRelatedObjectTypes: $H(MtimeShareRelatedObjectTypes),
    ShareRelatedObjectTypes: $H(ShareRelatedObjectTypes),
    avaliableTabs: [ShareTabType.shareToMtimeFriend, ShareTabType.shareToEMail, ShareTabType.shareToWeb, ShareTabType.showQrCode],
    relatedObjType: 0,
    shareRelatedObjType: 0,
    isAjaxData: true,
    shareButton: null,
    shareInfo: null,
    shareContainer: null,
    shareControlStyleType: ShareControlStyleType.FloatStyle,
    middleShareClassName: "boxshare",
    server: {
        saveShareInfo: function (l, k, j, g, h, m) {
            if (siteCommunityServiceUrl) {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "SaveShareInfoCrossDomainByFlash", [l, k, j, h, m], g, "/community.api?Ajax_CallBack=true", "post", "20000")
            }
        }, ForwardTweet: function (f, e, d) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteServiceUrl, "Mtime.Service.Pages.TwitterService", "ForwardTweetCrossDomainByFlash", f, e, "/Service/Twitter.msi", "post", "20000", d)
        }, getIsLogin: function (b) {
            if (typeof(siteCommunityServiceUrl) != "undefined") {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetIsLogin", [], b, "/community.api?Ajax_CallBack=true", "get", "20000")
            }
        }, getShareInfo: function (l, k, j, h, g, m) {
            if (siteCommunityServiceUrl) {
                if (h === "") {
                    return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetShareInfo", [l, k, j], g, "/community.api?Ajax_CallBack=true", "get", "20000", m)
                } else {
                    return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "GetShareInfoExpand", [l, k, j, h], g, "/community.api?Ajax_CallBack=true", "get", "20000", m)
                }
            }
        }
    },
    ButtonInfoMap: $H({
        toWeixin: {
            name: "toWeixin",
            shareClassName: "weixin",
            text: "微信",
            type: ShareTabType.showQrCode,
            urlFormat: "javascript:ShowQrCode('');"
        },
        toSinaMicroblogging: {
            name: "toSinaMicroblogging",
            shareClassName: "sinaweibo",
            text: "新浪微博",
            type: ShareTabType.shareToWeb,
            urlFormat: "javascript:GlobalSiteShare('http://service.t.sina.com.cn/share/share.php?appkey=166678827&pic={2}&ralateUid=1663735900&url={0}&title={1}');"
        },
        toQQ: {
            name: "toQQ",
            shareClassName: "qq",
            text: "QQ",
            type: ShareTabType.shareToWeb,
            urlFormat: "javascript:GlobalSiteShare('http://connect.qq.com/widget/shareqq/index.html?url={0}&desc=&title={1}&summary=&pics={2}&flash=&site=http://www.mtime.com');"
        },
        toQzone: {
            name: "toQzone",
            shareClassName: "qzone",
            text: "QQ空间",
            type: ShareTabType.shareToWeb,
            urlFormat: "javascript:GlobalSiteShare('http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url={0}&desc={1}');"
        },
        toEMail: {
            name: "toEMail",
            shareClassName: "email",
            text: "电子邮件",
            type: ShareTabType.shareToEMail,
            urlFormat: "mailto:?subject={3}&body={4}"
        },
        toMtime: {name: "toMtime", shareClassName: "mytime", text: "我的时光", type: ShareTabType.shareToMtimeFriend}
    }),
    options: {
        desc: "",
        expandMovieId: 0,
        shareContainer: null,
        shareButton: null,
        autoLoad: true,
        callback: null,
        shareSuccessCallBack: null
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    typeMapping: function () {
        if (this.isAjaxData) {
            if (this.shareRelatedObjType == 0) {
                if (this.relatedObjType == 0) {
                    throw"shareRelatedObjType not set and relatedObjType not set!"
                }
                switch (this.relatedObjType) {
                    case MtimeShareRelatedObjectTypes.Movie:
                        this.shareRelatedObjType = ShareRelatedObjectTypes.MovieGeneral;
                        break;
                    case MtimeShareRelatedObjectTypes.Person:
                        this.shareRelatedObjType = ShareRelatedObjectTypes.PersonNotFavorableComment;
                        break;
                    case MtimeShareRelatedObjectTypes.CMSNews:
                        this.shareRelatedObjType = ShareRelatedObjectTypes.NewsGeneral;
                        break;
                    case MtimeShareRelatedObjectTypes.MoviePicture:
                        this.shareRelatedObjType = ShareRelatedObjectTypes.PictureMovie;
                        break;
                    case MtimeShareRelatedObjectTypes.PersonPicture:
                        this.shareRelatedObjType = ShareRelatedObjectTypes.PicturePerson;
                        break;
                    case MtimeShareRelatedObjectTypes.Cinema:
                        this.shareRelatedObjType = ShareRelatedObjectTypes.Cinema;
                        break;
                    default:
                        throw"relatedObjType not Mapping shareRelatedObjType!";
                        break
                }
            }
        }
    },
    initialize: function (b) {
        this.setOptions(b);
        this.initializeField();
        this.typeMapping();
        this.initializeDOM();
        this.initializeEvent();
        this.load();
        this.render();
        this.initializeControl();
        Event.observe(window, "unload", this.close.bind(this))
    },
    initializeField: function () {
        this.avaliableTabs = $A(this.avaliableTabs);
        this.collectBoxId = null
    },
    initializeDOM: function () {
        var g = this.ButtonInfoMap.values();
        var h = [];
        var k = siteCommunityServiceUrl + "/ShareHandler.ashx?shareRelatedObjType=" + this.shareRelatedObjType + "&relatedId=" + this.getShareObjectId() + "&expandId=" + this.getShareExpandId() + "&expandContext=" + decodeURIComponent(this.getShareExpandContext()) + "&screenshotImageUrl=" + this.getScreenshotImageUrl();
        if (this.shareButton && this.shareContainer) {
            this.shareContainer.hide();
            if (this.shareControlStyleType == ShareControlStyleType.FloatStyle) {
                h.push('<ul class="clearfix">');
                for (var j = 0; j < g.length; ++j) {
                    var f = g[j];
                    if (this.avaliableTabs.indexOf(f.type) < 0) {
                        continue
                    }
                    if (f.name === "toMtime") {
                        h.push('<li class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:void(0);">' + f.text + "</a></li>")
                    } else {
                        if (f.name === "toEMail") {
                            h.push('<li class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:EmailIframe(\'' + k + "&method=" + f.name + "');\" >" + f.text + "</a></li>")
                        } else {
                            if (f.name === "toWeixin") {
                                h.push('<li class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:ShowQrCode(\'' + this.link + "');\" >" + f.text + "</a></li>")
                            } else {
                                h.push('<li class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:GlobalSiteShare(\'' + k + "&method=" + f.name + "');\" >" + f.text + "</a></li>")
                            }
                        }
                    }
                }
                h.push("</ul>");
                this.shareContainer.innerHTML = h.join("")
            } else {
                if (this.shareControlStyleType == ShareControlStyleType.ListStyle) {
                    for (var j = 0; j < g.length; ++j) {
                        var f = g[j];
                        if (this.avaliableTabs.indexOf(f.type) < 0) {
                            continue
                        }
                        if (f.name === "toMtime") {
                            h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:void(0);">' + f.text + "</a></p>")
                        } else {
                            if (f.name === "toEMail") {
                                h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:EmailIframe(\'' + k + "&method=" + f.name + "');\" >" + f.text + "</a></p>")
                            } else {
                                if (f.name === "toWeixin") {
                                    h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:ShowQrCode(\'' + this.link + "');\" >" + f.text + "</a></p>")
                                } else {
                                    h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:GlobalSiteShare(\'' + k + "&method=" + f.name + "');\" >" + f.text + "</a></p>")
                                }
                            }
                        }
                    }
                    this.shareContainer.innerHTML = h.join("")
                } else {
                    throw"shareControlStyleType does not implemented!"
                }
            }
        } else {
            if (this.shareButton && !this.shareContainer) {
                if (this.shareControlStyleType == ShareControlStyleType.OnlyShare || this.shareControlStyleType == ShareControlStyleType.OnlyShareSCM) {
                    this.shareContainer = Builder.node("div", {className: "inner newsharebar"}, [Builder.node("div", {className: "smtimebar"}, [Builder.node("div", {className: "sharebox"})])]);
                    for (var j = 0; j < g.length; ++j) {
                        var f = g[j];
                        if (this.avaliableTabs.indexOf(f.type) < 0) {
                            continue
                        }
                        if (f.name === "toMtime") {
                            h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:void(0);">' + f.text + "</a></p>")
                        } else {
                            if (f.name === "toEMail") {
                                h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:EmailIframe(\'' + k + "&method=" + f.name + "');\" >" + f.text + "</a></p>")
                            } else {
                                if (f.name === "toWeixin") {
                                    h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:ShowQrCode(\'' + this.link + "');\" >" + f.text + "</a></p>")
                                } else {
                                    h.push('<p class="' + f.shareClassName + '"><a method="' + f.name + '" href="javascript:GlobalSiteShare(\'' + k + "&method=" + f.name + "');\" >" + f.text + "</a></p>")
                                }
                            }
                        }
                    }
                    this.shareContainer.getElementsBySelector("div.sharebox")[0].innerHTML = h.join("")
                }
            }
        }
    },
    destroyDOM: function () {
        if (this.shareButton) {
            this.shareButton = null
        }
        if (this.shareContainer) {
            this.shareContainer = null
        }
        if (this.collectBoxId) {
            this.collectBoxId = null
        }
        if (this.windowOpen) {
            this.windowOpen = null
        }
    },
    initializeEvent: function () {
        if (!this.autoLoad) {
            this.onClickShareButtonHandler = this.onClickShareButton.bindAsEventListener(this);
            if (this.shareButton) {
                this.shareButton.observe("click", this.onClickShareButtonHandler)
            }
        }
        this.onClickshareContainerHandler = this.onClickshareContainer.bindAsEventListener(this);
        if (this.shareContainer) {
            this.shareContainer.observe("click", this.onClickshareContainerHandler)
        }
    },
    destroyEvent: function () {
        if (this.shareButton) {
            this.shareButton.stopObserving("click", this.onClickShareButtonHandler)
        }
        if (this.shareContainer) {
            this.shareContainer.stopObserving("click", this.onClickshareContainerHandler)
        }
    },
    initializeControl: function () {
        if (this.shareControlStyleType == ShareControlStyleType.OnlyShare || this.shareControlStyleType == ShareControlStyleType.OnlyShareSCM) {
            var d = this;
            var c = this.shareControlStyleType == ShareControlStyleType.OnlyShareSCM;
            this.collectBox = new DialogBox({
                element: this.shareButton,
                content: this.shareContainer.outerHTML,
                arrowNew: true,
                onClick: function (b) {
                    var g = Event.pointerX(b);
                    var h = Event.pointerY(b);
                    var a = Event.element(b);
                    if (a != this.element && !a.descendantOf(this.element) && !a.descendantOf(this.windowElement) && !Position.within(this.windowElement, g, h)) {
                        this.close();
                        d.close()
                    }
                },
                setupDimensions: function () {
                    if (c) {
                        this.windowElement.addClassName("my_shop")
                    }
                    var y = 0, A = 0;
                    var C = this.windowElement.getDimensions();
                    var D = C.width;
                    var B = C.height;
                    var w = this.elementX + this.elementWidth / 2;
                    var x = this.elementY;
                    var t = w;
                    var u = x + this.elementHeight;
                    if (this.arrowNew) {
                        var b = this.cardId.down("em." + this.indicationStyle)
                    } else {
                        var b = this.cardId.down("p." + this.indicationStyle)
                    }
                    var a;
                    if (b === undefined || b === null) {
                        a = 17
                    } else {
                        a = b.getHeight()
                    }
                    switch (this.positionType) {
                        case 0:
                            y = t - 47;
                            A = u + a;
                            break;
                        case 1:
                            y = t - (D - 47);
                            A = u + a;
                            break;
                        case 2:
                            y = w - 47;
                            A = x - a - B;
                            break;
                        case 3:
                            y = w - (D - 47);
                            A = x - a - B;
                            break;
                        default:
                            y = t - 47;
                            A = u + a;
                            break
                    }
                    var E = 0;
                    if (this.overlay) {
                        E = parseInt(this.overlay.getStyle("zIndex"), 10) + 1
                    } else {
                        E = Windows.getZIndex() + 1
                    }
                    if (Mtime.browser.ie && Mtime.browser.ie >= 7) {
                        var s = this.element.getHeight();
                        var v = this.element.getStyle("fontSize");
                        var z = parseInt(v.slice(0, 2), 10);
                        if (this.element.innerHTML !== "" && s > z + 10) {
                            y = 200
                        }
                    }
                    this.windowElement.setStyle({position: "absolute", top: A + "px", left: y + "px", zIndex: E})
                }
            });
            this.collectBoxId = $(this.collectBox.id);
            if (this.collectBoxId) {
                this.collectBoxId.observe("click", this.onClickshareContainerHandler)
            }
        }
    },
    destroyControl: function () {
        if (this.shareControlStyleType == ShareControlStyleType.OnlyShare || this.shareControlStyleType == ShareControlStyleType.OnlyShareSCM) {
            if (this.collectBox) {
                this.collectBox.close();
                this.collectBox = null
            }
        }
    },
    load: function () {
        if (this.autoLoad && this.isAjaxData) {
            this.server.getShareInfo(this.shareRelatedObjType, this.getShareObjectId(), this.getShareExpandId(), this.getShareExpandContext(), this.getShareInfoCallback.bind(this), this.shareContainer)
        }
    },
    render: function () {
    },
    renderWebShareLinks: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    },
    shareInfoLoaded: function () {
    },
    getShareInfoCallback: function (b) {
        if (typeof getShareInfoResult != "undefined" && getShareInfoResult.value) {
            this.shareInfo = getShareInfoResult.value;
            this.shareInfoLoaded();
            this.renderWebShareLinks();
            if (this.autoLoad) {
                this.onClickShareButtonHandler = this.onClickShareButton.bindAsEventListener(this);
                if (this.shareButton) {
                    this.shareButton.observe("click", this.onClickShareButtonHandler)
                }
            }
            this.showShareDialog(b)
        } else {
            $alert("加载对象信息出错，请刷新页面重试")
        }
    },
    onClickShareButton: function (d) {
        var c = Event.findElement(d, "div");
        this.callback && this.callback(c);
        if (c.hasClassName("curr")) {
            c.removeClassName("curr");
            if (this.shareContainer) {
                this.shareContainer.hide()
            }
        } else {
            if (c.hasClassName("notcurr")) {
                c.removeClassName("notcurr")
            }
            c.addClassName("curr");
            if (this.shareContainer) {
                this.shareContainer.show()
            }
        }
        Event.stop(d)
    },
    onClickshareContainer: function (C) {
        var B = Event.findElement(C, "a");
        if (!B || !B.readAttribute) {
            return
        }
        var J = B.readAttribute("method");
        if (!J) {
            return
        }
        if (this.shareRelatedObjType === ShareRelatedObjectTypes.MovieBehindTemplate) {
            var I = this.shareContainer.up().up().up().down("ul").getElementsBySelector("li");
            if (typeof I != "undefined" && I != null && I.length > 0) {
                var A = I[0];
                var G = A.readAttribute("title");
                var F = A.readAttribute("imgabstract");
                var H = A.down("img").readAttribute("src");
                this.title = G;
                this.pic = H;
                this.desc = F;
                var O = this.getShareObjectUrl();
                var N = this.getShareObjectTitle();
                var K = "";
                var M = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
                var v = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
                if (this.getShareObjectPicUrl()) {
                    K = this.getShareObjectPicUrl()
                }
                var w = this.shareContainer.getElementsBySelector("a").each(function (a) {
                    var c = a.readAttribute("method");
                    var b = this.ButtonInfoMap[c];
                    if (b && b.type == ShareTabType.shareToWeb) {
                        a.href = String.format(b.urlFormat, O, N, K)
                    }
                    if (b && b.type == ShareTabType.shareToEMail) {
                        a.href = String.format(b.urlFormat, O, N, K, M, v)
                    }
                }.bind(this))
            }
        } else {
            if (this.shareRelatedObjType === ShareRelatedObjectTypes.ImageNews) {
                if (this.imageId != this.expandMovieId) {
                    this.shareInfo = null;
                    this.expandMovieId = this.imageId;
                    var y = this.ButtonInfoMap.values();
                    var D = [];
                    var L = siteCommunityServiceUrl + "/ShareHandler.ashx?shareRelatedObjType=" + this.shareRelatedObjType + "&relatedId=" + this.getShareObjectId() + "&expandId=" + this.getShareExpandId() + "&screenshotImageUrl=" + this.getScreenshotImageUrl();
                    var z = L + "&method=" + J;
                    if (this.shareButton && this.shareContainer) {
                        if (this.shareControlStyleType == ShareControlStyleType.FloatStyle) {
                            D.push('<ul class="clearfix">');
                            for (var E = 0; E < y.length; ++E) {
                                var x = y[E];
                                if (this.avaliableTabs.indexOf(x.type) < 0) {
                                    continue
                                }
                                if (x.name === "toMtime") {
                                    D.push('<li class="' + x.shareClassName + '"><a method="' + x.name + '" href="javascript:void(0);">' + x.text + "</a></li>")
                                } else {
                                    if (x.name === "toEMail") {
                                        D.push('<li class="' + x.shareClassName + '"><a method="' + x.name + '" href="javascript:EmailIframe(\'' + L + "&method=" + x.name + "');\" >" + x.text + "</a></li>")
                                    } else {
                                        D.push('<li class="' + x.shareClassName + '"><a method="' + x.name + '" href="javascript:GlobalSiteShare(\'' + L + "&method=" + x.name + "');\" >" + x.text + "</a></li>")
                                    }
                                }
                            }
                            D.push("</ul>");
                            this.shareContainer.innerHTML = D.join("")
                        } else {
                            if (this.shareControlStyleType == ShareControlStyleType.ListStyle) {
                                for (var E = 0; E < y.length; ++E) {
                                    var x = y[E];
                                    if (this.avaliableTabs.indexOf(x.type) < 0) {
                                        continue
                                    }
                                    if (x.name === "toMtime") {
                                        D.push('<p class="' + x.shareClassName + '"><a method="' + x.name + '" href="javascript:void(0);">' + x.text + "</a></p>")
                                    } else {
                                        if (x.name === "toEMail") {
                                            D.push('<p class="' + x.shareClassName + '"><a method="' + x.name + '" href="javascript:EmailIframe(\'' + L + "&method=" + x.name + "');\" >" + x.text + "</a></p>")
                                        } else {
                                            D.push('<p class="' + x.shareClassName + '"><a method="' + x.name + '" href="javascript:GlobalSiteShare(\'' + L + "&method=" + x.name + "');\" >" + x.text + "</a></p>")
                                        }
                                    }
                                }
                                this.shareContainer.innerHTML = D.join("")
                            }
                        }
                    }
                    GlobalUrlValue = z
                }
            }
        }
        if (this.shareInfo === null && this.isAjaxData) {
            this.server.getShareInfo(this.shareRelatedObjType, this.getShareObjectId(), this.getShareExpandId(), this.getShareExpandContext(), this.getShareInfoCallback.bind(this, J), this.shareContainer)
        } else {
            this.showShareDialog(J, C)
        }
    },
    shareTrack: function (d) {
        var f = null;
        if (this.shareRelatedObjType != 0) {
            this.ShareRelatedObjectTypes.find(function (a) {
                if (a.value === this.relatedObjType) {
                    f = a.key
                }
                return false
            }.bind(this))
        } else {
            this.MtimeShareRelatedObjectTypes.find(function (a) {
                if (a.value === this.relatedObjType) {
                    f = a.key
                }
                return false
            }.bind(this))
        }
        if (!f) {
            return
        }
        var e = String.format("M11_Common_Share{0}_{1}", f, d);
        if (typeof tracker !== "undefined" && typeof tracker.trackAdView !== "undefined") {
            tracker.trackClick("M11_Common", e)
        }
    },
    showShareDialog: function (e, f) {
        var d = this.ButtonInfoMap[e];
        if (!d) {
            return
        }
        if (d.type == ShareTabType.shareToWeb || d.type == ShareTabType.shareToEMail) {
            if (!f) {
            }
            if (this.shareSuccessCallBack) {
                this.shareSuccessCallBack()
            }
            this.shareTrack(e)
        } else {
            if (d.type == ShareTabType.showQrCode) {
                if (this.shareSuccessCallBack) {
                    this.shareSuccessCallBack()
                }
                this.shareTrack(e)
            } else {
                if (f) {
                    Event.stop(f)
                }
                if (!this.shareInfo || typeof(this.shareInfo.isLogin) == "undefined" || this.shareInfo.isLogin == null) {
                    this.server.getIsLogin(function () {
                        if (typeof(getIsLogin) != "undefined" && getIsLogin.value) {
                            if (!getIsLogin.value.isLogin) {
                                $redirect(this.getSignInUrl());
                                return
                            }
                            switch (d.type) {
                                case ShareTabType.shareToMtimeFriend:
                                    this.showShareToMtimeFriendDialog();
                                    if (this.shareButton) {
                                        if (this.shareButton.hasClassName("curr")) {
                                            this.shareButton.removeClassName("curr");
                                            if (this.shareContainer) {
                                                this.shareContainer.hide()
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    throw"ButtonType does not implemented"
                            }
                        }
                    }.bind(this))
                } else {
                    if (!this.shareInfo.isLogin) {
                        $redirect(this.getSignInUrl());
                        return
                    }
                    switch (d.type) {
                        case ShareTabType.shareToMtimeFriend:
                            this.showShareToMtimeFriendDialog();
                            if (this.shareButton) {
                                if (this.shareButton.hasClassName("curr")) {
                                    this.shareButton.removeClassName("curr");
                                    if (this.shareContainer) {
                                        this.shareContainer.hide()
                                    }
                                }
                            }
                            break;
                        default:
                            throw"ButtonType does not implemented"
                    }
                }
            }
        }
    },
    showShareToMtimeFriendDialog: function () {
        new ShareToMtimeFriendDialog({shareControl: this})
    },
    getSignInUrl: function () {
        return siteMcUrl + "/member/signin/operation/?redirectUrl=" + encodeURIComponent(location.href)
    },
    getShareObjectUrl: function () {
        return ""
    },
    getShareObjectPicUrl: function () {
        return ""
    },
    getShareObjectMailTitle: function () {
        return ""
    },
    getShareObjectTitle: function () {
        return ""
    },
    getShareObjectId: function () {
        return 0
    },
    getShareExpandId: function () {
        return 0
    },
    getShareExpandContext: function () {
        return ""
    },
    getScreenshotImageUrl: function () {
        return ""
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一篇文章-" + this.getShareObjectTitle();
        var d = "你好，这篇文章写的不错，我想你可能会感兴趣，推荐你去看看！《" + this.getShareObjectTitle() + "》" + this.getShareObjectUrl();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    getShareObjectDefaultRecommendSuffix: function () {
        return ""
    },
    getMiddleShareHtml: function () {
        return ""
    },
    saveShareInternal: function (k, h, n, l, j, o) {
        if (this.shareRelatedObjType == ShareRelatedObjectTypes.MovieShortComment || this.shareRelatedObjType == ShareRelatedObjectTypes.MovieLeaveComment) {
            this.server.ForwardTweet([k, this.getShareObjectId(), false, false], n, l)
        } else {
            var m = this.getShareObjectId();
            if (this.shareRelatedObjType === ShareRelatedObjectTypes.MovieContentPlot || this.shareRelatedObjType === ShareRelatedObjectTypes.MovieRoleDescInfo || this.shareRelatedObjType === ShareRelatedObjectTypes.MovieBehindCurtain || this.shareRelatedObjType === ShareRelatedObjectTypes.MovieVerseLines || this.shareRelatedObjType === ShareRelatedObjectTypes.MovieBehindTemplate || this.shareRelatedObjType === ShareRelatedObjectTypes.MediaReview) {
                m = this.expandMovieId
            }
            this.server.saveShareInfo(this.relatedObjType, m, k, h, j, o)
        }
    },
    onSaveShareCallback: function (c, d) {
        if (d) {
            c(d)
        }
    },
    onTweetSaveShareCallback: function (c, d) {
        if (d) {
            c(d)
        }
    },
    saveShare: function (j, g, l, k, h, m) {
        this.saveShareInternal(j, this.onSaveShareCallback.bind(this, g), this.onTweetSaveShareCallback.bind(this, l), k, h, m)
    },
    close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyControl();
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var ShareMovieControl = Class.create();
Object.extend(ShareMovieControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieControl.prototype, {
    name: "ShareMovieControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.Movie,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return "分享电影"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一部电影《" + this.getShareObjectMailTitle() + "》";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieIsReleasedControl = Class.create();
Object.extend(ShareMovieIsReleasedControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieIsReleasedControl.prototype, {
    name: "ShareMovieIsReleasedControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieIsReleased,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return "分享电影"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一部电影《" + this.getShareObjectMailTitle() + "》";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieUpcomingControl = Class.create();
Object.extend(ShareMovieUpcomingControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieUpcomingControl.prototype, {
    name: "ShareMovieUpcomingControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieUpcoming,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return "分享电影"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一部电影《" + this.getShareObjectMailTitle() + "》";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieHasAwardsControl = Class.create();
Object.extend(ShareMovieHasAwardsControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieHasAwardsControl.prototype, {
    name: "ShareMovieHasAwardsControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieHasAwards,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return "分享电影"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一部电影《" + this.getShareObjectMailTitle() + "》";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieGeneralControl = Class.create();
Object.extend(ShareMovieGeneralControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieGeneralControl.prototype, {
    name: "ShareMovieGeneralControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieGeneral,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return "分享电影"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一部电影《" + this.getShareObjectMailTitle() + "》";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieContentPlotControl = Class.create();
Object.extend(ShareMovieContentPlotControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieContentPlotControl.prototype, {
    name: "ShareMovieContentPlotControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieContentPlot,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieParagraphId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieParagraphLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieParagraphId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》剧情";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieRoleDescInfoControl = Class.create();
Object.extend(ShareMovieRoleDescInfoControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieRoleDescInfoControl.prototype, {
    name: "ShareMovieRoleDescInfoControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieRoleDescInfo,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieRoleId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieRoleLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieRoleId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》角色介绍";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieRoleDescAllControl = Class.create();
Object.extend(ShareMovieRoleDescAllControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieRoleDescAllControl.prototype, {
    name: "ShareMovieRoleDescAllControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieRoleDescAll,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {movieId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareObjectUrl: function () {
        return this.shareInfo.movieRoleLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》角色介绍";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieBehindCurtainControl = Class.create();
Object.extend(ShareMovieBehindCurtainControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieBehindCurtainControl.prototype, {
    name: "ShareMovieBehindCurtainControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieBehindCurtain,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {behindSectionID: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareObjectUrl: function () {
        return this.shareInfo.behindSectionLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.behindSectionID
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.getScreenshotImageUrl()
    },
    getScreenshotImageUrl: function () {
        if (this.shareInfo) {
            return this.shareInfo.screenshotImageUrl
        } else {
            return ""
        }
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》幕后看点";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieVerseLinesControl = Class.create();
Object.extend(ShareMovieVerseLinesControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieVerseLinesControl.prototype, {
    name: "ShareMovieVerseLinesControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieVerseLines,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {behindSectionID: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareObjectUrl: function () {
        return this.shareInfo.behindSectionLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.behindSectionID
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.getScreenshotImageUrl()
    },
    getScreenshotImageUrl: function () {
        if (this.shareInfo) {
            return this.shareInfo.screenshotImageUrl
        } else {
            return ""
        }
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》台词金句";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieBehindTemplateControl = Class.create();
Object.extend(ShareMovieBehindTemplateControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieBehindTemplateControl.prototype, {
    name: "ShareMovieBehindTemplateControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Movie,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieBehindTemplate,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        title: "",
        link: "",
        pic: "",
        desc: "",
        movieId: "",
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    render: function () {
        this.server.getShareInfo(this.shareRelatedObjType, this.getShareObjectId(), this.getShareExpandId(), this.getShareExpandContext(), function () {
            if (typeof getShareInfoResult != "undefined" && getShareInfoResult.value) {
                this.shareInfo = getShareInfoResult.value
            } else {
                $alert("加载对象信息出错，请刷新页面重试")
            }
            var m = this.getShareObjectUrl();
            var l = this.getShareObjectTitle();
            var j = "";
            var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
            var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
            if (this.getShareObjectPicUrl()) {
                j = this.getShareObjectPicUrl()
            }
            var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
                var c = a.readAttribute("method");
                var b = this.ButtonInfoMap[c];
                if (b && b.type == ShareTabType.shareToWeb) {
                    a.href = String.format(b.urlFormat, m, l, j)
                }
                if (b && b.type == ShareTabType.shareToEMail) {
                    a.href = String.format(b.urlFormat, m, l, j, k, g)
                }
            }.bind(this))
        }.bind(this), this.shareContainer)
    },
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.title + " " + this.desc + this.link
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return "《" + this.shareInfo.movieName + "》-" + this.title + " " + this.desc + this.link
    },
    getShareObjectId: function () {
        return this.movieId
    },
    getShareExpandId: function () {
        return this.movieId
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.movieName
    },
    getShareObjectPicUrl: function () {
        return this.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》幕后";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieTelecinePageControl = Class.create();
Object.extend(ShareMovieTelecinePageControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieTelecinePageControl.prototype, {
    name: "ShareMovieTelecinePageControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Link,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        date: new Date(),
        movieTitle: "",
        cityName: "",
        imageUrl: "",
        link: "",
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    load: function () {
        this.getImagePath(null);
        ShareControlBase.prototype.load.apply(this)
    },
    getImagePath: function (b) {
        if (!this.imageUrl) {
            Mtime.Component.Ajax.get("Mtime.Service.Pages.TwitterService", "UploadPicByUrl", [this.link], function (a) {
                if (a && a.value && a.value.ImagePath) {
                    Mtime.Component.Ajax.get("Mtime.Service.Pages.TwitterService", "GetThumbnailImageUrlByImagePath", [a.value.ImagePath], function (d) {
                        if (d && d.value && d.value.ImageUrl) {
                            this.imageUrl = a.value.ImagePath;
                            this.link = d.value.ImageUrl;
                            b && b()
                        }
                    }.bind(this), "/Service/Twitter.msi", "get", "20000")
                }
            }.bind(this), "/Service/Twitter.msi", "get", "20000")
        } else {
            b && b()
        }
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.date + " " + this.cityName + " 最新影讯";
        d.push('<div class="picmid"><a title="' + this.movieTitle + '" target="_blank" href="' + this.link + '"><img alt="' + this.movieTitle + '" src="' + this.imageUrl + '" /></a></div>');
        d.push('<div class="txtmid"><h2><a target="_blank" href="' + this.link + '">' + this.movieTitle + "</a></h2><p>" + c + "</p></div>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "《" + this.movieTitle + "》 " + this.date + " " + this.cityName + " 最新影讯 " + this.link
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return "《" + this.movieTitle + "》 " + this.date + " " + this.cityName + " 最新影讯地址：" + this.link
    },
    getShareObjectPicUrl: function () {
        return this.imageUrl
    },
    getShareObjectMailTitle: function () {
        return "《" + this.movieTitle + "》 " + this.date + " " + this.cityName + " 影讯"
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    serverLinkControl: {
        recommendLinkToShare: function (j, k, h, g, f) {
            if (typeof(siteCommunityServiceUrl) != "undefined") {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "RecommendLinkToShare", [j, k, h, g], f, "/community.api?Ajax_CallBack=true", "get", "20000")
            }
        }
    },
    onSaveShareCallback: function (b) {
        if (typeof recommendLinkToShareResult !== "undefined") {
            b(recommendLinkToShareResult)
        }
    },
    saveShareInternal: function (e, d, f) {
        this.serverLinkControl.recommendLinkToShare(this.title, this.link, e, this.desc, d)
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl();
            j = j.replace(new RegExp("&", "ig"), "^PIC^")
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var SharePersonFavorableCommentControl = Class.create();
Object.extend(SharePersonFavorableCommentControl.prototype, ShareControlBase.prototype);
Object.extend(SharePersonFavorableCommentControl.prototype, {
    name: "SharePersonFavorableCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Person,
    shareRelatedObjType: ShareRelatedObjectTypes.PersonFavorableComment,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {personId: 0}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.personName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.personName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.personName + "</a></h2><h3>" + this.shareInfo.personNameEn + "</h3><ul><li>" + this.shareInfo.personSex + "，" + this.shareInfo.personConstellation + "</li><li>职业：" + this.shareInfo.personProfessions + "</li></ul></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return "分享影人"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.personLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.personId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = '关于"' + this.getShareObjectMailTitle() + '"';
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareNewsControl = Class.create();
Object.extend(ShareNewsControl.prototype, ShareControlBase.prototype);
Object.extend(ShareNewsControl.prototype, {
    name: "ShareNewsControl",
    relatedObjType: MtimeShareRelatedObjectTypes.CMSNews,
    shareRelatedObjType: ShareRelatedObjectTypes.News,
    isPicNews: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {newsId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.shareInfo.description;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "分享新闻"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.newsLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.newsId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "电影新闻分享：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareImageNewsControl = Class.create();
Object.extend(ShareImageNewsControl.prototype, ShareControlBase.prototype);
Object.extend(ShareImageNewsControl.prototype, {
    name: "ShareImageNewsControl",
    relatedObjType: MtimeShareRelatedObjectTypes.CMSGalleryImage,
    shareRelatedObjType: ShareRelatedObjectTypes.ImageNews,
    isPicNews: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {imageId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.isPicNews = true
    },
    initializeField: function () {
        ShareControlBase.prototype.initializeField.apply(this);
        this.expandMovieId = this.imageId
    },
    getMiddleShareHtml: function () {
        var d = [];
        var e = this.shareInfo.imageNewTitle;
        var f = this.shareInfo.imageTitle;
        d.push('<div class="picmid"><a title="' + e + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + e + '" src="' + this.getShareObjectPicUrl() + '" /></a></div>');
        d.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + e + "</a></h2><p>" + f + "</p></div>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "分享新闻"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.newsLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.imageId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "电影新闻分享：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieLongCommentControl = Class.create();
Object.extend(ShareMovieLongCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieLongCommentControl.prototype, {
    name: "ShareMovieLongCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Blog,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieLongComment,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {blogId: 0, urlTypeId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.shareInfo.description;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "分享影评"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.blogLink
    },
    getShareObjectTitle: function () {
        this.shareInfo.title = this.shareInfo.title.replace(/\"/g, "＂");
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.blogId
    },
    getShareExpandId: function () {
        return this.urlTypeId
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "[影评分享]" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieShortCommentControl = Class.create();
Object.extend(ShareMovieShortCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieShortCommentControl.prototype, {
    name: "ShareMovieShortCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Twitter,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieShortComment,
    middleShareClassName: "",
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {tweetId: 0}),
    shareMtimeContent: function () {
        return "分享微评"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.tweetLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.tweetId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "关于《" + this.getShareObjectMailTitle() + "》的短评";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMovieLeaveCommentControl = Class.create();
Object.extend(ShareMovieLeaveCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMovieLeaveCommentControl.prototype, {
    name: "ShareMovieLeaveCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Twitter,
    shareRelatedObjType: ShareRelatedObjectTypes.MovieLeaveComment,
    middleShareClassName: "",
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {tweetId: 0}),
    shareMtimeContent: function () {
        return "分享微评"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.tweetLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.tweetId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = '关于"' + this.getShareObjectMailTitle() + '"';
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareVideoMovieControl = Class.create();
Object.extend(ShareVideoMovieControl.prototype, ShareControlBase.prototype);
Object.extend(ShareVideoMovieControl.prototype, {
    name: "ShareVideoMovieControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Video,
    shareRelatedObjType: ShareRelatedObjectTypes.VideoMovie,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {videoId: 0}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.videoTitleName + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.shareInfo.videoTitleName + '" src="' + this.shareInfo.specificVideoUrl + '" /><i class="i_share_video">&nbsp;</i><i class="i_share_mask">&nbsp;</i></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.shareInfo.videoTitleName + "</a></h2><p>" + this.shareInfo.videoName + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return "分享视频"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.videoLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.videoId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享视频：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareVideoPersonControl = Class.create();
Object.extend(ShareVideoPersonControl.prototype, ShareControlBase.prototype);
Object.extend(ShareVideoPersonControl.prototype, {
    name: "ShareVideoPersonControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Video,
    shareRelatedObjType: ShareRelatedObjectTypes.VideoPerson,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {videoId: 0, personId: 0}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.videoTitleName + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.shareInfo.videoTitleName + '" src="' + this.shareInfo.specificVideoUrl + '" /><i class="i_share_video">&nbsp;</i><i class="i_share_mask">&nbsp;</i></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.shareInfo.videoTitleName + "</a></h2><p>" + this.shareInfo.videoName + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return "分享视频"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.videoLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.videoId
    },
    getShareExpandId: function () {
        return this.personId
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享视频：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var SharePictureMovieControl = Class.create();
Object.extend(SharePictureMovieControl.prototype, ShareControlBase.prototype);
Object.extend(SharePictureMovieControl.prototype, {
    name: "SharePictureMovieControl",
    relatedObjType: MtimeShareRelatedObjectTypes.MoviePicture,
    shareRelatedObjType: ShareRelatedObjectTypes.PictureMovie,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {imageId: 0}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.specificImageUrl + '" /></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.shareInfo.movieName + "</a></h2><p>" + this.shareInfo.specificImageTypeName + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return "分享图片"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.imageLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.imageId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享图片：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var SharePicturePersonControl = Class.create();
Object.extend(SharePicturePersonControl.prototype, ShareControlBase.prototype);
Object.extend(SharePicturePersonControl.prototype, {
    name: "SharePicturePersonControl",
    relatedObjType: MtimeShareRelatedObjectTypes.PersonPicture,
    shareRelatedObjType: ShareRelatedObjectTypes.PicturePerson,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {imageId: 0}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.personName + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.shareInfo.personName + '" src="' + this.shareInfo.specificImageUrl + '" /></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.shareInfo.personName + "</a></h2><p>" + this.shareInfo.specificImageTypeName + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return "分享图片"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.imageLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.imageId
    },
    getShareExpandId: function () {
        return this.personId
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享图片：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareCinemaControl = Class.create();
Object.extend(ShareCinemaControl.prototype, ShareControlBase.prototype);
Object.extend(ShareCinemaControl.prototype, {
    name: "ShareCinemaControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Cinema,
    shareRelatedObjType: ShareRelatedObjectTypes.Cinema,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {cinemaId: 0, date: new Date()}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.cinemaName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.cinemaName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.cinemaName + "</a></h2><p>" + this.shareInfo.description + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareExpandContext: function () {
        return encodeURIComponent(this.getDatetime())
    },
    getShareObjectUrl: function () {
        return this.shareInfo.cinemaLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.cinemaId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getDatetime: function () {
        var m = this.date.getFullYear();
        var k = this.date.getMonth() + 1;
        var h = this.date.getDate();
        var l = k.toString();
        if (k < 10) {
            l = "0" + l
        }
        var j = h.toString();
        if (h < 10) {
            j = "0" + j
        }
        var g = m + "年" + l + "月" + h + "日";
        return g
    },
    renderWebShareLinks: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl().replace(/\&/g, "^PIC^")
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    },
    getShareObjectDefaultRecommendContent: function (h) {
        var f = this.getDatetime();
        var g = '"' + this.getShareObjectMailTitle() + '"' + f + "影讯";
        var e = this.getShareObjectTitle();
        if (h === "subject") {
            return g
        } else {
            return e
        }
    }
});
var ShareUniversalControl = Class.create();
Object.extend(ShareUniversalControl.prototype, ShareControlBase.prototype);
Object.extend(ShareUniversalControl.prototype, {
    name: "ShareUniversalControl",
    relatedObjType: 51,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        title: "",
        fullTitle: "",
        link: "",
        pic: "",
        desc: "",
        newsId: 0,
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.title + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.title + '" src="' + this.getShareObjectPicUrl() + '" style="width:96px;height:96px" /></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.title + "</a></h2></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return this.title + this.link
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return this.fullTitle + this.link
    },
    getShareObjectMailTitle: function () {
        return this.title
    },
    getShareObjectId: function () {
        return this.newsId
    },
    getShareObjectPicUrl: function () {
        return this.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var ShareBlogControl = Class.create();
Object.extend(ShareBlogControl.prototype, ShareControlBase.prototype);
Object.extend(ShareBlogControl.prototype, {
    name: "ShareBlogControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Blog,
    shareRelatedObjType: ShareRelatedObjectTypes.Blog,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {blogId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.shareInfo.description;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "分享文章"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.blogLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.blogId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "[分享文章]" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareForumTopicControl = Class.create();
Object.extend(ShareForumTopicControl.prototype, ShareControlBase.prototype);
Object.extend(ShareForumTopicControl.prototype, {
    name: "ShareForumTopicControl",
    relatedObjType: MtimeShareRelatedObjectTypes.ForumTopic,
    shareRelatedObjType: ShareRelatedObjectTypes.ForumTopic,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {topicId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.shareInfo.description;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "分享文章"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.topicLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.topicId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "[分享文章]" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareActivityControl = Class.create();
Object.extend(ShareActivityControl.prototype, ShareControlBase.prototype);
Object.extend(ShareActivityControl.prototype, {
    name: "ShareActivityControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Activity,
    shareRelatedObjType: ShareRelatedObjectTypes.Activity,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {activityId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.shareInfo.description;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return "分享活动"
    },
    getShareObjectUrl: function () {
        return this.shareInfo.activityLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.activityId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "[分享活动]" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareMediaReviewControl = Class.create();
Object.extend(ShareMediaReviewControl.prototype, ShareControlBase.prototype);
Object.extend(ShareMediaReviewControl.prototype, {
    name: "ShareMediaReviewControl",
    relatedObjType: MtimeShareRelatedObjectTypes.MediaReview,
    shareRelatedObjType: ShareRelatedObjectTypes.MediaReview,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {mediaReviewId: 0}),
    getMiddleShareHtml: function () {
        var c = [];
        var d = "";
        if (this.shareInfo.movieLength !== "") {
            d = this.shareInfo.movieLength + "分钟 - "
        }
        c.push('<div class="picmid"><a title="' + this.shareInfo.movieName + '" target="_blank" href="' + this.shareInfo.coverLinkUrl + '"><img alt="' + this.shareInfo.movieName + '" src="' + this.shareInfo.coverImageUrl + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.shareInfo.coverLinkUrl + '">' + this.shareInfo.movieName + " (" + this.shareInfo.movieYear + ")</a></h2><h3>" + this.shareInfo.movieNameEn + "</h3><ul><li>" + d + this.shareInfo.movieTypes + "</li><li>导演：" + this.shareInfo.movieDirectors + "</li><li>主演：" + this.shareInfo.movieActors + "</li></ul></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.shareInfo.shareReason
    },
    getShareObjectUrl: function () {
        return this.shareInfo.mediaReviewLink
    },
    getShareObjectTitle: function () {
        this.shareInfo.title = this.shareInfo.title.replace(/\"/g, "＂");
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.mediaReviewId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectPicUrl: function () {
        return this.shareInfo.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享《" + this.getShareObjectMailTitle() + "》媒体影评";
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareLinkControl = Class.create();
Object.extend(ShareLinkControl.prototype, ShareControlBase.prototype);
Object.extend(ShareLinkControl.prototype, {
    name: "ShareLinkControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Link,
    isAjaxData: false,
    middleShareClassName: "",
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        title: "",
        link: "",
        pic: "",
        desc: "",
        mtimeDefaultContent: "",
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    shareMtimeContent: function () {
        return this.mtimeDefaultContent
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return this.title + this.link
    },
    getShareObjectMailTitle: function () {
        return this.title
    },
    getShareObjectId: function () {
        return 0
    },
    getShareObjectPicUrl: function () {
        return this.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    serverLinkControl: {
        recommendLinkToShare: function (j, k, h, g, f) {
            if (typeof(siteCommunityServiceUrl) != "undefined") {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "RecommendLinkToShare", [j, k, h, g], f, "/community.api?Ajax_CallBack=true", "get", "20000")
            }
        }
    },
    onSaveShareCallback: function (b) {
        if (typeof recommendLinkToShareResult !== "undefined") {
            b(recommendLinkToShareResult)
        }
    },
    saveShareInternal: function (e, d, f) {
        this.serverLinkControl.recommendLinkToShare(this.title, this.link, e, this.desc, d)
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var ShareOffsiteLinkControl = Class.create();
Object.extend(ShareOffsiteLinkControl.prototype, ShareControlBase.prototype);
Object.extend(ShareOffsiteLinkControl.prototype, {
    name: "ShareOffsiteLinkControl",
    relatedObjType: MtimeShareRelatedObjectTypes.OffsiteLink,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        title: "",
        link: "",
        pic: "",
        desc: "",
        mtimeDefaultContent: ""
    }),
    shareMtimeContent: function () {
        return this.mtimeDefaultContent
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return this.title + this.link
    },
    getShareObjectMailTitle: function () {
        return this.title
    },
    getShareObjectId: function () {
        return 0
    },
    getShareObjectPicUrl: function () {
        return this.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    initializeDOM: function () {
        var f = this.ButtonInfoMap.values();
        var g = [];
        if (this.shareContainer) {
            for (var h = 0; h < f.length; ++h) {
                var e = f[h];
                if (this.avaliableTabs.indexOf(e.type) < 0) {
                    continue
                }
                if (e.name === "toMtime" || e.name === "toQzone") {
                } else {
                    if (e.name === "toEMail") {
                        g.push('<a class="' + e.shareClassName + '" method="' + e.name + '" href="###" >' + e.text + "</a>")
                    } else {
                        g.push('<a class="' + e.shareClassName + '" method="' + e.name + '" href="###" target="_self" >' + e.text + "</a>")
                    }
                }
            }
            this.shareContainer.innerHTML = g.join("")
        }
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var ShareGoodsDetailControl = Class.create();
Object.extend(ShareGoodsDetailControl.prototype, ShareControlBase.prototype);
Object.extend(ShareGoodsDetailControl.prototype, {
    name: "ShareGoodsDetailControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Link,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        title: "",
        link: "",
        pic: "",
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.title + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.title + '" src="' + this.pic + '" /></a></div>');
        return b.join("")
    },
    shareMtimeContent: function () {
        return "分享商品"
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return this.title + this.link
    },
    getShareObjectMailTitle: function () {
        return this.title
    },
    getShareObjectId: function () {
        return 0
    },
    getShareObjectPicUrl: function () {
        return this.pic
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    serverLinkControl: {
        recommendLinkToShare: function (j, k, h, g, f) {
            if (typeof(siteCommunityServiceUrl) != "undefined") {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "RecommendLinkToShare", [j, k, h, g], f, "/community.api?Ajax_CallBack=true", "get", "20000")
            }
        }
    },
    onSaveShareCallback: function (b) {
        if (typeof recommendLinkToShareResult !== "undefined") {
            b(recommendLinkToShareResult)
        }
    },
    saveShareInternal: function (e, d, f) {
        this.serverLinkControl.recommendLinkToShare(this.title, this.link, e, this.desc, d)
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var ShareNewsCommentControl = Class.create();
Object.extend(ShareNewsCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareNewsCommentControl.prototype, {
    name: "ShareNewsCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.CMSNews,
    shareRelatedObjType: ShareRelatedObjectTypes.NewsComment,
    isPicNews: false,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        newsId: 0,
        commentContext: "",
        newsTitle: "",
        newsUrl: "",
        shareInfo: {isLogin: null, needAdditionalInfo: true}
    }),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.newsTitle;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        return this.commentContext
    },
    getShareObjectUrl: function () {
        return this.newsUrl
    },
    getShareObjectTitle: function () {
        return "<" + this.commentContext + "> 评：新闻 【" + this.newsTitle + "】 <" + this.newsUrl + ">"
    },
    getShareObjectId: function () {
        return this.newsId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.newsTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "新闻评论分享：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var ShareVideoMovieCommentControl = Class.create();
Object.extend(ShareVideoMovieCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareVideoMovieCommentControl.prototype, {
    name: "ShareVideoMovieCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Video,
    shareRelatedObjType: ShareRelatedObjectTypes.VideoMovieComment,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {videoId: 0, commentContext: ""}),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.videoTitleName + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.shareInfo.videoTitleName + '" src="' + this.shareInfo.specificVideoUrl + '" /><i class="i_share_video">&nbsp;</i><i class="i_share_mask">&nbsp;</i></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.shareInfo.videoTitleName + "</a></h2><p>" + this.shareInfo.videoName + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return this.commentContext
    },
    getShareObjectUrl: function () {
        return this.shareInfo.videoLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.videoId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareExpandContext: function () {
        return encodeURIComponent(this.commentContext)
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享视频评论：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareVideoPersonCommentControl = Class.create();
Object.extend(ShareVideoPersonCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareVideoPersonCommentControl.prototype, {
    name: "ShareVideoPersonCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.Video,
    shareRelatedObjType: ShareRelatedObjectTypes.VideoPersonComment,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        videoId: 0,
        personId: 0,
        commentContext: ""
    }),
    getMiddleShareHtml: function () {
        var b = [];
        b.push('<div class="picmid"><a title="' + this.shareInfo.videoTitleName + '" target="_blank" href="' + this.getShareObjectUrl() + '"><img alt="' + this.shareInfo.videoTitleName + '" src="' + this.shareInfo.specificVideoUrl + '" /><i class="i_share_video">&nbsp;</i><i class="i_share_mask">&nbsp;</i></a></div>');
        b.push('<div class="txtmid"><h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + this.shareInfo.videoTitleName + "</a></h2><p>" + this.shareInfo.videoName + "</p></div>");
        return b.join("")
    },
    shareMtimeContent: function () {
        return this.commentContext
    },
    getShareObjectUrl: function () {
        return this.shareInfo.videoLink
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.videoId
    },
    getShareExpandId: function () {
        return this.personId
    },
    getShareExpandContext: function () {
        return encodeURIComponent(this.commentContext)
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享视频评论：" + this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareSCMGoodsCommentControl = Class.create();
Object.extend(ShareSCMGoodsCommentControl.prototype, ShareControlBase.prototype);
Object.extend(ShareSCMGoodsCommentControl.prototype, {
    name: "ShareSCMGoodsCommentControl",
    relatedObjType: MtimeShareRelatedObjectTypes.SCMGoodsComment,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        goodsName: "",
        commentText: "",
        link: "",
        goodsImageUrl: "",
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    getMiddleShareHtml: function () {
        var c = [];
        var d = this.goodsImageUrl.substring(0, this.goodsImageUrl.lastIndexOf(".")) + "_160X160";
        d += this.goodsImageUrl.substring(this.goodsImageUrl.lastIndexOf("."));
        c.push('<div class="picmid"><a title="' + this.goodsName + '" target="_blank" href="' + this.link + '"><img alt="' + this.goodsName + '" src="' + d + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.link + '">' + this.goodsName + "</a></h2></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return "【商品】" + this.goodsName + ': "' + this.commentText + '" ' + this.link
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return this.goodsName + ': "' + this.commentText + '" ' + this.link
    },
    getShareObjectMailTitle: function () {
        return "商品评论分享：" + this.goodsName
    },
    getShareObjectId: function () {
        return 0
    },
    getShareObjectPicUrl: function () {
        return this.goodsImageUrl
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = this.getShareObjectTitle();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    serverLinkControl: {
        recommendLinkToShare: function (j, k, h, g, f) {
            if (typeof(siteCommunityServiceUrl) != "undefined") {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "RecommendLinkToShare", [j, k, h, g], f, "/community.api?Ajax_CallBack=true", "get", "20000")
            }
        }
    },
    onSaveShareCallback: function (b) {
        if (typeof recommendLinkToShareResult !== "undefined") {
            b(recommendLinkToShareResult)
        }
    },
    saveShareInternal: function (e, d, f) {
        this.serverLinkControl.recommendLinkToShare(this.goodsName, this.link, e, this.desc, d)
    },
    render: function () {
        var m = this.getShareObjectUrl();
        var l = this.getShareObjectTitle();
        var j = "";
        var k = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var g = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            j = this.getShareObjectPicUrl()
        }
        var h = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                a.href = String.format(b.urlFormat, m, l, j)
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, m, l, j, k, g)
            }
        }.bind(this))
    }
});
var ShareSCMGoodsDetailControl = Class.create();
Object.extend(ShareSCMGoodsDetailControl.prototype, ShareControlBase.prototype);
Object.extend(ShareSCMGoodsDetailControl.prototype, {
    name: "ShareSCMGoodsDetailControl",
    relatedObjType: MtimeShareRelatedObjectTypes.SCMGoodsComment,
    isAjaxData: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {
        goodsName: "",
        link: "",
        goodsImageUrl: "",
        desc: "",
        shareInfo: {isLogin: null, needAdditionalInfo: false}
    }),
    getMiddleShareHtml: function () {
        var c = [];
        var d = this.goodsImageUrl.substring(0, this.goodsImageUrl.lastIndexOf(".")) + "_160X160";
        d += this.goodsImageUrl.substring(this.goodsImageUrl.lastIndexOf("."));
        c.push('<div class="picmid"><a title="' + this.goodsName + '" target="_blank" href="' + this.link + '"><img alt="' + this.goodsName + '" src="' + d + '" /></a></div>');
        c.push('<div class="txtmid"><h2><a target="_blank" href="' + this.link + '">' + this.goodsName + "</a></h2></div>");
        return c.join("")
    },
    shareMtimeContent: function () {
        return this.goodsName + " | " + this.desc
    },
    onClickShareButton: function (d) {
        var c = Event.findElement(d, "dd");
        this.callback && this.callback(c);
        if (c.hasClassName("on")) {
            c.removeClassName("on");
            if (this.shareContainer) {
                this.shareContainer.hide()
            }
        } else {
            if (c.hasClassName("notcurr")) {
                c.removeClassName("notcurr")
            }
            c.addClassName("on");
            if (this.shareContainer) {
                this.shareContainer.show()
            }
        }
        Event.stop(d)
    },
    getShareObjectUrl: function () {
        return this.link
    },
    getShareObjectTitle: function () {
        return this.goodsName + " | " + this.desc + " | 时光商城"
    },
    getShareObjectMailTitle: function () {
        return "分享商品：" + this.goodsName
    },
    getShareObjectId: function () {
        return 0
    },
    getShareObjectPicUrl: function () {
        return this.goodsImageUrl
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = this.getShareObjectMailTitle();
        var d = "分享商品： " + this.getShareObjectTitle() + " " + this.getShareObjectUrl();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    },
    serverLinkControl: {
        recommendLinkToShare: function (j, k, h, g, f) {
            if (typeof(siteCommunityServiceUrl) != "undefined") {
                return Mtime.Component.Ajax.crossDomainCallBack(siteCommunityServiceUrl, "Mtime.Community.Services.CommunityService", "RecommendLinkToShare", [j, k, h, g], f, "/community.api?Ajax_CallBack=true", "get", "20000")
            }
        }
    },
    onSaveShareCallback: function (b) {
        if (typeof recommendLinkToShareResult !== "undefined") {
            b(recommendLinkToShareResult)
        }
    },
    saveShareInternal: function (e, d, f) {
        this.serverLinkControl.recommendLinkToShare(this.goodsName, this.link, e, this.desc, d)
    },
    render: function () {
        var q = this.getShareObjectUrl();
        var p = this.getShareObjectTitle();
        var n = "";
        var m = this.goodsName;
        var l = this.desc;
        var o = encodeURIComponent(this.getShareObjectDefaultRecommendContent("subject"));
        var j = encodeURIComponent(this.getShareObjectDefaultRecommendContent("body"));
        if (this.getShareObjectPicUrl()) {
            n = this.getShareObjectPicUrl()
        }
        var k = this.shareContainer.getElementsBySelector("a").each(function (a) {
            var c = a.readAttribute("method");
            var b = this.ButtonInfoMap[c];
            if (b && b.type == ShareTabType.shareToWeb) {
                if (c == "toSinaMicroblogging" || c == "toTencentMicroblogging") {
                    a.href = String.format(b.urlFormat, q, p, n)
                } else {
                    a.href = String.format(b.urlFormat, q, m, n, p, l)
                }
            }
            if (b && b.type == ShareTabType.shareToEMail) {
                a.href = String.format(b.urlFormat, q, m, n, p, l, o, j)
            }
        }.bind(this))
    }
});
var ShareTopListControl = Class.create();
Object.extend(ShareTopListControl.prototype, ShareControlBase.prototype);
Object.extend(ShareTopListControl.prototype, {
    name: "ShareTopListControl",
    relatedObjType: MtimeShareRelatedObjectTypes.TopList,
    shareRelatedObjType: ShareRelatedObjectTypes.TopList,
    isPicNews: false,
    options: Object.extend(Object.extend({}, ShareControlBase.prototype.options), {topListId: 0}),
    renderWebShareLinks: function () {
        ShareControlBase.prototype.renderWebShareLinks.apply(this);
        this.middleShareClassName = "infoshare"
    },
    getMiddleShareHtml: function () {
        var d = [];
        var c = this.shareInfo.description;
        if (c.bytelength() > this.EXCERPT_SIZE * 2) {
            c = c.substr(0, this.EXCERPT_SIZE) + "..."
        }
        d.push('<h2><a target="_blank" href="' + this.getShareObjectUrl() + '">' + c + "</a></h2>");
        return d.join("")
    },
    shareMtimeContent: function () {
        var b = this.shareInfo.description;
        if (b.bytelength() > this.EXCERPT_SIZE * 2) {
            b = b.substr(0, this.EXCERPT_SIZE) + "..."
        }
        return b
    },
    getShareObjectUrl: function () {
        return this.shareInfo.link
    },
    getShareObjectTitle: function () {
        return this.shareInfo.title
    },
    getShareObjectId: function () {
        return this.topListId
    },
    getShareExpandId: function () {
        return 0
    },
    getShareObjectMailTitle: function () {
        return this.shareInfo.mailTitle
    },
    getShareObjectDefaultRecommendContent: function (f) {
        var e = "分享一个时光热榜-" + this.getShareObjectMailTitle();
        var d = "你好，这个时光热榜不错哦，我猜你会喜欢！《" + this.getShareObjectMailTitle() + "》" + this.getShareObjectUrl();
        if (f === "subject") {
            return e
        } else {
            return d
        }
    }
});
var ShareControlManager = Class.create();
Object.extend(ShareControlManager.prototype, {
    controls: null, initialize: function () {
        this.controls = []
    }, swtichNewControl: function (e, h) {
        var f = parseInt(e.shareRelatedObjType, 10);
        var g;
        switch (f) {
            case ShareRelatedObjectTypes.Movie:
                g = new ShareMovieControl({
                    shareControlStyleType: h,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieIsReleased:
                g = new ShareMovieIsReleasedControl({
                    shareControlStyleType: h,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieUpcoming:
                g = new ShareMovieUpcomingControl({
                    shareControlStyleType: h,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieHasAwards:
                g = new ShareMovieHasAwardsControl({
                    shareControlStyleType: h,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieGeneral:
                g = new ShareMovieGeneralControl({
                    shareControlStyleType: h,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieContentPlot:
                g = new ShareMovieContentPlotControl({
                    shareControlStyleType: h,
                    movieParagraphId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    expandMovieId: e.expandMovieId,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieRoleDescInfo:
                g = new ShareMovieRoleDescInfoControl({
                    shareControlStyleType: h,
                    movieRoleId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    expandMovieId: e.expandMovieId,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieRoleDescAll:
                g = new ShareMovieRoleDescAllControl({
                    shareControlStyleType: h,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieBehindCurtain:
                g = new ShareMovieBehindCurtainControl({
                    shareControlStyleType: h,
                    behindSectionID: e.relatedId,
                    screenshotImageUrl: e.screenshotImageUrl,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    expandMovieId: e.expandMovieId,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieVerseLines:
                g = new ShareMovieVerseLinesControl({
                    shareControlStyleType: h,
                    behindSectionID: e.relatedId,
                    screenshotImageUrl: e.screenshotImageUrl,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    expandMovieId: e.expandMovieId,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieBehindTemplate:
                g = new ShareMovieBehindTemplateControl({
                    shareControlStyleType: h,
                    title: e.title,
                    link: e.link,
                    pic: e.pic,
                    desc: e.desc,
                    movieId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    expandMovieId: e.expandMovieId,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieTelecinePage:
                g = new ShareMovieTelecinePageControl({
                    shareControlStyleType: h,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    movieTitle: e.movieTitle,
                    cityName: e.cityName,
                    imageUrl: e.imageUrl,
                    date: e.date,
                    link: e.link,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.PersonFavorableComment:
                g = new SharePersonFavorableCommentControl({
                    shareControlStyleType: h,
                    personId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.News:
                g = new ShareNewsControl({
                    shareControlStyleType: h,
                    newsId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.ImageNews:
                g = new ShareImageNewsControl({
                    shareControlStyleType: h,
                    imageId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieLongComment:
                g = new ShareMovieLongCommentControl({
                    shareControlStyleType: h,
                    blogId: e.relatedId,
                    urlTypeId: e.expandId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieShortComment:
                g = new ShareMovieShortCommentControl({
                    shareControlStyleType: h,
                    tweetId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MovieLeaveComment:
                g = new ShareMovieLeaveCommentControl({
                    shareControlStyleType: h,
                    tweetId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.VideoMovie:
                g = new ShareVideoMovieControl({
                    shareControlStyleType: h,
                    videoId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.VideoPerson:
                g = new ShareVideoPersonControl({
                    shareControlStyleType: h,
                    videoId: e.relatedId,
                    personId: e.expandId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.PictureMovie:
                g = new SharePictureMovieControl({
                    shareControlStyleType: h,
                    imageId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    desc: e.desc,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.PicturePerson:
                g = new SharePicturePersonControl({
                    shareControlStyleType: h,
                    imageId: e.relatedId,
                    personId: e.expandId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    desc: e.desc,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.Cinema:
                g = new ShareCinemaControl({
                    shareControlStyleType: h,
                    cinemaId: e.relatedId,
                    date: e.date,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.IndexNewsTop:
                g = new ShareUniversalControl({
                    shareControlStyleType: h,
                    newsId: e.relatedId,
                    title: e.title,
                    fullTitle: e.fullTitle,
                    link: e.link,
                    pic: e.pic,
                    desc: e.desc,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.Blog:
                g = new ShareBlogControl({
                    shareControlStyleType: h,
                    blogId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.ForumTopic:
                g = new ShareForumTopicControl({
                    shareControlStyleType: h,
                    topicId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.Activity:
                g = new ShareActivityControl({
                    shareControlStyleType: h,
                    activityId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.MediaReview:
                g = new ShareMediaReviewControl({
                    shareControlStyleType: h,
                    mediaReviewId: e.relatedId,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    expandMovieId: e.expandMovieId,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.Link:
                g = new ShareLinkControl({
                    shareControlStyleType: h,
                    title: e.title,
                    link: e.link,
                    pic: e.pic,
                    desc: e.desc,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.OffsiteLink:
                g = new ShareOffsiteLinkControl({
                    title: e.title,
                    link: e.link,
                    pic: e.pic,
                    desc: e.desc,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.GoodsDetail:
                g = new ShareGoodsDetailControl({
                    shareControlStyleType: h,
                    title: e.title,
                    link: e.link,
                    pic: e.pic,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            case ShareRelatedObjectTypes.SCMGoodsDetail:
                g = new ShareSCMGoodsDetailControl({
                    shareControlStyleType: h,
                    goodsName: e.title,
                    link: e.link,
                    goodsImageUrl: e.pic,
                    desc: e.desc,
                    shareButton: e.shareButton,
                    shareContainer: e.shareContainer,
                    autoLoad: false,
                    callback: e.callback,
                    shareSuccessCallBack: e.shareSuccessCallBack
                });
                break;
            default:
                break
        }
        return g
    }, initShareControl: function (c) {
        var d = [];
        c.each(function (a) {
            var b;
            var h = ShareControlStyleType.FloatStyle;
            if (a.shareControlStyleType) {
                h = a.shareControlStyleType
            }
            var j = parseInt(a.shareRelatedObjType, 10);
            var k = this;
            var b = k.swtichNewControl(a, h);
            if (b) {
                this.controls.push(b);
                d.push(b)
            }
        }.bind(this));
        return d
    }, destroyControl: function () {
        this.controls.each(function (b) {
            b.close()
        });
        this.controls = null
    }, close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyControl()
        }
    }
});
function EmailIframe(d) {
    var c = document.createElement("iframe");
    c.style.display = "none";
    c.style.width = "0px";
    c.style.height = "0px";
    c.src = d;
    document.body.appendChild(c);
    setTimeout(function () {
        document.body.removeChild(c)
    }, 8000)
}
function GlobalSiteShare(z) {
    if (GlobalUrlValue !== "") {
        z = GlobalUrlValue;
        GlobalUrlValue = ""
    }
    var q = z.indexOf("?");
    if (q > 0) {
        var w = z.substring(q);
        var u = z.substring(0, q);
        var p = [];
        var y = w.substring(1);
        var v = y.split("&");
        for (var s = 0; s < v.length; s++) {
            var x = v[s].indexOf("=");
            if (x == -1) {
                continue
            }
            var j = v[s].substring(0, x);
            var A = v[s].substring(x + 1);
            A = A.replace(/\^PIC\^/g, encodeURIComponent("&"));
            p.push({key: j, value: decodeURIComponent(A)})
        }
        var r = null;
        if (p.length > 0) {
            r = u + "?";
            for (var t = 0; t < p.length; t++) {
                if (t != 0) {
                    r += "&"
                }
                r += p[t].key + "=" + encodeURIComponent(p[t].value)
            }
        }
        if (r != null) {
            z = r
        }
    }
    window.open(z)
}
function ShowQrCode(f) {
    var d = "";
    if (getNewValue) {
        d = encodeURIComponent(getNewValue(location.href))
    } else {
        d = encodeURIComponent(location.href)
    }
    d = siteServiceUrl + "/GetQrCodeHandler.ashx?qrCodeStr=" + d;
    var e = [];
    e.push('<div class="tc">');
    e.push('<h4 class="px18">分享到微信朋友圈</h4>');
    e.push('<p><img width="232" height="232" alt="二维码" src="' + d + '"></p>');
    e.push('<p class="c_666 px14">扫描上方二维码 分享到微信</p>');
    e.push("</div>");
    this.dialog = new Dialog({title: "", content: e.join("")})
}
var loadMiniCart = true;
var TopMenu = Class.create();
Object.extend(TopMenu.prototype, {
    server: {
        getUser: function (b) {
            return Mtime.Component.Ajax.get("Mtime.Service.Pages.ManagerService", "GetUserInfo2", [], b, "/Service/Manager.msi", "get", "10000")
        }, getSuggestions: function (d, c) {
            return Mtime.Component.Ajax.request(siteServiceUrl, "Mtime.Service.Pages.UtilityService", "GetSuggestionKeywords", [d], c, "/service/utility.msi", "get", "20000")
        }, addRegisterDayBonus: function (c, d) {
            Mtime.Component.Ajax.get("Mtime.Service.Pages.ScoreService", "AddRegisterDayBonus", [], c, "/Service/Score.msi", "get", "20000", d)
        }
    },
    MainNavPageType: {NotSet: 0, Home: 1, News: 2, Theater: 3, Community: 4, Movie: 5, Mall: 6},
    options: {
        type: 1,
        mainNavPage: 0,
        actionUrl: siteSearchUrl + "/search/",
        timer: 1,
        showSearchBoxHrefs: ["http://theater.mtime.com", "http://news.mtime.com/pix", "http://i.mtime.com", "http://group.mtime.com", "http://my.mtime.com", "http://piao.mtime.com", "http://mall.mtime.com"],
        pageSearchControlSearchTip: ""
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initialize: function (b) {
        this.setOptions(b);
        this.initializeField();
        this.initializeDOM();
        this.initializeData();
        this.initializeEvent()
    },
    initializeField: function () {
        this.isInitialize = false;
        this.isInitializeManager = false;
        this.type = topMenuValues.mainNavType == "Channel" ? 0 : 1;
        this.mainNavPage = topMenuValues.mainNavPage
    },
    initializeDOM: function () {
        this.topMenuRegion = $("topMenuRegion");
        if (!this.topMenuRegion) {
            this.topMenuRegion = $("topbar")
        }
        if (this.topMenuRegion) {
            this.addDOM()
        }
    },
    destroyDOM: function () {
        this.removeDOM();
        if (this.topMenuRegion) {
            this.topMenuRegion = null
        }
    },
    initializeData: function () {
        if (!this.topMenuRegion) {
            return
        }
        this.topMenuValues = topMenuValues;
        this.server.getUser(function (g) {
            if (g && g.value) {
                var h = g.value;
                this.isLogin = h.userId > 0 ? true : false;
                this.user = {
                    isLogin: this.isLogin,
                    id: h.userId,
                    nickname: h.nickName,
                    homeUrl: h.homeUrl,
                    blogUrl: h.blogUrl,
                    headSrc: h.headSrc,
                    isSaveEmail: h.isSaveEmail,
                    isAutoExit: h.isAutoExit,
                    signupTime: h.signupTime,
                    email: h.email,
                    cityId: h.cityId,
                    cityName: h.cityName,
                    isAnniversary: h.isAnniversary,
                    authorizeUrl: typeof h.authorizeUrl === "undefined" ? "" : h.authorizeUrl,
                    isVirtual: typeof h.isVirtual === "undefined" ? false : h.isVirtual,
                    enableMobileLogin: true,
                    customizeMenus: typeof h.CustomizeMenus === "undefined" ? null : h.CustomizeMenus,
                    headSrc128: typeof h.headSrc128 === "undefined" ? "" : h.headSrc128,
                    shortNickName: typeof h.shortNickName === "undefined" ? "" : h.shortNickName,
                    currentXP: typeof h.currentXP === "undefined" ? "0" : h.currentXP,
                    level: typeof h.level === "undefined" ? "0" : h.level
                };
                var f, e = Globals.getParam("redirectUrl");
                if (e && e.trim().length > 0) {
                    f = e
                } else {
                    f = encodeURIComponent(location.href)
                }
                this.message = {
                    receiveCount: h.receiveCount,
                    promptCount: h.promptCount,
                    noticeCount: h.noticeCount,
                    allCount: h.allCount,
                    unreadCount: h.receiveCount + h.promptCount + h.noticeCount
                };
                this.urls = {
                    mytime: siteMcUrl,
                    signIn: siteUrl + "/member/signin/?redirectUrl=" + f,
                    signUp: siteUrl + "/member/signup/?redirectUrl=" + f,
                    signOut: siteUrl + "/member/signout/?redirectUrl=" + f,
                    siteHelp: siteUrl + "/help/",
                    forgetPasswordUrl: h.forgetPasswordUrl
                };
                this.unreadMsgCount = h.allCount;
                this.render()
            }
        }.bind(this))
    },
    render: function () {
        if (this.mainNavPage !== this.MainNavPageType.Mall && this.mainNavPage !== this.MainNavPageType.Theater && this.bgLink && this.topMenuValues && this.topMenuValues.bgStartDate && this.topMenuValues.bgEndDate) {
            var d = new Date();
            var c = new Date(this.topMenuValues.bgEndDate.getFullYear(), this.topMenuValues.bgEndDate.getMonth(), this.topMenuValues.bgEndDate.getDate(), 23, 59, 59);
            if (this.topMenuValues.bgStartDate < d && d < c) {
                if (this.topMenuValues.bgUrl) {
                    this.bgLink.href = this.topMenuValues.bgUrl;
                    this.bgLink.target = "_blank";
                    this.bgLink.addClassName("linkpoint")
                } else {
                    this.bgLink.href = "javascript:void(0)";
                    this.bgLink.removeClassName("linkpoint")
                }
                if (this.topMenuValues.bgSrc && this.topMenuValues.bgSrc.length > 0) {
                    this.topRegion.style.backgroundImage = "url(" + this.topMenuValues.bgSrc + ")";
                    this.topRegion.style.backgroundPosition = "center top";
                    this.topRegion.style.backgroundRepeat = "no-repeat"
                }
                if (this.topMenuValues.bgHeight > 0) {
                    this.topRegion.style.height = this.topMenuValues.bgHeight + "px"
                }
                if (this.topMenuValues.logoBGColor) {
                    this.logoRegion.style.backgroundColor = this.topMenuValues.logoBGColor
                }
                if (this.topMenuValues.searchTip) {
                    this.pageSearchControlSearchTip = this.topMenuValues.searchTip
                }
            }
        }
        this.initializeControl()
    },
    initializeEvent: function () {
        if (this.closeButton) {
            this.onClickCloseButtonHandler = this.onClickCloseButton.bindAsEventListener(this);
            this.closeButton.observe("click", this.onClickCloseButtonHandler)
        }
        Event.observe(window, "unload", this.close.bind(this))
    },
    destroyEvent: function () {
        if (this.closeButton) {
            this.closeButton.stopObserving("click", this.onClickCloseButtonHandler)
        }
    },
    initializeControl: function () {
        if (!this.cityMenu && this.userCityRegion) {
            this.cityMenu = new CityMenu({container: this.userCityRegion, userInfo: this.user})
        }
        if (!this.messageMenu && this.userMessageRegion) {
            this.messageMenu = new MessageMenu({
                container: this.userMessageRegion,
                message: this.message,
                user: this.user,
                isLogin: this.user.isLogin
            })
        }
        if (!this.userMenu && this.userLoginRegion) {
            this.userMenu = new UserMenu({
                container: this.userLoginRegion.parentNode,
                menu: this.userLoginRegion,
                userInfo: this.user,
                urls: this.urls
            })
        }
        if (typeof PageSearchControl != "undefined") {
            var d = 0;
            if (this.mainNavPage == 3) {
                d = 2
            } else {
                if (this.mainNavPage == 6) {
                    d = 4
                }
            }
            if (this.searchRegion && this.suggestRegion) {
                this.searchMenu = new PageSearchControl({
                    searchRegion: this.searchRegion,
                    suggestRegion: this.suggestRegion,
                    text: this.pageSearchControlSearchTip,
                    typeRegion: this.searchTypeRegion,
                    typeListRegion: this.searchTypeListRegion,
                    searchType: d
                })
            }
        } else {
            $loadJs("/js/2014/PageSearchControl.js", function () {
                var a = 0;
                if (this.mainNavPage == 3) {
                    a = 2
                } else {
                    if (this.mainNavPage == 6) {
                        a = 4
                    }
                }
                if (this.searchRegion && this.suggestRegion) {
                    this.searchMenu = new PageSearchControl({
                        searchRegion: this.searchRegion,
                        suggestRegion: this.suggestRegion,
                        text: this.pageSearchControlSearchTip,
                        typeRegion: this.searchTypeRegion,
                        typeListRegion: this.searchTypeListRegion,
                        searchType: a
                    })
                }
            }.bind(this))
        }
        if (!this.navigationMenu && this.navMenuButton) {
            this.navigationMenu = new TopMenuNavigationMenu({
                menuRegion: this.navMenuButton,
                contentRegion: this.navMenuRegion,
                onOverCallback: function () {
                    if (!this.navMenuButton.hasClassName("currmenuicon")) {
                        this.navMenuButton.addClassName("currmenuicon")
                    }
                }.bind(this),
                onOutCallback: function () {
                    if (this.navMenuButton.hasClassName("currmenuicon")) {
                        this.navMenuButton.removeClassName("currmenuicon")
                    }
                }.bind(this)
            })
        }
        if (typeof ManagerExecuter !== "function") {
            var c = []
        } else {
            manager = new ManagerExecuter({
                managerRegion: this.topMenuRegion,
                user: this.user,
                urls: this.urls,
                customizeMenus: this.user.customizeMenus
            })
        }
        if (typeof loadMiniCart != "undefined" && loadMiniCart) {
            if (!this.miniCart && this.miniCartRegion) {
                this.miniCart = new MiniCart({container: this.miniCartRegion, isLogin: this.user.isLogin})
            }
        }
    },
    addDOM: function () {
        this.searchTypeRegion = Builder.node("div", {className: "selctsearch"}, [Builder.node("strong", ["全部"]), Builder.node("i", [""]), Builder.node("span", ["|"])]);
        this.searchTypeListRegion = Builder.node("div", {
            className: "selectshow",
            style: "display:none;"
        }, [Builder.node("ul", [Builder.node("li", {searchtype: "0"}, ["全部"]), Builder.node("li", {searchtype: "1"}, ["电影"]), Builder.node("li", {searchtype: "3"}, ["影人"]), Builder.node("li", {searchtype: "2"}, ["影院"]), Builder.node("li", {searchtype: "4"}, ["商品"])])]);
        if (this.type == 0) {
            this.searchRegion = Builder.node("div", {className: "topsearch"}, [Builder.node("div", {className: "searchshadow"}, [this.searchTypeRegion, this.searchTypeListRegion, Builder.node("input", {
                type: "text",
                className: "text",
                value: ""
            }), Builder.node("input", {type: "button", className: "button"})])])
        } else {
            this.searchRegion = Builder.node("div", {className: "topsearch"}, [this.searchTypeRegion, this.searchTypeListRegion, Builder.node("input", {
                type: "text",
                className: "text",
                value: ""
            }), Builder.node("input", {type: "button", className: "button"})])
        }
        this.suggestRegion = Builder.node("div", {className: "showsearch", style: "display:none;"});
        this.userLoginRegion = Builder.node("div", {className: "loginbox", style: "display:none;"});
        this.userMessageRegion = Builder.node("div", {
            className: "emailbox",
            style: "display:none;"
        }, [Builder.node("i", {className: "ico_email"})]);
        this.userCityRegion = Builder.node("div", {className: "citybox", style: "display:none;"});
        this.bgLink = Builder.node("a", {href: "javascript:void(0)", className: "searchlink"}, []);
        this.miniCartRegion = Builder.node("div", {
            className: "shopbox onlyshop",
            onclick: "headTrackClick('M14_HeadNav_MiniCart_Region')",
            style: "display:none;"
        });
        if (this.type === 0) {
            this.addChannelDOM()
        } else {
            this.addDetailDOM()
        }
    },
    removeDOM: function () {
        if (this.type === 0) {
        } else {
        }
    },
    addChannelDOM: function () {
        this.topRegion = Builder.node("div", {className: "bartop"}, [Builder.node("div", {className: "topmid"}, [this.bgLink, this.searchRegion, this.suggestRegion])]);
        this.logoRegion = Builder.node("h1", {
            className: "mtimelogo",
            onclick: "headTrackClick('M14_HeadNav_MiddleNav')"
        }, [Builder.node("a", {title: "Mtime时光网", href: "http://www.mtime.com"}, ["Mtime时光网"])]);
        this.topMenuRegion.appendChild(this.topRegion);
        var h = Builder.node("div", {className: "topline bigtopline"});
        this.topMenuRegion.appendChild(h);
        var e = Builder.node("ul", {className: "navbar"});
        e.appendChild(Builder.node("li", {className: this.mainNavPage == this.MainNavPageType.Home ? "curr" : ""}, [Builder.node("a", {
            href: "http://www.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Index')"
        }, [Builder.node("em", ["首页"]), Builder.node("span", ["首页"])])]));
        e.appendChild(Builder.node("li", {className: this.mainNavPage == this.MainNavPageType.News ? "curr" : ""}, [Builder.node("a", {
            href: "http://news.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_News')"
        }, [Builder.node("em", ["新闻"]), Builder.node("span", ["新闻"])])]));
        e.appendChild(Builder.node("li", {className: this.mainNavPage == this.MainNavPageType.Theater ? "curr" : ""}, [Builder.node("a", {
            href: "http://theater.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Theater')"
        }, [Builder.node("em", ["影院"]), Builder.node("span", ["影院"])])]));
        if (typeof loadMiniCart != "undefined" && loadMiniCart) {
            e.appendChild(Builder.node("li", {className: this.mainNavPage == this.MainNavPageType.Mall ? "curr" : ""}, [Builder.node("a", {
                href: "http://mall.mtime.com/",
                onclick: "headTrackClick('M14_HeadNav_LeftNav_Mall')"
            }, [Builder.node("em", ["商城"]), Builder.node("span", ["商城"]), Builder.node("i", {className: "i_new"})])]))
        }
        e.appendChild(Builder.node("li", {className: this.mainNavPage == this.MainNavPageType.Community ? "curr" : ""}, [Builder.node("a", {
            href: "http://www.mtime.com/community/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Community')"
        }, [Builder.node("em", ["社区"]), Builder.node("span", ["社区"])])]));
        e.appendChild(Builder.node("li", {className: this.mainNavPage == this.MainNavPageType.Movie ? "curr" : ""}, [Builder.node("a", {
            href: "http://movie.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Movie')"
        }, [Builder.node("em", ["发现"]), Builder.node("span", ["发现"])])]));
        var f = Builder.node("div", {className: "userbar"});
        if (typeof loadMiniCart != "undefined" && loadMiniCart) {
            f.appendChild(this.miniCartRegion)
        }
        f.appendChild(this.userMessageRegion);
        f.appendChild(this.userLoginRegion);
        f.appendChild(this.userCityRegion);
        var g = Builder.node("div", {className: "menubar"}, [Builder.node("div", {className: "menubg"}, [Builder.node("div", {className: "menumid clearfix"}, [this.logoRegion, e, f])])]);
        this.topMenuRegion.appendChild(g)
    },
    addDetailDOM: function () {
        var h = Builder.node("div", {className: "topline"});
        this.topMenuRegion.appendChild(h);
        this.logoRegion = Builder.node("h1", {
            className: "mtimelogo",
            onclick: "headTrackClick('M14_HeadNav_MiddleNav')"
        }, [Builder.node("a", {href: "http://www.mtime.com", title: "Mtime时光网"}, ["Mtime时光网"])]);
        this.navMenuButton = Builder.node("a", {href: "http://www.mtime.com/", className: "menuicon", title: "导航"}, []);
        var e = Builder.node("div", {className: "userbar"});
        if (typeof loadMiniCart != "undefined" && loadMiniCart) {
            e.appendChild(this.miniCartRegion)
        }
        e.appendChild(this.userMessageRegion);
        e.appendChild(this.userLoginRegion);
        e.appendChild(this.userCityRegion);
        this.navMenuRegion = Builder.node("div", {className: "menupop navpop", style: "display:none;"});
        var g = Builder.node("ul");
        g.appendChild(Builder.node("li", [Builder.node("a", {
            href: "http://www.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Index')"
        }, [Builder.node("i", {className: "ico_n_index"}), "首页"])]));
        g.appendChild(Builder.node("li", [Builder.node("a", {
            href: "http://news.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_News')"
        }, [Builder.node("i", {className: "ico_n_news"}), "新闻"])]));
        g.appendChild(Builder.node("li", [Builder.node("a", {
            href: "http://theater.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Theater')"
        }, [Builder.node("i", {className: "ico_n_ticket"}), "影院"])]));
        if (typeof loadMiniCart != "undefined" && loadMiniCart) {
            g.appendChild(Builder.node("li", [Builder.node("a", {
                href: "http://mall.mtime.com/",
                onclick: "headTrackClick('M14_HeadNav_LeftNav_Mall')"
            }, [Builder.node("i", {className: "ico_n_mall"}), "商城"])]))
        }
        g.appendChild(Builder.node("li", [Builder.node("a", {
            href: "http://www.mtime.com/community/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Community')"
        }, [Builder.node("i", {className: "ico_n_bbs"}), "社区"])]));
        g.appendChild(Builder.node("li", [Builder.node("a", {
            href: "http://movie.mtime.com/",
            onclick: "headTrackClick('M14_HeadNav_LeftNav_Movie')"
        }, [Builder.node("i", {className: "ico_n_find"}), "发现"])]));
        this.navMenuRegion.appendChild(g);
        var f = Builder.node("div", {className: "menubar"}, [Builder.node("div", {className: "menubg"}, [Builder.node("div", {className: "menumid clearfix"}, [this.logoRegion, Builder.node("div", {className: "navsearch clearfix"}, [this.navMenuButton, this.searchRegion, this.suggestRegion, this.navMenuRegion]), e])])]);
        this.topMenuRegion.appendChild(f)
    },
    removeChannelDOM: function () {
    },
    closed: false,
    close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyEvent();
            this.destroyDOM()
        }
    }
});
var MessageMenu = Class.create();
Object.extend(MessageMenu.prototype, {
    server: {
        getUnreadMessages: function (b) {
            return Mtime.Component.Ajax.get("Mtime.Service.Pages.MessageService", "GetUnreadMessages", [], b, "/Service/Message.msi", "get", "20000")
        }, addRegisterDayBonus: function (c, d) {
            return Mtime.Component.Ajax.get("Mtime.Service.Pages.ScoreService", "AddRegisterDayBonus", [], c, "/Service/Score.msi", "get", "20000", d)
        }, clearAllUnreadCount: function (b) {
            return Mtime.Component.Ajax.get("Mtime.Service.Pages.MessageService", "ClearAllUnreadCount", [], b, "/Service/Message.msi", "get", "20000")
        }
    }, options: {container: null, user: null, isLogin: false}, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeEvent();
        this.render()
    }, initializeEvent: function () {
        Event.observe(this.container, "click", this.onClickContainer.bindAsEventListener(this))
    }, render: function () {
        this.getUnReadMessage();
        if (this.isLogin) {
            this.container.show()
        }
    }, onClickContainer: function (g) {
        var f = Event.element(g);
        if (f.hasClassName("close_btn")) {
            this.container.down('[_type="message"]') && this.server.clearAllUnreadCount(null);
            this.container.down(".m_message_notice").remove();
            var h = this.container.down(".m_message em");
            if (h) {
                h.removeClassName("newmsg")
            }
            var k = getCookie("virtualuser");
            if (this.user.isVirtual && (typeof(k) === "undefined" || k !== "virtualuser")) {
                setCookie("virtualuser", "virtualuser")
            }
        } else {
            if (!this.user.isAnniversary) {
                var j = siteMcUrl + "/message/usermessage/";
                $redirect(j)
            }
        }
    }, getUnReadMessage: function () {
        this.server.getUnreadMessages(function (y) {
            var q = null;
            var s = this.user.isAnniversary;
            if (s) {
                q = Builder.build('<p _type="anniversary"><a href="#" onclick="return false;">注册周年积分奖励，点击领取</a></p>')
            }
            var t = this.user.isVirtual;
            var z = t;
            if (t) {
                var B = getCookie("virtualuser");
                if (B && B === "virtualuser") {
                    z = false
                }
            }
            var u = this.container.down(".m_message_notice");
            u && u.remove();
            u = null;
            if ((!y || !y.value) && !q && !z) {
                return
            } else {
                if (!u) {
                    u = Builder.build('<div class="m_message_notice tl"><em></em><span class="close_btn fr">×</span></div>');
                    this.container.appendChild(u)
                }
                var A = u.down('[_type="message"]');
                A && A.remove();
                if (q) {
                    u.appendChild(q);
                    var x = this.user.nickname;
                    var r = this.closeMsg.bind(this);
                    var w = this.container;
                    var p = this.server.addRegisterDayBonus;
                    Event.observe(q, "click", function () {
                        var a = new Dialog({
                            title: "注册周年积分奖励!",
                            windowClassName: "w420",
                            content: "<div>亲爱的" + x + "，感谢你对时光网一年的支持，时光网有你更精彩。</div>",
                            buttonRegionClass: "tip_btnbox",
                            buttons: [{
                                title: "领取", buttonStyle: "btn_blue mr12", callback: function () {
                                    var d = null;
                                    if (d = w.down('[_type="message"]')) {
                                        var b = w.down('[_type="anniversary"]');
                                        b && b.remove()
                                    } else {
                                        w.down(".m_message_notice").remove();
                                        var c = w.down(".m_message em");
                                        if (c) {
                                            c.removeClassName("newmsg")
                                        }
                                    }
                                    p();
                                    a.close()
                                }
                            }]
                        })
                    }.bind(this))
                }
                if (z && this.user.authorizeUrl) {
                    var C = Builder.build(String.format('<p>完善信息后，服务更安全 <a href="{0}">立即设置</a></p>', this.user.authorizeUrl));
                    u.appendChild(C)
                }
                if (y && y.value) {
                    u.appendChild(Builder.build(y.value));
                    this.container.addClassName("curremail")
                }
            }
            var v = this.container.down(".m_message em");
            if (typeof v != "undefined") {
                if (this.container.down(".m_message_notice")) {
                    if (!v.hasClassName("newmsg")) {
                        v.addClassName("newmsg")
                    }
                } else {
                    v.removeClassName("newmsg")
                }
            }
        }.bind(this))
    }, closeMsg: function (d) {
        var e = Event.element(d);
        if (e.hasClassName("close_btn")) {
            this.container.down('[_type="message"]') && this.server.clearAllUnreadCount(null);
            this.container.down(".m_message_notice").remove();
            var f = this.container.down(".m_message em");
            f.removeClassName("newmsg")
        }
    }
});
var UserMenu = Class.create();
Object.extend(UserMenu.prototype, {
    server: {
        getUser: function (b) {
            return Mtime.Component.Ajax.get("Mtime.Community.Controls.CommunityPages.ManagerService", "GetUserInfo2", [], b, "/utility/managerservice.m", "get", "10000")
        }, signIn: function (j, m, l, k, o, n, h) {
            return Mtime.Component.Ajax.get("Mtime.Community.Controls.CommunityPages.ManagerService", "SignIn", [j, m, l, k, o, n], h, "/utility/managerservice.m", "get", "10000")
        }, refreshVcode: function (b) {
            return Mtime.Component.Ajax.get("Mtime.Community.Controls.CommunityPages.ManagerService", "RefreshVcode", [], b, "/utility/managerservice.m", "get", "10000")
        }, getXpAndLevel: function (d, c) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "GetUserXpAndLevel", [d], c, "/Member.api", "get", "60000")
        }, getCouponsForLogin: function (b) {
            return Mtime.Component.Ajax.get("Mtime.Service.Pages.ManagerService", "GetCouponsForLogin", [], b, "/Service/Manager.msi", "get", "10000", "")
        }
    },
    options: {
        container: null,
        menu: null,
        userInfo: null,
        urls: null,
        onSignInCallback: null,
        hasLoadXpAndLevel: false
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initialize: function (b) {
        this.setOptions(b);
        this.initializeDOM();
        this.render();
        this.initializeEvent()
    },
    initializeDOM: function () {
        this.loginNameText = Builder.node("input", {type: "text", value: "输入电子邮箱地址", className: "tip"});
        this.passwordText = Builder.node("input", {type: "password", value: "", className: "tip"});
        this.autoLoginCheckBox = Builder.node("i", {className: "ico_m_check"}, [""]);
        this.signInButton = Builder.node("input", {type: "button", value: "登录", className: "btn_login"});
        this.registerButton = Builder.node("input", {type: "button", value: "成为会员", className: "btn_regist"});
        this.vcodeText = Builder.node("input", {type: "text", value: "输入验证码", className: "tip"});
        this.refreshVcodeButton = Builder.node("a", {href: "#"}, ["刷新"])
    },
    initializeEvent: function () {
        Event.observe(this.menu, "click", this.onClickMenu.bind(this));
        if (this.userInfo.isLogin) {
            this.initializeLoginEvent()
        } else {
            this.initializeUnloginEvent();
            this.onClickHandler = this.onClick.bindAsEventListener(this);
            Event.observe(document.body, "click", this.onClickHandler)
        }
    },
    onClick: function (f) {
        var g = Event.pointerX(f);
        var h = Event.pointerY(f);
        var e = Event.element(f);
        if (this.unloginRegion && this.menu !== e.up("div.loginbox") && this.unloginRegion !== e.up("div.menupop") && !Position.within(this.container, g, h) && !Position.within(this.unloginRegion, g, h)) {
            if (!this.userInfo.isLogin) {
                if (this.unloginRegion.visible()) {
                    this.menu.removeClassName("currlogin");
                    this.unloginRegion.hide()
                }
            }
        }
    },
    initializeLoginEvent: function () {
    },
    initializeUnloginEvent: function () {
        new Textbox({text: this.loginNameText, watermarkText: "输入电子邮箱地址"});
        new Textbox({text: this.passwordText, watermarkText: ""});
        new Textbox({text: this.vcodeText, watermarkText: "输入验证码"});
        this.autoLoginCheckBox.observe("click", this.onClickAutoLoginCheckBox.bind(this));
        this.signInButton.observe("click", this.onClickSignInButton.bind(this));
        this.registerButton.observe("click", this.onClickRegisterButton.bind(this));
        this.checkAccountHandler = this.checkAccount.bind(this);
        Event.observe(this.loginNameText, "change", this.checkAccountHandler);
        this.checkPasswordHandler = this.checkPassword.bind(this);
        Event.observe(this.passwordText, "change", this.checkPasswordHandler);
        this.refreshVcodeHandler = this.refreshVcode.bind(this);
        Event.observe(this.refreshVcodeButton, "click", this.refreshVcodeHandler);
        document.onkeydown = function () {
            var b = window.event;
            if (typeof b !== "undefined" && b.keyCode === 13) {
                this.onClickSignInButton()
            }
        }.bind(this)
    },
    render: function () {
        if (this.userInfo.isLogin) {
            var d = Builder.node("span", {className: "logintxt"}, [this.userInfo.nickname]);
            var f = Builder.node("span", {className: "loginpic"}, [Builder.node("img", {
                height: "27",
                width: "27",
                src: this.userInfo.headSrc
            })]);
            this.menu.appendChild(d);
            this.menu.appendChild(f);
            this.loginRegion = this.buildLoginDOM();
            this.loginRegion.hide();
            this.container.appendChild(this.loginRegion);
            if (!this.userInfo.isAutoExit) {
                this.autoLoginCheckBox.addClassName("ico_m_currcheck")
            }
            this.navigationMenu = new TopMenuNavigationMenu({
                menuRegion: this.menu,
                contentRegion: this.loginRegion,
                onOverCallback: this.onOverCallback.bind(this),
                onOutCallback: this.onOutCallback.bind(this)
            })
        } else {
            var d = Builder.node("span", {className: "logintxt"}, ["请登录"]);
            var f = Builder.node("span", {className: "loginpic"}, [Builder.node("i", {className: "ico_user"})]);
            this.menu.appendChild(d);
            this.menu.appendChild(f);
            this.unloginRegion = this.buildUnloginDOM();
            var e = Builder.build('<dl class="otherlogin"><dt>使用第三方登录</dt><dd style="margin-left: 42px;"><a href="http://passport.mtime.com/unitelogin/dispatch/weibo/web/" title="新浪" class="o_sina"></a><a href="http://passport.mtime.com/unitelogin/dispatch/qq/web/" title="QQ" class="o_qq"></a></dd></dl>');
            this.unloginRegion.appendChild(e);
            this.unloginRegion.hide();
            this.container.appendChild(this.unloginRegion);
            this.loginNameText.value = this.userInfo.email
        }
        this.menu.style.display = "block"
    },
    showVcodeRegion: function () {
        $(this.vcodeText.parentNode).show();
        if (!this.vcodeImageRegion) {
            this.server.refreshVcode(function (b) {
                if (b && b.value && b.value.id) {
                    this.vcodeImageRegion = this.buildVcodeImageDOM(b.value.src);
                    this.vcodeText.parentNode.parentNode.appendChild(this.vcodeImageRegion);
                    this.vcodeId = b.value.id;
                    this.vcodeImage = this.vcodeImageRegion.down().down();
                    this.vcodeImageRegion.show()
                }
            }.bind(this))
        } else {
            this.refreshVcode();
            this.vcodeImageRegion.show()
        }
    },
    hideVcodeRegion: function () {
        if (this.vcodeText) {
            $(this.vcodeText.parentNode).hide()
        }
        if (this.vcodeImageRegion) {
            this.vcodeImageRegion.hide()
        }
    },
    onOverCallback: function () {
        if (!this.menu.hasClassName("currlogin")) {
            this.menu.addClassName("currlogin");
            if (this.userInfo.isLogin && !this.hasLoadXpAndLevel) {
                this.server.getXpAndLevel(this.userInfo.id, function (b) {
                    if (b && b.value) {
                        $("topMenuLevelContainer").innerHTML = "M" + b.value.level;
                        this.hasLoadXpAndLevel = true
                    }
                    this.server.getCouponsForLogin(function (a) {
                        if (a && a.value) {
                            if (a.value.totalCount == 0) {
                                $("myCouponsCount").innerHTML = "我的优惠券"
                            } else {
                                $("myCouponsCount").innerHTML = "我的优惠券(" + a.value.totalCount + ")"
                            }
                            if (a.value.mtimeCardCount == 0) {
                                $("myMtimeCardCount").innerHTML = "我的礼品卡"
                            } else {
                                $("myMtimeCardCount").innerHTML = "我的礼品卡(" + a.value.mtimeCardCount + ")"
                            }
                            if (a.value.willEndCount > 0) {
                                $("couponWillEndContainer").show();
                                $("urlEcommerceOrMovie").href = a.value.urlEcommerceOrMovie;
                                $("willEndCount").innerHTML = a.value.willEndCount
                            } else {
                                $("couponWillEndContainer").hide()
                            }
                        }
                    }.bind(this))
                }.bind(this))
            }
        }
    },
    onOutCallback: function () {
        if (this.menu.hasClassName("currlogin")) {
            this.menu.removeClassName("currlogin")
        }
    },
    isShowing: function () {
        return this.loginRegion.style.display != "none"
    },
    onClickMenu: function () {
        if (this.userInfo.isLogin) {
            if (this.isShowing()) {
                this.loginRegion.hide();
                this.onOutCallback()
            } else {
                this.loginRegion.show();
                this.onOverCallback()
            }
        } else {
            if (this.unloginRegion.visible()) {
                this.menu.removeClassName("currlogin");
                this.unloginRegion.hide()
            } else {
                this.unloginRegion.show();
                this.menu.addClassName("currlogin")
            }
        }
    },
    onClickRegisterButton: function () {
        if (typeof sitePassportUrl === "undefined") {
            sitePassportUrl = "http://passport.mtime.com"
        }
        var b = sitePassportUrl + "/member/signin/?redirectUrl=" + encodeURIComponent(location.href);
        location.href = b
    },
    onClickVcodeText: function () {
        if (!this.vcodeImageRegion) {
            this.server.refreshVcode(function (b) {
                if (b && b.value && b.value.id) {
                    this.vcodeImageRegion = this.buildVcodeImageDOM(b.value.src);
                    this.vcodeText.parentNode.parentNode.appendChild(this.vcodeImageRegion);
                    this.vcodeId = b.value.id;
                    this.vcodeImage = this.vcodeImageRegion.down().down()
                }
            }.bind(this))
        }
    },
    refreshVcode: function (b) {
        this.server.refreshVcode(function (a) {
            if (a && a.value && a.value.id) {
                this.vcodeText.value = "";
                this.vcodeText.focus();
                this.vcodeId = a.value.id;
                this.vcodeImage.src = a.value.src
            }
        }.bind(this))
    },
    checkAccount: function () {
        var c = this.loginNameText.value.trim(), d = true;
        if (this.userInfo.enableMobileLogin) {
            if (!Validator.Email.test(c) && !Validator.MobilePhone.test(c)) {
                $(this.loginNameText.parentNode).addClassName("false");
                d = false
            } else {
                $(this.loginNameText.parentNode).removeClassName("false")
            }
        } else {
            if (!Validator.Email.test(c)) {
                $(this.loginNameText.parentNode).addClassName("false");
                d = false
            } else {
                $(this.loginNameText.parentNode).removeClassName("false")
            }
        }
        return d
    },
    checkPassword: function () {
        var c = this.passwordText.value, d = true;
        if (!Validator.Password.test(c)) {
            $(this.passwordText.parentNode).addClassName("false");
            d = false
        } else {
            $(this.passwordText.parentNode).removeClassName("false")
        }
        return d
    },
    checkVcode: function () {
        var d = this.vcodeText.value.trim(), c = true;
        if (!d) {
            $(this.vcodeText.parentNode).addClassName("false");
            c = false
        } else {
            $(this.vcodeText.parentNode).removeClassName("false")
        }
        return c
    },
    onClickSignInButton: function () {
        var e = this.loginNameText.value;
        var f = $encodePassword(this.passwordText.value);
        var d = !this.autoLoginCheckBox.hasClassName("ico_m_currcheck");
        if (this.checkAccount() && this.checkPassword() && (!this.vcodeId || this.checkVcode()) && !this.lock) {
            this.lock = true;
            this.server.signIn(e, f, true, d, this.vcodeId || "", this.vcodeId ? this.vcodeText.value : "", function (a) {
                this.lock = false;
                if (a && a.value) {
                    if (a.value.success) {
                        this.callback && this.callback(a.value);
                        location.reload()
                    } else {
                        if (a.value.isShieldStatus) {
                            this.showSecurityRiskDialog(a.value.shieldTip)
                        } else {
                            if (a.value.needResetPassword) {
                                this.showResetPasswordDialog(a.value.resetPasswordUrl)
                            } else {
                                if (a.value.redirect) {
                                    $redirect(a.value.message)
                                } else {
                                    if (a.value.isRobot) {
                                        this.showVcodeRegion();
                                        if (!a.value.vcodeValid) {
                                            $(this.vcodeText.parentNode).addClassName("false")
                                        }
                                        return
                                    } else {
                                        this.hideVcodeRegion();
                                        this.vcodeId = null
                                    }
                                    $(this.loginNameText.parentNode).addClassName("false");
                                    $(this.passwordText.parentNode).addClassName("false")
                                }
                            }
                        }
                    }
                }
            }.bind(this))
        }
    },
    showSecurityRiskDialog: function (b) {
        this.securityRiskDialog = new Dialog({
            title: "提示",
            windowClassName: "w375",
            content: '<div class="yahei px14 tc bold">' + b + "</div>",
            buttonRegionClass: "tip_btnbox",
            buttons: [{
                title: "确定",
                keyCode: Event.KEY_RETURN,
                buttonStyle: "t_btn_blue_i bold",
                callback: function (a) {
                    a.close()
                }.bind(this)
            }]
        })
    },
    showResetPasswordDialog: function (b) {
        this.needResetPwdDialog = new Dialog({
            title: "很抱歉，账户已被冻结",
            windowClassName: "w375",
            content: '<div class="yahei px14 tc bold">由于长时间未登录，账户已被临时冻结。<br/>请点击按钮，重新邮件激活。 </div>',
            buttonRegionClass: "tip_btnbox",
            buttons: [{
                title: "立即激活",
                keyCode: Event.KEY_RETURN,
                buttonStyle: "t_btn_blue_i bold",
                callback: function (a) {
                    location.href = b
                }.bind(this)
            }]
        })
    },
    onClickAutoLoginCheckBox: function () {
        if (this.autoLoginCheckBox.hasClassName("ico_m_currcheck")) {
            this.autoLoginCheckBox.removeClassName("ico_m_currcheck")
        } else {
            this.autoLoginCheckBox.addClassName("ico_m_currcheck")
        }
    },
    buildLoginDOM: function () {
        var j = Builder.node("div", {className: "menupop loginpop"});
        var f = Builder.node("div", {className: "my_name"});
        var g = Builder.node("dl", {className: "my_business clearfix"});
        var h = Builder.node("div", {className: "my_couptip", id: "couponWillEndContainer"});
        var k = Builder.node("ul", {className: "loginexit"});
        f.appendChild(Builder.node("a", {
            className: "my_pic __r_c_",
            href: siteMcUrl,
            pan: "M14_HeadNav_LoginBox_UserPic"
        }, [Builder.node("img", {src: this.userInfo.headSrc128, width: "96px", height: "96px"})]));
        f.appendChild(Builder.node("dl", [Builder.node("dt", [Builder.node("a", {
            href: siteMcUrl,
            pan: "M14_HeadNav_LoginBox_UserName",
            className: "__r_c_"
        }, [this.userInfo.shortNickName]), Builder.node("a", {
            href: "http://vip.mtime.com/myprivilege",
            pan: "M14_HeadNav_LoginBox_VipsuserLevel",
            className: "__r_c_"
        }, Builder.node("span", {id: "topMenuLevelContainer"}, ["M0"]))]), Builder.node("dd", [Builder.node("a", {
            href: "http://vip.mtime.com/myprivilege/",
            pan: "M14_HeadNav_LoginBox_VipsuserPrivilege",
            className: "__r_c_"
        }, ["我的特权"]), Builder.node("i", {className: "c_a5 mlr5"}, ["|"]), Builder.node("a", {
            href: "http://vip.mtime.com/gift/",
            pan: "M14_HeadNav_LoginBox_VipsuserPackage",
            className: "__r_c_"
        }, ["会员礼包"])])]));
        g.appendChild(Builder.node("dd", [Builder.node("a", {
            href: siteMcUrl + "/account/",
            pan: "M14_HeadNav_LoginBox_MyMoiveTicket",
            className: "__r_c_"
        }, ["我的电影票"])]));
        g.appendChild(Builder.node("dd", [Builder.node("a", {
            href: siteMcUrl + "/account/ecommerce/orderList/",
            pan: "M14_HeadNav_LoginBox_MyShop",
            className: "__r_c_"
        }, ["我的商品"])]));
        g.appendChild(Builder.node("dd", [Builder.node("a", {
            id: "myCouponsCount",
            href: siteMcUrl + "/account/voucher/",
            pan: "M14_HeadNav_LoginBox_MyVoucher",
            className: "__r_c_"
        }, ["我的优惠券"])]));
        g.appendChild(Builder.node("dd", [Builder.node("a", {
            id: "myMtimeCardCount",
            href: siteMcUrl + "/account/moviecard/",
            pan: "M14_HeadNav_LoginBox_MyMoiveCard",
            className: "__r_c_"
        }, ["我的礼品卡"])]));
        h.appendChild(Builder.node("i"));
        h.appendChild(Builder.node("p", {
            pan: "M14_HeadNav_LoginBox_WillEndCount",
            className: "__r_c_"
        }, ["您有", Builder.node("b", {id: "willEndCount"}, ["0"]), "张优惠券即将过期 ", Builder.node("a", {
            href: siteMcUrl + "/account/voucher/ecommerce/",
            id: "urlEcommerceOrMovie"
        }, ["点击查看"])]));
        k.appendChild(Builder.node("li", {
            pan: "M14_HeadNav_LoginBox_MyMtime",
            className: "__r_c_"
        }, [Builder.node("p", ["我的：", Builder.node("a", {href: siteMcUrl + "/app/blog/"}, ["博客"]), Builder.node("em", ["|"]), Builder.node("a", {href: siteMcUrl + "/app/group/"}, ["群组"]), Builder.node("em", ["|"]), Builder.node("a", {href: siteMcUrl + "/movie/"}, ["电影"]), Builder.node("em", ["|"]), Builder.node("a", {href: siteMcUrl + "/favorite/"}, ["收藏"])]), Builder.node("p", ["游戏：", Builder.node("a", {href: siteMcUrl + "/app/card/"}, ["卡片大富翁"]), Builder.node("em", ["|"]), Builder.node("a", {href: siteMcUrl + "/app/quiz/"}, ["猜电影"])])]));
        k.appendChild(Builder.node("li", [Builder.node("a", {
            href: this.urls.signOut,
            className: "btn_exit __r_c_",
            pan: "M14_HeadNav_LoginBox_Exit"
        }, ["登出"]), Builder.node("a", {
            href: siteMcUrl + "/profile/basic/",
            className: "__r_c_",
            pan: "M14_HeadNav_LoginBox_Profile"
        }, ["修改资料"]), Builder.node("em", ["|"]), Builder.node("a", {
            href: siteMcUrl + "/profile/password/",
            className: "__r_c_",
            pan: "M14_HeadNav_LoginBox_Password"
        }, ["密码"])]));
        j.appendChild(f);
        j.appendChild(g);
        j.appendChild(h);
        j.appendChild(k);
        return j
    },
    buildUnloginDOM: function () {
        var b = Builder.node("div", {className: "menupop loginpop"}, [Builder.node("dl", {className: "logintool"}, [Builder.node("dt", ["为爱电影的人"]), Builder.node("dd", {className: "userlogin"}, [Builder.node("ul", [Builder.node("li", {className: ""}, [Builder.node("label", {className: "name"}, ["用户名："]), this.loginNameText, Builder.node("i", {className: "ico_false"}, [])]), Builder.node("li", {className: ""}, [Builder.node("label", {className: "password"}, ["输入密码："]), this.passwordText, Builder.node("i", {className: "ico_false"}, [])]), Builder.node("li", {
            className: "",
            style: "display:none"
        }, [Builder.node("label", {className: "code"}, ["输入验证码："]), this.vcodeText, Builder.node("i", {className: "ico_false"}, [])])]), Builder.node("p", {className: "usertip"}, [Builder.node("a", {
            href: siteMcUrl + "/member/forget_password/",
            target: "_blank",
            className: "fr"
        }, ["忘记密码"]), this.autoLoginCheckBox, "下次自动登录"]), Builder.node("div", {className: "btn"}, [this.signInButton, this.registerButton])])])]);
        return b
    },
    buildVcodeImageDOM: function (c) {
        var d = Builder.node("li", {className: "codetxt"}, [Builder.node("span", {id: "img_vcode"}, [Builder.node("img", {
            width: "90",
            height: "45",
            src: c,
            style: "cursor:pointer",
            className: "v_m mr6"
        })]), this.refreshVcodeButton]);
        return d
    }
});
var CityMenu = Class.create();
Object.extend(CityMenu.prototype, {
    server: {
        getData: function (b) {
            if (typeof threaterListBoxData !== "undefined" && threaterListBoxData.locations) {
                b && b()
            } else {
                $loadJs("/Utility/Data/TheaterListBoxData.m", b)
            }
        }, getTheaterCityHomeURL: function (c, d) {
            if (typeof theaterService === "undefined") {
                return Mtime.Component.Ajax.callBack("Mtime.Showtime.Pages.ShowtimeService", "GetTheaterCityHomeURL", [c], d, "/service/showtime.ms", "get", "25000")
            } else {
                return Mtime.Component.Ajax.request(theaterService, "Mtime.Showtime.Pages.ShowtimeService", "GetTheaterCityHomeURL", [c], d, "/service/showtime.ms", "get", "25000")
            }
        }
    },
    options: {
        userInfo: null,
        items: [],
        hotSearchItems: [290, 292, 293, 365, 974, 366, 561, 880, 791],
        HOTROWSIZE: 10,
        order: "热门",
        container: "",
        optionTS: "<li><a value=#{Id}>#{NameCn}</a></li>",
        content: {
            regRxpAG: ["A", "B", "C", "D", "E", "F", "G"],
            regRxpHL: ["H", "I", "J", "K", "L"],
            regRxpMT: ["M", "N", "O", "P", "Q", "R", "S", "T"],
            regRxpWZ: ["U", "V", "W", "X", "Y", "Z"]
        }
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initialize: function (b) {
        this.setOptions(b);
        this.optionTemplate = new Template(this.optionTS);
        this.initializeDOM();
        this.initializeEvent();
        this.getItems()
    },
    initializeDOM: function () {
        this.headPanel = $(this.container);
        this.closeNode = Builder.node("span", {className: "btn_close fr"}, ["关闭"]);
        this.contentBox = Builder.node("div");
        this.searchCityText = Builder.node("input", {type: "text", className: "text"});
        this.tab = Builder.node("span", {className: "cityletter"}, [Builder.node("a", {
            href: "#",
            onclick: "return false;",
            className: "on",
            _v: "热门"
        }, ["热门", Builder.node("b", [" "])]), Builder.node("a", {
            href: "#",
            onclick: "return false;",
            _v: "A-G"
        }, ["A-G", Builder.node("b", [" "])]), Builder.node("a", {
            href: "#",
            onclick: "return false;",
            _v: "H-L"
        }, ["H-L", Builder.node("b", [" "])]), Builder.node("a", {
            href: "#",
            onclick: "return false;",
            _v: "M-T"
        }, ["M-T", Builder.node("b", [" "])]), Builder.node("a", {
            href: "#",
            onclick: "return false;",
            _v: "W-Z"
        }, ["W-Z", Builder.node("b", [" "])])]);
        this.span = Builder.node("span", {className: "fl w30 bold mt5"});
        this.bodyPanel = $(Builder.node("div", {
            id: "showtime_tip",
            className: "showtime_tip",
            style: "display:none;width: 560px; position: absolute; top: 35px; right: 0px;"
        }, [Builder.node("div", {className: "share_tit clearfix"}, [this.closeNode, Builder.node("div", {className: "citysearch"}, [this.searchCityText]), this.tab]), Builder.node("div", {className: "p15"}, [this.contentBox])]));
        this.headPanel.appendChild(Builder.node("span", {className: "currcity"}, ["我在：" + this.userInfo.cityName]));
        this.headPanel.appendChild(Builder.node("i", {className: "ico_city"}));
        this.headPanel.parentNode.insertBefore(this.bodyPanel, this.headPanel);
        var c = this.tab.childNodes;
        for (var d = 0; d < c.length; d++) {
            Event.observe(c[d], "click", function (e) {
                var a = e.target || e.srcElement;
                if (a.innerHTML !== this.order) {
                    $A(this.tab.childNodes).each(function (f) {
                        f.className = ""
                    });
                    a.className = "on";
                    var b = a.readAttribute("_v");
                    this.order = b;
                    this.reload(b)
                }
            }.bind(this))
        }
        this.headPanel.style.display = "block"
    },
    initializeEvent: function () {
        this.ononSearchCityTextFocusHandler = this.onSearchCityTextFocus.bind(this);
        this.onSearchCityTextBlurHandler = this.onSearchCityTextBlur.bind(this);
        this.onHeadPanelClickHandler = this.onHeadPanelClick.bind(this);
        this.onCloseHandler = this.hide.bind(this);
        Event.observe(this.searchCityText, "focus", this.ononSearchCityTextFocusHandler);
        Event.observe(this.searchCityText, "blur", this.onSearchCityTextBlurHandler);
        Event.observe(this.headPanel, "click", this.onHeadPanelClickHandler);
        Event.observe(this.closeNode, "click", this.onCloseHandler);
        this.onClickPageHandler = this.onClickPage.bindAsEventListener(this);
        Event.observe(document.body, "click", this.onClickPageHandler)
    },
    destroyEvent: function () {
        this.ononSearchCityTextFocusHandler && Event.stopObserving(this.searchCityText, "focus", this.ononSearchCityTextFocusHandler);
        this.onSearchCityTextBlurHandler && Event.stopObserving(this.searchCityText, "blur", this.onSearchCityTextBlurHandler);
        this.onHeadPanelClickHandler && Event.stopObserving(this.headPanel, "click", this.onHeadPanelClickHandler);
        this.onCloseHandler && Event.stopObserving(this.closeNode, "click", this.onCloseHandler)
    },
    destroyPanel: function () {
        this.closeNode = null;
        this.contentBox = null;
        this.searchCityText = null;
        this.tab = null;
        this.span = null;
        this.bodyPanel = null;
        this.headPanel = null
    },
    close: function () {
        this.destroyEvent();
        this.destroyPanel()
    },
    onClickPage: function (d) {
        var e = Event.pointerX(d);
        var f = Event.pointerY(d);
        if (!Position.within(this.container, e, f) && !Position.within(this.bodyPanel, e, f)) {
            if (this.isShowing()) {
                this.hide();
                this.headPanel.removeClassName("oncity")
            }
        }
    },
    getHotItems: function () {
        if (!this.hotItems) {
            this.hotItems = this.items.sortBy(function (b) {
                return -b.OTicketCinemaCount
            }).slice(0, 40).sortBy(function (b) {
                return -b.VisitedTimes
            }).collect(function (b) {
                return b.Id
            })
        }
        return this.hotItems
    },
    reload: function (k) {
        this.contentBox.update();
        var f = null;
        switch (k) {
            case"热门":
                if (!this.content.HOT) {
                    this.content.HOT = this.createOptions(this.getCitys(this.items, this.getHotItems()), " ", true)
                }
                this.contentBox.update(this.content.HOT);
                break;
            case"A-G":
                if (!this.content.AG) {
                    f = this.items.findAll(function (a) {
                        var b = /[A-G]/.test(a.initial);
                        return b
                    });
                    this.content.AG = this.makeNodes(f, this.content.regRxpAG)
                }
                this.contentBox.update(this.content.AG);
                break;
            case"H-L":
                if (!this.content.HL) {
                    f = this.items.findAll(function (a) {
                        var b = /[H-L]/.test(a.initial);
                        return b
                    });
                    this.content.HL = this.makeNodes(f, this.content.regRxpHL)
                }
                this.contentBox.update(this.content.HL);
                break;
            case"M-T":
                if (!this.content.MT) {
                    f = this.items.findAll(function (a) {
                        var b = /[M-T]/.test(a.initial);
                        return b
                    });
                    this.content.MT = this.makeNodes(f, this.content.regRxpMT)
                }
                this.contentBox.update(this.content.MT);
                break;
            case"W-Z":
                if (!this.content.WZ) {
                    f = this.items.findAll(function (a) {
                        var b = /[M-Z]/.test(a.initial);
                        return b
                    });
                    this.content.WZ = this.makeNodes(f, this.content.regRxpWZ)
                }
                this.contentBox.update(this.content.WZ);
                break;
            default:
                break
        }
        var j = this.contentBox.getElementsByTagName("li");
        for (var h = 0, g = j.length; h < g; h++) {
            Event.observe(j[h], "click", function (b) {
                var a = b.target || b.srcElement;
                this.command(a.getAttribute("value"))
            }.bind(this))
        }
    },
    makeNodes: function (h, k) {
        var o = new StringBuilder();
        for (var l = 0, j = k.length; l < j; l++) {
            var n = k[l];
            var m = h.findAll(function (a) {
                return a.initial === n
            }).sortBy(function (a) {
                return -a.VisitedTimes
            });
            if (m.length > 0) {
                o.append(this.createOptions(m, n))
            }
        }
        return o.toString()
    },
    getItems: function () {
        this.locations = [];
        this.server.getData(function () {
            if (typeof threaterListBoxData !== "undefined" && threaterListBoxData.locations) {
                this.locations = threaterListBoxData.locations.List
            }
            this.items = [];
            this.locations.each(function (c) {
                var d = {
                    Id: c.Id,
                    NameCn: c.NameCn,
                    NameEn: c.NameEn,
                    initial: c.NameEn.charAt(0).toLocaleUpperCase(),
                    PinyinShort: c.PinyinShort || "",
                    VisitedTimes: c.VisitedTimes,
                    OTicketCinemaCount: c.OTicketCinemaCount
                };
                this.items.push(d)
            }.bind(this));
            this.reload("热门");
            this.cityAutoComplete = new CityAutoComplete({
                text: this.searchCityText,
                textWatermarkText: "直接输入城市或城市拼音",
                listRegion: this.contentBox,
                itemTemplate: '<li value="#{Id}"><span class="fl">#{NameEn}</span><span class="fr">#{NameCn}</span></li>',
                idFieldName: "Id",
                titleFieldName: "NameCn",
                searchDatas: this.items,
                defaultSearchDatas: this.getCitys(this.items, this.hotSearchItems),
                choiceCount: 9,
                doCommand: this.command.bind(this),
                onEnterCallback: function (f) {
                    var h = this.contentBox.getElementsByTagName("li");
                    for (var g = 0, e = h.length; g < e; g++) {
                        if (h[g].className === "on") {
                            this.command(h[g].getAttribute("value"));
                            break
                        }
                    }
                }.bind(this)
            })
        }.bind(this))
    },
    createOptions: function (k, h, j) {
        var l = Builder.node("div");
        var m = new StringBuilder();
        m.append('<div class="city_list2 clearfix">');
        m.append('<span class="fl w30 bold mt5">' + h + "</span>");
        m.append("<ul>");
        for (var g = 0; g < k.length; g++) {
            if (j && g !== 0 && (g % this.HOTROWSIZE === 0)) {
                m.append('</ul><span class="fl w30 bold mt5"> </span><ul>')
            }
            m.append(this.optionTemplate.evaluate(k[g]))
        }
        m.append("</ul>");
        m.append("</div>");
        return m.toString()
    },
    onSearchCityTextFocus: function () {
        $A(this.tab.childNodes).each(function (b) {
            b.className = ""
        })
    },
    onSearchCityTextBlur: function (b) {
        this.order = null
    },
    onHeadPanelClick: function (b) {
        b && Event.stop(b);
        if (this.isShowing()) {
            this.hide();
            this.headPanel.removeClassName("oncity")
        } else {
            this.show();
            this.headPanel.addClassName("oncity")
        }
    },
    isShowing: function () {
        return (this.bodyPanel.style.display != "none")
    },
    show: function () {
        this.bodyPanel.show()
    },
    hide: function () {
        this.bodyPanel.hide()
    },
    command: function (f) {
        if (f !== null) {
            setCookie("DefaultCity-CookieKey", f);
            var h = "http://theater." + mtimeCookieDomain + "/[^/]+";
            var k = ["/", "/cinema/", "/movie/", "/ecoupon/", "/ticket/", "/card/", "/movie/\\d+/", "/discount/", "/discount/information/", "/discount/coupon/", "/movie/lastday/"];
            var j = null;
            var g = k.find(function (a) {
                var b = new RegExp(("^" + h + "(" + a + ")$").replace(/\//g, "\\/"));
                j = (location.protocol + "//" + location.host + location.pathname).match(b);
                return j && j.length == 2
            });
            if (!g) {
                location.reload()
            }
            this.server.getTheaterCityHomeURL(f, function (a) {
                if (a && a.value) {
                    if (g) {
                        $redirect(a.value.replace(/\/$/, "") + j[1])
                    } else {
                        if (callbackUrl) {
                            $redirect(callbackUrl)
                        } else {
                            $redirect(a.value)
                        }
                    }
                }
            })
        }
    },
    getCitys: function (o, l) {
        var m = [];
        for (var n = 0; n < l.length; n++) {
            var k = l[n];
            var j = this.getCity(o, k);
            if (j !== null) {
                m.push(j)
            }
        }
        if (m.length < l.length) {
            var q = l.length - m.length;
            for (var p = 0; p < o.length; p++) {
                if (q === 0) {
                    break
                }
                if (!l.include(o[p].Id)) {
                    m.push(o[p]);
                    q--
                }
            }
        }
        return m
    },
    getCity: function (h, e) {
        for (var f = 0, g = h.length; f < g; f++) {
            if (e == h[f].Id) {
                return h[f]
            }
        }
        return null
    }
});
var CityAutoComplete = Class.create();
Object.extend(Object.extend(CityAutoComplete.prototype, AutoCompleteBase.prototype), {
    initialize: function (b) {
        this.baseInitialize(b)
    }, initializeField: function () {
        this.firstRender = true;
        this.isShowMoreResult = false;
        this.isAutoMatchSuggest = true
    }, initializeDOM: function () {
    }, destroyDOM: function () {
        this.listTipText = null
    }, initializeEvent: function () {
    }, destroyEvent: function () {
    }, baseInitializeEvent: function () {
    }, getEntry: function (e) {
        this.active = false;
        var f = this.listRegion.getElementsByTagName("li");
        var d = f.length;
        if (d > e && e >= 0) {
            return $(f[e])
        }
        return null
    }, onEnterText: function () {
        if (this.onEnterCallback) {
            this.onEnterCallback()
        }
    }, onObserverEvent: function () {
        var b = this.getSearchKeyword();
        if (b.length >= this.minChars) {
            if (b != this.keyword) {
                this.keyword = b;
                this.render()
            }
        } else {
            this.keyword = "";
            this.active = false;
            this.render()
        }
    }, onBlurText: function (d, c) {
        this.active = false;
        this.hasFocus = false;
        this.removeObserver()
    }, reset: function () {
        this.selectedValue = null;
        this.index = 0;
        this.searchIndex = 0;
        this.entryCount = 0
    }, onClick: function () {
    }, onClickText: function () {
    }, onFocusText: function (b) {
        this.hasFocus = true;
        this.active = true;
        this.render()
    }, match: function (c, d) {
        if ((d.PinyinShort.toLowerCase().indexOf(c.toLowerCase()) === 0)) {
            return true
        } else {
            if ((d.NameCn.toLowerCase().indexOf(c.toLowerCase()) === 0) || (d.NameEn.toLowerCase().indexOf(c.toLowerCase()) === 0)) {
                return true
            }
        }
        return false
    }, onKeydownText: function (b) {
        if (this.handled) {
            this.handled = false;
            return
        }
        this.handled = true;
        switch (b.keyCode) {
            case Event.KEY_RETURN:
                this.onEnterText();
                Event.stop(b);
                return;
            case Event.KEY_LEFT:
            case Event.KEY_RIGHT:
                return;
            case Event.KEY_UP:
                this.markPrevious();
                Event.stop(b);
                return;
            case Event.KEY_DOWN:
                this.markNext();
                Event.stop(b);
                return;
            default:
                this.addObserver();
                this.onChangeInput();
                break
        }
    }, doRender: function () {
        if (this.isEnable) {
            if (this.changed) {
                this.reset()
            }
            var c = this.getSearchKeyword();
            if (c.length === 0) {
                this.removeObserver();
                this.renderDefaultList();
                if (Prototype.Browser.Gecko && this.hasFocus) {
                    setTimeout(function () {
                        var a = this.getSearchKeyword();
                        if (a != this.keyword) {
                            var b = this.getMoreLists();
                            if (this.isAlwayShowList || this.entryCount > 0) {
                                this.renderTip(a, this.entryCount);
                                this.renderList(this.changed, b)
                            }
                        }
                    }.bind(this), 1000)
                }
            } else {
                var d = this.getMoreLists();
                if (this.isAlwayShowList || this.entryCount > 0) {
                    this.renderTip(c, this.entryCount);
                    this.renderList(this.changed, d);
                    this.showSelectList()
                } else {
                    this.hideSelectList()
                }
            }
        }
    }, renderTip: function (c, d) {
        if (this.firstRender || c === "") {
            this.firstRender = false;
            this.listTipText = "输入中文/拼音或按&uarr;&darr;选择"
        } else {
            if (d > 0) {
                this.listTipText = c + "，按拼音数排序"
            } else {
                this.listTipText = "对不起，找不到：" + c
            }
        }
    }, renderList: function (l, o) {
        var j = '<div class="sou_window"><div class="sou_note">' + this.listTipText + '</div><div class="sou_city"><ul>' + o + "</ul></div></div>";
        this.listRegion.update(j);
        var n = this.listRegion.getElementsByTagName("li");
        for (var k = 0, h = n.length; k < h; k++) {
            var m = n[k];
            m.style.cursor = "pointer";
            Event.observe(m, "click", function (b) {
                var a = b.target || b.srcElement;
                if (a.nodeName == "SPAN") {
                    a = a.parentNode
                }
                this.doCommand(a.getAttribute("value"))
            }.bind(this));
            Event.observe(m, "mouseover", function (b) {
                var c = this;
                if (Prototype.Browser.IE) {
                    var a = b.target || b.srcElement;
                    if (a.nodeName == "SPAN") {
                        c = a.parentNode
                    } else {
                        c = a
                    }
                }
                $A(c.parentNode.childNodes).each(function (d) {
                    d.className = ""
                });
                c.className = "on"
            });
            Event.observe(m, "mouseout", function (b) {
                var c = this;
                if (Prototype.Browser.IE) {
                    var a = b.target || b.srcElement;
                    if (a.nodeName == "SPAN") {
                        c = a.parentNode
                    } else {
                        c = a
                    }
                }
                c.className = ""
            })
        }
        if (n.length > 0) {
            n[0].className = "on"
        }
    }
});
var TopMenuNavigationMenu = Class.create();
Object.extend(TopMenuNavigationMenu.prototype, {
    options: {
        menuRegion: null,
        contentRegion: null,
        onOverCallback: null,
        onOutCallback: null,
        overTimeout: 1,
        outTimeout: 100
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeField();
        this.initializeEvent()
    }, initializeField: function () {
        this.outMenuTimer = null;
        this.overMenuTimer = null
    }, initializeEvent: function () {
        this.onOverMenuHandler = this.onOverMenu.bindAsEventListener(this);
        this.menuRegion.observe("mouseover", this.onOverMenuHandler);
        this.contentRegion.observe("mouseover", this.onOverMenuHandler);
        this.onOutMenuHandler = this.onOutMenu.bindAsEventListener(this);
        this.menuRegion.observe("mouseout", this.onOutMenuHandler);
        this.contentRegion.observe("mouseout", this.onOutMenuHandler);
        this.onClickMenuHandler = this.onClickMenu.bindAsEventListener(this);
        this.menuRegion.observe("click", this.onClickMenuHandler)
    }, destroyEvent: function () {
        this.menuRegion.stopObserving("mouseover", this.onOverMenuHandler);
        this.menuRegion.stopObserving("mouseout", this.onOutMenuHandler);
        this.menuRegion.stopObserving("click", this.onClickMenuHandler)
    }, onOverMenu: function (b) {
        this.handleOverMenuTimer();
        this.handleOutMenuTimer();
        this.overMenuTimer = FunctionExt.defer(function () {
            this.onMenuOverCallback()
        }, this.overTimeout, this)
    }, onOutMenu: function (b) {
        this.handleOverMenuTimer();
        this.handleOutMenuTimer();
        this.outMenuTimer = FunctionExt.defer(function () {
            this.onMenuOutCallback()
        }, this.outTimeout, this)
    }, handleOverMenuTimer: function () {
        if (this.overMenuTimer !== null) {
            clearTimeout(this.overMenuTimer);
            this.overMenuTimer = null
        }
    }, handleOutMenuTimer: function () {
        if (this.outMenuTimer !== null) {
            clearTimeout(this.outMenuTimer);
            this.outMenuTimer = null
        }
    }, onClickMenu: function (b) {
        this.onOverMenu()
    }, onMenuOverCallback: function () {
        if (!this.contentRegion.visible()) {
            this.contentRegion.show()
        }
        if (this.onOverCallback) {
            this.onOverCallback()
        }
    }, onMenuOutCallback: function () {
        if (this.contentRegion.visible()) {
            this.contentRegion.hide()
        }
        if (this.onOutCallback) {
            this.onOutCallback()
        }
    }
});
var SearchAutoComplete = Class.create();
Object.extend(Object.extend(SearchAutoComplete.prototype, AutoCompleteBase.prototype), {
    initialize: function (b) {
        this.baseInitialize(b)
    }, initializeField: function () {
        this.isAlwayShowList = false;
        this.isShowMoreResult = false
    }, selectEntry: function () {
        this.active = false;
        var b = this.getCurrentEntry();
        this.selectedEntry(b)
    }, getEntry: function (c) {
        var d = null;
        if (c >= 0) {
            d = $(this.listRegion.childNodes[c])
        }
        return d
    }, getOption: function (d) {
        var f = null;
        var e = this.idFieldName;
        this.searchDatas.each(function (a) {
            if (a[e] == d) {
                f = a;
                throw $break
            }
        });
        return f
    }, render: function () {
        if (this.getSearchDataCallback !== null) {
            this.getSearchDataCallback(function (b) {
                this.searchDatas = b;
                this.doRender()
            }.bind(this))
        } else {
            this.doRender()
        }
    }, getMoreLists: function () {
        var m = this.searchDatas, l = null, j = new StringBuilder(), k = 0;
        for (var h = 0, g = m.length; h < g; h++) {
            l = m[h];
            j.append(this.createListItem(l));
            k++;
            this.searchIndex = h
        }
        this.entryCount += k;
        return j.toString()
    }, renderList: function (c, d) {
        this.listRegion.innerHTML = d
    }, onClickText: function () {
        this.render()
    }, createListItem: function (d) {
        var c = this.itemTemplate.evaluate(d);
        c = c.replace("<em >被查询 0 次</em>", "");
        return c
    }
});
var MiniCart = Class.create();
Object.extend(MiniCart.prototype, {
    server: {
        getCartList: function (b) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteTradeServiceUrl, "Mtime.Trade.Pages.TradeService", "GetMiniCartList", [], b, "/Service/Trade.p", "get", "60000")
        }, removeCartItem: function (d, c) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteTradeServiceUrl, "Mtime.Trade.Pages.TradeService", "RemoveCartItemsBySkus", [d], c, "/Service/Trade.p", "get", "60000")
        }
    }, options: {container: null, isLogin: false}, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeDOM();
        this.initializeEvent();
        this.render()
    }, initializeDOM: function () {
        this.headPanel = $(this.container);
        var d = Builder.node("i", {className: "ico_shop"});
        Event.observe(d, "click", function () {
            window.location.href = siteTradeServiceUrl + "/cart/"
        });
        var c = Builder.node("em", {className: "emailnum", id: "emGoodsCount", style: "display:none;"}, [0]);
        Event.observe(c, "click", function () {
            window.location.href = siteTradeServiceUrl + "/cart/"
        });
        this.headPanel.appendChild(d);
        this.headPanel.appendChild(c);
        if (this.isLogin) {
            this.headPanel.removeClassName("onlyshop")
        }
        this.bodyPanel = Builder.node("div", {className: "menupop m_shopcart", style: "display:none;"});
        this.bodyPanel.appendChild(this.getNoDataElement());
        this.headPanel.appendChild(this.bodyPanel);
        this.headPanel.style.display = "block"
    }, initializeEvent: function () {
        this.navigationMenu = new TopMenuNavigationMenu({
            menuRegion: this.headPanel,
            contentRegion: this.bodyPanel,
            onOverCallback: this.onHeadPanelOver.bind(this),
            onOutCallback: this.onHeadPanelOut.bind(this)
        });
        this.deleteRegionClickHandler = this.deleteRegionClick.bind(this);
        this.bodyPanel.observe("click", this.deleteRegionClickHandler)
    }, render: function () {
        this.server.getCartList(function (c) {
            if (c && c.value) {
                this.clearBodyPanel();
                var d = c.value.count;
                if (d > 0) {
                    $("emGoodsCount").innerHTML = d;
                    $("emGoodsCount").show()
                } else {
                    $("emGoodsCount").hide()
                }
                if (d > 0) {
                    this.bodyPanel.innerHTML = c.value.html
                } else {
                    this.bodyPanel.appendChild(this.getNoDataElement())
                }
            }
        }.bind(this))
    }, deleteRegionClick: function (k) {
        var j = Event.element(k);
        var m = j.readAttribute("method");
        while (!m && j != document.body) {
            j = j.up();
            m = j.readAttribute("method")
        }
        if (!m) {
            return
        }
        var h = j.readAttribute("actid");
        var o = j.readAttribute("sku").split("|");
        var n = "";
        for (var l = 0; l < o.length; l++) {
            if (n.length > 0) {
                n += ","
            }
            n += o[l] + "|" + h + "|false"
        }
        this.server.removeCartItem(n, function (a) {
            if (a && a.value && a.value.success) {
                this.render()
            }
        }.bind(this))
    }, destroyEvent: function () {
        this.onHeadPanelOverHandler && Event.stopObserving(this.headPanel, "mouseover", this.onHeadPanelOverHandler);
        this.onHeadPanelOutHandler && Event.stopObserving(this.headPanel, "mouseout", this.onHeadPanelOutHandler)
    }, destroyPanel: function () {
        this.goodsListPanel = null;
        this.bodyPanel = null;
        this.headPanel = null
    }, close: function () {
        this.destroyEvent();
        this.destroyPanel()
    }, getNoDataElement: function () {
        return Builder.node("p", {className: "tips"}, ["您的购物车还是空的，赶快选购哦。"])
    }, clearBodyPanel: function () {
        this.bodyPanel.update("")
    }, onHeadPanelOver: function (b) {
        if (!this.headPanel.hasClassName("currshop")) {
            this.headPanel.addClassName("currshop")
        }
    }, onHeadPanelOut: function (b) {
        if (this.headPanel.hasClassName("currshop")) {
            this.headPanel.removeClassName("currshop")
        }
    }
});
var headTrackClick = function (c) {
    var d = "M14_HeadNav";
    if (typeof tracker != "undefined") {
        tracker.trackClick(d, c)
    }
};
var topMenu = null;
var manager = null;
function initializeManager(b) {
    topMenu = new TopMenu(b)
}
function reinitializeManager() {
    topMenu.close();
    topMenu.initializeUser()
}
var StaticManager = Class.create();
StaticManager.prototype = {
    options: {
        mainNavigaionType: -1,
        subNavigaionType: -1,
        isMini: false,
        isChannelFooter: false,
        is2011MiniFooter: false,
        showTopSearch: true,
        showTopSearchInChannel: false
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, server: {
        createSubscriber: function (d, c) {
            return Mtime.Component.Ajax.request(siteChannelServiceUrl, "Mtime.Channel.Pages.DatabaseService", "CreateSubscriber", [d], c, "/service/Database.mcs", "get", "20000")
        }
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeField();
        this.initializeDOM();
        this.initializeEvent();
        this.load();
        this.render()
    }, initializeField: function () {
    }, initializeDOM: function () {
        this.topRegion = $("top");
        this.bottomRegion = $("bottom")
    }, destroyDOM: function () {
        this.topRegion = null;
        this.bottomRegion = null;
        this.iptEMail = this.btnSubscribe = null
    }, initializeEvent: function () {
        Event.observe(window, "unload", this.close.bind(this))
    }, destroyEvent: function () {
        if (this.btnSubscribe) {
            this.btnSubscribe.stopObserving("click", this.onClickBtnSubscribeHandler)
        }
        if (this.iptEMailTextbox) {
            this.iptEMailTextbox.close()
        }
    }, load: function () {
        if (document.documentElement.clientWidth <= 1024) {
            $(document.documentElement).addClassName("scroll1024")
        }
    }, render: function () {
        this.renderManager();
        this.renderHeader();
        this.renderNavigatorBar();
        this.renderFooter()
    }, renderHeader: function () {
    }, renderManager: function () {
        if (typeof initializeManager !== "function") {
            $loadJs("/js/2014/topmenu.js", function () {
                initializeManager({})
            }.bind(this))
        } else {
            initializeManager({})
        }
    }, renderNavigatorBar: function () {
    }, renderFooter: function () {
        if (this.bottomRegion) {
            if (this.isMini) {
                this.renderMiniFooter()
            } else {
                this.renderMtimeFooter()
            }
        }
    }, renderMtimeFooter: function () {
        var b = ['<div class="footout">', '<span class="topline"></span>', '<div class="footinner clearfix">', '<div class="fotlogo">', "<dl>", '<dt><a href="http://www.mtime.com" title="Mtime时光网">Mtime时光网</a></dt>', '<dd class="clearfix"><a href="http://jobs.mtime.com/">加入我们</a><a href="http://feature.mtime.com/help/contact.htm" class="ml30">联系我们</a></dd>', '<dd class="clearfix"><a href="http://group.mtime.com/help/">站务论坛</a><a href="http://my.mtime.com/reportError/" class="ml30">问题反馈</a></dd>', '<dd class="clearfix"><a href="http://feature.mtime.com/help/rules.htm">社区准则</a><a href="http://www.mtime.com/sitemap.htm" class="ml30">网站地图</a></dd>', '<dd class="clearfix"><a href="http://feature.mtime.com/help/privacy.htm">隐私政策</a><a href="http://en.mtime.com/" class="ml30">English</a></dd>', "</dl>", '<i class="line"></i>', "</div>", '<div class="fotmap">', '<dl style="width:42px;">', "<dt>栏目</dt>", '<dd><a href="http://news.mtime.com/">新闻</a></dd>', '<dd><a href="http://theater.mtime.com/">影院</a></dd>', '<dd><a href="http://mall.mtime.com">商城</a></dd>', '<dd><a href="http://www.mtime.com/community/">社区</a></dd>', "</dl>", '<dl style="width:185px;">', "<dt>推荐</dt>", '<dd><a href="http://www.mtime.com/top/movie/top100/">电影热榜</a><a href="http://movie.mtime.com/new/" class="ml30">全球新片</a></dd>', '<dd><a href="http://news.mtime.com/conversation/">时光对话</a><a href="http://movie.mtime.com/classic/" class="ml30">典藏佳片</a></dd>', '<dd><a href="http://video.mtime.com/trailer/">新片预告</a><a href="http://award.mtime.com/all/" class="ml30">电影节</a></dd>', '<dd><a href="http://www.mtime.com/weekly/">时光周刊</a><a href=" http://my.mtime.com/app/card/" class="ml30">卡片大富翁</a></dd>', "</dl>", "<dl>", "<dt>关注我们</dt>", '<dd><a href="http://weibo.com/movietime?topnav=1&wvr=5&topsug=1" target="_blank" class="sina" title="新浪">新浪</a></dd>', '<dd><a id="aWeiXinPicButton" href="javascript:void(0)" class="weixin" title="微信">微信</a></dd>', '<dd><a href="http://www.mtime.com/rss/" target="_blank" class="rss" title="RSS">RSS</a></dd>', "</dl>", '<i class="line"></i>', '<div id="divWeiXinPicContainer" class="my_layer none"> <em class="l_arrow">&nbsp;</em>', '<div class="inner p15" style="width:195px;">', "<i></i>", "<p>扫描二维码，微信实时关注时光网</p>", "</div>", "</div>", "</div>", '<div class="fotweek">', "<dl>", (window.topMenuValues && window.topMenuValues.footer) ? topMenuValues.footer : "", '<dd class="input"><input id="iptEMail" type="text" value="邮箱地址" class="c_a5" /><a id="btnSubscribe" href="javascript:void(0)">订阅</a></dd>', "</dl>", "</div>", '<div class="fothr">', "<dl>", "<dt><strong>手机购票</strong> 方便 实惠</dt>", '<dd><a href="http://feature.mtime.com/mobile/" target="_blank"><i></i></a></dd>', "<dd>扫描二维码 下载客户端</dd>", "</dl>", "</div>", "</div>", "</div>"].concat(this.getMiniFooterHTMLArray());
        this.bottomRegion.innerHTML = b.join("");
        this.iptEMail = $("iptEMail");
        this.btnSubscribe = $("btnSubscribe");
        this.iptEMailTextbox = new Textbox({
            text: this.iptEMail,
            watermarkText: "邮箱地址",
            onKeypressCallback: this.onEnter.bind(this)
        });
        this.onClickBtnSubscribeHandler = this.orderWeekly.bindAsEventListener(this);
        this.btnSubscribe.observe("click", this.onClickBtnSubscribeHandler);
        this.aWeiXinPicButton = $("aWeiXinPicButton");
        this.divWeiXinPicContainer = $("divWeiXinPicContainer");
        this.divWeiXinPicContainer.hide();
        this.divWeiXinPicContainer.removeClassName("none");
        this.onWeiXinPicButtonHandler = this.clickWeiXinPicButton.bindAsEventListener(this);
        this.aWeiXinPicButton.observe("click", this.onWeiXinPicButtonHandler);
        this.onClickPageHandler = this.onClickPage.bindAsEventListener(this);
        Event.observe(document.body, "click", this.onClickPageHandler)
    }, renderMiniFooter: function () {
        this.bottomRegion.innerHTML = this.getMiniFooterHTMLArray().join("")
    }, getMiniFooterHTMLArray: function () {
        var b = ['<div class="db_foot">', "<p>", '<span class="mr12">北京动艺时光网络科技有限公司</span>Copyright 2006-', new Date().getFullYear(), " Mtime.com Inc. All rights reserved.<br>", '<a href="http://feature.mtime.com/help/icp.htm" target="_blank" class="mr12 ml12">京ICP证050715号</a> ', '<a href="http://feature.mtime.com/help/videolicence.htm" target="_blank" class="mr12">网络视听许可证0108265号</a>', '<a href="http://feature.mtime.com/help/network.htm" target="_blank" class="mr12">网络文化经营许可证</a>', '<a href="http://feature.mtime.com/help/tvlicence.htm" target="_blank" class="mr12">广播电视节目制作经营许可证(京)字第1435号</a>', "<span>京公网安备：110105000744号</span>", "</p>", '<div class="credible"><a href="http://feature.mtime.com/help/credible.htm" target="_blank" title="可信网站" class="credibler"></a><a href="http://feature.mtime.com/help/goodfaith.htm" target="_blank" title="诚信网站" class="goodfaith"></a></div>', "</div>"];
        return b
    }, orderWeekly: function () {
        var b = $F(this.iptEMail);
        b = b.trim();
        if (b.length > 0) {
            if (!Validator.validateEmail(b)) {
                $alert("请输入合法的邮箱地址！");
                this.iptEMail.focus();
                return
            } else {
                this.server.createSubscriber(b, this.onCreateSubScriberCallBack.bind(this))
            }
        }
    }, onCreateSubScriberCallBack: function () {
        var b = orderResult;
        if (b && b.value && b.value.success) {
            $alert("您已成功订阅周刊，您的邮箱将会定期收到最新一期的周刊。");
            this.iptEMail.value = "请输入邮箱地址"
        } else {
            $alert("订阅周刊失败，请重试或联系管理员。")
        }
    }, onEnter: function (b) {
        switch (b.keyCode) {
            case Event.KEY_RETURN:
                this.orderWeekly();
                Event.stop(b);
                break;
            default:
                break
        }
    }, clickWeiXinPicButton: function (b) {
        this.divWeiXinPicContainer.show()
    }, onClickPage: function (d) {
        var e = Event.pointerX(d);
        var f = Event.pointerY(d);
        if (!Position.within(this.divWeiXinPicContainer, e, f) && !Position.within(this.aWeiXinPicButton, e, f)) {
            if (this.divWeiXinPicContainer.visible()) {
                this.divWeiXinPicContainer.hide()
            }
        }
    }, closed: false, close: function () {
        if (!this.closed) {
            this.closed = true;
            this.destroyEvent();
            this.destroyDOM()
        }
    }
};
function f_navigationBarSearch() {
    var d = $("navSearchValueInput").value.Trim();
    var c = $F("navSearchTypeSelect");
    if (d !== "") {
        window.open("/search/" + (c == "all" ? "" : c) + "?" + encodeURI(d), "_blank")
    }
}
if (typeof(Widgets) == "undefined") {
    var Widgets = Class.create()
}
Widgets.Base = Class.create();
Object.extend(Widgets.Base.prototype, {
    options: {}, initialize: function (b) {
        this.setOptions(b);
        this.initializeDom();
        this.initializeEvent();
        this.load()
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initializeDom: function () {
    }, initializeEvent: function () {
        Event.observe(window, "unload", this.close.bind(this))
    }, load: function () {
    }, close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    }, destroyField: function () {
    }, destroyEvent: function () {
    }, destroyDOM: function () {
    }
});
Widgets.SlidesControl = Class.create();
Object.extend(Widgets.SlidesControl.prototype, Widgets.Base.prototype);
Object.extend(Widgets.SlidesControl.prototype, {
    options: {
        slidesRegion: "movieSlidesItems",
        prev: "moviePrev",
        next: "movieNext",
        duration: 0.5,
        showLength: 4,
        videID: 10404,
        items: [],
        autoTimeout: null,
        type: "detail",
        itemWidth: 245,
        isHide: false,
        moreLink: "",
        imageTemplate: '<li class="#{sty}"><span class="sharp"></span><a href="#{Url}" class="db_avideo"><img src="#{ImagePath}" alt="#{TitleSamll}" /><em></em><b></b><i>#{hdHtml}#{Length}</i><span>#{TitleSamll}</span></a></li>',
        callback: null
    }, isOther: false, initializeDom: function () {
        this.$slidesRegion = $(this.slidesRegion);
        if (!this.$slidesRegion) {
            return
        }
        this.$prev = $(this.prev);
        this.$next = $(this.next);
        this.scale = this.itemWidth * this.showLength;
        this.$slidesRegion.style.width = (this.scale * 2 + 100) + "px";
        if (this.type === "other") {
            this.isOther = true;
            this.$prev_i = this.$prev.down("i");
            this.$next_i = this.$next.down("i");
            if (this.moreLink !== "") {
                this.moreLinkOjb = {
                    nextBtnTemplate: '<a class="next_screen" href="#{morelink}" target="_blank"><em></em><i class="on"></i></a>',
                    morelink: this.moreLink
                };
                this.$parentRegion = this.$next.up();
                var d = new Template(this.moreLinkOjb.nextBtnTemplate), c = d.evaluate(this.moreLinkOjb);
                this.nextBtnHtml = Builder.build(c);
                this.nextBtnHtml.hide();
                this.$parentRegion.appendChild(this.nextBtnHtml)
            }
        }
        this.durations = this.durationInfo(this.duration)
    }, renderImage: function (g) {
        if (!g) {
            return
        }
        var f, h = new Template(this.imageTemplate), e = new StringBuilder();
        if (typeof this.videoID !== "undefined" && typeof g.VideoID !== "undefined" && g.VideoID == this.videoID) {
            g.sty = "on"
        } else {
            g.sty = ""
        }
        g.hdHtml = g.HD === 1 ? '<strong title="HD"></strong>' : "";
        f = h.evaluate(g);
        return f.toString()
    }, show: function () {
        var l = [];
        for (var h = this.min; h < this.length && h < this.max; h++) {
            var j = this.imgItems[h];
            var g = this.renderImage(j);
            var m = Builder.build(g);
            if (this.type === "list") {
                if (this.isVideoList) {
                    if (h % 2 === 0) {
                        var k = Builder.build("<li></li>");
                        k.appendChild(m);
                        this.$slidesRegion.appendChild(k);
                        l.push(k)
                    } else {
                        j = this.imgItems[h];
                        g = this.renderImage(j);
                        m = Builder.build(g);
                        l[l.length - 1].appendChild(m)
                    }
                } else {
                    var k = Builder.build("<li></li>");
                    k.appendChild(m);
                    this.$slidesRegion.appendChild(k);
                    l.push(k)
                }
            } else {
                this.$slidesRegion.appendChild(m);
                l.push(m)
            }
            if (h === 0) {
                if (this.isOther) {
                    this.$prev_i.removeClassName("on")
                } else {
                    this.$prev.hide()
                }
            }
            if (h === this.length - 1) {
                if (this.type === "other") {
                    if (this.moreLink !== "") {
                        if (this.isHide && this.items.length <= this.showLength) {
                            this.$next_i.hide()
                        } else {
                            this.nextBtnHtml.show()
                        }
                    } else {
                        this.$next_i.removeClassName("on")
                    }
                } else {
                    this.$next.hide()
                }
            }
        }
        this.selectedItems = l
    }, load: function () {
        this.pageIndex = 0;
        this.imgItems = [];
        if (typeof this.items === "undefined" || this.items.length === 0) {
            return
        }
        this.imgItems = this.items;
        this.length = this.imgItems.length;
        this.amount = Math.ceil(this.length / this.showLength);
        var f, h, e, g = 0;
        for (i = 0; i < this.length; i++) {
            if (typeof this.imgItems[i] !== "undefined" && typeof this.imgItems[i].VideoID !== "undefined") {
                if (this.imgItems[i].VideoID == this.videoID) {
                    f = this.imgItems[i];
                    g = i + 1;
                    if (i > 0) {
                        h = this.imgItems[i - 1]
                    }
                    if (i < this.length - 1) {
                        e = this.imgItems[i + 1]
                    }
                    this.callback && this.callback({nowVideo: f, preVideo: h, nextVideo: e, videoCount: this.length})
                }
            }
        }
        if (this.length > this.showLength) {
            if (this.isOther) {
                this.$prev_i.addClassName("on");
                this.$next_i.addClassName("on")
            } else {
                this.$prev.show();
                this.$next.show()
            }
        } else {
            if (this.isOther) {
                if (this.isHide) {
                    this.$prev_i.hide();
                    this.$next_i.hide()
                } else {
                    this.$prev_i.removeClassName("on");
                    this.$next_i.removeClassName("on")
                }
            } else {
                this.$prev.hide();
                this.$next.hide()
            }
        }
        this.isVideoList = this.type === "list" && this.length >= 20 ? true : false;
        this.pageIndex = Math.ceil(g / this.showLength);
        this.min = (this.pageIndex - 1) * this.showLength;
        if (this.min < 0) {
            this.min = 0
        }
        this.max = this.pageIndex * this.showLength;
        if (this.max < this.showLength) {
            this.max = this.showLength
        }
        if (this.isVideoList) {
            this.$slidesRegion.up().style.height = "274px";
            this.max = this.max * 2
        }
        this.show();
        if (typeof SlidesControl != "undefined") {
            this.instantiation()
        } else {
            $loadJs(["/js/2011/SlidesControl.js"], function () {
                this.instantiation()
            }.bind(this))
        }
    }, durationInfo: function (d) {
        var f = Math.floor(d * 10) / 10, e;
        switch (f) {
            case 0.2:
                e = {className: "transition2", duration: 200};
                break;
            case 0.3:
                e = {className: "transition3", duration: 300};
                break;
            case 0.4:
                e = {className: "transition4", duration: 400};
                break;
            case 0.5:
                e = {className: "transition5", duration: 500};
                break;
            case 0.6:
                e = {className: "transition6", duration: 600};
                break;
            default:
                e = {className: "transition6", duration: 600}
        }
        return e
    }, instantiation: function () {
        var d = this;
        var c = new SlidesControl({
            slidesRegion: this.slidesRegion,
            prev: this.prev,
            next: this.next,
            scale: d.scale,
            duration: this.duration,
            type: "neat",
            prevSlides: function () {
                if (this.lock) {
                    return
                }
                if (typeof d.$prev_i !== "undefined" && !d.$prev_i.hasClassName("on")) {
                    return
                }
                var w = null;
                d.selectedItems = this.slidesRegion.immediateDescendants();
                var a = [], b = d.selectedItems[0], u, x, y, z, A = [], t, q, s = 0;
                if (d.isVideoList) {
                    d.min -= d.showLength * 2;
                    d.max -= d.showLength * 2
                } else {
                    d.min -= d.showLength;
                    d.max -= d.showLength
                }
                if (d.min <= 0) {
                    d.min = 0;
                    if (d.isVideoList) {
                        d.max = d.showLength * 2
                    } else {
                        d.max = d.showLength
                    }
                }
                if (d.max >= d.length) {
                    if (d.isVideoList) {
                        d.max = d.length - d.selectedItems.length * 2;
                        d.min = d.max - d.showLength * 2
                    } else {
                        d.max = d.length - d.selectedItems.length;
                        d.min = d.max - d.showLength
                    }
                }
                if (d.isOther) {
                    d.$next_i.addClassName("on")
                } else {
                    d.$next.show()
                }
                if (d.nextBtnHtml) {
                    d.nextBtnHtml.hide()
                }
                for (var r = d.min; r < d.length && r < d.max; r++) {
                    t = d.imgItems[r];
                    q = d.renderImage(t);
                    z = Builder.build(q);
                    if (r === 0) {
                        if (d.isOther) {
                            d.$prev_i.removeClassName("on")
                        } else {
                            d.$prev.hide()
                        }
                    }
                    if (d.type === "list") {
                        if (d.isVideoList) {
                            if (r % 2 === 0) {
                                var v = Builder.build("<li></li>");
                                v.appendChild(z);
                                this.slidesRegion.insertBefore(v, b);
                                a.push(v)
                            } else {
                                t = d.imgItems[r];
                                q = d.renderImage(t);
                                z = Builder.build(q);
                                a[a.length - 1].appendChild(z)
                            }
                        } else {
                            var v = Builder.build("<li></li>");
                            v.appendChild(z);
                            this.slidesRegion.insertBefore(v, b);
                            a.push(v)
                        }
                    } else {
                        a.push(z);
                        this.slidesRegion.insertBefore(z, b)
                    }
                }
                if (d.isVideoList) {
                    s = Math.floor((d.max - d.min) / 2)
                } else {
                    s = d.max - d.min
                }
                d.scale = d.itemWidth * s;
                this.slidesRegion.style.left = this.position - d.scale + "px";
                if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
                    FunctionExt.defer(function () {
                        this.slidesRegion.addClassName(d.durations.className);
                        this.slidesRegion.style.left = "0px"
                    }, 10, this);
                    FunctionExt.defer(function () {
                        this.slidesRegion.removeClassName(d.durations.className);
                        for (r = d.showLength - s; r < d.showLength; r++) {
                            d.selectedItems[r] && this.slidesRegion.removeChild(d.selectedItems[r])
                        }
                        this.lock = false
                    }, d.durations.duration, this);
                    return
                }
                w = {
                    node: this.slidesRegion,
                    duration: this.duration,
                    scale: d.scale,
                    position: this.position - d.scale,
                    callback: function () {
                        for (r = d.showLength - s; r < d.showLength; r++) {
                            d.selectedItems[r] && this.slidesRegion.removeChild(d.selectedItems[r])
                        }
                        this.slidesRegion.style.left = this.position;
                        d.selectedItems = a
                    }.bind(this)
                };
                this.move(w)
            },
            nextSlides: function () {
                if (this.lock) {
                    return
                }
                if (typeof d.$next_i !== "undefined" && !d.$next_i.hasClassName("on")) {
                    return
                }
                var a = null;
                if (d.isVideoList) {
                    d.min += d.showLength * 2;
                    d.max += d.showLength * 2
                } else {
                    d.min += d.showLength;
                    d.max += d.showLength
                }
                if (d.isOther) {
                    d.$prev_i.addClassName("on")
                } else {
                    d.$prev.show()
                }
                d.show();
                d.selectedItems = this.slidesRegion.immediateDescendants();
                if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
                    this.slidesRegion.addClassName(d.durations.className);
                    this.slidesRegion.style.left = -d.scale + "px";
                    FunctionExt.defer(function () {
                        this.slidesRegion.removeClassName(d.durations.className);
                        for (i = 0; i < d.showLength; i++) {
                            d.selectedItems[i] && this.slidesRegion.removeChild(d.selectedItems[i])
                        }
                        this.slidesRegion.style.left = this.position;
                        this.lock = false
                    }, d.durations.duration, this);
                    return
                }
                a = {
                    node: this.slidesRegion,
                    duration: this.duration,
                    scale: -d.scale,
                    position: this.position,
                    callback: function () {
                        for (i = 0; i < d.showLength; i++) {
                            d.selectedItems[i] && this.slidesRegion.removeChild(d.selectedItems[i])
                        }
                        this.slidesRegion.style.left = this.position
                    }.bind(this)
                };
                this.move(a)
            },
            move: function (a) {
                this.lock = true;
                new Fx.Move(a.node, {
                    scaleX: true,
                    duration: a.duration,
                    distances: {left: a.scale, position: a.position},
                    callback: function () {
                        this.lock = false;
                        a.callback && a.callback()
                    }.bind(this)
                })
            }
        })
    }
});
Widgets.BehindSlidesControl = Class.create();
Object.extend(Widgets.BehindSlidesControl.prototype, Widgets.Base.prototype);
Object.extend(Widgets.BehindSlidesControl.prototype, {
    options: {
        slidesRegion: "movieSlidesItems",
        prev: "moviePrev",
        next: "movieNext",
        presentNumber: "present_number",
        sumNumber: "sum_number",
        titleTxt: "title_txt",
        describeTxt: "describe_txt",
        duration: 0.5,
        type: "neat",
        callback: null
    }, initializeDom: function () {
        this.$slidesRegion = $(this.slidesRegion);
        if (!this.$slidesRegion) {
            return
        }
        this.$presentNumber = $(this.presentNumber);
        this.$sumNumber = $(this.sumNumber);
        this.$titleTxt = $(this.titleTxt);
        this.$describeTxt = $(this.describeTxt);
        this.$images = this.$slidesRegion.immediateDescendants();
        this.imagesLen = this.$images.length;
        this.$slidesRegion.style.width = (this.$images[0].offsetWidth * this.imagesLen + 100) + "px";
        if (this.$sumNumber) {
            this.$sumNumber.innerHTML = "/" + this.imagesLen
        }
        this.presentNum = 1;
        if (this.$presentNumber) {
            this.$presentNumber.innerHTML = this.presentNum
        }
        if (this.$titleTxt) {
            this.$titleTxt.innerHTML = Widgets.Utils.strShort(this.$images[0].readAttribute("title"), 24)
        }
        if (this.$describeTxt) {
            this.$describeTxt.innerHTML = Widgets.Utils.strShort(this.$images[0].readAttribute("imgabstract"), 46)
        }
    }, load: function () {
        if (typeof SlidesControl !== "undefined") {
            this.instanceSlidesControl()
        } else {
            $loadJs(["/js/2011/SlidesControl.js"], function () {
                this.instanceSlidesControl()
            }.bind(this))
        }
    }, instanceSlidesControl: function () {
        var d = this;
        if (!this.$slidesRegion || this.$slidesRegion.children.length < 2) {
            return
        }
        var c = new SlidesControl({
            slidesRegion: this.slidesRegion,
            prev: this.prev,
            next: this.next,
            scale: this.$images[0].offsetWidth,
            duration: this.duration,
            type: "neat",
            move: function (a) {
                this.lock = true;
                d.moveMethod(a.scale);
                new Fx.Move(a.node, {
                    scaleX: true,
                    duration: a.duration,
                    distances: {left: a.scale, position: a.position},
                    callback: function () {
                        this.lock = false;
                        a.callback && a.callback()
                    }.bind(this)
                })
            }
        })
    }, moveMethod: function (b) {
        if (b > 0) {
            this.presentNum -= 1;
            if (this.presentNum <= 0) {
                this.presentNum = this.imagesLen
            }
        } else {
            this.presentNum += 1;
            if (this.presentNum > this.imagesLen) {
                this.presentNum = 1
            }
        }
        this.$presentNumber.innerHTML = this.presentNum;
        if (this.$titleTxt) {
            this.$titleTxt.innerHTML = Widgets.Utils.strShort(this.$images[this.presentNum - 1].readAttribute("title"), 24)
        }
        if (this.$describeTxt) {
            this.$describeTxt.innerHTML = Widgets.Utils.strShort(this.$images[this.presentNum - 1].readAttribute("imgabstract"), 46)
        }
        this.callback && this.callback()
    }, destroyDOM: function () {
        this.$presentNumber = null;
        this.$sumNumber = null;
        this.$titleTxt = null;
        this.$describeTxt = null;
        this.$images = null;
        this.imagesLen = null
    }
});
var Fixedable = function (l, r) {
    var l = $(l);
    var r = $(r);

    function m(b, e) {
        b.setStyle({zIndex: 100, width: b.offsetWidth + "px"});
        if (Mtime.browser.ie === 6) {
            b.style.position = "absolute";
            document.body.style.backgroundImage = "url(about:blank)";
            document.body.style.backgroundAttachment = "fixed";
            var g = (typeof e.width !== "undefined") || b.offsetWidth, c = (typeof e.height !== "undefined") || b.offsetHeight, f = 0;
            if (typeof e.top !== "undefined") {
                var a = $$(".c_content")[0] || $$(".c_out")[0];
                if (a) {
                    f = Position.cumulativeOffset(a)[1]
                }
            }
            var d = function () {
                var h = (typeof e.top !== "undefined") ? e.top - f : (document.documentElement.clientHeight - e.bottom - c);
                h = h - Position.cumulativeOffset(b.up())[1];
                !isNaN(h) && b.style.setExpression("top", "eval(document.documentElement.scrollTop + " + h + ") + 'px'")
            };
            d()
        } else {
            b.style.position = "fixed";
            (typeof e.top !== "undefined") && (b.style.top = e.top + "px");
            (typeof e.bottom !== "undefined") && (b.style.bottom = e.bottom + "px");
            (typeof e.left !== "undefined") && (b.style.left = e.left + "px");
            (typeof e.right !== "undefined") && (b.style.right = e.right + "px")
        }
        return b
    }

    function s(a, b) {
        a.setStyle({zIndex: "", width: ""});
        if (Mtime.browser.ie === 6) {
            a.style.position = "static";
            a.style.width = "100%";
            a.style.top = "";
            document.body.style.backgroundImage = "";
            document.body.style.backgroundAttachment = ""
        } else {
            a.style.position = "";
            (typeof b.top !== "undefined") && (a.style.top = "");
            (typeof b.bottom !== "undefined") && (a.style.bottom = "");
            (typeof b.left !== "undefined") && (a.style.left = "");
            (typeof b.right !== "undefined") && (a.style.right = "")
        }
    }

    if (l && r) {
        var n = false, o = null;
        var p = function () {
            var a = 0, b = 0;
            if (typeof(window.pageYOffset) == "number") {
                b = window.pageYOffset;
                a = window.pageXOffset
            } else {
                if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                    b = document.body.scrollTop;
                    a = document.body.scrollLeft
                } else {
                    if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                        b = document.documentElement.scrollTop;
                        a = document.documentElement.scrollLeft
                    }
                }
            }
            return [a, b]
        };
        var q = function () {
            var b = 0, a = 0;
            if (typeof(window.innerWidth) == "number") {
                b = window.innerWidth;
                a = window.innerHeight
            } else {
                if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                    b = document.documentElement.clientWidth;
                    a = document.documentElement.clientHeight
                } else {
                    if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                        b = document.body.clientWidth;
                        a = document.body.clientHeight
                    }
                }
            }
            return [b, a]
        };
        var k = function () {
            var a = Position.cumulativeOffset(l.up())[1];
            var c = p()[1];
            var b = Position.cumulativeOffset(r)[1] + r.offsetHeight;
            if (c > (b - l.offsetHeight) || c < a) {
                if (n) {
                    n = false;
                    s(l, {top: 0})
                }
            } else {
                if (c > a && !n) {
                    m(l, {top: 0});
                    n = true
                }
            }
        };
        Event.observe(window, "scroll", k);
        Event.observe(window, "resize", k);
        return function () {
            Event.stopObserving(window, "scroll", k);
            Event.stopObserving(window, "resize", k)
        }
    } else {
        return null
    }
};
var AutoHeight = Class.create();
Object.extend(AutoHeight, {
    getXY: function (h) {
        var j = 0, k = 0;
        if (h.getBoundingClientRect) {
            var a = h.getBoundingClientRect();
            var b = document.documentElement;
            j = a.left + Math.max(b.scrollLeft, document.body.scrollLeft) - b.clientLeft;
            k = a.top + Math.max(b.scrollTop, document.body.scrollTop) - b.clientTop
        } else {
            for (; h != document.body; j += h.offsetLeft, k += h.offsetTop, h = h.offsetParent) {
            }
        }
        return {x: j, y: k}
    },
    cssList: ["font-family", "font-size", "font-style", "font-weight", "font-variant", "line-height", "margin-top", "margin-bottom", "margin-left", "margin-right", "overflow-x", "overflow-y", "padding-top", "padding-bottom", "padding-left", "padding-right", "word-break", "word-wrap"],
    startAuto: function (b) {
        b = $(b);
        lengthExpMax = 210;
        normalTemplate = "还可以输入{0}字";
        warnTemplate = '<span class="c_red">字数超出{0}字</span>';
        this.handler = AutoHeight.auto.bind(b);
        Event.observe(b, "blur", this.handler);
        Event.observe(b, "focus", this.handler);
        Event.observe(b, "keyup", this.handler);
        Event.observe(b, "mouseup", this.handler);
        return AutoHeight.stopAuto.bind(b, this.handler)
    },
    auto: function (q) {
        var z = this.up(".ele_replay").down('[_type="fd"]');
        var x = this.up();
        if (this.value.length == 0) {
            this.style.height = "";
            z.innerHTML = String.format(normalTemplate, [lengthExpMax]);
            if (q.type === "blur") {
                AutoHeight.stopAuto(this)
            }
            return
        }
        if (!AutoHeight.mirrorCtn) {
            AutoHeight.mirrorCtn = $(Builder.node("div"));
            document.body.appendChild(AutoHeight.mirrorCtn);
            AutoHeight.mirrorCtn.style.opacity = "0";
            AutoHeight.mirrorCtn.style.zIndex = -1000;
            AutoHeight.mirrorCtn.style.position = "absolute"
        }
        mirrorCtn = AutoHeight.mirrorCtn;
        cssList = AutoHeight.cssList;
        styleObj = {};
        for (var r = 0, s = cssList.length, l, y, u; r < s; ++r) {
            l = cssList[r];
            if ((y = this.getStyle(l))) {
                styleObj[l] = y
            }
        }
        mirrorCtn.setStyle(styleObj);
        mirrorCtn.setStyle({width: this.offsetWidth + "px"});
        var A = this.value;
        if (A.lastIndexOf("\n") != -1 && A.lastIndexOf("\n") + 1 == A.length) {
            A += "占"
        }
        mirrorCtn.innerHTML = A.replace(/\n/g, "<br/>");
        var w = AutoHeight.getXY(this);
        mirrorCtn.style.top = w.y + "px";
        mirrorCtn.style.left = w.x + "px";
        var v = mirrorCtn.offsetHeight;
        if (v < 60) {
            v = 60
        }
        x.style.height = this.style.height = v + "px";
        if (A) {
            var t = Widgets.Utils.strLen(A), o;
            if (t > 0) {
                o = lengthExpMax - t;
                if (o >= 0) {
                    z.innerHTML = String.format(normalTemplate, [o])
                } else {
                    z.innerHTML = String.format(warnTemplate, [Math.abs(o)])
                }
            }
        }
        z = x = v = null
    },
    stopAuto: function (e, f) {
        var g = e.up();
        var h = g.up(".ele_replay").down(".mt6");
        h && h.remove();
        e.value = "发表评论";
        if (f) {
            g.removeClassName("focus");
            e.style.height = "30px";
            g.style.height = "30px"
        }
        e.setAttribute("toggle", "0");
        Event.stopObserving(e, "blur", AutoHeight.handler);
        Event.stopObserving(e, "focus", AutoHeight.handler);
        Event.stopObserving(e, "keyup", AutoHeight.handler);
        Event.stopObserving(e, "mouseup", AutoHeight.handler)
    }
});
!function () {
    function m(f, e) {
        var d = 0, a = f.split(""), p = [];
        e = (e || 12) * 2;
        var c = a.length;
        for (var b = 0; b < c; ++b) {
            if (a[b].charCodeAt(0) < 299) {
                d++
            } else {
                d += 2
            }
            p.push(a[b]);
            if (d >= e - 2) {
                p.push("..");
                break
            }
        }
        return p.join("")
    }

    function l(e) {
        var d = 0, a = e.split("");
        var c = a.length;
        for (var b = 0; b < c; ++b) {
            if (a[b].charCodeAt(0) < 299) {
                d++
            } else {
                d += 2
            }
        }
        return Math.ceil(d / 2)
    }

    function k(b) {
        var e = 0, a, c = b.length;
        for (var d = 0; d < c; d++) {
            a = l(b[d]);
            if (a > e) {
                e = a
            }
        }
        return e
    }

    function g(b) {
        var y = b.value;
        var a = this.addLength || 0;
        this.lengthExp = this.lengthExp || {};
        this.lengthExp.max = this.lengthExp.max || 210;
        this.lengthExp.min = this.lengthExp.min || 41;
        this.lengthExp.surl = this.lengthExp.surl || 21;
        var c = y.trim().length;
        if (c > 0 && y != this.defaultText && y.trim() != "发表评论") {
            var n = this.lengthExp.min, f = this.lengthExp.max, x = this.lengthExp.surl, o = y;
            var d = 0;
            if (!this.notShortUrl) {
                var z = y.match(/(http|https):\/\/[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)+([-A-Z0-9a-z\$\.\+\!\_\*\(\)\/\,\:;@&=\?~#%]*)*/gi) || [];
                for (var e = 0, c = z.length; e < c; e++) {
                    var w = z[e].bytelength2();
                    if (/^(http:\/\/murl.tv)/.test(z[e])) {
                        continue
                    } else {
                        d += w <= f ? x : (w - f + x)
                    }
                    o = o.replace(z[e], "")
                }
            }
            return Math.ceil((d + o.bytelength2()) / 2) + a
        } else {
            return 0 + a
        }
    }

    function h(r, d, q, f) {
        var a = [];
        var c, b, s = Math.round((f * r) / d), e = Math.round(q * d / r);
        if (s <= q) {
            c = s;
            b = f
        } else {
            if (e <= f) {
                c = q;
                b = e
            }
        }
        a[0] = c;
        a[1] = b;
        return a
    }

    function j(c) {
        var d = 0, e = 0;
        if (c.getBoundingClientRect) {
            var a = c.getBoundingClientRect();
            var b = document.documentElement;
            d = Math.round(a.left + Math.max(b.scrollLeft, document.body.scrollLeft) - b.clientLeft);
            e = Math.round(a.top + Math.max(b.scrollTop, document.body.scrollTop) - b.clientTop)
        } else {
            for (; c != document.body; d += c.offsetLeft, e += c.offsetTop, c = c.offsetParent) {
            }
        }
        return {x: d, y: e}
    }

    Widgets.Utils = {strShort: m, strLen: l, strArrayMax: k, getLength: g, getRatioSize: h, getXY: j}
}();
Fx.Move = Class.create();
Object.extend(Object.extend(Fx.Move.prototype, Fx.Base.prototype), {
    setup: function (c) {
        var d;
        if (this.options.distances) {
            d = this.options.distances;
            this.currentLeft = d.left;
            this.currentTop = d.top;
            this.originalPosition = d.position;
            this.factor = 1
        } else {
            this.steps = -1
        }
    }, update: function (j, l) {
        var d = this.factor * l;
        var k = this.currentLeft * d;
        var m = this.currentTop * d;
        var h = {};
        if (j.options.scaleX) {
            h.left = this.originalPosition + Math.round(k) + "px"
        }
        if (j.options.scaleY) {
            h.top = this.originalPosition + Math.round(m) + "px"
        }
        this.element.setStyle(h)
    }, end: function (b) {
        if (b.options.callback) {
            b.options.callback()
        }
    }
});
Widgets.Comment = Class.create();
Object.extend(Widgets.Comment.prototype, Widgets.Base.prototype);
Object.extend(Widgets.Comment.prototype, {
    options: {
        container: "",
        lengthExpMax: 210,
        pageClient: null,
        type: "",
        commentTemplate: '<div class="mt6"><a method="submit" href="#" onclick="return false;" class="replaybtn">回复</a><p class="pt3 c_a5" _type="fd">还可以输入210字</p></div>',
        commentCollBackTemplate: '<div class="pic_32 mt20"><a href="{0}" target="_blank" title="{1}"><img src="{2}" width="32" height="32" alt="{1}"></a><p class="px14"><a href="{0}" target="_blank">{1}</a></p><p class="mt3 px12">{3}</p><p class="mt5 px12">{4}</p></div>',
        commentCollBackTemplate1: '<div class="pic_32 mt20"><a href="{0}" target="_blank" title="{1}"><img src="{2}" width="32" height="32" alt="{1}"></a><p class="px14"><a href="{0}" target="_blank">{1}</a><span class="pl25">{3}</span></p><p class="mt5 px12">{4}</p></div>'
    }, server: {
        AddTweetComment: function (p, q, n, l, k, o, j) {
            var m = Mtime.isValue(n) ? [p, q, n, l, false, 0, false, k] : [p, q, 0, l, false, 0, false, k];
            return Mtime.Component.Ajax.crossDomainCallBack(siteServiceUrl, "Mtime.Service.Pages.TwitterService", "AddTweetCommentWithOptionCrossDomainByFlash", m, j, "/Service/Twitter.msi", "post", "20000", o)
        }
    }, initializeDom: function () {
        this.$contentRegion = $(this.container);
        this.editors = []
    }, initializeEvent: function () {
        this.clickRegionHandler = this.onClickRegion.bind(this);
        Event.observe(this.$contentRegion, "click", this.clickRegionHandler);
        Event.observe(window, "unload", this.close.bind(this))
    }, onClickRegion: function (u) {
        var s = Event.element(u);
        var w = s.readAttribute("method");
        while (!w && s != document.body) {
            s = s.up();
            w = s.readAttribute("method")
        }
        if (!w) {
            return
        }
        switch (w) {
            case"comment":
                Event.stop(u);
                var E = s.readAttribute("toggle");
                if (typeof E === "undefined" || E !== "1") {
                    s.setAttribute("toggle", "1")
                } else {
                    return
                }
                var G = s.up(".mod_short");
                this.tweetid = G.readAttribute("tweetid");
                this.oldClickElementHtml = s.value;
                this.textarea = s;
                s.value = "";
                s.style.height = "60px";
                var y = s.up(".ele_replay");
                var A = s.up();
                A.style.height = "60px";
                A.addClassName("focus");
                var x = Builder.build(this.commentTemplate);
                y && y.appendChild(x);
                AutoHeight.startAuto(s);
                break;
            case"submit":
                Event.stop(u);
                var G = s.up(".mod_short");
                var F = G.readAttribute("tweetid");
                var r = G.down('[region="newcomment"]');
                var D = G.down("textarea");
                var t = D.value;
                var C;
                var B;
                B = B ? B.checked : false;
                var v = Widgets.Utils.strLen(t);
                if (v > 0 && v <= this.lengthExpMax && !/^发表评论.*:$/.test(t)) {
                    var z = null;
                    if (/发表评论.*:/.test(t)) {
                        z = this.replyParentid
                    }
                    if (this.pageClient && !this.pageClient.isLogin) {
                        if (typeof UserLoginDialog !== "undefined") {
                            new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        } else {
                            $loadJs("/js/2014/UserLoginDialog.js", function () {
                                new UserLoginDialog({
                                    callback: function () {
                                        location.reload()
                                    }
                                })
                            }.bind(this))
                        }
                        return
                    }
                    this.server.AddTweetComment(t, F, this.replyParentid, C, B, s, function (b) {
                        if (b && b.value && this.addCommentErrorCheck(b)) {
                            if (!r) {
                                return
                            }
                            var c;
                            if (this.type === "") {
                                c = this.commentCollBackTemplate
                            } else {
                                if (this.type === "movie") {
                                    c = this.commentCollBackTemplate1
                                }
                            }
                            var a = String.format(c, [b.value.commentUserUrl, b.value.commentUserNickName, b.value.commentUserAvatar, b.value.commentTime, b.value.commentTextBody]);
                            r.innerHTML = a;
                            AutoHeight.stopAuto(D)
                        }
                    }.bind(this))
                } else {
                    if (v == 0 || /^发表评论.*:$/.test(t)) {
                        $alert("你还没有填写内容，请填写后提交。", null, null, 4)
                    } else {
                        $alert("内容过长,请确认内容字符长度小于" + editor.lengthExp.max + "。", null, null, 4)
                    }
                    AutoHeight.stopAuto(D)
                }
                break
        }
    }, addCommentErrorCheck: function (j) {
        var k = parseInt(j.value.success);
        if (k === 1) {
            var f = function (a) {
                switch (a) {
                    case 0:
                        return "保存失败.";
                    case -1:
                        return "内容过长.";
                    case -2:
                        return "内容为空.";
                    case -3:
                        return "非法的用户.";
                    case -4:
                        return "微评不存在.";
                    case -5:
                        return "原回复已被删除.";
                    case -6:
                        return "在对方的黑名单中.";
                    case -7:
                        return "原作者微评正在审核中，如有不便敬请谅解！";
                    default:
                        return ""
                }
            };
            var h = function (a) {
                switch (a) {
                    case 0:
                        return "保存失败.";
                    case -1:
                        return "内容过长.";
                    case -2:
                        return "非法的用户.";
                    case -3:
                        return "原微评不存在.";
                    case -4:
                        return "原微评不存在.";
                    case -5:
                        return "原作者微评正在审核中，如有不便敬请谅解！";
                    default:
                        return ""
                }
            };
            var g = "";
            if (parseInt(j.value.commentId, 10) <= 0) {
                g = f(parseInt(j.value.commentId, 10))
            }
            if (!g && j.value.isForward && parseInt(j.value.forwardTweetId, 10) <= 0) {
                g = h(parseInt(j.value.forwardTweetId, 10))
            }
            if (!g && j.value.isCommentToRoot && parseInt(j.value.commentToRootId, 10) <= 0) {
                g = f(parseInt(j.value.commentToRootId, 10))
            }
            if (!g && parseInt(j.value.replyResult, 10) != 1) {
                g = j.value.replyError || ""
            }
            if (g) {
                $alert(g, null, null, 4);
                return false
            } else {
                return true
            }
        } else {
            if (k === -4) {
                $alert("你的评论有被禁止的内容，请修改后发布。", null, null, 4);
                return false
            } else {
                if (k === -5) {
                    $alert("你的评论有被禁止的内容，请修改后发布。", null, null, 4);
                    return false
                } else {
                    if (k === -20) {
                        $alert("你发布的速度太快了，休息一下吧：）", null, null, 4);
                        return false
                    } else {
                        if (k === -30) {
                            $alert("非法用户", null, null, 4);
                            return false
                        }
                    }
                }
            }
        }
    }
});
Widgets.BlogComment = Class.create();
Object.extend(Widgets.BlogComment.prototype, Widgets.Comment.prototype);
Object.extend(Widgets.BlogComment.prototype, {
    options: {
        container: "",
        lengthExpMax: 210,
        pageClient: null,
        clickTextareaCallBack: null,
        blurTextareaCallBack: null,
        isStyleChange: true,
        callback: null,
        commentTemplate: '<div class="mt6"><a method="submit" href="#" onclick="return false;" class="replaybtn">回复</a><p class="pt3 c_a5" _type="fd">还可以输入210字</p></div>',
        commentCollBackTemplate: '<div class="pic_32 mt20"><a href="{0}" target="_blank" title="{1}"><img src="{2}" width="32" height="32" alt="{1}"></a><p class="px14"><a href="{0}" target="_blank">{1}</a></p><p class="mt3 px12">{3}</p><p class="mt5 px12">{4}</p></div>'
    }, onClickRegion: function (p) {
        var n = Event.element(p);
        var r = n.readAttribute("method");
        while (!r && n != document.body) {
            n = n.up();
            r = n.readAttribute("method")
        }
        if (!r) {
            return
        }
        switch (r) {
            case"comment":
                Event.stop(p);
                var y = n.getAttribute("toggle");
                this.clickElement = n;
                if (typeof y === "undefined" || y !== "1") {
                    n.setAttribute("toggle", "1");
                    var t = n.getAttribute("objid");
                    var u = n.getAttribute("objtypeid");
                    this.clickTextareaCallBack && this.clickTextareaCallBack(u, t, n)
                } else {
                    return
                }
                this.oldClickElementHtml = n.value;
                this.textarea = n;
                n.value = "";
                n.style.height = "60px";
                var v = n.up(".ele_replay");
                if (this.isStyleChange) {
                    var w = n.up();
                    w.style.height = "60px";
                    w.addClassName("focus")
                }
                var s = Builder.build(this.commentTemplate);
                v && v.appendChild(s);
                AutoHeight.startAuto(n);
                this.clickElementHandler = this.elementBlur.bind(this);
                Event.observe(n, "blur", this.clickElementHandler);
                break;
            case"submit":
                Event.stop(p);
                var v = n.up(".ele_replay");
                var x = v.down("textarea");
                var t = x.getAttribute("objid");
                var u = x.getAttribute("objtypeid");
                var o = x.value;
                var q = Widgets.Utils.strLen(o);
                if (q > 0 && q <= this.lengthExpMax && !/^发表评论.*:$/.test(o)) {
                    if (this.pageClient && !this.pageClient.isLogin) {
                        if (typeof UserLoginDialog !== "undefined") {
                            new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        } else {
                            $loadJs("/js/2014/UserLoginDialog.js", function () {
                                new UserLoginDialog({
                                    callback: function () {
                                        location.reload()
                                    }
                                })
                            }.bind(this))
                        }
                        return
                    }
                    this.callback && this.callback(o, u, t, x);
                    if (!this.isStyleChange) {
                        AutoHeight.stopAuto(x, this.isStyleChange)
                    }
                } else {
                    if (q == 0 || /^发表评论.*:$/.test(o)) {
                        $alert("你还没有填写内容，请填写后提交。", null, null, 4)
                    } else {
                        $alert("内容过长,请确认内容字符长度小于" + editor.lengthExp.max + "。", null, null, 4)
                    }
                    AutoHeight.stopAuto(x, this.isStyleChange)
                }
                break
        }
    }, elementBlur: function (d) {
        if (this.clickElement) {
            var e = this.clickElement.getAttribute("objid");
            var f = this.clickElement.getAttribute("objtypeid");
            this.blurTextareaCallBack && this.blurTextareaCallBack(f, e, this.clickElement);
            Event.stopObserving(this, "blur", this.clickElementHandler);
            e = f = this.clickElement = null
        }
    }
});
var GoodsComment = Class.create();
Object.extend(GoodsComment.prototype, Widgets.Comment.prototype);
Object.extend(GoodsComment.prototype, {
    options: {
        container: "",
        lengthExpMax: 210,
        focusClassName: null,
        commentTemplate: '<div class="mt6"><a method="submit" href="#" onclick="return false;" class="replaybtn">回复</a><p class="pt3 c_a5" _type="fd">还可以输入210字</p></div>',
        callback: null,
        clickTextareaCallBack: null,
        blurTextareaCallBack: null
    }, onClickRegion: function (o) {
        var m = Event.element(o);
        var q = m.readAttribute("method");
        while (!q && m != document.body) {
            m = m.up();
            q = m.readAttribute("method")
        }
        if (!q) {
            var u = $$(".ele_replay")[0];
            var v = u.down("textarea");
            NewAutoHeight.stopAuto(v);
            return
        }
        switch (q) {
            case"comment":
                Event.stop(o);
                var w = m.getAttribute("toggle");
                this.clickElement = m;
                if (typeof w === "undefined" || w !== "1") {
                    m.setAttribute("toggle", "1");
                    var s = m.getAttribute("objid");
                    var t = m.getAttribute("objtypeid");
                    this.clickTextareaCallBack && this.clickTextareaCallBack(t, s, m)
                } else {
                    return
                }
                this.oldClickElementHtml = m.value;
                this.textarea = m;
                m.value = "";
                m.style.height = "60px";
                var u = m.up(".ele_replay");
                var r = Builder.build(this.commentTemplate);
                if (u) {
                    u.appendChild(r);
                    if (this.focusClassName && !u.hasClassName(this.focusClassName)) {
                        u.addClassName(this.focusClassName)
                    }
                }
                NewAutoHeight.startAuto(m, this.focusClassName);
                break;
            case"submit":
                Event.stop(o);
                var u = m.up(".ele_replay");
                var v = u.down("textarea");
                var s = v.getAttribute("objid");
                var t = v.getAttribute("objtypeid");
                var n = v.value;
                var p = Widgets.Utils.strLen(n);
                if (p > 0 && p <= this.lengthExpMax && !/^发表评论.*$/.test(n)) {
                    this.callback && this.callback(n, t, s, v);
                    NewAutoHeight.stopAuto(v)
                } else {
                    if (p == 0 || /^发表评论.*$/.test(n)) {
                        $alert("你还没有填写内容，请填写后提交。", null, null, 4)
                    } else {
                        NewAutoHeight.stopAuto(v);
                        $alert("内容过长,请确认内容字符长度小于" + this.lengthExpMax + "。", null, null, 4)
                    }
                }
                break
        }
    }
});
var NewAutoHeight = Class.create();
Object.extend(NewAutoHeight, {
    getXY: function (h) {
        var j = 0, k = 0;
        if (h.getBoundingClientRect) {
            var a = h.getBoundingClientRect();
            var b = document.documentElement;
            j = a.left + Math.max(b.scrollLeft, document.body.scrollLeft) - b.clientLeft;
            k = a.top + Math.max(b.scrollTop, document.body.scrollTop) - b.clientTop
        } else {
            for (; h != document.body; j += h.offsetLeft, k += h.offsetTop, h = h.offsetParent) {
            }
        }
        return {x: j, y: k}
    },
    cssList: ["font-family", "font-size", "font-style", "font-weight", "font-variant", "line-height", "margin-top", "margin-bottom", "margin-left", "margin-right", "overflow-x", "overflow-y", "padding-top", "padding-bottom", "padding-left", "padding-right", "word-break", "word-wrap"],
    startAuto: function (c, d) {
        c = $(c);
        lengthExpMax = 210;
        normalTemplate = "还可以输入{0}字";
        NewAutoHeight.focusClassName = d;
        warnTemplate = '<span class="c_red">字数超出{0}字</span>';
        this.handler = NewAutoHeight.auto.bind(c);
        Event.observe(c, "blur", this.handler);
        Event.observe(c, "focus", this.handler);
        Event.observe(c, "keyup", this.handler);
        Event.observe(c, "mouseup", this.handler);
        return NewAutoHeight.stopAuto.bind(c, this.handler)
    },
    auto: function (q) {
        var z = this.up(".ele_replay").down('[_type="fd"]');
        var x = this.up();
        if (this.value.length == 0) {
            return
        }
        if (!NewAutoHeight.mirrorCtn) {
            NewAutoHeight.mirrorCtn = $(Builder.node("div"));
            document.body.appendChild(NewAutoHeight.mirrorCtn);
            NewAutoHeight.mirrorCtn.style.opacity = "0";
            NewAutoHeight.mirrorCtn.style.zIndex = -1000;
            NewAutoHeight.mirrorCtn.style.position = "absolute"
        }
        mirrorCtn = NewAutoHeight.mirrorCtn;
        cssList = NewAutoHeight.cssList;
        styleObj = {};
        for (var r = 0, s = cssList.length, l, y, u; r < s; ++r) {
            l = cssList[r];
            if ((y = this.getStyle(l))) {
                styleObj[l] = y
            }
        }
        mirrorCtn.setStyle(styleObj);
        mirrorCtn.setStyle({width: this.offsetWidth + "px"});
        var A = this.value;
        if (A.lastIndexOf("\n") != -1 && A.lastIndexOf("\n") + 1 == A.length) {
            A += "占"
        }
        mirrorCtn.innerHTML = A.replace(/\n/g, "<br/>");
        var w = NewAutoHeight.getXY(this);
        mirrorCtn.style.top = w.y + "px";
        mirrorCtn.style.left = w.x + "px";
        var v = mirrorCtn.offsetHeight;
        if (v < 60) {
            v = 60
        }
        if (A) {
            var t = Widgets.Utils.strLen(A), o;
            if (t > 0) {
                o = lengthExpMax - t;
                if (o >= 0) {
                    z.innerHTML = String.format(normalTemplate, [o])
                } else {
                    z.innerHTML = String.format(warnTemplate, [Math.abs(o)])
                }
            }
        }
        z = x = v = null
    },
    stopAuto: function (d) {
        var e = d.up(".ele_replay");
        if (!e) {
            return
        }
        var f = e.down(".mt6");
        f && f.remove();
        d.value = "发表评论";
        if (NewAutoHeight.focusClassName && e.hasClassName(NewAutoHeight.focusClassName)) {
            e.removeClassName(NewAutoHeight.focusClassName)
        }
        e.style.height = d.style.height = "40px";
        d.setAttribute("toggle", "0");
        Event.stopObserving(d, "blur", NewAutoHeight.handler);
        Event.stopObserving(d, "focus", NewAutoHeight.handler);
        Event.stopObserving(d, "keyup", NewAutoHeight.handler);
        Event.stopObserving(d, "mouseup", NewAutoHeight.handler)
    }
});
Widgets.IndexTopSlidesControl = Class.create();
Object.extend(Widgets.IndexTopSlidesControl.prototype, Widgets.BehindSlidesControl.prototype);
Object.extend(Widgets.IndexTopSlidesControl.prototype, {
    options: {
        slidesRegion: "movieSlidesItems",
        dotSlidesRegion: "",
        backSlidesRegion: "",
        txtSlidesRegion: "",
        advertisementRegion: "",
        prev: "prev",
        next: "next",
        autoTimeout: null,
        items: [],
        imageTemplate: "",
        duration: 0.5,
        type: "neat",
        toRemoveFrames: []
    }, initializeDom: function () {
        this.removeFrames(this.toRemoveFrames);
        this.$slidesRegion = $(this.slidesRegion);
        if (!this.$slidesRegion) {
            return
        }
        this.$presentNumber = $(this.presentNumber);
        this.$sumNumber = $(this.sumNumber);
        this.$titleTxt = $(this.titleTxt);
        this.$describeTxt = $(this.describeTxt);
        this.$images = this.$slidesRegion.immediateDescendants();
        this.imagesLen = this.$images.length;
        if (this.$sumNumber) {
            this.$sumNumber.innerHTML = "/" + this.imagesLen
        }
        this.presentNum = 1;
        if (this.$presentNumber) {
            this.$presentNumber.innerHTML = this.presentNum
        }
        if (this.$titleTxt) {
            this.$titleTxt.innerHTML = Widgets.Utils.strShort(this.$images[0].readAttribute("title"), 24)
        }
        if (this.$describeTxt) {
            this.$describeTxt.innerHTML = Widgets.Utils.strShort(this.$images[0].readAttribute("imgabstract"), 46)
        }
    }, removeFrames: function (d) {
        if (!d || d.length == 0) {
            return
        }
        this.$slidesRegion = $(this.slidesRegion);
        if (this.$slidesRegion) {
            this.$slidesRegions = this.$slidesRegion.immediateDescendants()
        }
        this.$dotSlidesRegion = $(this.dotSlidesRegion);
        if (this.$dotSlidesRegion) {
            this.$dotSlidesRegions = this.$dotSlidesRegion.immediateDescendants()
        }
        this.$backSlidesRegion = $(this.backSlidesRegion);
        if (this.$backSlidesRegion) {
            this.$backSlidesRegions = this.$backSlidesRegion.immediateDescendants()
        }
        this.$txtSlidesRegion = $(this.txtSlidesRegion);
        if (this.$txtSlidesRegion) {
            this.$txtSlidesRegions = this.$txtSlidesRegion.immediateDescendants()
        }
        this.$advertisementRegion = $(this.advertisementRegion);
        if (this.$advertisementRegion) {
            this.$advertisementRegions = this.$advertisementRegion.immediateDescendants()
        }
        for (var c = 0; c < d.length; c++) {
            this.removeFrame(this.$slidesRegions, d[c]);
            this.removeFrame(this.$dotSlidesRegions, d[c]);
            this.removeFrame(this.$backSlidesRegions, d[c]);
            this.removeFrame(this.$txtSlidesRegions, d[c]);
            this.removeFrame(this.$advertisementRegions, d[c])
        }
    }, removeFrame: function (d, c) {
        if (!d || d.length == 0) {
            return
        }
        if (d.length > c) {
            $(d[c]).remove()
        }
    }, destroyDOM: function () {
        this.$presentNumber = null;
        this.$sumNumber = null;
        this.$titleTxt = null;
        this.$describeTxt = null;
        this.$images = null;
        this.imagesLen = null;
        if (this.mySlidesControl) {
            this.mySlidesControl.close()
        }
    }, instanceSlidesControl: function () {
        if (!this.$slidesRegion) {
            return
        }
        var b = this;
        this.mySlidesControl = new SlidesControl({
            slidesRegion: this.slidesRegion,
            prev: this.prev,
            next: this.next,
            scale: this.$images[0].offsetWidth,
            duration: this.duration,
            type: "neat",
            autoTimeout: this.autoTimeout,
            initializeField: function () {
                this.slidesRegion = $(this.slidesRegion);
                this.prev = $(this.prev);
                this.next = $(this.next);
                this.currentIndex = 0;
                this.position = 0;
                this.length = 0;
                if (this.slidesRegion) {
                    this.items = this.slidesRegion.immediateDescendants();
                    this.length = this.items.length;
                    this.clientHeight = document.documentElement.clientHeight || document.body.clientHeight
                }
                this.lock = false;
                this.$dotSlidesRegion = $(b.dotSlidesRegion);
                if (this.$dotSlidesRegion) {
                    this.$dotSlidesRegions = this.$dotSlidesRegion.immediateDescendants()
                }
                this.$backSlidesRegion = $(b.backSlidesRegion);
                if (this.$backSlidesRegion) {
                    this.$backSlidesRegions = this.$backSlidesRegion.immediateDescendants()
                }
                this.$txtSlidesRegion = $(b.txtSlidesRegion);
                if (this.$txtSlidesRegion) {
                    this.$txtSlidesRegions = this.$txtSlidesRegion.immediateDescendants();
                    if (this.$txtSlidesRegions) {
                        var a = this.$txtSlidesRegions[0];
                        var f = a.getAttribute("rectype");
                        if (f === "special") {
                            this.$txtSlidesRegion.style.zIndex = 1
                        }
                    }
                }
                this.$adRegion = $(b.advertisementRegion);
                if (this.$adRegion) {
                    this.$adRegion.style.display = "none";
                    this.$adRegions = this.$adRegion.immediateDescendants()
                }
                this.prevIndex = 0;
                for (var e = 0; e < this.length; e++) {
                    if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                        this.$adRegions[e].adClose = this.$adRegions[e].down('i[method="close"]');
                        this.$adRegions[e].adTip = this.$adRegions[e].down('div[method="tip"]');
                        if (this.$adRegions[e].adTip && this.$adRegions[e].adTip.childElements().length > 0) {
                            this.$adRegions[e].removeClassName("none");
                            this.$adRegions[e].setAttribute("tiptype", "1")
                        }
                    }
                    if (e === 0) {
                        this.items[e].style.opacity = 1;
                        this.items[e].style.zIndex = 3;
                        if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                            this.$adRegions[e].style.opacity = 1;
                            this.$adRegions[e].style.zIndex = 3;
                            if (this.$adRegions[e].adTip && this.$adRegions[e].adTip.childElements().length > 0) {
                                this.$adRegion.style.display = "block"
                            }
                        }
                        if (this.$txtSlidesRegion && this.$txtSlidesRegions && this.$txtSlidesRegions.length > 0) {
                            this.$txtSlidesRegions[e].style.opacity = 1;
                            this.$txtSlidesRegions[e].style.display = "block"
                        }
                        if (this.$dotSlidesRegion && this.$dotSlidesRegions && this.$dotSlidesRegions.length > 0) {
                            if (this.$dotSlidesRegions.length == 1) {
                                this.$dotSlidesRegion.style.display = "none";
                                if (this.prev) {
                                    this.prev.style.display = "none"
                                }
                                if (this.next) {
                                    this.next.style.display = "none"
                                }
                            } else {
                                this.$dotSlidesRegions[e].addClassName("on")
                            }
                        }
                    } else {
                        if (Mtime.browser.ie > 0 && Mtime.browser.ie <= 9) {
                            this.items[e].style.display = "none"
                        }
                        this.items[e].style.opacity = 0;
                        this.items[e].style.zIndex = 1;
                        if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                            this.$adRegions[e].style.opacity = 0;
                            this.$adRegions[e].style.zIndex = 1
                        }
                    }
                }
                this.timeOutMethod()
            },
            initializeEvent: function () {
                this.slidesRegionOverHendler = this.slidesRegionOver.bind(this);
                this.slidesRegionOutHendler = this.slidesRegionOut.bind(this);
                this.slidesRegionClickHendler = this.slidesRegionClick.bind(this);
                if (this.slidesRegion) {
                    Event.observe(this.slidesRegion, "mouseover", this.slidesRegionOverHendler);
                    Event.observe(this.slidesRegion, "mouseout", this.slidesRegionOutHendler)
                }
                if (this.$adRegion) {
                    Event.observe(this.$adRegion, "mouseover", this.slidesRegionOverHendler);
                    Event.observe(this.$adRegion, "mouseout", this.slidesRegionOutHendler);
                    Event.observe(this.$adRegion, "click", this.slidesRegionClickHendler)
                }
                if (this.prev) {
                    this.onPrevClickHandler = this.onPrevClick.bind(this);
                    Event.observe(this.prev, "click", this.onPrevClickHandler);
                    Event.observe(this.prev, "mouseover", this.slidesRegionOverHendler);
                    Event.observe(this.prev, "mouseout", this.slidesRegionOutHendler)
                }
                if (this.next) {
                    this.onNextClickHandler = this.onNextClick.bind(this);
                    Event.observe(this.next, "click", this.onNextClickHandler);
                    Event.observe(this.next, "mouseover", this.slidesRegionOverHendler);
                    Event.observe(this.next, "mouseout", this.slidesRegionOutHendler)
                }
                if (this.$dotSlidesRegion) {
                    this.dotSlidesRegionClickHandler = this.dotSlidesRegionClick.bind(this);
                    Event.observe(this.$dotSlidesRegion, "click", this.dotSlidesRegionClickHandler);
                    Event.observe(this.$dotSlidesRegion, "mouseover", this.slidesRegionOverHendler);
                    Event.observe(this.$dotSlidesRegion, "mouseout", this.slidesRegionOutHendler)
                }
                Event.observe(window, "unload", this.close.bind(this))
            },
            onPrevClick: function (a) {
                var d = a.target || a.srcElement;
                Event.stop(a);
                if (this.lock) {
                    return
                }
                if (this.amount && this.currentIndex < 1) {
                    this.currentIndex = this.amount - 1
                } else {
                    if (this.currentIndex < 1) {
                        this.currentIndex = this.length - 1
                    } else {
                        this.currentIndex--
                    }
                }
                this.myTimeout && clearTimeout(this.myTimeout);
                this.move()
            },
            onNextClick: function (a) {
                var d = a.target || a.srcElement;
                Event.stop(a);
                if (this.lock) {
                    return
                }
                if (this.amount && this.currentIndex > this.amount - 2) {
                    this.currentIndex = 0
                } else {
                    if (this.currentIndex > this.length - 2) {
                        this.currentIndex = 0
                    } else {
                        this.currentIndex++
                    }
                }
                this.myTimeout && clearTimeout(this.myTimeout);
                this.move()
            },
            move: function (a) {
                this.lock = true;
                this.slidesRegionMethod();
                this.backSlidesRegionsMethod();
                this.dotSlidesRegionsMethod();
                this.txtSlidesRegionsMethod();
                this.prevIndex = this.currentIndex
            },
            slidesRegionMethod: function () {
                if (this.slidesRegion) {
                    var q = this.items[this.prevIndex], p = this.items[this.currentIndex], a, l, o, n, k, m;
                    if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                        a = this.$adRegions[this.prevIndex];
                        l = this.$adRegions[this.currentIndex];
                        o = this.$adRegions[this.currentIndex].adTip;
                        n = l.getAttribute("tiptype");
                        k = a.getAttribute("tiptype");
                        m = l.getAttribute("tiptype")
                    }
                    if (q !== p) {
                        if (!q.hasClassName("transition8")) {
                            q.addClassName("transition8")
                        }
                        if (!p.hasClassName("transition8")) {
                            p.addClassName("transition8")
                        }
                        if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                            if (!a.hasClassName("transition8")) {
                                a.addClassName("transition8")
                            }
                            if (!l.hasClassName("transition8")) {
                                l.addClassName("transition8")
                            }
                            this.beforeAdMthod(l, o, n)
                        }
                        p.style.zIndex = 3;
                        q.style.zIndex = 1;
                        q.style.opacity = 0;
                        p.style.opacity = 1;
                        if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                            n = l.getAttribute("tiptype");
                            if (n) {
                                a.style.zIndex = 1;
                                l.style.zIndex = 3;
                                a.style.opacity = 0;
                                l.style.opacity = 1;
                                a.style.filter = "alpha(opacity=0)";
                                l.style.filter = "alpha(opacity=100)"
                            } else {
                                if (this.$adRegion) {
                                    this.$adRegion.style.display = "none";
                                    a.addClassName("none");
                                    l.addClassName("none");
                                    a.style.zIndex = 1;
                                    l.style.zIndex = 1;
                                    a.style.opacity = 0;
                                    l.style.opacity = 0;
                                    a.style.filter = "alpha(opacity=0)";
                                    l.style.filter = "alpha(opacity=0)"
                                }
                            }
                        }
                        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
                            FunctionExt.defer(function () {
                                this.timeOutMethod();
                                if (this.$adRegion && this.$adRegions && this.$adRegions.length > 0) {
                                    this.afterAdMethod(a, k)
                                }
                                if (!this.$txtSlidesRegions || (this.$txtSlidesRegions && this.$txtSlidesRegions.length <= 0)) {
                                    this.lock = false
                                }
                                q = p = a = l = o = k = m = n = null
                            }.bind(this), 800)
                        } else {
                            this.lock = false;
                            q.style.display = "none";
                            p.style.display = "block";
                            this.timeOutMethod();
                            this.afterAdMethod(a, k);
                            q = p = a = l = o = k = m = n = null
                        }
                    }
                }
            },
            beforeAdMthod: function (a, f, e) {
                if (!e && f && f.childElements().length > 0) {
                    a.removeClassName("none");
                    a.setAttribute("tiptype", "1");
                    if (this.$adRegion) {
                        this.$adRegion.style.display = "block"
                    }
                } else {
                    if (e === "1") {
                        if (this.$adRegion) {
                            this.$adRegion.style.display = "block"
                        }
                    }
                }
            },
            afterAdMethod: function (a, d) {
                if (d === "1") {
                    a.addClassName("none");
                    a.setAttribute("tiptype", "2")
                }
                if (d === "2") {
                    a.removeClassName("none");
                    a.setAttribute("tiptype", "1")
                }
            },
            backSlidesRegionsMethod: function () {
                if (this.$backSlidesRegions) {
                    var d = this.$backSlidesRegions[this.prevIndex], a = this.$backSlidesRegions[this.currentIndex];
                    if (d !== a) {
                        if (!d.hasClassName("transition3")) {
                            d.addClassName("transition3")
                        }
                        if (!a.hasClassName("transition3")) {
                            a.addClassName("transition3")
                        }
                        a.style.opacity = 1;
                        d.style.opacity = 0;
                        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
                            FunctionExt.defer(function () {
                                a.style.zIndex = 3;
                                d.style.zIndex = 1;
                                d = a = null
                            }.bind(this), 300)
                        } else {
                            a.style.zIndex = 3;
                            d.style.zIndex = 1;
                            d = a = null
                        }
                    }
                }
            },
            dotSlidesRegionsMethod: function () {
                if (this.$dotSlidesRegions) {
                    var d = this.$dotSlidesRegions[this.prevIndex], a = this.$dotSlidesRegions[this.currentIndex];
                    if (d.hasClassName("on")) {
                        d.removeClassName("on")
                    }
                    if (!a.hasClassName("on")) {
                        a.addClassName("on")
                    }
                    d = a = null
                }
            },
            txtSlidesRegionsMethod: function () {
                if (this.$txtSlidesRegions) {
                    var e = this.$txtSlidesRegions[this.prevIndex], a = this.$txtSlidesRegions[this.currentIndex];
                    if (e !== a) {
                        if (this.$txtSlidesRegion.style.zIndex === "1" || this.$txtSlidesRegion.style.zIndex === 1) {
                            this.$txtSlidesRegion.style.zIndex = 3
                        }
                        if (!e.hasClassName("transition4")) {
                            e.addClassName("transition4")
                        }
                        var f = a.getAttribute("rectype");
                        if (f === "special") {
                            this.$txtSlidesRegion.style.zIndex = 1
                        }
                        if (!a.hasClassName("transition6")) {
                            a.addClassName("transition6")
                        }
                        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
                            e.style.opacity = 0;
                            FunctionExt.defer(function () {
                                a.style.display = "block";
                                e.style.display = "none"
                            }.bind(this), 400);
                            FunctionExt.defer(function () {
                                a.style.opacity = 1;
                                this.lock = false;
                                e = a = f = null
                            }.bind(this), 600)
                        } else {
                            a.style.opacity = 1;
                            a.style.display = "block";
                            e.style.display = "none";
                            e = a = f = null
                        }
                    }
                }
            },
            dotSlidesRegionClick: function (h) {
                Event.stop(h);
                var g = Event.element(h), k;
                var a = this.$dotSlidesRegion.immediateDescendants(), j;
                if (g && !g.hasClassName("on")) {
                    if (this.lock) {
                        return
                    }
                    k = parseInt(g.innerHTML, 10);
                    if (!isNaN(k)) {
                        for (j = 0; j < a.length; j++) {
                            if (a[j].innerHTML == k) {
                                this.currentIndex = j;
                                this.move();
                                console.log(a[j].innerHTML + "==" + j)
                            }
                        }
                    }
                }
            },
            timeOutMethod: function () {
                this.myTimeout && clearTimeout(this.myTimeout);
                var a, e = false, f;
                f = this.getXY(this.slidesRegion);
                a = document.documentElement.scrollTop || document.body.scrollTop;
                if (f.y > (a - 400) && f.y < (a + this.clientHeight + 550)) {
                    e = true
                }
                if (this.autoTimeout) {
                    if (e) {
                        this.myTimeout = setTimeout(function () {
                            f = this.getXY(this.slidesRegion);
                            a = document.documentElement.scrollTop || document.body.scrollTop;
                            if (f.y > (a - 400) && f.y < (a + this.clientHeight + 550)) {
                                e = true
                            } else {
                                e = false
                            }
                            if (e) {
                                if (this.amount && this.currentIndex > this.amount - 2) {
                                    this.currentIndex = 0
                                } else {
                                    if (this.currentIndex > this.length - 2) {
                                        this.currentIndex = 0
                                    } else {
                                        this.currentIndex++
                                    }
                                }
                                this.nextSlides()
                            } else {
                                this.myTimeout = setTimeout(function () {
                                    this.timeOutMethod()
                                }.bind(this), 3000)
                            }
                            a = e = f = null
                        }.bind(this), this.autoTimeout * 1000)
                    } else {
                        a = e = f = null;
                        this.myTimeout = setTimeout(function () {
                            this.timeOutMethod()
                        }.bind(this), 3000)
                    }
                }
            },
            slidesRegionOver: function (a) {
                this.myTimeout && clearTimeout(this.myTimeout)
            },
            slidesRegionOut: function (a) {
                this.timeOutMethod()
            },
            slidesRegionClick: function (f) {
                var a = Event.element(f);
                var h = a.getAttribute("method");
                if (h === "close") {
                    Event.stop(f);
                    var g = a.up("div.ddtiper");
                    if (g.getAttribute("method") === "tipbox") {
                        g.addClassName("none");
                        if (this.$adRegion) {
                            this.$adRegion.style.display = "none"
                        }
                    }
                }
            },
            getXY: function (j) {
                var k = 0, l = 0;
                if (j.getBoundingClientRect) {
                    var a = j.getBoundingClientRect();
                    var h = document.documentElement;
                    k = Math.round(a.left + Math.max(h.scrollLeft, document.body.scrollLeft) - h.clientLeft);
                    l = Math.round(a.top + Math.max(h.scrollTop, document.body.scrollTop) - h.clientTop)
                } else {
                    for (; j != document.body; k += j.offsetLeft, l += j.offsetTop, j = j.offsetParent) {
                    }
                }
                return {x: k, y: l}
            },
            close: function () {
                this.destroyEvent();
                this.destroyDOM()
            },
            destroyEvent: function () {
                if (this.$adRegion) {
                    Event.stopObserving(this.$adRegion, "mouseover", this.slidesRegionOverHendler);
                    Event.stopObserving(this.$adRegion, "mouseout", this.slidesRegionOutHendler);
                    Event.stopObserving(this.$adRegion, "click", this.slidesRegionClickHendler)
                }
                if (this.slidesRegion) {
                    Event.stopObserving(this.slidesRegion, "mouseover", this.slidesRegionOverHendler);
                    Event.stopObserving(this.slidesRegion, "mouseout", this.slidesRegionOutHendler)
                }
                if (this.prev) {
                    Event.stopObserving(this.prev, "click", this.onPrevClickHandler);
                    Event.stopObserving(this.prev, "mouseover", this.slidesRegionOverHendler);
                    Event.stopObserving(this.prev, "mouseout", this.slidesRegionOutHendler)
                }
                if (this.next) {
                    Event.stopObserving(this.next, "click", this.onNextClickHandler);
                    Event.stopObserving(this.next, "mouseover", this.slidesRegionOverHendler);
                    Event.stopObserving(this.next, "mouseout", this.slidesRegionOutHendler)
                }
                if (this.$dotSlidesRegion) {
                    Event.stopObserving(this.$dotSlidesRegion, "click", this.dotSlidesRegionClickHandler);
                    Event.stopObserving(this.$dotSlidesRegion, "mouseover", this.slidesRegionOverHendler);
                    Event.stopObserving(this.$dotSlidesRegion, "mouseout", this.slidesRegionOutHendler)
                }
            },
            destroyDOM: function () {
                this.$adRegion = null;
                this.slidesRegion = null;
                this.prev = null;
                this.next = null;
                this.currentIndex = null;
                this.position = null;
                this.items = null;
                this.length = null;
                this.lock = null;
                this.$dotSlidesRegion = null;
                this.clientHeight = null
            }
        })
    }
});
!function () {
    var b = Class.create();
    Object.extend(b.prototype, {
        options: {
            goodsRegionId: "goodsRegion",
            showMarketPrice: false,
            showTag: false,
            showMultiTag: false,
            showPricebyMainProp: false,
            priceTemplate: "{0}",
            mainPropPriceTemplate: "{0}",
            marketPriceTemplate: "{0}",
            tagTemplate: "{0}",
            newTemplate: ""
        }, server: {
            getGoodsPriceAndTagInfo: function (d, a) {
                return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "GetGoodsPriceAndTagInfo", [d], a, "/Mall.api", "get", "60000", null)
            }
        }, initialize: function (a) {
            this.setOptions(a);
            this.initializeDom();
            this.load()
        }, setOptions: function (a) {
            Object.extend(Object.extend(this, this.options), a)
        }, initializeDom: function (a) {
            this.goodsRegion = $(this.goodsRegionId);
            this.goodsRegions = this.goodsRegion.getElementsBySelector('[goodsid!=""]')
        }, load: function () {
            var j = [], k = "", a = 0, l = 0;
            for (a = 0; a < this.goodsRegions.length; a++) {
                var m = this.goodsRegions[a];
                var h = parseInt(m.getAttribute("goodsid"), 10) || 0;
                if (h > 0) {
                    j.push(h)
                }
            }
            this.server.getGoodsPriceAndTagInfo(j, function (g) {
                if (g && g.value && g.value.success && g.value.result && g.value.result.length > 0) {
                    for (a = 0; a < g.value.result.length; a++) {
                        var c = g.value.result[a];
                        var e = this.goodsRegion.getElementsBySelector('[goodsid="' + c.id + '"]');
                        for (l = 0; l < e.length; l++) {
                            var d = e[l];
                            d.getElementsBySelector('[data-selector="price"]').each(function (n) {
                                if (c.price > 0) {
                                    n.innerHTML = String.format(this.priceTemplate, this.formatPrice(c.price))
                                }
                            }.bind(this));
                            d.getElementsBySelector('[data-mainprop!=""]').each(function (p) {
                                if (c.has_activity) {
                                    if (c.price > 0) {
                                        p.innerHTML = String.format(this.mainPropPriceTemplate, this.formatPrice(c.price))
                                    }
                                } else {
                                    var n = parseInt(p.getAttribute("data-mainprop"), 10) || 0;
                                    if (n > 0) {
                                        var o = c.mainprop.find(function (u) {
                                            return u.mainprop_id == n
                                        });
                                        if (o && o.mainprop_price > 0) {
                                            p.innerHTML = String.format(this.mainPropPriceTemplate, this.formatPrice(o.mainprop_price))
                                        }
                                    }
                                }
                            }.bind(this));
                            d.getElementsBySelector('[data-selector="market"]').each(function (n) {
                                if (this.showMarketPrice) {
                                    if (c.market_price > 0) {
                                        n.innerHTML = String.format(this.marketPriceTemplate, this.formatPrice(c.market_price))
                                    }
                                } else {
                                    if (c.has_activity) {
                                        if (c.market_price > 0) {
                                            n.innerHTML = String.format(this.marketPriceTemplate, this.formatPrice(c.market_price))
                                        }
                                    } else {
                                        n.hide()
                                    }
                                }
                            }.bind(this));
                            if (this.showTag) {
                                var s = d.getElementsBySelector('[data-selector="tag"]'), q = "";
                                if (s && s.length > 0) {
                                    var r = s[0];
                                    var f = r.className == "tipsbox isNewTag";
                                    if (c.has_activity) {
                                        q = String.format(this.tagTemplate, c.tag);
                                        if (this.showMultiTag && f) {
                                            q += this.newTemplate
                                        }
                                    } else {
                                        if (f) {
                                            q = this.showMultiTag ? this.newTemplate : String.format(this.tagTemplate, "新品")
                                        }
                                    }
                                    r.innerHTML = q
                                }
                            }
                        }
                    }
                }
            }.bind(this))
        }, formatPrice: function (a) {
            return a % 100 == 0 ? (a / 100).toFixed(0) : (a / 100).toFixed(2)
        }, close: function () {
            this.destroyDOM()
        }, destroyDOM: function () {
            this.goodsRegion = null;
            this.goodsRegions = null
        }
    });
    window.GoodsPriceControl = b
}();
!function () {
    var b = Class.create();
    Object.extend(b.prototype, {
        options: {
            isMoveFocus: false,
            focusElement: null,
            magnifierElement: null,
            magnifierWidth: 1000,
            focusZindex: 100,
            magnifierScale: 0,
            magnifierZindex: 101,
            eMagnifierMages: null,
            focusArae: {width: 200, height: 200},
            basePointer: {top: 0, left: 0},
            _mousepos: {top: 0, left: 0},
            loadImgEl: {}
        }, initialize: function (a) {
            this.initializeField();
            if (this.initializeDOM(a)) {
                this.initializeControl();
                this.initializeEvent()
            }
        }, initializeField: function () {
            this.moveBodyHandler = this.bodyMagnifierMousemove.bind(this)
        }, initializeDOM: function (a) {
            this.options.magnifierContainer = $(a.magnifierContainer);
            if (!this.options.magnifierContainer) {
                return false
            }
            this.updateDom();
            this.mouseMagnifier(this.options.eMagnifierMages);
            return true
        }, initializeEvent: function () {
            Event.observe(document.body, "mousemove", this.moveBodyHandler)
        }, initializeControl: function () {
        }, destroy: function () {
            if (!this.destroyed) {
            }
        }, destroyField: function () {
        }, destroyDOM: function () {
            this.options.magnifierElement = null;
            this.options.eMagnifierMages = null;
            this.options.magnifierImg = null;
            this.options.focusElement = null;
            this.options.focusLoad = null
        }, destroyEvent: function () {
        }, destroyControl: function () {
        }, updateDom: function () {
            this.options.magnifierElement = this.options.magnifierContainer.down("div['data-magnifiers']", 1);
            this.options.eMagnifierMages = this.options.magnifierContainer.down("img['data-maxImg']");
            this.options.magnifierImg = this.options.magnifierContainer.down("img['data-magnifierImg']", 1);
            this.options.focusElement = this.options.magnifierContainer.down("i['data-focusPoint']");
            this.options.focusLoad = this.options.magnifierElement.down("div['data-load']")
        }, setMaxImg: function (a) {
            var d = a.getAttribute("data-maxImg");
            this.options.magnifierImg.setAttribute("src", d)
        }, setMagnifierScale: function (a) {
            this.options.magnifierScale = this.options.magnifierWidth / a.offsetWidth
        }, mouseMagnifier: function (a) {
            this.options.basePointer = {
                top: this.getAbsoluteTop(a),
                left: this.getAbsoluteLeft(a),
                width: this.options.eMagnifierMages.getWidth(),
                height: this.options.eMagnifierMages.getHeight()
            }
        }, getMousePoint: function (f) {
            var a = document.body, g = 0, h = 0;
            if (typeof window.pageYOffset != "undefined") {
                g = window.pageXOffset;
                h = window.pageYOffset
            } else {
                if (typeof document.compatMode != "undefined" && document.compatMode != "BackCompat") {
                    g = document.documentElement.scrollLeft;
                    h = document.documentElement.scrollTop
                } else {
                    if (typeof a != "undefined") {
                        g = a.scrollLeft;
                        h = a.scrollTop
                    }
                }
            }
            g += f.clientX;
            h += f.clientY;
            this.options._mousepos.left = g;
            this.options._mousepos.top = h;
            return this.options._mousepos
        }, pointCheck: function (k, a, q) {
            var n = this.getMousePoint(k), p = q && q.width || a.offsetWidth, l = q && q.height || a.offsetHeight, m = this.getAbsoluteLeft(a), o = this.getAbsoluteTop(a);
            n.left += q && q.left || 0;
            if (n.left < (m + p) && m < n.left && n.top > o && n.top < (o + l)) {
                return true
            }
            return false
        }, bodyMagnifierMousemove: function (d) {
            if (!this.options.eMagnifierMages) {
                this.updateDom()
            }
            var d = d || window.event, a = this.options.eMagnifierMages;
            if (a && this.pointCheck(d, a)) {
                this.options.focusLoad.show();
                this.options.magnifierImg.hide();
                this.options.isMoveFocus = true;
                this.setMagnifierScale(a);
                this.focusStatus();
                if (!this.options.isMoveFocus) {
                    return
                }
                this.focusPos(a, d);
                this.magnifierPos(a, d)
            } else {
                this.options.isMoveFocus = false;
                this.focusStatus()
            }
        }, focusPos: function (a, g) {
            var j = this.getMousePoint(g), k = j.top - this.options.focusArae.height / 2 - this.options.basePointer.top, h = j.left - this.options.focusArae.width / 2 - this.options.basePointer.left;
            if ((k + this.options.focusArae.height) > this.options.basePointer.height) {
                k = this.options.basePointer.height - this.options.focusArae.height
            }
            if ((h + this.options.focusArae.width) > this.options.basePointer.width) {
                h = this.options.basePointer.width - this.options.focusArae.width
            }
            if (k < 0) {
                k = 0
            }
            if (h < 0) {
                h = 0
            }
            this.options.focusElement.setStyle({top: k + "px", left: h + "px"})
        }, focusStatus: function () {
            var a = this;
            this.options.isMoveFocus && (this.options.focusElement.setStyle({display: "block"}) && this.options.magnifierElement.setStyle({display: "block"})) || (this.options.focusElement.setStyle({display: "none"}) && this.options.magnifierElement.setStyle({display: "none"}));
            this.setMaxImg(this.options.eMagnifierMages);
            this.loadImg(this.options.eMagnifierMages.getAttribute("data-maxImg"), function () {
                a.options.focusLoad.hide();
                a.options.magnifierImg.show()
            })
        }, loadImg: function (j, g) {
            if (this.options.loadImgEl[j]) {
                g && g();
                return
            }
            this.options.loadImgEl[j] = false;
            var k = this, h = new Image(), a = navigator.appName.toLowerCase();
            h.src = j;
            if (a.indexOf("netscape") == -1) {
                h.onreadystatechange = function () {
                    if (h.readyState == "complete") {
                        k.options.loadImgEl[j] = true;
                        g && g()
                    }
                }
            } else {
                h.onload = function () {
                    if (h.complete == true) {
                        k.options.loadImgEl[j] = true;
                        g && g()
                    }
                }
            }
        }, magnifierPos: function (a, g) {
            var j = this.getMousePoint(g), k = this.options.magnifierScale * (j.top - this.getAbsoluteTop(a) - this.options.focusArae.height / 2), h = this.options.magnifierScale * (j.left - this.getAbsoluteLeft(a) - this.options.focusArae.width / 2);
            if (k < 0) {
                k = 0
            }
            if (h < 0) {
                h = 0
            }
            if (h > 530) {
                h = 530
            }
            if (k > 530) {
                k = 530
            }
            this.options.magnifierImg.setStyle({top: "-" + k + "px", left: "-" + h + "px"})
        }, getAbsoluteLeft: function (e) {
            var f = e.offsetLeft, a = e.offsetParent;
            while (a !== null) {
                f += a.offsetLeft;
                a = a.offsetParent
            }
            return f
        }, getAbsoluteTop: function (e) {
            var f = e.offsetTop, a = e.offsetParent;
            while (a !== null) {
                f += a.offsetTop;
                a = a.offsetParent
            }
            return f
        }
    });
    window.MagnifyingGlass = b
}();
var ShowProduct = Class.create();
Object.extend(ShowProduct.prototype, {
    options: {
        container: "",
        data: null,
        currentIndex: 0,
        thumTemplate: '<dd {0}><span><img src="{1}" alt="{2}" title="{2}" /></span>{3}</dd>',
        smallTemplate: '<dt><span {3}>{2}<img src="{0}" alt="{1}" title="{1}" /></span></dt>'
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeDom();
        this.initializeEvent();
        this.load()
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initializeDom: function () {
        this.container = $(this.container);
        this.len = 0;
        if (this.data && this.data.list) {
            this.list = this.data.list;
            this.len = this.list.length
        }
        this.bigList = [];
        this.videoNumber = -1;
        for (var b = 0; b < this.len; b++) {
            if (this.list[b].isvideo === undefined) {
                this.bigList.push(this.list[b].big)
            } else {
                this.videoNumber = b
            }
        }
        this.isPhone = this.browserRedirect();
        this.buildHtml()
    }, initializeEvent: function () {
        if (this.container) {
            this.onOverHandler = this.onOver.bind(this);
            this.onClickContainerHandler = this.onClickContainer.bind(this);
            Event.observe(this.container, "mouseover", this.onOverHandler);
            Event.observe(this.container, "click", this.onClickContainerHandler)
        }
        Event.observe(window, "unload", this.close.bind(this))
    }, load: function () {
    }, buildHtml: function () {
        if (this.len > 0) {
            this.container.innerHTML = "";
            var e = "", f = "";
            if (this.videoNumber === this.currentIndex) {
                e = "<i></i>";
                f = this.getLabelVideoUrl(this.currentIndex)
            }
            var d = String.format(this.smallTemplate, [this.list[this.currentIndex].small, this.data.title, e, f]);
            for (i = 0; i < this.len; i++) {
                e = "";
                if (this.list[i].isvideo !== undefined) {
                    e = "<i></i>";
                    f = this.getLabelVideoUrl(i)
                }
                if (i === this.currentIndex) {
                    d += String.format(this.thumTemplate, ['class="on" ' + f, this.list[i].thum, this.data.title, e])
                } else {
                    d += String.format(this.thumTemplate, [f, this.list[i].thum, this.data.title, e])
                }
            }
            this.container.innerHTML = d
        }
        this.containerlist = this.container.immediateDescendants();
        this.smallEl = this.containerlist.shift()
    }, onOver: function (d) {
        var c = Event.findElement(d, "dd");
        this.onOverMethod(c)
    }, onOut: function (b) {
    }, onClickContainer: function (j) {
        var g = Event.findElement(j, "dt"), f = Event.findElement(j, "dd"), h = Event.findElement(j, "i"), k;
        if (g !== document) {
            if (this.videoNumber > -1) {
                if (this.currentIndex !== this.videoNumber) {
                    if (this.currentIndex > this.videoNumber) {
                        k = this.currentIndex - 1
                    } else {
                        k = this.currentIndex
                    }
                }
            } else {
                k = this.currentIndex
            }
            this.fullProduct = new ShowFullProduct({currentIndex: k, data: this.bigList})
        }
        if (!this.isPhone) {
            if (f !== document) {
                this.onOverMethod(f)
            }
        }
    }, onOverMethod: function (f) {
        if (f != document && !f.hasClassName("on")) {
            var g = this.smallEl.down("img"), h;
            if (g) {
                h = this.containerlist.indexOf(f);
                g.src = this.list[h].small;
                this.containerlist[this.currentIndex].removeClassName("on");
                f.addClassName("on");
                this.currentIndex = h;
                var j = this.smallEl.down("span");
                if (this.videoNumber === this.currentIndex) {
                    if (j) {
                        var k = this.getVideoUrl(this.currentIndex);
                        j.setAttribute("videourl", k);
                        this.i_label = Builder.node("i");
                        j.appendChild(this.i_label)
                    }
                } else {
                    if (j && this.i_label) {
                        j.removeAttribute("videourl");
                        this.i_label.remove();
                        this.i_label = null
                    }
                }
            }
        }
    }, getLabelVideoUrl: function (b) {
        if (!this.isPhone) {
            if (this.list[b].flv) {
                return 'videourl="' + this.list[b].flv + '"'
            }
        } else {
            if (this.list[b].mp4) {
                return 'videourl="' + this.list[b].mp4 + '"'
            }
        }
        return ""
    }, getVideoUrl: function (b) {
        if (!this.isPhone) {
            if (this.list[b].flv) {
                return this.list[b].flv
            }
        } else {
            if (this.list[b].mp4) {
                return this.list[b].mp4
            }
        }
        return ""
    }, browserRedirect: function () {
        var m = navigator.userAgent.toLowerCase();
        var h = m.match(/ipad/i) == "ipad";
        var j = m.match(/iphone/i) == "iphone";
        var k = m.match(/iphone/i) == "ipod";
        var g = m.match(/android/i) == "android";
        var l = m.match(/windows mobile/i) == "windows mobile";
        if (h || j || k || g || l) {
            return true
        }
        return false
    }, close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    }, destroyField: function () {
    }, destroyEvent: function () {
        if (this.container) {
            Event.stopObserving(this.container, "over", this.onOverHandler);
            Event.stopObserving(this.container, "click", this.onClickContainerHandler)
        }
    }, destroyDOM: function () {
        this.container = null;
        this.data = null;
        this.thumTemplate = null;
        this.smallTemplate = null;
        this.list = null
    }
});
var NewShowProduct = Class.create();
Object.extend(NewShowProduct.prototype, ShowProduct.prototype);
Object.extend(NewShowProduct.prototype, {
    options: {
        container: "",
        data: null,
        currentIndex: 0,
        thumTemplate: '<div class="s_proimglist" style="{4}"><a id="prevthumbtn" href="javascript:;" class="last" style="{0}"$pan$><i></i></a><a id="nextthumbtn" href="javascript:;" class="next" style="{1}"$pan$><i></i></a><div class="s_proimglister" style="{5}"><ul id="thumRegion" style="width:{2}px; position: relative;">{3}</ul></div></div>',
        thumItmeTemplate: '<li class="{0}"><a href="javascript:;"$pan$><img src="{1}" width="60" height="60"></a></li>',
        smallTemplate: '<div class="s_main_img"$pan$><img data-selector="smallImg" src="{0}" data-maxImg="{1}" width="470" height="470"><i data-focusPoint="true" style="display: none;"></i></div>',
        bigTemplate: '<div class="s_main_imgzoom" data-magnifiers="true"><div class="load48" data-load="true"></div><img style="display: none"  data-magnifierImg="true" src=""  width="1000" height="1000" /></div>',
        videoTemplate: '<a {0} class="s_provideo" style="{1}"$pan$><i></i></a>',
        pageName: "",
        changePan: "",
        bigPicPan: "",
        videoPan: ""
    }, initialize: function (d) {
        this.setOptions(d);
        if (this.changePan != "") {
            var c = " onclick=\"if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.changePan + "'); }\"";
            this.thumTemplate = this.thumTemplate.replace("$pan$", c);
            this.thumItmeTemplate = this.thumItmeTemplate.replace("$pan$", c)
        } else {
            this.thumTemplate = this.thumTemplate.replace("$pan$", "");
            this.thumItmeTemplate = this.thumItmeTemplate.replace("$pan$", "")
        }
        if (this.bigPicPan != "") {
            var c = " onclick=\"if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.bigPicPan + "'); }\"";
            this.smallTemplate = this.smallTemplate.replace("$pan$", c)
        } else {
            this.smallTemplate = this.smallTemplate.replace("$pan$", "")
        }
        if (this.videoPan != "") {
            var c = " onclick=\"if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.videoPan + "'); }\"";
            this.videoTemplate = this.videoTemplate.replace("$pan$", c)
        } else {
            this.videoTemplate = this.videoTemplate.replace("$pan$", "")
        }
    }, render: function (b) {
        this.data = b;
        if (this.data && this.data.list && this.data.list.length) {
            this.list = b.list;
            this.len = this.list.length;
            this.initializeDom();
            this.initializeEvent();
            this.load()
        } else {
            return false
        }
    }, initializeDom: function () {
        this.contentRegion = this.container;
        this.container = $(this.container);
        this.isPhone = this.browserRedirect();
        if (this.data.video.flv) {
            this.Info = {showNum: 4, showRegionWidth: 312, showRegionWidthCss: "", parentRegionWidthCss: ""}
        } else {
            this.Info = {
                showNum: 5,
                showRegionWidth: 390,
                showRegionWidthCss: "width:390px;",
                parentRegionWidthCss: "width:448px;"
            }
        }
        this.buildHtml()
    }, buildHtml: function () {
        if (this.len > 0) {
            this.currentIndex = 0;
            this.container.innerHTML = "";
            var j = "", o = "", m = "", n = 78 * this.len, l = "", k = "";
            var h = String.format(this.smallTemplate, [this.list[this.currentIndex].small, this.list[this.currentIndex].big]);
            h += this.bigTemplate;
            for (i = 0; i < this.len; i++) {
                if (i === this.currentIndex) {
                    m += String.format(this.thumItmeTemplate, ["on", this.list[i].thum])
                } else {
                    m += String.format(this.thumItmeTemplate, ["", this.list[i].thum])
                }
            }
            if (this.currentIndex <= this.Info.showNum) {
                l = "display:none;"
            }
            if (this.len <= this.Info.showNum) {
                k = l = "display:none;"
            }
            h += String.format(this.thumTemplate, [l, k, n, m, this.Info.parentRegionWidthCss, this.Info.showRegionWidthCss]);
            h += String.format(this.videoTemplate, [this.getVideoUrl(), (this.data.video.flv ? "" : "display:none;")]);
            this.container.innerHTML = h
        }
        this.containerlist = this.container.down("ul").immediateDescendants();
        this.smallEl = this.container.down('img[data-selector="smallImg"]')
    }, load: function () {
        if (typeof MagnifyingGlass !== "undefined") {
            new MagnifyingGlass({magnifierContainer: this.contentRegion})
        }
        if (typeof Widgets !== "undefined" && Widgets.CallOutVideo !== "undefined") {
            new Widgets.CallOutVideo({slidesRegion: this.contentRegion})
        } else {
            $loadSubJs("/js/2014/CallOutVideo.js", function () {
                new Widgets.CallOutVideo({slidesRegion: this.contentRegion})
            }.bind(this))
        }
        if (typeof Widgets !== "undefined" && typeof Widgets.DistanceSlidesControl !== "undefined") {
            this.distanceSlidesControl = new Widgets.DistanceSlidesControl({
                slidesRegion: "thumRegion",
                prev: "prevthumbtn",
                next: "nextthumbtn",
                showRegionWidth: this.Info.showRegionWidth,
                slidesRegionWidth: this.Info.showRegionWidth,
                currentPosition: 0,
                itemWidth: 78
            })
        } else {
            $loadSubJs(["/js/2014/widgets/Base.js", "/js/2014/widgets/DistanceSlidesControl.js"], function () {
                this.distanceSlidesControl = new Widgets.DistanceSlidesControl({
                    slidesRegion: "thumRegion",
                    prev: "prevthumbtn",
                    next: "nextthumbtn",
                    showRegionWidth: this.Info.showRegionWidth,
                    slidesRegionWidth: this.Info.showRegionWidth,
                    currentPosition: 0,
                    itemWidth: 78
                })
            }.bind(this))
        }
    }, onOver: function (d) {
        var c = Event.findElement(d, "li");
        this.onOverMethod(c)
    }, onOverMethod: function (d) {
        if (d != document && !d.hasClassName("on")) {
            var e = this.smallEl, f;
            if (e) {
                f = this.containerlist.indexOf(d);
                e.src = this.list[f].small;
                e.setAttribute("data-maxImg", this.list[f].big);
                this.containerlist[this.currentIndex].removeClassName("on");
                d.addClassName("on");
                this.currentIndex = f
            }
        }
    }, getVideoUrl: function (b) {
        if (!this.isPhone) {
            if (this.data.video.flv) {
                return 'videourl="' + this.data.video.flv + '"'
            }
        } else {
            if (this.data.video.mp4) {
                return 'videourl="' + this.data.video.mp4 + '"'
            }
        }
        return ""
    }, destroyDOM: function () {
        this.container = null;
        this.data = null;
        this.thumTemplate = null;
        this.smallTemplate = null;
        this.list = null;
        if (this.distanceSlidesControl) {
            this.distanceSlidesControl.close();
            this.distanceSlidesControl = null
        }
    }
});
var ShowFullProduct = Class.create();
Object.extend(ShowFullProduct.prototype, {
    options: {currentIndex: null, data: null}, initialize: function (b) {
        this.setOptions(b);
        this.initializeDom()
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initializeDom: function () {
        if (this.data && this.data.length > 0) {
            this.len = this.data.length;
            this.load()
        }
    }, initializeEvent: function () {
        if (this.closeBtn) {
            this.onClickCloseBtnHandler = this.onClickCloseBtn.bind(this);
            Event.observe(this.closeBtn, "click", this.onClickCloseBtnHandler);
            Event.observe(this.img, "click", this.onClickCloseBtnHandler)
        }
        if (this.prevBtn) {
            this.onClickPrevBtnHandler = this.onClickPrevBtn.bind(this);
            Event.observe(this.prevBtn, "click", this.onClickPrevBtnHandler)
        }
        if (this.nextBtn) {
            this.onClickNextBtnHandler = this.onClickNextBtn.bind(this);
            Event.observe(this.nextBtn, "click", this.onClickNextBtnHandler)
        }
        Event.observe(window, "unload", this.close.bind(this))
    }, load: function () {
        this.clientHeight = document.documentElement.clientHeight;
        this.clientWidth = document.documentElement.clientWidth;
        this.scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        if (typeof this.width === "undefined" || typeof this.height === "undefined" || this.width === 0 || this.height === 0) {
            var e = '<div style="visibility:hidden;"><img id="img" src="' + this.data[this.currentIndex] + '"></div>';
            var d = Builder.build(e);
            document.body.appendChild(d);
            var f = $("img");
            f.onload = function () {
                this.height = f.height;
                d && d.remove();
                d = f = null;
                this.buildHtml();
                this.initializeEvent()
            }.bind(this)
        } else {
            this.buildHtml();
            this.initializeEvent()
        }
    }, buildHtml: function () {
        var f = document.body.clientHeight || document.documentElement.clientHeight, g = this.scrollTop + 20, h = this.scrollTop + Math.round(this.clientHeight / 2 - this.height / 2), e = Math.round(this.height / 2);
        this.closeBtn = Builder.node("div", {className: "i_pop_close", style: "top:" + g + "px"});
        this.img = Builder.node("img", {src: this.data[this.currentIndex]});
        this.prevBtn = Builder.node("a", {className: "lastnews", title: "上一张", style: "top:" + e + "px"});
        this.nextBtn = Builder.node("a", {className: "nextnews", title: "下一张", style: "top:" + e + "px"});
        this.photobox = Builder.node("div", {
            className: "photobox",
            style: "top:" + h + "px"
        }, [this.img, this.prevBtn, this.nextBtn]);
        this.showProduct = Builder.node("div", {
            className: "store_photo",
            style: "height:" + f + "px"
        }, [this.closeBtn, this.photobox]);
        document.body.appendChild(this.showProduct)
    }, onClickCloseBtn: function (b) {
        if (this.showProduct) {
            this.showProduct.remove();
            this.close()
        }
    }, onClickPrevBtn: function (b) {
        this.currentIndex--;
        if (this.currentIndex < 0) {
            this.currentIndex = this.len - 1
        }
        this.img.src = this.data[this.currentIndex]
    }, onClickNextBtn: function (b) {
        this.currentIndex++;
        if (this.currentIndex >= this.len) {
            this.currentIndex = 0
        }
        this.img.src = this.data[this.currentIndex]
    }, close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    }, destroyField: function () {
    }, destroyEvent: function () {
        if (this.closeBtn) {
            Event.stopObserving(this.closeBtn, "click", this.onClickCloseBtnHandler);
            Event.stopObserving(this.img, "click", this.onClickCloseBtnHandler)
        }
        if (this.prevBtn) {
            Event.stopObserving(this.prevBtn, "click", this.onClickPrevBtnHandler)
        }
        if (this.nextBtn) {
            Event.stopObserving(this.nextBtn, "click", this.onClickNextBtnHandler)
        }
    }, destroyDOM: function () {
        this.data = null;
        this.closeBtn = null;
        this.img = null;
        this.prevBtn = null;
        this.nextBtn = null;
        this.photobox = null;
        this.currentIndex = null;
        this.height = null
    }
});
var ActivityTimerControl = Class.create();
Object.extend(ActivityTimerControl.prototype, {
    options: {
        start: new Date(),
        current: new Date(),
        end: new Date(),
        region: "",
        time: 500,
        activetype: "",
        startCallBack: null,
        endCallBack: null
    }, initialize: function (b) {
        this.setOptions(b);
        this.startTimestamp = new Date().getTime();
        this.startTime = typeof this.start == "undefined" ? new Date().getTime() : this.start.getTime();
        this.endTime = typeof this.end == "undefined" ? new Date().getTime() : this.end.getTime();
        this.currentTime = typeof this.current == "undefined" ? new Date().getTime() : this.current.getTime();
        this.region = $(this.region);
        if (this.region) {
            this.value = {
                day1: "0",
                day2: "0",
                day3: "0",
                hour1: "0",
                hour2: "0",
                min1: "0",
                min2: "0",
                sec1: "0",
                sec2: "0",
                des: ""
            };
            this.diff = 0;
            this.startDiff = this.currentTime - this.startTime;
            this.endDiff = this.endTime - this.currentTime;
            if (this.startDiff >= 0) {
                if (this.endDiff > 0) {
                    this.value.des = "距结束";
                    this.diff = this.endDiff
                } else {
                    this.value = null
                }
            } else {
                this.value.des = "距开始";
                this.diff = Math.abs(this.startDiff)
            }
            this.interval()
        }
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, interval: function () {
        if (this.diff > 0) {
            var g = this.diff;
            var f = Math.floor(g / 1000 / 60 / 60 / 24);
            this.value.day1 = this.convertDay(f).p1;
            this.value.day2 = this.convertDay(f).p2;
            this.value.day3 = this.convertDay(f).p3;
            g -= f * 24 * 60 * 60 * 1000;
            var h = Math.floor(g / 1000 / 60 / 60);
            this.value.hour1 = this.convert(h).p1;
            this.value.hour2 = this.convert(h).p2;
            g -= h * 60 * 60 * 1000;
            var j = Math.floor(g / 1000 / 60);
            this.value.min1 = this.convert(j).p1;
            this.value.min2 = this.convert(j).p2;
            g -= j * 60 * 1000;
            var k = Math.floor(g / 1000);
            this.value.sec1 = this.convert(k).p1;
            this.value.sec2 = this.convert(k).p2;
            this.diff -= 500;
            this.timeout = setTimeout(this.interval.bind(this), this.time)
        } else {
            if (this.value.des == "距结束") {
                this.timeout && clearTimeout(this.timeout);
                this.endCallBack && this.endCallBack()
            } else {
                if (this.value.des == "距开始") {
                    this.value.des = "距结束";
                    this.diff = this.endTime - this.startTime;
                    this.startCallBack && this.startCallBack();
                    this.timeout = setTimeout(this.interval.bind(this), this.time)
                } else {
                }
            }
        }
        this.render(this.value)
    }, render: function (f) {
        if (f) {
            var d = new StringBuilder();
            if (this.activetype == 1) {
                d.append('<dl class="s_timedown">');
                d.append("<dt></dt>");
                d.append('<dd><!-- class="false"-->');
                d.append("<em>#{des}:</em>");
                if (f.day1 > 0) {
                    d.append("<i>#{day1}</i>")
                }
                d.append("<i>#{day2}</i>");
                d.append("<i>#{day3}</i>");
                d.append("<em>天</em><i>#{hour1}</i><i>#{hour2}</i><em>时</em><i>#{min1}</i><i>#{min2}</i><em>分</em><i>#{sec1}</i><i>#{sec2}</i><em>秒</em>");
                d.append("</dd>");
                d.append("</dl>")
            } else {
                if (this.activetype == 2) {
                    d.append("<p><i></i>#{des}:<em>");
                    if (f.day1 > 0) {
                        d.append("#{day1}")
                    }
                    d.append("#{day2}");
                    d.append("#{day3}");
                    d.append("</em>天<em>#{hour1}#{hour2}</em>时<em>#{min1}#{min2}</em>分<em>#{sec1}#{sec2}</em>秒</p>")
                }
            }
            var e = new Template(d.toString());
            this.region.update(e.evaluate(f))
        } else {
            this.region.hide()
        }
    }, convert: function (b) {
        var b = b.toString();
        if (b.length > 1) {
            return {p1: b.charAt(0), p2: b.charAt(1)}
        } else {
            return {p1: "0", p2: b.charAt(0)}
        }
    }, convertDay: function (b) {
        var b = b.toString();
        if (b.length == 2) {
            return {p1: 0, p2: b.charAt(0), p3: b.charAt(1)}
        } else {
            if (b.length > 2) {
                return {p1: b.charAt(0), p2: b.charAt(1), p3: b.charAt(2)}
            } else {
                return {p1: "0", p2: "0", p3: b.charAt(0)}
            }
        }
    }, close: function () {
        if (!this.closed) {
            this.destroy();
            this.closed = true
        }
    }, destroy: function () {
        if (!this.destroyed) {
            this.timeout && clearTimeout(this.timeout);
            this.destroyed = true
        }
    }
});
var SelectSkuLayer = Class.create();
Object.extend(SelectSkuLayer.prototype, {
    server: {
        getGoodsInfoByGoodsId: function (e, d, f) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "GetGoodsInfosByGoodsIdForDialog", [e], d, "/Ecommerce.api", "get", "60000", null)
        }, buyNow: function (e, d, f) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "BuyNow", [e], d, "/Ecommerce.api", "get", "60000", f)
        }, addCart: function (e, d, f) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "AddCart", [e], d, "/Ecommerce.api", "get", "60000", f)
        }
    },
    options: {
        goodsId: 0,
        container: null,
        clickElement: null,
        submitCallBack: null,
        params: null,
        action: "buynow",
        pageName: "",
        propertyPan: "",
        confirmPan: "",
        cancelPan: ""
    },
    initialize: function (b) {
        this.setOptions(b);
        this.initializeDom();
        this.initializeEvent();
        this.load()
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initializeDom: function () {
    },
    initializeEvent: function () {
        this.onClickContainerHandler = this.onClickContainer.bind(this);
        this.container && Event.observe(this.container, "click", this.onClickContainerHandler);
        this.onClickDocumentHandler = this.onClickDocument.bind(this);
        Event.observe(document, "click", this.onClickDocumentHandler);
        Event.observe(window, "unload", this.close.bind(this))
    },
    onClickDocument: function (e) {
        var d = Event.element(e);
        if (d == document || !d.up('div[id="' + this.container.id + '"]')) {
            var f = d.readAttribute("method");
            if (f != this.action) {
                this.container.hide()
            }
        }
    },
    load: function () {
        if (this.goodsId > 0) {
            this.server.getGoodsInfoByGoodsId(this.goodsId, this.loadDataCallBack.bind(this), this.clickElement)
        }
    },
    onClickContainer: function (e) {
        var d = $(Event.element(e));
        if (d && d != document) {
            var f = d.readAttribute("method");
            switch (f) {
                case"reduce":
                    this.reduceCount(d);
                    break;
                case"add":
                    this.addCount(d);
                    break;
                case"pvkey":
                    this.selectProperty(d);
                    break;
                case"confirm":
                    if (this.action == "addcart") {
                        this.onClickAddCartBtn()
                    } else {
                        this.onClickBuyNowBtn()
                    }
                    break;
                case"cancel":
                    this.container.hide();
                    break;
                case"closeTip":
                    this.container.down(".s_proboxs").removeClassName("s_proerr");
                    break;
                default:
                    break
            }
        }
    },
    onKeydownBuycountInput: function (b) {
        setTimeout(function () {
            var d = isNaN(parseInt(this.buycountInput.value, 10)) ? 1 : parseInt(this.buycountInput.value, 10);
            this.buycountInput.value = Math.min(this.maxcountincart, d);
            var a = Number(this.buycountInput.value);
            if (a <= 1) {
                this.buycountInput.previous(0).addClassName("false");
                this.buycountInput.next(0).removeClassName("false")
            } else {
                this.buycountInput.previous(0).removeClassName("false");
                if (a >= Math.min(Number(this.qtyRegion.readAttribute("qty")), this.maxcountincart)) {
                    this.buycountInput.next(0).addClassName("false")
                }
            }
        }.bind(this), 100)
    },
    reduceCount: function (d) {
        var f = isNaN(Number(this.qtyRegion.readAttribute("qty"))) ? 0 : Number(this.qtyRegion.readAttribute("qty"));
        var e = Number(this.buycountInput.value);
        if (e <= 1) {
            return
        }
        e--;
        this.buycountInput.value = e;
        if (e <= 1) {
            d.addClassName("false")
        }
        if (e < f) {
            d.next("a").removeClassName("false")
        }
    },
    addCount: function (d) {
        var f = isNaN(Number(this.qtyRegion.readAttribute("qty"))) ? 0 : Number(this.qtyRegion.readAttribute("qty"));
        var e = Number(this.buycountInput.value);
        if (e >= Math.min(f, this.maxcountincart)) {
            return
        }
        e++;
        this.buycountInput.value = e;
        if (e >= Math.min(f, this.maxcountincart)) {
            d.addClassName("false")
        }
        if (e > 1) {
            d.previous("a").removeClassName("false")
        }
    },
    selectProperty: function (f) {
        f = f.tagName == "A" ? f : f.up("a");
        this.currClickPvKey = f.readAttribute("pvkey");
        var g = f.readAttribute("pvkey");
        f.up("dd").getElementsBySelector("a[method=pvkey]").each(function (a) {
            if (a.readAttribute("pvkey") == g) {
                a.toggleClassName("on");
                if (a.down("em")) {
                    a.down("em").toggle()
                }
            } else {
                a.removeClassName("on");
                if (a.down("em")) {
                    a.down("em").hide()
                }
            }
        });
        this.setPropertyValueDomStyle(f);
        var j = this.getSelectedSku();
        if (j) {
            this.container.down(".s_proboxs").removeClassName("s_proerr");
            this.setBuyBtnStyle(j)
        } else {
            var k = this.goodsStatus;
            var h = this.goodsTotalQty;
            this.qtyRegion.setAttribute("qty", h);
            this.styleControl(k, h)
        }
    },
    setPropertyValueDomStyle: function (j) {
        var o = this.container.getElementsBySelector('[method="pvkey"]').findAll(function (a) {
            return a.hasClassName("on")
        });
        var l = this.skuList.clone(), m = 0;
        for (m = 0; m < o.length; m++) {
            l = l.findAll(function (a) {
                return a.pvkey.split("|").include(o[m].readAttribute("pvkey")) && a.qty > 0 && a.status == 1
            })
        }
        var h = this.container.getElementsBySelector('[data-selector="property"]');
        for (m = 0; m < h.length; m++) {
            var k = h[m];
            if (j.hasClassName("on") && j.up("dd").readAttribute("pid") == k.readAttribute("pid")) {
                continue
            }
            var n = k.getElementsBySelector("a").findAll(function (a) {
                    return a.hasClassName("on")
                }).length > 0;
            k.getElementsBySelector("a").each(function (b) {
                var c = null;
                if (n) {
                    var a = b.hasClassName("on");
                    if (a) {
                        c = l
                    } else {
                        c = this.skuList
                    }
                } else {
                    c = l
                }
                var d = c.find(function (e) {
                    return e.pvkey.split("|").include(b.readAttribute("pvkey")) && e.qty > 0 && e.status == 1
                }.bind(this));
                if (d) {
                    b.removeClassName("false")
                } else {
                    b.addClassName("false")
                }
            }.bind(this))
        }
    },
    getSelectedSku: function () {
        if (this.skuList && this.skuList.length > 0) {
            var o = [], j = 0;
            this.container.getElementsBySelector('a[method="pvkey"]').each(function (a) {
                if (a.hasClassName("on")) {
                    o.push(a.readAttribute("pvkey"))
                }
            });
            var q = this.skuList.clone();
            for (j = 0; j < o.length; j++) {
                q = q.findAll(function (a) {
                    return a.pvkey.split("|").include(o[j])
                })
            }
            var n = this.container.getElementsBySelector('[data-selector="property"]').length;
            if (o.length == n && q.length == 1) {
                this.container.getElementsBySelector('a[method="pvkey"]').each(function (a) {
                    if (q[0].pvkey.split("|").include(a.readAttribute("pvkey"))) {
                        a.removeClassName("false")
                    }
                });
                return q[0]
            } else {
                var m = q[0].saleprice, l = q[0].saleprice;
                for (var k = 0; k < q.length; k++) {
                    var p = q[k].saleprice;
                    if (p < m) {
                        m = p
                    }
                    if (p > m) {
                        l = p
                    }
                }
                this.salePriceRegion.update(this.getPriceRangeText(null, m, l))
            }
        }
        return null
    },
    setBuyBtnStyle: function (d) {
        this.salePriceRegion.update(this.priceToString(d.saleprice));
        if (d.marketprice > 0) {
            this.marketPriceRegion.up("em") && this.marketPriceRegion.up("em").show();
            this.marketPriceRegion.update("￥" + this.priceToString(d.marketprice))
        } else {
            this.marketPriceRegion.up("em") && this.marketPriceRegion.up("em").hide()
        }
        this.qtyRegion.setAttribute("qty", d.qty);
        if (d.qty > 0 && d.qty < 10) {
            this.qtyRegion.addClassName("stockless");
            this.qtyRegion.innerHTML = "库存紧张"
        } else {
            this.qtyRegion.innerHTML = ""
        }
        var c = Number(this.buycountInput.value);
        this.buycountInput.value = Math.min(c, d.qty);
        this.styleControl(d.status, d.qty)
    },
    styleControl: function (d, c) {
        if (d != 1) {
            this.qtyRegion.innerHTML = "";
            this.confirmBtn.update("已下架");
            this.confirmBtn.addClassName("btn_false")
        } else {
            if (c <= 0) {
                this.qtyRegion.setAttribute("qty", 0);
                this.qtyRegion.innerHTML = "";
                this.confirmBtn.update('<i class="shouqing"></i>已售罄');
                this.confirmBtn.addClassName("btn_false")
            } else {
                this.buycountInput.value = this.buycountInput.value == 0 ? 1 : this.buycountInput.value;
                if (c > 0 && c < 10) {
                    this.qtyRegion.addClassName("stockless");
                    this.qtyRegion.innerHTML = "库存紧张"
                } else {
                    this.qtyRegion.innerHTML = ""
                }
                this.qtyRegion.setAttribute("qty", c);
                this.confirmBtn.update("确定");
                this.confirmBtn.removeClassName("btn_false")
            }
        }
    },
    onClickAddCartBtn: function () {
        if (this.confirmBtn.hasClassName("btn_false")) {
            return
        }
        var e = this.getSelectedSku();
        if (e != null) {
            this.container.down(".s_proboxs").removeClassName("s_proerr");
            var d = Number(this.buycountInput.value);
            var f = e.sku + "|" + d + "|0|false";
            this.overLayerId = this.addOverlay(this.confirmBtn);
            this.overLayerId.style.cssText += "position:fixed;";
            this.server.addCart(f, function (a) {
                if (a && a.value) {
                    if (a.value.success) {
                        location.href = a.value.cartPage
                    } else {
                        if (a.value.errcode == "checkfailed") {
                            if (a.value.giftskus) {
                                $alert("该商品不可售")
                            } else {
                                if (a.value.noqty) {
                                    $alert("该商品库存不足")
                                } else {
                                    if (a.value.nosale) {
                                        $alert("该商品不可售")
                                    } else {
                                        if (a.value.morethanlimitbuy) {
                                            $alert("该商品超过限购数量")
                                        } else {
                                        }
                                    }
                                }
                            }
                        } else {
                            $alert("添加购物车失败")
                        }
                    }
                } else {
                    $alert("网络异常，请稍后重试")
                }
            }.bind(this), null)
        } else {
            this.container.down(".s_proboxs").addClassName("s_proerr")
        }
    },
    onClickBuyNowBtn: function () {
        if (this.confirmBtn.hasClassName("btn_false")) {
            return
        }
        var e = this.getSelectedSku();
        if (e != null) {
            this.container.down(".s_proboxs").removeClassName("s_proerr");
            var d = Number(this.buycountInput.value);
            var f = e.sku + "|" + d;
            this.overLayerId = this.addOverlay(this.confirmBtn);
            this.overLayerId.style.cssText += "position:fixed;";
            this.server.buyNow(f, function (b) {
                if (b && b.value) {
                    if (b.value.success) {
                        this.buyNow(f)
                    } else {
                        if (b.value.errcode == "nologin") {
                            if (window.UserLoginDialog === undefined) {
                                $loadJs("/js/2014/UserLoginDialog.js", function () {
                                    var c = new UserLoginDialog({
                                        callback: function () {
                                            this.buyNow(f)
                                        }.bind(this)
                                    })
                                }.bind(this))
                            } else {
                                var a = new UserLoginDialog({
                                    callback: function () {
                                        this.buyNow(f)
                                    }.bind(this)
                                })
                            }
                        } else {
                            if (b.value.errcode == "checkfailed") {
                                if (b.value.morethanlimitbuy) {
                                    $alert("该商品超过活动限购数量")
                                }
                                if (b.value.giftskus) {
                                    $alert("该商品不可售")
                                } else {
                                    if (b.value.noqty) {
                                        $alert("该商品库存不足")
                                    } else {
                                        if (b.value.nosale) {
                                            $alert("该商品不可售")
                                        } else {
                                        }
                                    }
                                }
                            } else {
                                $alert("购买失败，请联系客服")
                            }
                        }
                    }
                } else {
                    $alert("网络异常，请稍后重试！")
                }
                this.removeOverlay(this.confirmBtn, this.overLayerId)
            }.bind(this), null)
        } else {
            this.container.down(".s_proboxs").addClassName("s_proerr")
        }
    },
    buyNow: function (b) {
        this.server.buyNow(b, function (h) {
            if (h && h.value) {
                if (h.value.success) {
                    var a = Builder.node("form", {style: "display:none", method: "post", action: h.value.confirmpage});
                    var f = Builder.node("input", {type: "hidden", name: "skus", value: b});
                    a.appendChild(f);
                    document.body.appendChild(a);
                    a.submit()
                } else {
                    if (h.value.errcode == "nologin") {
                        if (window.UserLoginDialog === undefined) {
                            $loadJs("/js/2014/UserLoginDialog.js", function () {
                                var c = new UserLoginDialog({
                                    callback: function () {
                                        location.reload()
                                    }
                                })
                            }.bind(this))
                        } else {
                            var g = new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        }
                    } else {
                        if (h.value.errcode == "checkfailed") {
                            if (h.value.giftskus) {
                                $alert("该商品不可售")
                            } else {
                                if (h.value.noqty) {
                                    $alert("该商品库存不足")
                                } else {
                                    if (h.value.nosale) {
                                        $alert("该商品不可售")
                                    } else {
                                    }
                                }
                            }
                        } else {
                            $alert("购买失败，请联系客服")
                        }
                    }
                }
            } else {
                $alert("网络异常，请稍后重试！")
            }
        }.bind(this), this.buyNowBtn)
    },
    loadDataCallBack: function (b) {
        if (b && b.value) {
            this.skuList = b.value.skus;
            this.maxcountincart = b.value.maxcountincart;
            this.goodsStatus = b.value.goodsStatus;
            this.goodsTotalQty = b.value.qty;
            this.renderDialog(b.value);
            this.container.removeClassName("s_proerr");
            this.qtyRegion = this.container.getElementsBySelector('em[data-selector="qtyRegion"]')[0];
            this.buycountInput = this.container.getElementsBySelector('input[data-selector="buycount"]')[0];
            this.salePriceRegion = this.container.getElementsBySelector('strong[data-selector="salePriceRegion"]')[0];
            this.marketPriceRegion = this.container.getElementsBySelector('i[data-selector="marketPriceRegion"]')[0];
            this.confirmBtn = this.container.getElementsBySelector('a[method="confirm"]')[0];
            this.onKeydownBuycountInputHandler = this.onKeydownBuycountInput.bind(this);
            this.buycountInput && Event.observe(this.buycountInput, "keypress", this.onKeydownBuycountInputHandler);
            this.initByParams();
            this.autoSelectProperty()
        }
    },
    renderDialog: function (m) {
        var n = new StringBuilder();
        n.append('<i class="sharp"></i>');
        n.append(String.format('<div class="s_proprice"><span>￥<strong data-selector="salePriceRegion">{0}</strong></span>', this.getPriceRangeText(null, m.lowestSalePrice, m.highestSalePrice)));
        if (m.lowestMartketPrice > 0) {
            n.append(String.format('<em style="">市场价：<i data-selector="marketPriceRegion" style="text-decoration:line-through;">￥{0}</i></em>', this.priceToString(m.lowestMartketPrice)))
        } else {
            n.append(String.format('<em style="display:none;">市场价：<i data-selector="marketPriceRegion" style="text-decoration:line-through;">￥{0}</i></em>', this.priceToString(m.lowestMartketPrice)))
        }
        n.append("</div>");
        n.append('<div class="s_proboxs">');
        n.append('<i method="closeTip" class="close" title="关闭"></i>');
        n.append('<span class="err_txt">请选择您要的商品信息</span>');
        n.append('<dl class="s_prodes">');
        if (m.properties && m.properties.length > 0) {
            for (var o = 0; o < m.properties.length; o++) {
                var p = m.properties[o];
                n.append(String.format('<dd data-selector="property" pid="{1}"><strong>{0}：</strong>', p.displayName, p.propertyId));
                n.append(this.renderPropertyValue(p));
                n.append("</dd>")
            }
        }
        n.append("<dd><strong>数量：</strong>");
        n.append('<div class="num_ctrl">');
        n.append('<a method="reduce" href="#" class="minus false" onclick="return false;"></a>');
        n.append('<input id="buycountInput" data-selector="buycount" type="text" value="1">');
        n.append('<a method="add" href="#" class="add" onclick="return false;"></a>');
        n.append("</div>");
        var q = "";
        var k = "";
        if (m.displaystockless == true) {
            q = "库存紧张";
            k = "stockless"
        }
        if (m.flashSale) {
            n.append(String.format('<span><em class ="' + k + '" data-selector="qtyRegion" qty="' + m.qty + '">{0}</em>每人限购{1}件</span>', q, m.maxcountincart))
        } else {
            n.append(String.format('<span><em class ="' + k + '" data-selector="qtyRegion" qty="' + m.qty + '">{0}</em></span>', q))
        }
        n.append("</dd>");
        n.append("</dl>");
        n.append("</div>");
        n.append('<div class="s_tipbtnbox">');
        var l = "return false;";
        if (this.confirmPan != "") {
            l = "if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.confirmPan + "'); } return false;"
        }
        var j = "return false;";
        if (this.cancelPan != "") {
            j = "if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.cancelPan + "'); } return false;"
        }
        n.append('<a method="confirm" href="#" class="btn_red" onclick="' + l + '">确定</a>');
        n.append('<a method="cancel" href="#" class="btn_grey" onclick="' + j + '">取消</a>');
        n.append("</div>");
        this.container.innerHTML = n.toString();
        this.container.show()
    },
    renderPropertyValue: function (m) {
        var h = "return false;";
        if (this.propertyPan != "") {
            h = "if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.propertyPan + "'); } return false;"
        }
        var k = new StringBuilder();
        if (m.primary) {
            k.append('<span class="list colorimg">');
            for (var l = 0; l < m.values.length; l++) {
                var o = m.values[l];
                var j = o.sale ? "" : "false";
                var n = m.propertyId + "_" + o.vid;
                k.append(String.format('<a method="pvkey" href="#" title="{0}" pvkey="{1}" class="{2}" onclick="' + h + '"><img method="pvkey" src="{3}" width="50" height="50"/><em style="display:none"></em></a>', o.vname, n, j, o.img))
            }
        } else {
            k.append('<span class="list ">');
            for (var l = 0; l < m.values.length; l++) {
                var o = m.values[l];
                var j = o.sale ? "" : "false";
                var n = m.propertyId + "_" + o.vid;
                k.append(String.format('<a method="pvkey" pvkey="{1}" class="{2}" href="#" title="{0}" class="false" onclick="' + h + '"><i method="pvkey">{0}</i><em style="display:none"></em></a>', o.vname, n, j))
            }
        }
        k.append("</span>");
        return k.toString()
    },
    destroyField: function () {
    },
    destroyEvent: function () {
        this.container && Event.stopObserving(this.container, "click", this.onClickContainerHandler);
        this.clickElement && Event.stopObserving(this.clickElement, "click", this.onClickElementHandler);
        Event.stopObserving(document, "click", this.onClickDocumentHandler)
    },
    destroyDOM: function () {
    },
    getPriceRangeText: function (f, h, g, e) {
        if (!e) {
            e = 1000
        }
        if (f) {
            return this.priceToString(f.saleprice * e * 0.001)
        } else {
            if (h == g) {
                return this.priceToString(h * e * 0.001)
            } else {
                return this.priceToString(h * e * 0.001) + "-" + this.priceToString(g * e * 0.001)
            }
        }
    },
    priceToString: function (g) {
        var h = g / 100;
        var f = parseInt(h);
        if (h == f) {
            return h
        } else {
            var b = g - f * 100;
            return f + "." + b.toPaddedString(2)
        }
    },
    addOverlay: function (j) {
        if (typeof j === "undefined" || j === null) {
            return
        }
        var k = new Date().getTime();
        var h = "submitOverlay_" + k;
        var f = j.parentNode;
        var g = Element.extend(document.createElement("div"));
        g.id = h;
        g.className = "submiting";
        document.body.appendChild(g);
        Position.clone(j, g);
        return g
    },
    removeOverlay: function (d, c) {
        if (typeof d === "undefined" || d === null) {
            return
        }
        if (c) {
            FunctionExt.defer(function (b, a) {
                document.body.removeChild(a)
            }, 1000, this, [d, c])
        }
    },
    close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    },
    autoSelectProperty: function () {
        this.container.getElementsBySelector('span[class^="list "]').each(function (d) {
            var c = d.getElementsBySelector('a[method="pvkey"]');
            if (c && c.length == 1 && !c[0].hasClassName("on")) {
                this.selectProperty(c[0])
            }
        }.bind(this))
    },
    initByParams: function () {
        this.container.getElementsBySelector('a[method="pvkey"]').each(function (b) {
            if (this.params && this.params.length > 0) {
                if (this.params.include(b.readAttribute("pvkey"))) {
                    this.selectProperty(b)
                }
            }
        }.bind(this))
    }
});
var NewSelectSkuLayer = Class.create();
Object.extend(NewSelectSkuLayer.prototype, {
    server: {
        getGoodsInfoByGoodsId: function (e, d, f) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "GetGoodsInfosByGoodsIdForDialog", [e], d, "/Ecommerce.api", "get", "60000", null)
        }, buyNow: function (e, d, f) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "BuyNow", [e], d, "/Ecommerce.api", "get", "60000", f)
        }, addCart: function (e, d, f) {
            return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "AddCart", [e], d, "/Ecommerce.api", "get", "60000", f)
        }
    },
    options: {
        goodsId: 0,
        container: null,
        clickElement: null,
        submitCallBack: null,
        params: null,
        pageName: "",
        propertyPan: "",
        confirmPan: "",
        cancelPan: "",
        addToCartOrBuyNow: "buyNow"
    },
    initialize: function (b) {
        this.setOptions(b);
        this.load()
    },
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initializeDom: function () {
        this.container = $("container");
        this.buycountInput = $("buycountInput1")
    },
    initializeEvent: function () {
        this.onClickContainerHandler = this.onClickContainer.bind(this);
        this.container && Event.observe(this.container, "click", this.onClickContainerHandler);
        this.onClickDocumentHandler = this.onClickDocument.bind(this);
        Event.observe(document, "click", this.onClickDocumentHandler);
        this.onKeydownBuycountInputHandler = this.onKeydownBuycountInput.bind(this);
        this.buycountInput && Event.observe(this.buycountInput, "keypress", this.onKeydownBuycountInputHandler);
        Event.observe(window, "unload", this.close.bind(this))
    },
    onClickDocument: function (d) {
        var c = Event.element(d);
        if (c == document) {
            this.container.hide()
        }
    },
    load: function () {
        if (this.goodsId > 0) {
            this.server.getGoodsInfoByGoodsId(this.goodsId, this.loadDataCallBack.bind(this), this.clickElement)
        }
    },
    onClickContainer: function (e) {
        var d = $(Event.element(e));
        if (d && d != document) {
            var f = d.readAttribute("method");
            switch (f) {
                case"reduce":
                    this.reduceCount(d);
                    break;
                case"add":
                    this.addCount(d);
                    break;
                case"pvkey":
                    this.selectProperty(d);
                    break;
                case"confirm":
                    if (this.addToCartOrBuyNow == "buyNow") {
                        this.onClickBuyNowBtn()
                    } else {
                        this.onClickAddCartBtn()
                    }
                    break;
                case"cancel":
                    this.container.update();
                    this.container.hide();
                    break;
                case"closeTip":
                    $$(".my_layer.sg_tip")[0].removeClassName("sg_tiperr");
                    break;
                default:
                    break
            }
        }
    },
    onKeydownBuycountInput: function (b) {
        setTimeout(function () {
            var g = isNaN(Number(this.qtyRegion.readAttribute("maxqty"))) ? 0 : Number(this.qtyRegion.readAttribute("maxqty"));
            var k = isNaN(Number(this.qtyRegion.readAttribute("qty"))) ? 0 : Number(this.qtyRegion.readAttribute("qty"));
            var j = isNaN(parseInt(this.buycountInput.value, 10)) ? 1 : parseInt(this.buycountInput.value, 10);
            var h = Math.min(this.maxcountincart, j);
            if (h <= 0) {
                h = 1
            }
            this.buycountInput.value = h;
            var a = Number(this.buycountInput.value);
            if (k > 0 && a < g) {
                this.buycountInput.next(0).removeClassName("false")
            }
            if (a <= 1) {
                this.buycountInput.previous(0).addClassName("false")
            } else {
                this.buycountInput.previous(0).removeClassName("false");
                if (a >= Math.min(g, this.maxcountincart) || k <= 0) {
                    this.buycountInput.next(0).addClassName("false")
                }
            }
        }.bind(this), 100)
    },
    reduceCount: function (e) {
        var h = isNaN(Number(this.qtyRegion.readAttribute("qty"))) ? 0 : Number(this.qtyRegion.readAttribute("qty"));
        var g = isNaN(Number(this.qtyRegion.readAttribute("maxqty"))) ? 0 : Number(this.qtyRegion.readAttribute("maxqty"));
        var f = Number(this.buycountInput.value);
        if (f <= 1) {
            return
        }
        f--;
        this.buycountInput.value = f;
        if (f <= 1) {
            e.addClassName("false")
        }
        if (h > 0 && f < g) {
            e.next("a").removeClassName("false")
        }
    },
    addCount: function (e) {
        var h = isNaN(Number(this.qtyRegion.readAttribute("qty"))) ? 0 : Number(this.qtyRegion.readAttribute("qty"));
        var g = isNaN(Number(this.qtyRegion.readAttribute("maxqty"))) ? 0 : Number(this.qtyRegion.readAttribute("maxqty"));
        var f = Number(this.buycountInput.value);
        if (f >= Math.min(g, this.maxcountincart)) {
            return
        }
        f++;
        this.buycountInput.value = f;
        if (f >= Math.min(g, this.maxcountincart) || h <= 0) {
            e.addClassName("false")
        }
        if (f > 1) {
            e.previous("a").removeClassName("false")
        }
    },
    selectProperty: function (f) {
        if (f.tagName == "IMG") {
            $("viewimg").setAttribute("src", f.readAttribute("bigimg"))
        } else {
            if (f.down(0) != undefined && f.down(0).tagName == "IMG") {
                $("viewimg").setAttribute("src", f.down(0).readAttribute("bigimg"))
            }
        }
        f = f.tagName == "A" ? f : f.up("a");
        this.currClickPvKey = f.readAttribute("pvkey");
        var g = f.readAttribute("pvkey");
        f.up("dd").getElementsBySelector("a[method=pvkey]").each(function (a) {
            if (a.readAttribute("pvkey") == g) {
                a.toggleClassName("on");
                if (a.down("em")) {
                    a.down("em").toggle()
                }
            } else {
                a.removeClassName("on");
                if (a.down("em")) {
                    a.down("em").hide()
                }
            }
        });
        this.setPropertyValueDomStyle(f);
        var j = this.getSelectedSku();
        if (j) {
            $$(".my_layer.sg_tip")[0].removeClassName("sg_tiperr");
            this.setBuyBtnStyle(j)
        } else {
            var k = this.goodsStatus;
            var h = this.goodsTotalQty;
            this.styleControl(k, h, this.goodsMaxQty, this.displaystockless)
        }
    },
    setPropertyValueDomStyle: function (j) {
        var o = this.container.getElementsBySelector('[method="pvkey"]').findAll(function (a) {
            return a.hasClassName("on")
        });
        var l = this.skuList.clone(), m = 0;
        for (m = 0; m < o.length; m++) {
            l = l.findAll(function (a) {
                return a.pvkey.split("|").include(o[m].readAttribute("pvkey")) && a.qty > 0 && a.status == 1
            })
        }
        var h = this.container.getElementsBySelector('[data-selector="property"]');
        for (m = 0; m < h.length; m++) {
            var k = h[m];
            if (j.hasClassName("on") && j.up("dd").readAttribute("pid") == k.readAttribute("pid")) {
                continue
            }
            var n = k.getElementsBySelector("a").findAll(function (a) {
                    return a.hasClassName("on")
                }).length > 0;
            k.getElementsBySelector("a").each(function (b) {
                var c = null;
                if (n) {
                    var a = b.hasClassName("on");
                    if (a) {
                        c = l
                    } else {
                        c = this.skuList
                    }
                } else {
                    c = l
                }
                var d = c.find(function (e) {
                    return e.pvkey.split("|").include(b.readAttribute("pvkey")) && e.qty > 0 && e.status == 1
                }.bind(this));
                if (d) {
                    b.removeClassName("false")
                } else {
                    b.addClassName("false")
                }
            }.bind(this))
        }
    },
    getSelectedSku: function () {
        if (this.skuList && this.skuList.length > 0) {
            var o = [], j = 0;
            this.container.getElementsBySelector('a[method="pvkey"]').each(function (a) {
                if (a.hasClassName("on")) {
                    o.push(a.readAttribute("pvkey"))
                }
            });
            var q = this.skuList.clone();
            for (j = 0; j < o.length; j++) {
                q = q.findAll(function (a) {
                    return a.pvkey.split("|").include(o[j])
                })
            }
            var n = this.container.getElementsBySelector('[data-selector="property"]').length;
            if (o.length == n && q.length == 1) {
                this.container.getElementsBySelector('a[method="pvkey"]').each(function (a) {
                    if (q[0].pvkey.split("|").include(a.readAttribute("pvkey"))) {
                        a.removeClassName("false")
                    }
                });
                return q[0]
            } else {
                var m = q[0].saleprice, l = q[0].saleprice;
                for (var k = 0; k < q.length; k++) {
                    var p = q[k].saleprice;
                    if (p < m) {
                        m = p
                    }
                    if (p > m) {
                        l = p
                    }
                }
                this.salePriceRegion.update(this.getPriceRangeText(null, m, l))
            }
        }
        return null
    },
    setBuyBtnStyle: function (d) {
        this.salePriceRegion.update(this.priceToString(d.saleprice));
        this.qtyRegion.setAttribute("qty", d.qty);
        this.qtyRegion.setAttribute("maxqty", d.availMaxQty);
        var c = Number(this.buycountInput.value);
        this.buycountInput.value = Math.min(c, d.availMaxQty);
        this.styleControl(d.status, d.qty, d.availMaxQty, d.displaystockless)
    },
    styleControl: function (h, g, f, e) {
        if (g <= 0 || h != 1) {
            this.qtyRegion.setAttribute("qty", 0);
            this.qtyRegion.innerHTML = "";
            this.buyNowBtn.update('<i class="shouqing"></i>已售罄');
            this.buyNowBtn.addClassName("btn_false")
        } else {
            this.buycountInput.value = this.buycountInput.value == 0 ? 1 : this.buycountInput.value;
            if (e == true) {
                this.qtyRegion.addClassName("stockless");
                this.qtyRegion.innerHTML = "库存紧张"
            } else {
                this.qtyRegion.innerHTML = ""
            }
            this.qtyRegion.setAttribute("qty", g);
            this.buyNowBtn.update("确认");
            this.buyNowBtn.removeClassName("btn_false")
        }
        this.qtyRegion.setAttribute("maxqty", f)
    },
    onClickAddCartBtn: function (h) {
        if (this.buyNowBtn.hasClassName("btn_false")) {
            return
        }
        var g = this.getSelectedSku();
        if (g != null) {
            var f = Number(this.buycountInput.value);
            var k = g.sku + "|" + f;
            if (this.buyfullgiftacts != null && this.buyfullgiftacts.length > 0) {
                var j = this.buyfullgiftacts[0];
                k += "|0|false"
            } else {
                k += "|0|false"
            }
            this.server.addCart(k, function (a) {
                if (a && a.value) {
                    if (a.value.success) {
                        location.href = a.value.cartPage
                    } else {
                        if (a.value.errcode == "checkfailed") {
                            if (a.value.giftskus) {
                                $alert("该商品不可售")
                            } else {
                                if (a.value.noqty) {
                                    $alert("该商品库存不足")
                                } else {
                                    if (a.value.nosale) {
                                        $alert("该商品不可售")
                                    } else {
                                        if (a.value.morethanlimitbuy) {
                                            $alert("该商品超过限购数量")
                                        } else {
                                        }
                                    }
                                }
                            }
                        } else {
                            $alert("添加购物车失败")
                        }
                    }
                } else {
                    $alert("网络异常，请稍后重试")
                }
            }.bind(this), this.buyNowBtn)
        } else {
            $$(".my_layer.sg_tip")[0].addClassName("sg_tiperr")
        }
    },
    onClickBuyNowBtn: function () {
        if (this.buyNowBtn.hasClassName("btn_false")) {
            return
        }
        var e = this.getSelectedSku();
        if (e != null) {
            $$(".my_layer.sg_tip")[0].removeClassName("sg_tiperr");
            var d = Number(this.buycountInput.value);
            var f = e.sku + "|" + d;
            this.server.buyNow(f, function (b) {
                if (b && b.value) {
                    if (b.value.success) {
                        this.buyNow(f)
                    } else {
                        if (b.value.errcode == "nologin") {
                            if (window.UserLoginDialog === undefined) {
                                $loadJs("/js/2014/UserLoginDialog.js", function () {
                                    var c = new UserLoginDialog({
                                        callback: function () {
                                            this.buyNow(f)
                                        }.bind(this)
                                    })
                                }.bind(this))
                            } else {
                                var a = new UserLoginDialog({
                                    callback: function () {
                                        this.buyNow(f)
                                    }.bind(this)
                                })
                            }
                        } else {
                            if (b.value.errcode == "checkfailed") {
                                if (b.value.morethanlimitbuy) {
                                    $alert("你所购买的商品数量已超过活动限购数")
                                }
                                if (b.value.giftskus) {
                                    $alert("该商品不可售")
                                } else {
                                    if (b.value.noqty) {
                                        $alert("该商品库存不足")
                                    } else {
                                        if (b.value.nosale) {
                                            $alert("该商品不可售")
                                        } else {
                                        }
                                    }
                                }
                            } else {
                                $alert("购买失败，请联系客服")
                            }
                        }
                    }
                } else {
                    $alert("网络异常，请稍后重试！")
                }
            }.bind(this), this.buyNowBtn)
        } else {
            $$(".my_layer.sg_tip")[0].addClassName("sg_tiperr")
        }
    },
    buyNow: function (b) {
        this.server.buyNow(b, function (h) {
            if (h && h.value) {
                if (h.value.success) {
                    var a = Builder.node("form", {style: "display:none", method: "post", action: h.value.confirmpage});
                    var f = Builder.node("input", {type: "hidden", name: "skus", value: b});
                    a.appendChild(f);
                    document.body.appendChild(a);
                    a.submit()
                } else {
                    if (h.value.errcode == "nologin") {
                        if (window.UserLoginDialog === undefined) {
                            $loadJs("/js/2014/UserLoginDialog.js", function () {
                                var c = new UserLoginDialog({
                                    callback: function () {
                                        location.reload()
                                    }
                                })
                            }.bind(this))
                        } else {
                            var g = new UserLoginDialog({
                                callback: function () {
                                    location.reload()
                                }
                            })
                        }
                    } else {
                        if (h.value.errcode == "checkfailed") {
                            if (h.value.morethanlimitbuy) {
                                $alert("你所购买的商品数量已超过活动限购数")
                            } else {
                                if (h.value.giftskus) {
                                    $alert("该商品不可售")
                                } else {
                                    if (h.value.noqty) {
                                        $alert("该商品库存不足")
                                    } else {
                                        if (h.value.nosale) {
                                            $alert("该商品不可售")
                                        } else {
                                        }
                                    }
                                }
                            }
                        } else {
                            $alert("购买失败，请联系客服")
                        }
                    }
                }
            } else {
                $alert("网络异常，请稍后重试！")
            }
        }.bind(this), this.buyNowBtn)
    },
    loadDataCallBack: function (b) {
        if (b && b.value) {
            this.skuList = b.value.skus;
            this.maxcountincart = b.value.maxcountincart;
            this.goodsStatus = b.value.goodsStatus;
            this.goodsTotalQty = b.value.qty;
            this.goodsMaxQty = b.value.goodsMaxQty;
            this.displaystockless = b.value.displaystockless;
            this.renderDialog(b.value);
            this.container.removeClassName("s_proerr");
            this.qtyRegion = this.container.getElementsBySelector('em[data-selector="qtyRegion"]')[0];
            this.buycountInput = this.container.getElementsBySelector('input[data-selector="buycount1"]')[0];
            this.salePriceRegion = this.container.getElementsBySelector('b[data-selector="salePriceRegion"]')[0];
            this.buyNowBtn = this.container.getElementsBySelector('a[method="confirm"]')[0];
            this.initByParams();
            this.autoSelectProperty()
        }
    },
    renderDialog: function (l) {
        var m = new StringBuilder();
        m.append('<div id="container">');
        m.append('<span class="err_txt">请选择您要的商品信息</span>');
        m.append('<span title="关闭" class="err_close" method="closeTip"></span>');
        m.append('<div class="sg_tipbox">');
        m.append('    	<img width="120" height="120" class="img" id="viewimg" alt="商品名称" src="' + l.goodsmainpic + '">');
        m.append('        <div class="s_proboxs">');
        m.append('            <dl class="s_prodes">');
        var o = "价格";
        if (l.paymentmode && l.paymentmode == 2) {
            o = "定金"
        }
        m.append("            	<dd><strong>" + o + '：</strong><span class="sg_price">￥<b data-selector="salePriceRegion">' + this.getPriceRangeText(null, l.lowestSalePrice, l.highestSalePrice) + "</b></span></dd>");
        if (l.properties.length > 0) {
            for (var n = 0; n < l.properties.length; n++) {
                var p = l.properties[n];
                m.append(String.format('<dd data-selector="property" pid="{1}"><strong>{0}：</strong>', p.displayName, p.propertyId));
                m.append(this.renderPropertyValue(p));
                m.append("</dd>")
            }
        }
        m.append("                <dd><strong>数量：</strong>");
        m.append('                    <div class="num_ctrl">');
        m.append('<a method="reduce" href="#" class="minus false" onclick="return false;"></a>');
        m.append('<input id="buycountInput1" data-selector="buycount1" type="text" value="1">');
        m.append('<a method="add" href="#" class="add" onclick="return false;"></a>');
        m.append("                    </div>");
        var q = "";
        var j = "";
        if (l.displaystockless == true) {
            q = "库存紧张";
            j = "stockless"
        }
        if (l.flashSale) {
            m.append(String.format('<span><em class ="' + j + '" data-selector="qtyRegion" maxqty="' + l.goodsMaxQty + '" qty="' + l.qty + '">{0}</em> 每人限购{1}件</span>', q, l.maxcountincart))
        } else {
            m.append(String.format('<span><em class ="' + j + '" data-selector="qtyRegion" maxqty="' + l.goodsMaxQty + '" qty="' + l.qty + '">{0}</em></span>', q))
        }
        m.append("                  </dd>");
        m.append("            </dl>");
        m.append("        </div>");
        m.append("</div>");
        var k = "return false;";
        if (this.confirmPan != "") {
            k = "if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.confirmPan + "'); } return false;"
        }
        m.append('<div class="sg_tipbtn"><a  method="confirm" href="#" class="btn_red" onclick="' + k + '">确认</a></div>');
        m.append("</div>");
        this.dialog = new Dialog({
            title: l.goodsname,
            windowClassName: "sg_tip",
            content: m.toString(),
            buttons: [],
            readyCallback: this.onReady.bind(this)
        })
    },
    onReady: function (b) {
        this.initializeDom();
        this.initializeEvent();
        b.buttonRegion.hide()
    },
    renderPropertyValue: function (m) {
        var h = "return false;";
        if (this.propertyPan != "") {
            h = "if (typeof tracker != 'undefined') { tracker.trackClick('" + this.pageName + "', '" + this.propertyPan + "'); } return false;"
        }
        var k = new StringBuilder();
        if (m.primary) {
            k.append('<span class="list colorimg">');
            for (var l = 0; l < m.values.length; l++) {
                var o = m.values[l];
                var j = o.sale ? "" : "false";
                var n = m.propertyId + "_" + o.vid;
                k.append(String.format('<a method="pvkey" href="#" title="{0}" pvkey="{1}" class="{2}" onclick="' + h + '"><img method="pvkey" src="{3}" width="50" height="50" bigimg="{4}"/><em style="display:none"></em></a>', o.vname, n, j, o.img, o.bigimg))
            }
        } else {
            k.append('<span class="list ">');
            for (var l = 0; l < m.values.length; l++) {
                var o = m.values[l];
                var j = o.sale ? "" : "false";
                var n = m.propertyId + "_" + o.vid;
                k.append(String.format('<a method="pvkey" pvkey="{1}" class="{2}" href="#" title="{0}" class="false" onclick="' + h + '"><i method="pvkey">{0}</i><em style="display:none"></em></a>', o.vname, n, j))
            }
        }
        k.append("</span>");
        return k.toString()
    },
    destroyField: function () {
    },
    destroyEvent: function () {
        this.container && Event.stopObserving(this.container, "click", this.onClickContainerHandler);
        this.clickElement && Event.stopObserving(this.clickElement, "click", this.onClickElementHandler);
        Event.stopObserving(document, "click", this.onClickDocumentHandler)
    },
    destroyDOM: function () {
    },
    getPriceRangeText: function (f, h, g, e) {
        if (!e) {
            e = 1000
        }
        if (f) {
            return this.priceToString(f.saleprice * e * 0.001)
        } else {
            if (h == g) {
                return this.priceToString(h * e * 0.001)
            } else {
                return this.priceToString(h * e * 0.001) + "-" + this.priceToString(g * e * 0.001)
            }
        }
    },
    priceToString: function (g) {
        var h = g / 100;
        var f = parseInt(h);
        if (h == f) {
            return h
        } else {
            var b = g - f * 100;
            return f + "." + b.toPaddedString(2)
        }
    },
    close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    },
    autoSelectProperty: function () {
        this.container.getElementsBySelector('span[class^="list "]').each(function (d) {
            var c = d.getElementsBySelector('a[method="pvkey"]');
            if (c && c.length == 1 && !c[0].hasClassName("on")) {
                this.selectProperty(c[0])
            }
        }.bind(this))
    },
    initByParams: function () {
        this.container.getElementsBySelector('a[method="pvkey"]').each(function (b) {
            if (this.params && this.params.length > 0) {
                if (this.params.include(b.readAttribute("pvkey"))) {
                    this.selectProperty(b)
                }
            }
        }.bind(this))
    }
});
var FloatRegionControl = Class.create();
Object.extend(FloatRegionControl.prototype, {
    options: {
        referenceRegion: "",
        floatRegion: "",
        fixedBottom: null,
        callBack: null
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeDom();
        this.initializeEvent();
        this.load()
    }, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initializeDom: function () {
        this.referenceRegion = $(this.referenceRegion);
        this.floatRegion = $(this.floatRegion)
    }, initializeEvent: function () {
        this.myscrollhandler = this.myscrollWindow.bind(this);
        Event.observe(window, "scroll", this.myscrollhandler);
        this.onClickFloatRegionHandler = this.onClickFloatRegion.bind(this);
        this.floatRegion && Event.observe(this.floatRegion, "click", this.onClickFloatRegionHandler);
        Event.observe(window, "unload", this.close.bind(this))
    }, load: function () {
        var b = document.documentElement.scrollTop || document.body.scrollTop;
        this.coordY = this.referenceRegion.getBoundingClientRect().top + b
    }, onClickFloatRegion: function (e) {
        if (this.callBack) {
            var d = Event.element(e);
            if (d && d != document) {
                while (d.hasAttribute && !d.hasAttribute("method")) {
                    d = d.up()
                }
                if (!d.readAttribute) {
                    return
                }
                var f = d.readAttribute("method");
                if (f == "detail" || f == "review" || f == "purchase" || f == "flashrule") {
                    this.referenceRegion.getElementsBySelector("dd").each(function (a) {
                        if (a.down("a").readAttribute("method") == f) {
                            a.addClassName("on")
                        } else {
                            a.removeClassName("on")
                        }
                    });
                    this.floatRegion.getElementsBySelector("dd").each(function (a) {
                        if (a.down("a").readAttribute("method") == f) {
                            a.addClassName("on")
                        } else {
                            a.removeClassName("on")
                        }
                    })
                }
                this.callBack(d)
            }
        }
    }, myscrollWindow: function () {
        var c = this.fixedBottom ? this.fixedBottom.getBoundingClientRect().top : NaN;
        var d = this.referenceRegion.getBoundingClientRect().top + 15;
        if (d <= 0) {
            if (!isNaN(c) && c <= 0) {
                this.floatRegion && this.floatRegion.hide()
            } else {
                this.floatRegion && this.floatRegion.show();
                if (Mtime.browser.ie && Mtime.browser.ie < 7) {
                    this.floatRegion.style.top = document.documentElement.scrollTop + "px"
                }
            }
        } else {
            this.floatRegion && this.floatRegion.hide()
        }
    }, close: function () {
        this.destroyEvent();
        this.destroyDOM();
        this.destroyField()
    }, destroyField: function () {
    }, destroyEvent: function () {
        Event.stopObserving(window, "scroll", this.myscrollhandler);
        this.floatRegion && Event.stopObserving(this.floatRegion, "click", this.onClickFloatRegionHandler)
    }, destroyDOM: function () {
        this.referenceRegion = null;
        this.floatRegion = null
    }
});
!function () {
    var b = Class.create();
    Object.extend(b.prototype, {
        options: {
            popUpBoxNext: null,
            popUpBoxPrev: null,
            popUpBoxLoad: null,
            popUpBoxImg: null,
            baseDom: false,
            baseHtml: '<div class="popUpBox" style="display:none;opacity: 0.6;filter:alpha(opacity=60);-moz-opacity:0.6;zoom: 1; z-index: 110; position: absolute; margin: 0px; padding: 0px; top: 0px; left: 0px; width: 1920px; height: 5604px; background: rgb(0, 0, 0);"></div><div class="s_gradebesttip popUpBox" id="popUpBoxMain" style="display:none;"><span class="close" id="popUpBoxClose" title="关闭"></span><a href="javascript:void(0)" class="lastpic" id="popUpBoxPrev" title="上一张"></a><a href="javascript:void(0)" class="nextpic" id="popUpBoxNext" title="下一张"></a><img style="display: none" src="http://static1.mtime.cn/20141017100151/images/2014/load.png" alt="图片名称" id="popUpBoxImg" /><div class="load48" id="popUpBoxLoad"></div></div>',
            popState: false,
            popImgs: [],
            popCurrent: 0
        }, initialize: function (a) {
            this.initializeDOM(a);
            this.initializeControl()
        }, initializeField: function () {
            this.resizeAniHandler = this.resizeAni.bind(this);
            this.hideBaseDomHandler = this.hideBaseDom.bind(this);
            this.prevImgHandler = this.prevImg.bind(this);
            this.nextImgHandler = this.nextImg.bind(this)
        }, initializeDOM: function (a) {
        }, initializeEvent: function () {
            Event.observe(window, "resize", this.resizeAniHandler);
            Event.observe($("popUpBoxClose"), "click", this.hideBaseDomHandler);
            Event.observe($("popUpBoxPrev"), "click", this.prevImgHandler);
            Event.observe($("popUpBoxNext"), "click", this.nextImgHandler)
        }, initializeControl: function () {
        }, destroy: function () {
        }, destroyField: function () {
        }, destroyDOM: function () {
        }, destroyEvent: function () {
        }, destroyControl: function () {
        }, updateImgRes: function (a) {
            this.options.popImgs = a;
            return this
        }, show: function () {
            if (!this.options.baseDom) {
                this.createDom();
                this.initializeField();
                this.initializeEvent()
            }
            this.showBaseDom();
            this.updateImg(0);
            this.options.popState = true;
            document.body.style.overflow = "hidden"
        }, resizeAni: function (a) {
            if (this.options.popState) {
                this.updatePos()
            }
        }, createDom: function () {
            this.options.baseDom = true;
            new Insertion.Bottom(document.body, this.options.baseHtml)
        }, prevImg: function () {
            if (this.options.popCurrent <= 0) {
                return
            }
            --this.options.popCurrent;
            $("popUpBoxLoad").show();
            $("popUpBoxImg").hide();
            this.updateImg(this.options.popCurrent)
        }, nextImg: function () {
            if (this.options.popCurrent >= this.options.popImgs.length - 1) {
                return
            }
            ++this.options.popCurrent;
            $("popUpBoxLoad").show();
            this.updateImg(this.options.popCurrent)
        }, updateImg: function (a) {
            var f = this, e = this.options.popImgs[a];
            this.loadImg(e, function () {
                f.fadeOut($("popUpBoxImg"), function () {
                    $("popUpBoxImg").hide();
                    $("popUpBoxImg").setAttribute("src", e);
                    $("popUpBoxLoad").hide();
                    $("popUpBoxImg").show();
                    f.fadeIn($("popUpBoxImg"));
                    f.updatePos()
                })
            })
        }, updatePos: function () {
            var h = document.documentElement.clientWidth - 57 * 2 - 110, a = document.documentElement.clientHeight, g = $("popUpBoxImg");
            _w = g.width, _h = g.height;
            if (!g.getAttribute("data-baseWidth")) {
                g.setAttribute("data-baseWidth", _w)
            } else {
                _w = parseInt(g.getAttribute("data-baseWidth"), 10)
            }
            var f = function (d, c, m, l, e) {
                if (c > m) {
                    c = m - 200
                }
                if (e) {
                    c = c - 100;
                    console.log(c)
                }
                d.setStyle({width: c + "px", marginLeft: "-" + (c / 2) + "px"});
                if (d.height > l) {
                    f(d, c, m, l, true)
                } else {
                    d.setStyle({top: (l - d.height) / 2 + "px", marginTop: "0px"})
                }
            };
            f(g, _w, h, a);
            $("popUpBoxMain").setStyle({top: (document.documentElement.scrollTop + document.body.scrollTop) + "px"})
        }, showBaseDom: function () {
            this.updateStyle();
            $$("div.popUpBox")[0].show();
            $$("div.popUpBox")[1].show()
        }, hideBaseDom: function () {
            this.updateStyle();
            $$("div.popUpBox")[0].hide();
            $$("div.popUpBox")[1].hide();
            $("popUpBoxLoad").show();
            $("popUpBoxImg").hide();
            document.body.style.overflow = ""
        }, updateStyle: function () {
            $("popUpBoxMain").style.height = document.documentElement.clientHeight + "px"
        }, loadImg: function (j, g) {
            var k = this, h = new Image(), a = navigator.appName.toLowerCase();
            if (a.indexOf("netscape") == -1) {
                h.onreadystatechange = function () {
                    if (h.readyState == "complete") {
                        g && g()
                    }
                }
            } else {
                h.onload = function () {
                    if (h.complete == true) {
                        g && g()
                    }
                }
            }
            h.src = j
        }, animate: function (k) {
            var m = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
            var n = new Date(), a = k.delta || function (c) {
                    return c
                }, l, j;

            function o() {
                var c = new Date() - n;
                l = c / k.duration;
                l = l > 1 ? 1 : l;
                k.step(a(l));
                if (l < 1) {
                    if (m) {
                        m(o)
                    }
                } else {
                    k.callback && k.callback()
                }
            }

            if (m) {
                m(o)
            } else {
                j = setInterval(function () {
                    o();
                    l === 1 && clearInterval(j)
                }, k.delay || 25)
            }
        }, fadeIn: function (d, a) {
            this.animate({
                duration: 500, step: function (c) {
                    d.setStyle({opacity: c})
                }.bind(this), callback: a
            })
        }, fadeOut: function (d, a) {
            this.animate({
                duration: 500, step: function (c) {
                    d.setStyle({opacity: 1 - c})
                }.bind(this), callback: a
            })
        }
    });
    window.PopUpBox = b
}();
!function () {
    var b = Class.create();
    Object.extend(b.prototype, ScrollBar.prototype);
    Object.extend(b.prototype, {
        initializeDom: function () {
            var a = $(Builder.build(this.scrollbarTemplate));
            this.content.parentNode.appendChild(a);
            this.scrollbar = {container: a, tracker: a.down("em")};
            this.content.up().setAttribute("tabindex", "0")
        }
    });
    window.ScrollBarControl = b
}();
var Pager = Class.create();
Pager.prototype = {
    threshold: 7,
    maxThreshold: 10,
    options: {pageRegionId: "", pageIndex: 1, pageSize: 10, totalRecord: 1, callback: null},
    setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    },
    initialize: function (b) {
        this.setOptions(b);
        this.initializeDOM();
        this.initializeEvent()
    },
    initializeDOM: function () {
        this.pageRegion = $(this.pageRegionId)
    },
    initializeEvent: function () {
        this.onClickPageRegionHander = this.onClickPageRegion.bindAsEventListener(this);
        this.pageRegion.observe("click", this.onClickPageRegionHander)
    },
    removeEvent: function () {
        this.pageRegion.stopObserving("click", this.onClickPageRegionHander)
    },
    render: function (s, y) {
        this.pageIndex = parseInt(s, 10);
        if (typeof y != "undefined") {
            this.totalRecord = y
        }
        var x = this.threshold, o = this.maxThreshold, s = this.pageIndex, t = this.pageSize, y = this.totalRecord, r = Math.ceil(parseFloat(y / t));
        if (r > 1) {
            var q = new StringBuilder();
            q.append('<div class="pagenav tc" onclick="return false;">');
            if (s == 1) {
                q.append('<a class="mr10 false" onclick="return false;">上一页</a>')
            } else {
                var v = s - 1;
                q.append('<a href="#" class="mr10 next" pageIndex="' + v + '" onclick="return false;">上一页</a>')
            }
            var w = 0, n = r, u = -1;
            if (r <= o) {
                w = 1;
                n = r
            } else {
                if (s < x && r > o) {
                    w = 1;
                    n = x;
                    u = 0
                } else {
                    if (s > (r - x + 1)) {
                        w = r - x + 1;
                        n = r;
                        u = 1
                    } else {
                        if (s >= x && s <= (r - x + 1)) {
                            w = s - 2;
                            n = s + 2;
                            u = 2
                        }
                    }
                }
            }
            q.append(this._createPageNumberHTML(w, n, r, u, s));
            if (s == r) {
                q.append('<a class="mr10 false" onclick="return false;">下一页</a>')
            } else {
                var p = s + 1;
                q.append('<a href="#" class="mr10 next" pageIndex="' + p + '" onclick="return false;">下一页</a>')
            }
            q.append("</div>");
            this.pageRegion.update(q.toString())
        } else {
            this.pageRegion.update("")
        }
    },
    _createPageNumberHTML: function (s, k, o, r, p) {
        var q = new StringBuilder(), m = "", l = "";
        switch (r) {
            case 0:
                l = this._createBehindHTML(o);
                break;
            case 1:
                m = this._createFrontHTML();
                break;
            case 2:
                m = this._createFrontHTML();
                l = this._createBehindHTML(o);
                break;
            default:
                break
        }
        q.append(m);
        for (var n = s; n <= k; n++) {
            if (p == n) {
                q.append('<a href="#" class="on" onclick="return false;">');
                q.append(n);
                q.append("</a>")
            } else {
                q.append('<a class="num" href="#" onclick="return false;" pageIndex="' + n + '">');
                q.append(n);
                q.append("</a>")
            }
        }
        q.append(l);
        return q.toString()
    },
    _createBehindHTML: function (d) {
        var c = new StringBuilder();
        c.append("<span>...</span>");
        c.append('<a class="num" href="#" onclick="return false;" pageIndex="' + (d - 1) + '">');
        c.append(d - 1);
        c.append("</a>");
        c.append('<a class="num" href="#" onclick="return false;" pageIndex="' + d + '">');
        c.append(d);
        c.append("</a>");
        return c.toString()
    },
    _createFrontHTML: function () {
        var b = new StringBuilder();
        b.append('<a class="num" href="#" onclick="return false;" pageIndex="1">');
        b.append(1);
        b.append("</a>");
        b.append('<a class="num" href="#" onclick="return false;" pageIndex="2">');
        b.append(2);
        b.append("</a>");
        b.append("<span>...</span>");
        return b.toString()
    },
    onClickPageRegion: function (e) {
        var d = Event.element(e);
        var f = d.readAttribute("pageIndex");
        if (f && parseInt(f, 10) > 0) {
            f = parseInt(f, 10);
            this.callback && this.callback(f)
        }
    },
    close: function () {
        this.removeEvent();
        this.pageRegion.update("");
        this.pageRegion = null
    }
};