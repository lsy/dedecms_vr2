Widgets.DistanceSlidesControl = Class.create();
Object.extend(Widgets.DistanceSlidesControl.prototype, Widgets.Base.prototype);
Object.extend(Widgets.DistanceSlidesControl.prototype, {
    options: {
        slidesRegion: "",
        prev: "",
        next: "",
        showRegionWidth: 695,
        slidesRegionWidth: 1000,
        currentPosition: 0,
        itemWidth: 0,
        duration: 0.5,
        type: "neat"
    }, initializeDom: function () {
        this.$slidesRegion = $(this.slidesRegion);
        if (!this.$slidesRegion) {
            return
        }
        if (Mtime.browser.ie > 0 || Mtime.browser.ie < 9) {
            this.$slidesRegion.addClassName("transition6")
        }
        this.$prev = $(this.prev);
        this.$next = $(this.next);
        this.span = this.$next.next("span.shadow")
    }, initializeEvent: function () {
        this.onPrevClickHandler = this.onPrevClick.bind(this);
        this.onNextClickHandler = this.onNextClick.bind(this);
        Event.observe(this.$prev, "click", this.onPrevClickHandler);
        Event.observe(this.$next, "click", this.onNextClickHandler);
        Event.observe(window, "unload", this.close.bind(this))
    }, load: function () {
        this.reset()
    }, onLoader: function (b) {
    }, getItems: function () {
        this.$slidesRegions = this.$slidesRegion.immediateDescendants();
        this.length = this.$slidesRegions.length;
        if (this.itemWidth > 0) {
            this.amount = this.itemWidth * this.length
        } else {
            for (var b = 0; b < this.length; b++) {
                this.amount += this.$slidesRegions[b].clientWidth
            }
        }
        this.$slidesRegion.style.width = this.amount + 100 + "px";
        this.amountIndex = Math.ceil(this.amount / this.slidesRegionWidth)
    }, reset: function () {
        this.isReset = true;
        this.currentPosition = 0;
        this.$slidesRegion.style.left = this.currentPosition + "px";
        this.pageIndex = 0;
        this.amount = 0;
        this.getItems();
        if (this.amount > this.slidesRegionWidth) {
            this.$next.show();
            if (this.currentPosition === 0) {
                this.$prev.hide()
            } else {
                this.$prev.show()
            }
        } else {
            this.$prev.hide();
            this.$next.hide()
        }
    }, onPrevClick: function (c) {
        var d = c.target || c.srcElement;
        Event.stop(c);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
            this.$next.show();
            this.pageIndex--;
            this.currentPosition += this.slidesRegionWidth;
            if (this.pageIndex <= 0) {
                this.currentPosition = 0;
                this.$prev.hide()
            }
            this.$slidesRegion.style.left = this.currentPosition + "px"
        } else {
            this.prevSlides()
        }
    }, onNextClick: function (d) {
        var f = d.target || d.srcElement;
        Event.stop(d);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
            this.$prev.show();
            this.pageIndex++;
            this.currentPosition -= this.slidesRegionWidth;
            if (this.pageIndex >= this.amountIndex - 1) {
                var e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
                this.currentPosition = this.currentPosition + (this.slidesRegionWidth - e);
                this.$next.hide();
                if (this.span) {
                    this.span.hide()
                }
            }
            this.$slidesRegion.style.left = this.currentPosition + "px"
        } else {
            this.nextSlides()
        }
    }, prevSlides: function () {
        if (this.lock) {
            return
        }
        var c = null;
        var d = 0;
        this.$next.show();
        if (this.span) {
            this.span.show()
        }
        this.pageIndex--;
        d = this.slidesRegionWidth;
        if (this.pageIndex <= 0) {
            d = Math.abs(this.currentPosition);
            this.$prev.hide()
        }
        c = {
            node: this.slidesRegion,
            duration: this.duration,
            scale: d,
            position: this.currentPosition,
            callback: function () {
                if (this.pageIndex <= 0) {
                    this.currentPosition = 0
                }
            }.bind(this)
        };
        this.move(c);
        this.currentPosition += this.slidesRegionWidth
    }, nextSlides: function () {
        if (this.lock) {
            return
        }
        var d = null;
        var f = 0;
        this.$prev.show();
        this.pageIndex++;
        var e;
        f = -this.slidesRegionWidth;
        if (this.pageIndex >= this.amountIndex - 1) {
            e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
            f = f + (this.slidesRegionWidth - e);
            this.$next.hide();
            if (this.span) {
                this.span.hide()
            }
        }
        d = {
            node: this.slidesRegion,
            duration: this.duration,
            scale: f,
            position: this.currentPosition,
            callback: function () {
                if (this.pageIndex >= this.amountIndex - 1) {
                    e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
                    this.currentPosition = this.currentPosition + (this.slidesRegionWidth - e)
                }
            }.bind(this)
        };
        this.move(d);
        this.currentPosition -= this.slidesRegionWidth
    }, move: function (b) {
        this.lock = true;
        new Fx.Move(b.node, {
            scaleX: true,
            duration: b.duration,
            distances: {left: b.scale, position: b.position},
            callback: function () {
                this.lock = false;
                b.callback && b.callback()
            }.bind(this)
        })
    }, destroyEvent: function () {
        this.onPrevClickHandler && Event.stopObserving(this.$prev, "click", this.onPrevClickHandler);
        this.onNextClickHandler && Event.stopObserving(this.$next, "click", this.onNextClickHandler)
    }, destroyDOM: function () {
        this.slidesRegion = null;
        this.prev = null;
        this.next = null
    }
});
Widgets.indexDistanceSlidesControl = Class.create();
Object.extend(Widgets.indexDistanceSlidesControl.prototype, Widgets.DistanceSlidesControl.prototype);
Object.extend(Widgets.indexDistanceSlidesControl.prototype, {
    initializeDom: function () {
        this.$slidesRegion = $(this.slidesRegion);
        if (Mtime.browser.ie > 0 || Mtime.browser.ie < 9) {
            this.$slidesRegion.addClassName("transition6")
        }
        this.$prev = $(this.prev);
        this.$next = $(this.next);
        this.spanr = this.$next.next("i.shadowr");
        this.spanl = this.$next.next("i.shadowl");
        this.spanl && (this.spanl.hide())
    }, getItems: function () {
        this.$slidesRegions = this.$slidesRegion.immediateDescendants();
        this.length = this.$slidesRegions.length;
        if (this.itemWidth > 0) {
            this.amount = this.itemWidth * this.length
        } else {
            for (var b = 0; b < this.length; b++) {
                this.amount += this.$slidesRegions[b].clientWidth
            }
        }
        this.$slidesRegion.style.width = this.amount + 100 + "px";
        this.amountIndex = Math.ceil(this.amount / this.slidesRegionWidth) - 1
    }, onPrevClick: function (c) {
        var d = c.target || c.srcElement;
        Event.stop(c);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        this.spanr.show();
        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
            this.$next.show();
            this.pageIndex--;
            this.currentPosition += this.slidesRegionWidth;
            if (this.pageIndex <= 0) {
                this.currentPosition = 0;
                this.$prev.hide();
                this.spanl && (this.spanl.hide())
            }
            this.$slidesRegion.style.left = this.currentPosition + "px"
        } else {
            this.prevSlides()
        }
    }, onNextClick: function (d) {
        var f = d.target || d.srcElement;
        Event.stop(d);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        this.spanl.show();
        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
            this.$prev.show();
            this.pageIndex++;
            this.currentPosition -= this.slidesRegionWidth;
            this.$slidesRegion.style.left = this.currentPosition + "px";
            if (this.pageIndex >= this.amountIndex - 1) {
                var e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
                this.$next.hide();
                this.spanr && (this.spanr.hide())
            }
        } else {
            this.nextSlides()
        }
    }, prevSlides: function () {
        if (this.lock) {
            return
        }
        var c = null;
        var d = 0;
        this.$next.show();
        this.pageIndex--;
        d = this.slidesRegionWidth;
        c = {
            node: this.slidesRegion,
            duration: this.duration,
            scale: d,
            position: this.currentPosition,
            callback: function () {
                if (this.pageIndex <= 0) {
                    this.currentPosition = 0;
                    this.spanl && (this.spanl.hide())
                }
            }.bind(this)
        };
        this.move(c);
        if (this.pageIndex <= 0) {
            this.pageIndex = 0;
            d = Math.abs(this.currentPosition);
            this.$prev.hide()
        }
        this.currentPosition += this.slidesRegionWidth
    }, nextSlides: function () {
        if (this.lock) {
            return
        }
        var d = null;
        var f = 0;
        this.$prev.show();
        this.pageIndex++;
        var e;
        f = -this.slidesRegionWidth;
        d = {
            node: this.slidesRegion,
            duration: this.duration,
            scale: f,
            position: this.currentPosition,
            callback: function () {
                if (this.pageIndex >= this.amountIndex - 1) {
                    e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
                    this.spanr && (this.spanr.hide())
                }
            }.bind(this)
        };
        this.move(d);
        if (this.pageIndex >= this.amountIndex - 1) {
            e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
            f = f + (this.slidesRegionWidth - e);
            this.$next.hide();
            if (this.span) {
                this.span.hide()
            }
        }
        this.currentPosition -= this.slidesRegionWidth
    }
});
Widgets.ShowButtonDistanceSlidesControl = Class.create();
Object.extend(Widgets.ShowButtonDistanceSlidesControl.prototype, Widgets.DistanceSlidesControl.prototype);
Object.extend(Widgets.ShowButtonDistanceSlidesControl.prototype, {
    options: {
        slidesRegion: "",
        prev: "",
        next: "",
        prevCss: "",
        nextCss: "",
        showRegionWidth: 695,
        slidesRegionWidth: 1000,
        currentPosition: 0,
        itemWidth: 0,
        duration: 0.5,
        type: "neat"
    }, reset: function () {
        this.isReset = true;
        this.currentPosition = 0;
        this.$slidesRegion.style.left = this.currentPosition + "px";
        this.pageIndex = 0;
        this.amount = 0;
        this.getItems();
        if (this.amount > this.slidesRegionWidth) {
            this.showNextbtn();
            if (this.currentPosition === 0) {
                this.hidePrevbtn()
            } else {
                this.showPrevbtn()
            }
        } else {
            this.$prev.hide();
            this.$next.hide()
        }
    }, onPrevClick: function (c) {
        var d = c.target || c.srcElement;
        Event.stop(c);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        this.showNextbtn();
        this.pageIndex--;
        this.currentPosition += this.slidesRegionWidth;
        if (this.pageIndex <= 0) {
            this.currentPosition = 0;
            this.hidePrevbtn()
        }
        this.$slidesRegion.style.left = this.currentPosition + "px"
    }, showPrevbtn: function () {
        if (this.prevCss != "") {
            if (this.$prev.hasClassName(this.prevCss)) {
                this.$prev.removeClassName(this.prevCss)
            }
        } else {
            this.$prev.show()
        }
    }, hidePrevbtn: function () {
        if (this.prevCss != "") {
            if (!this.$prev.hasClassName(this.prevCss)) {
                this.$prev.addClassName(this.prevCss)
            }
        } else {
            this.$prev.show()
        }
    }, showNextbtn: function () {
        if (this.nextCss != "") {
            if (this.$next.hasClassName(this.nextCss)) {
                this.$next.removeClassName(this.nextCss)
            }
        } else {
            this.$next.show()
        }
    }, hideNextbtn: function () {
        if (this.nextCss != "") {
            if (!this.$next.hasClassName(this.nextCss)) {
                this.$next.addClassName(this.nextCss)
            }
        } else {
            this.$next.hide()
        }
    }, onNextClick: function (c) {
        var d = c.target || c.srcElement;
        Event.stop(c);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        this.showPrevbtn();
        this.pageIndex++;
        this.currentPosition -= this.slidesRegionWidth;
        if (this.pageIndex >= this.amountIndex - 1) {
            this.hideNextbtn();
            if (this.span) {
                this.span.hide()
            }
        }
        this.$slidesRegion.style.left = this.currentPosition + "px"
    }, prevSlides: function () {
        if (this.lock) {
            return
        }
        var c = null;
        var d = 0;
        this.$next.show();
        this.showNextbtn();
        if (this.span) {
            this.span.show()
        }
        this.pageIndex--;
        d = this.slidesRegionWidth;
        if (this.pageIndex <= 0) {
            d = Math.abs(this.currentPosition);
            this.hidePrevbtn()
        }
        c = {
            node: this.slidesRegion,
            duration: this.duration,
            scale: d,
            position: this.currentPosition,
            callback: function () {
                if (this.pageIndex <= 0) {
                    this.currentPosition = 0
                }
            }.bind(this)
        };
        this.move(c);
        this.currentPosition += this.slidesRegionWidth
    }, nextSlides: function () {
        if (this.lock) {
            return
        }
        var d = null;
        var f = 0;
        this.showPrevbtn();
        this.pageIndex++;
        var e;
        f = -this.slidesRegionWidth;
        if (this.pageIndex >= this.amountIndex - 1) {
            e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
            f = f + (this.slidesRegionWidth - e);
            this.hideNextbtn();
            if (this.span) {
                this.span.hide()
            }
        }
        d = {
            node: this.slidesRegion,
            duration: this.duration,
            scale: f,
            position: this.currentPosition,
            callback: function () {
                if (this.pageIndex >= this.amountIndex - 1) {
                    e = (this.amount - this.showRegionWidth) % this.slidesRegionWidth;
                    this.currentPosition = this.currentPosition + (this.slidesRegionWidth - e)
                }
            }.bind(this)
        };
        this.move(d);
        this.currentPosition -= this.slidesRegionWidth
    }
});
Widgets.WholePageDistanceSlidesControl = Class.create();
Object.extend(Widgets.WholePageDistanceSlidesControl.prototype, Widgets.DistanceSlidesControl.prototype);
Object.extend(Widgets.WholePageDistanceSlidesControl.prototype, {
    onPrevClick: function (d) {
        if (this.lock) {
            return
        }
        var f = d.target || d.srcElement;
        Event.stop(d);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        this.$next.show();
        var e = this.currentPosition;
        this.currentPosition += this.slidesRegionWidth;
        if (this.currentPosition >= 0) {
            this.currentPosition = 0;
            this.$prev.hide()
        }
        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
            this.$slidesRegion.style.left = this.currentPosition + "px";
            setTimeout(function () {
                this.lock = false
            }.bind(this), 600)
        } else {
            this.slideMethod(this.$slidesRegion, e, this.currentPosition)
        }
    }, onNextClick: function (d) {
        if (this.lock) {
            return
        }
        var f = d.target || d.srcElement;
        Event.stop(d);
        if (this.amount <= this.slidesRegionWidth) {
            this.getItems();
            return
        }
        this.$prev.show();
        this.pageIndex++;
        var e = this.currentPosition;
        this.currentPosition -= this.slidesRegionWidth;
        if ((this.showRegionWidth - this.currentPosition) >= this.amount) {
            this.currentPosition = -(this.amount - this.showRegionWidth);
            this.$next.hide();
            if (this.span) {
                this.span.hide()
            }
        }
        if (Mtime.browser.ie === 0 || Mtime.browser.ie > 9) {
            this.$slidesRegion.style.left = this.currentPosition + "px";
            setTimeout(function () {
                this.lock = false
            }.bind(this), 600)
        } else {
            this.slideMethod(this.$slidesRegion, e, this.currentPosition)
        }
    }, slideMethod: function (e, h, f) {
        var g = {
            element: e, duration: this.duration, speed: 10, startScale: h, endScale: f, callback: function () {
                this.lock = false
            }.bind(this)
        };
        this.lock = true;
        this.move(g)
    }, move: function (j) {
        var l = j.speed;
        var h = j.startScale;
        var i = j.endScale;
        var g = j.startScale;

        function k() {
            var a = false;
            if (h > i) {
                g -= l;
                if (g <= i) {
                    g = i;
                    a = true
                }
            } else {
                if (h < i) {
                    g += l;
                    if (g >= i) {
                        g = i;
                        a = true
                    }
                } else {
                    g = i;
                    a = true
                }
            }
            j.element.setStyle({left: g + "px"});
            if (a) {
                j.callback()
            } else {
                setTimeout(k, j.duration)
            }
        }

        k()
    }
});
!function (g, l, k) {
    (function (p, e, o) {
        var b = [], d = 0, c = 13, a = -1, f = false;
        p.animateManage = function (m) {
            this.context = m
        };
        animateManage.prototype = {
            init: function () {
                this.start(this.context)
            }, stop: function (m) {
                clearInterval(a);
                f = false
            }, start: function (m) {
                if (m) {
                    this.pushQueue(m)
                }
                if (f || b.length === 0) {
                    return false
                }
                this.tick();
                return true
            }, tick: function () {
                var m = this;
                f = true;
                a = setInterval(function () {
                    if (b.length === 0) {
                        m.stop()
                    } else {
                        var r = 0, n = b.length;
                        for (; r < n; r++) {
                            b[r] && m.go(b[r], r)
                        }
                    }
                }, c)
            }, go: function (n, C) {
                var D = this.now(), H = n.startTime, J = n.time, t = n.context, I = H + J, E = n.name, m = n.value, G = n.startValue, B = n.effect, F = 1;
                if (D >= I) {
                    b[C] = null;
                    this.delQueue()
                } else {
                    m = this.aniEffect({e: t, ting: J, n: D, st: H, sPos: G, tPos: m}, B)
                }
                t.style[E] = E == "zIndex" ? m : m + "px";
                this.goCallBack(n.callback, n.uid)
            }, aniEffect: function (n, r) {
                r = r || "linear";
                var m = {
                    linear: function (q) {
                        var u = (q.n - q.st) / q.ting, v = q.sPos + (q.tPos - q.sPos) * u;
                        return v
                    }
                };
                return m[r](n)
            }, goCallBack: function (n, v) {
                var t = 0, m = b.length, u = true;
                for (; t < m; t++) {
                    if (b[t].uid == v) {
                        u = false
                    }
                }
                if (u) {
                    n && n()
                }
            }, pushQueue: function (A) {
                var t = A.context, C = A.time || 1000, n = A.callback, u = A.effect, B = A.starCss, m = A.css, z = "", D = this.setUID(t);
                for (z in m) {
                    b.push({
                        context: t,
                        time: C,
                        name: z,
                        value: parseInt(m[z], 10),
                        startValue: parseInt((B[z] || 0)),
                        effect: u,
                        uid: D,
                        callback: n,
                        startTime: this.now()
                    })
                }
            }, delQueue: function () {
                var m = 0, n = b.length;
                for (; m < n; m++) {
                    if (b[m] === null) {
                        b.splice(m, 1)
                    }
                }
            }, now: function () {
                return new Date().getTime()
            }, getUID: function (m) {
                return m.getAttribute("aniUID")
            }, setUID: function (m, n) {
                var r = this.getUID(m);
                if (r) {
                    return r
                }
                r = n || d++;
                m.setAttribute("aniUID", r);
                return r
            }
        }
    })(window, document);
    function i(a) {
        var b = a.offsetTop;
        if (a.offsetParent != null) {
            b += i(a.offsetParent)
        }
        return b
    }

    function h(a) {
        var b = a.offsetLeft;
        if (a.offsetParent != null) {
            b += h(a.offsetParent)
        }
        return b
    }

    var j = function (a) {
        this.speed = 200;
        this.afterUpdateBack = a.afterUpdateBack;
        this.init = function () {
            if (!g("t_actornaver")) {
                return
            }
            this.initDom();
            this.initEvent()
        };
        this.updateBack = function (d) {
            if (d < 0) {
                d = this.num + d
            }
            if (d >= this.num) {
                d = Math.abs(this.num - d)
            }
            this.afterUpdateBack && this.afterUpdateBack(d)
        };
        this.initDom = function () {
            this.layerParent = a.layerParent;
            this.layer = a.layer;
            this.otherLayerHtml = this.layerParent.innerHTML;
            this.prev = a.prev;
            this.next = a.next;
            this.spirits = a.layer.immediateDescendants("dd");
            this.size = a.size;
            this.currPage = a.currPage || 0;
            this.num = this.spirits.length;
            this.max = this.num - 2;
            new Insertion.Bottom(this.layerParent, this.otherLayerHtml);
            this.layerParent.down("dl", 1).setAttribute("id", "otherLayer");
            this.otherLayer = this.layerParent.down("dl", 1);
            this.otherSpirits = this.otherLayer.immediateDescendants("dd");
            this.sliderWidth = this.num * this.size;
            this.layer.setStyle({position: "absolute", left: "0px", width: this.sliderWidth + "px"});
            this.otherLayer.setStyle({
                position: "absolute",
                left: this.sliderWidth - 5 + "px",
                width: this.sliderWidth + "px"
            }).setAttribute("clone", "1");
            var d = this.spirits[2];
            this.baseclickW = d.getWidth() / 2;
            this.baseClickX = h(d) + this.baseclickW
        };
        this.initEvent = function () {
            var d = this;
            Event.observe(this.prev, "click", function (e) {
                d.prevAction(e)
            });
            Event.observe(this.next, "click", function (e) {
                d.nextAction(e)
            });
            this.bindDD()
        };
        var b = this, c = {
            converter: function (s, f) {
                f = f || Event.element(s).getAttribute("data-num");
                var v = b.currPage, d, t = 0, w = s.clientX, e = w - b.baseClickX, u = Math.ceil((Math.abs(e) - b.baseclickW) / b.size);
                if (v < 0) {
                    v = b.num + v
                }
                if (v >= b.num) {
                    v = Math.abs(b.num - v)
                }
                if (e <= -b.baseclickW) {
                    b.prevAction(u)
                }
                if (e >= b.baseclickW) {
                    b.nextAction(u)
                }
            }
        };
        c.uconverter = c.converter.bindAsEventListener(c);
        this.bindDD = function () {
            var d = this;
            this.spirits.each(function (e, f) {
                g(e).setAttribute("data-num", f);
                g(e).stopObserving("click", c.uconverter);
                g(e).observe("click", c.uconverter)
            });
            this.otherSpirits.each(function (e, f) {
                g(e).setAttribute("data-num", f);
                g(e).stopObserving("click", c.uconverter);
                g(e).observe("click", c.uconverter)
            })
        };
        this.prevAction = function (e) {
            if (this.currPage >= (this.num + 1) && this.state == "next") {
                this.oldPage = this.oldPage - this.num;
                this.currPage = this.currPage - this.num;
                var d = this.layer, f = this.spirits;
                this.layer = this.otherLayer;
                this.spirits = this.otherSpirits;
                this.otherLayer = d;
                this.otherSpirits = f
            } else {
                this.oldPage = this.currPage
            }
            this.state = "prev";
            var n = this;
            if (typeof e == "number") {
                this.currPage -= e
            } else {
                this.currPage--
            }
            if (this.currPage <= 1) {
                if (this.currPage <= -(this.num - 2)) {
                    this.updateUi(function () {
                        new animateManage({
                            context: n.otherLayer,
                            effect: "linear",
                            time: n.speed,
                            starCss: {left: n.otherLayer.style.left},
                            css: {left: -1 * (n.sliderWidth - (2 - n.currPage + n.currPage + (n.num - 2)) * n.size)},
                            callback: function () {
                                var q = n.layer, r = n.spirits;
                                n.layer = n.otherLayer;
                                n.spirits = n.otherSpirits;
                                n.otherLayer = q;
                                n.otherSpirits = r;
                                var m = n.currPage + (n.num - 2);
                                n.currPage = 2 + m;
                                n.layerParent.insertBefore(n.layer, n.otherLayer);
                                setTimeout(function () {
                                    n.layer.setStyle({position: "abslute", left: "-" + m * n.size + "px"})
                                });
                                n.rightPos = {left: i(n.otherLayer), top: h(n.otherLayer)};
                                if (m == -1) {
                                    n.otherLayer.setStyle({left: "-" + (n.sliderWidth - 20 + m * n.size) + "px"})
                                } else {
                                    n.otherLayer.setStyle({left: (n.sliderWidth - 20) + "px"})
                                }
                                n.bindDD()
                            }
                        }).init();
                        n.updateBack(n.currPage)
                    }, function () {
                    })
                } else {
                    if (this.currPage == 1) {
                        n.layerParent.insertBefore(n.otherLayer, n.layer);
                        setTimeout(function () {
                            n.otherLayer.setStyle({left: "-" + n.sliderWidth + "px"});
                            n.bindDD();
                            n.updateUi(function () {
                                new animateManage({
                                    context: n.otherLayer,
                                    effect: "linear",
                                    time: n.speed,
                                    starCss: {left: n.otherLayer.style.left},
                                    css: {left: -1 * (n.sliderWidth - (2 - n.currPage) * n.size)},
                                    callback: function () {
                                    }
                                }).init()
                            }, function () {
                                n.updateBack(n.currPage)
                            })
                        })
                    } else {
                        this.updateUi(function () {
                            new animateManage({
                                context: n.otherLayer,
                                effect: "linear",
                                time: n.speed,
                                starCss: {left: n.otherLayer.style.left},
                                css: {left: -1 * (n.sliderWidth - (2 - n.currPage) * n.size)},
                                callback: function () {
                                }
                            }).init()
                        }, function () {
                            n.updateBack(n.currPage)
                        })
                    }
                }
            } else {
                if (this.currPage == 2) {
                    n.layerParent.insertBefore(n.layer, n.otherLayer);
                    setTimeout(function () {
                        n.otherLayer.setStyle({left: (n.sliderWidth - 20) + "px"})
                    });
                    n.bindDD()
                }
                this.updateUi(function () {
                    new animateManage({
                        context: n.otherLayer,
                        effect: "linear",
                        time: n.speed,
                        starCss: {left: n.otherLayer.style.left},
                        css: {left: n.sliderWidth - (n.currPage - 2) * n.size},
                        callback: function () {
                        }
                    }).init()
                }, function () {
                    n.updateBack(n.currPage)
                })
            }
        };
        this.nextAction = function (f) {
            if (this.currPage <= 0 && this.state == "prev") {
                this.oldPage = this.num + this.currPage;
                this.currPage = this.num + this.currPage;
                var e = this.layer, o = this.spirits;
                this.layer = this.otherLayer;
                this.spirits = this.otherSpirits;
                this.otherLayer = e;
                this.otherSpirits = o
            } else {
                this.oldPage = this.currPage
            }
            this.state = "next";
            if (typeof f == "number") {
                this.currPage += f
            } else {
                this.currPage++
            }
            var p = this;
            if (this.currPage >= this.num) {
                var d = this.num * 2 - (this.num - 2);
                if (this.currPage >= d) {
                    this.updateUi(function () {
                        new animateManage({
                            context: p.otherLayer,
                            effect: "linear",
                            time: p.speed,
                            starCss: {left: p.otherLayer.style.left},
                            css: {left: p.sliderWidth - (p.currPage - 2) * p.size},
                            callback: function () {
                            }
                        }).init()
                    }, function () {
                        var m = p.layer, n = p.spirits;
                        p.layer = p.otherLayer;
                        p.spirits = p.otherSpirits;
                        p.otherLayer = m;
                        p.otherSpirits = n;
                        p.currPage = 2 + p.currPage - d;
                        p.layerParent.insertBefore(p.layer, p.otherLayer);
                        p.layer.setStyle({position: "abslute", left: "0px"});
                        p.rightPos = {left: h(p.otherLayer), top: i(p.otherLayer)};
                        p.otherLayer.setStyle({left: (p.sliderWidth - 20) + "px"});
                        p.updateBack(p.currPage);
                        p.bindDD()
                    })
                } else {
                    this.updateUi(function () {
                        new animateManage({
                            context: p.otherLayer,
                            effect: "linear",
                            time: p.speed,
                            starCss: {left: p.otherLayer.style.left},
                            css: {left: p.sliderWidth - (p.currPage - 2) * p.size},
                            callback: function () {
                            }
                        }).init()
                    }, function () {
                        p.updateBack(p.currPage)
                    })
                }
            } else {
                this.updateUi(function () {
                    new animateManage({
                        context: p.otherLayer,
                        effect: "linear",
                        time: p.speed,
                        starCss: {left: p.otherLayer.style.left},
                        css: {left: p.sliderWidth - (p.currPage - 2) * p.size - 15},
                        callback: function () {
                        }
                    }).init()
                }, function () {
                    p.updateBack(p.currPage)
                })
            }
        };
        this.updateUi = function (e, d) {
            this.spirits.each(function (o, p) {
                g(o).removeClassName("on");
                g(o).down("img", 0).show();
                g(o).down("img", 1).hide()
            });
            this.otherSpirits.each(function (o, p) {
                g(o).removeClassName("on");
                g(o).down("img", 0).show();
                g(o).down("img", 1).hide()
            });
            if (this.currPage >= this.num) {
                this.oldPage = this.num - this.oldPage;
                var f = this.currPage - this.num;
                this.otherSpirits[f].addClassName("on");
                new animateManage({
                    context: this.layer,
                    effect: "linear",
                    time: this.speed,
                    starCss: {left: this.layer.style.left},
                    css: {left: -1 * (this.currPage - 2) * this.size},
                    callback: function () {
                        d && d()
                    }
                }).init();
                this.otherSpirits[f].down("img", 0).hide();
                this.otherSpirits[f].down("img", 1).show()
            } else {
                if (this.currPage <= -1) {
                    this.oldPage = this.num - this.oldPage;
                    var f = this.currPage + this.num;
                    this.otherSpirits[f].addClassName("on");
                    new animateManage({
                        context: this.layer,
                        effect: "linear",
                        time: this.speed,
                        starCss: {left: this.layer.style.left},
                        css: {left: -1 * (this.currPage - 2) * this.size},
                        callback: function () {
                            d && d()
                        }
                    }).init();
                    this.otherSpirits[f].down("img", 0).hide();
                    this.otherSpirits[f].down("img", 1).show()
                } else {
                    var f = this.currPage;
                    this.spirits[this.currPage].addClassName("on");
                    new animateManage({
                        context: this.layer,
                        effect: "linear",
                        time: this.speed,
                        starCss: {left: this.layer.style.left},
                        css: {left: -1 * (this.currPage - 2) * this.size},
                        callback: function () {
                            d && d()
                        }
                    }).init();
                    this.spirits[f].down("img", 0).hide();
                    this.spirits[f].down("img", 1).show()
                }
            }
            e && e()
        };
        this.init();
        return this
    };
    l.MallSlider = j
}($, window);
var MallIndexNewClient = Class.create();
Object.extend(MallIndexNewClient.prototype, {
    server: {
        getGoodsPrice: function (d, c) {
            //return Mtime.Component.Ajax.crossDomainCallBack(siteGoodsServiceUrl, "Mtime.Ecommerce.Services", "GetGoodsPrice", [d], c, "/Mall.api", "get", "60000", null)
        }
    }, options: {}, setOptions: function (b) {
        Object.extend(Object.extend(this, this.options), b)
    }, initialize: function (b) {
        this.setOptions(b);
        this.initializeServerData();
        this.initializeDOM();
        this.initializeEvent();
        this.initializeControl();
        this.load()
    }, initializeServerData: function () {
    }, initializeDOM: function () {
        this.categoryMenuDiv = $("categoryMenuDiv");
        this.categoryContentDiv = $("categoryContentDiv");
        this.contentDiv = $("contentDiv")
    }, destroyDOM: function () {
        this.categoryMenuDiv = null;
        this.categoryContentDiv = null;
        this.contentDiv = null
    }, initializeEvent: function () {
        this.onMouseoverCategoryMenuDivHandler = this.showAllCategory.bind(this);
        if (this.categoryMenuDiv) {
            this.categoryMenuDiv.observe("mouseover", this.onMouseoverCategoryMenuDivHandler)
        }
        this.onMouseoutCategoryContentDivHandler = this.hideAllCategory.bind(this);
        if (this.categoryMenuDiv) {
            this.categoryMenuDiv.observe("mouseout", this.onMouseoutCategoryContentDivHandler)
        }
        if (this.categoryContentDiv) {
            this.categoryContentDiv.observe("mouseout", this.onMouseoutCategoryContentDivHandler)
        }
    }, destroyEvent: function () {
        if (this.categoryMenuDiv) {
            this.categoryMenuDiv.stopObserving("mouseover", this.onMouseoverCategoryMenuDivHandler)
        }
        this.categoryContentDiv.stopObserving("mouseover", this.onMouseoverCategoryMenuDivHandler);
        if (this.categoryMenuDiv) {
            this.categoryMenuDiv.stopObserving("mouseout", this.onMouseoutCategoryContentDivHandler)
        }
        this.categoryContentDiv.stopObserving("mouseout", this.onMouseoutCategoryContentDivHandler)
    }, initializeControl: function () {
        if ($("headImgDotSlidesRegion")) {
            if (!Mtime.browser.ie || Mtime.browser.ie > 9) {
                var h = $("headImgSlidesRegion").immediateDescendants();
                for (var g = 1, j = h.length; g < j; g++) {
                    h[g].style.display = "block"
                }
            }
            this.headImgSlide = new Widgets.IndexTopSlidesControl({
                slidesRegion: "headImgSlidesRegion",
                dotSlidesRegion: "headImgDotSlidesRegion",
                backSlidesRegion: "headImgBackSlidesRegion",
                prev: "headImgPre",
                next: "headImgNext",
                autoTimeout: 5
            })
        }
        if ($("layerParent")) {
            var i = $("layerParent"), k = 2;
            i.immediateDescendants("li").each(function (a) {
                $(a).hide()
            });
            i.down("li", k).show();
            var l = new MallSlider({
                layerParent: $("t_actornaver"),
                layer: $("t_actornaverLayer"),
                prev: $("topicPre"),
                next: $("topicNext"),
                currPage: 2,
                size: 205,
                afterUpdateBack: function (a) {
                    i.down("li", k).hide();
                    k = a;
                    i.down("li", k).show()
                }
            })
        }
        if ($("slidesRegionTopGoods")) {
            this.topMovieSlideCtrl = new Widgets.WholePageDistanceSlidesControl({
                slidesRegion: "slidesRegionTopGoods",
                prev: "goPre",
                next: "goBack",
                showRegionWidth: (313 + 30) * 3,
                slidesRegionWidth: (313 + 30) * 3,
                itemWidth: 313 + 30
            })
        }
    }, destroyControl: function () {
        if (this.headImgSlide) {
            this.headImgSlide.close();
            this.headImgSlide = null
        }
    }, load: function () {
        if (this.contentDiv) {
            this.countDown = setInterval(this.renderCountDown.bind(this), 1000);
            this.updateCategoryRecommendGoodsPrice()
        }
    }, showAllCategory: function () {
        this.categoryMenuDiv.addClassName("on");
        $show(this.categoryContentDiv)
    }, hideAllCategory: function () {
        this.categoryMenuDiv.removeClassName("on");
        $hide(this.categoryContentDiv)
    }, renderCountDown: function () {
        var c = false;
        var d = this.contentDiv.getElementsBySelector("div.t_timedown");
        $A(d).each(function (o) {
            var a = $(o);
            var b = a.readAttribute("start");
            var s = a.readAttribute("end");
            var v = new Date().getTime();
            var w, r, t, u, x, q = "", p = "";
            if (b > v) {
                q = "距开始";
                w = Math.ceil((b - v) / 1000);
                c = true
            } else {
                if (s > v) {
                    q = "距结束";
                    w = Math.ceil((s - v) / 1000);
                    c = true
                } else {
                    q = "";
                    w = 0
                }
            }
            if (w > 0) {
                r = Math.floor(w / 60 / 60 / 24);
                t = Math.floor((w - r * 60 * 60 * 24) / 60 / 60);
                u = Math.floor((w - r * 60 * 60 * 24 - t * 60 * 60) / 60);
                x = w - r * 60 * 60 * 24 - t * 60 * 60 - u * 60;
                if (r >= 10) {
                    p += "<i>" + r.toString().charAt(0) + "</i><i>" + r.toString().charAt(1) + "</i><em>天</em>"
                } else {
                    if (r > 0) {
                        p += "<i>0</i><i>" + r + "</i><em>天</em>"
                    } else {
                        p += "<i>0</i><i>0</i><em>天</em>"
                    }
                }
                if (t >= 10) {
                    p += "<i>" + t.toString().charAt(0) + "</i><i>" + t.toString().charAt(1) + "</i><em>:</em>"
                } else {
                    if (t > 0) {
                        p += "<i>0</i><i>" + t + "</i><em>:</em>"
                    } else {
                        p += "<i>0</i><i>0</i><em>:</em>"
                    }
                }
                if (u >= 10) {
                    p += "<i>" + u.toString().charAt(0) + "</i><i>" + u.toString().charAt(1) + "</i><em>:</em>"
                } else {
                    if (u > 0) {
                        p += "<i>0</i><i>" + u + "</i><em>:</em>"
                    } else {
                        p += "<i>0</i><i>0</i><em>:</em>"
                    }
                }
                if (x >= 10) {
                    p += "<i>" + x.toString().charAt(0) + "</i><i>" + x.toString().charAt(1) + "</i>"
                } else {
                    if (x > 0) {
                        p += "<i>0</i><i>" + x + "</i>"
                    } else {
                        p += "<i>0</i><i>0</i>"
                    }
                }
            } else {
                p = "";
                a.previous("b.t_timeover", 0).removeAttribute("style")
            }
            a.down("p", 0).innerHTML = q;
            a.down("p", 1).innerHTML = p
        });
        if (!c) {
            clearInterval(this.countDown);
            this.countDown = null
        }
    }, updateCategoryRecommendGoodsPrice: function () {
        var b = this.getCategoryRecommendGoodsIds();
        if (b.length > 0) {
            this.server.getGoodsPrice(b, this.getGoodsPriceCallback.bind(this))
        }
    }, getCategoryRecommendGoodsIds: function () {
        this.priceNodes = this.contentDiv.getElementsBySelector('strong[method="price"]');
        if (this.priceNodes && this.priceNodes.length > 0) {
            var k;
            var h, l;
            var g = [];
            for (var i = 0, j = this.priceNodes.length; i < j; i++) {
                k = this.priceNodes[i];
                h = k.getAttribute("goodsid");
                l = k.getAttribute("obj");
                if (h && l && l === "0") {
                    g.push(h)
                }
            }
            return g.join(",")
        }
        return ""
    }, getGoodsPriceCallback: function (p) {
        if (p && p.value) {
            var o = p.value.prices;
            var l;
            var i, m, n;
            for (var j = 0, k = this.priceNodes.length; j < k; j++) {
                l = this.priceNodes[j];
                i = l.getAttribute("goodsid");
                m = l.getAttribute("obj");
                if (i && m && m === "0") {
                    n = this.getPriceByGoodsID(i, o);
                    if (n.length > 0) {
                        l.innerHTML = String.format("￥<i>{0}</i>", n)
                    }
                }
            }
        }
    }, getPriceByGoodsID: function (f, j) {
        var g;
        for (var h = 0, i = j.length; h < i; h++) {
            g = j[h];
            if (g.goodsId == parseInt(f, 10)) {
                return g.price
            }
        }
        return ""
    }, close: function () {
        this.destroyControl();
        this.destroyEvent();
        this.destroyDOM()
    }
});