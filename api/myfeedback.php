<?php
/**
 * 提供给APP进行查看我的收藏的接口
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

#include_once (dirname(__FILE__)."/../include/common.inc.php");

include_once (dirname(__FILE__)."/../member/config.php");

$userid = $_POST['username'];
$mid = $_POST['mid'];
$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$end = $number;

$row = $dsql->GetOne("Select * From `#@__member` where mid='$mid'");
if(!is_array($row)){
	show_message(17);
}

$query = "Select * From `#@__feedback` where mid='{$mid}' order by dtime desc limit $start, $end";
$dsql->SetQuery($query);
$dsql->Execute();
$rows = $dsql->GetTotalRow();
$sdata = array();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $arctitle = htmlspecialchars($row['arctitle']);
	    $msg = htmlspecialchars($row['msg']);
	    $dtime = GetDateTimeMk($row['dtime']);
	    $aid = intval($row['aid']);
		$data[] = array(
			'aid'    => intval($row['aid']),
			'arctitle'    => urlencode($arctitle),
			'msg'    => urlencode($msg),
			'arcurl'    => 'http://www.vr2.tv/wap.php?action=article&id='.$aid,
			'dtime'   => $dtime,
		);
		$aidsarr[] = $aid;
	}
	$data = multi_array_sort($data,'dtime','SORT_DESC');
}
function multi_array_sort($multi_array,$sort_key,$sort=SORT_DESC){
	if(is_array($multi_array)){
		foreach ($multi_array as $row_array){
			if(is_array($row_array)){
				$key_array[] = $row_array[$sort_key];
			}else{
				return false;
			}
		}
	}else{
		return false;
	}
	array_multisort($key_array,$sort,$multi_array);
	return $multi_array;
} 
show_message(0,$data);

