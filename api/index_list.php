<?php
/**
 * 根据指定的类别ID获得该ID的文章列表
 * 当ID为空时调用最新的文章列表
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$order = trim($_POST['order']);
if($order == 'hot'){
    $order = 'click DESC';
}else{
    $order = 'id DESC';
}
$end = $number;
$typeid = intval($_REQUEST['typeid']);
$flag = $_REQUEST['flag'];
$channel = intval($_REQUEST['channel']);
if(in_array($flag,array('h','c','f','a','s','b','p'))){
	$flag = $_REQUEST['flag'];
}else{
	$flag = 'a';
}
$where = " WHERE 1 ";
if($typeid){
#	show_message(1);
	if($typeid == -1){
		$where .= " AND flag like '%{$flag}%' ";
	}else{
		$where .= " AND typeid={$typeid} ";
	}
}
if($channel){
    $where .= " AND channel = {$channel} ";
}

//轮播
$lunbo = getArticle(0,'f',0,0,5);
$data['lunbo'] = $lunbo;

//快讯
$kuaixun = getArticle(0,'s',0,0,5);
$data['kuaixun'] = $kuaixun;

//设备
$remenshebei = getArticle(172,null,0,0,5);
$data['remenshebei'] = $remenshebei;

//精选
$jingxuan = getArticle(0,'a',0,0,10,0);
$newjingxuan = array();
if(!empty($jingxuan)){
    foreach($jingxuan as $key => $art){
        if(isset($art['app_system']) && !stristr($art['app_system'],$from)){
            continue;
        }
        $newjingxuan[] = $art;
    }
}
$data['jingxuan'] = $newjingxuan;

//热门搜索词
$data['hot_searchs'] = $cfg_hot_searchs;

show_message(0,$data);
