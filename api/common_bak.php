<?php
/**
 * 
 * 接口调用的公用验证方法和提示信息
 * @author      jch
 * @date        2014年8月16日
 */
define('INAPI', true);
define('UC_API', true);
/**
 * 超时时间
 */
$overtime = 60;
$openkey = 'f64898e4-4f47-4897-bbba-34a525fei';
$openid = 2014;
error_reporting(0);
header("Content-Type: text/html; charset=UTF-8");

//if(!isset($_POST['openid'])||!isset($_POST['actoken'])||!isset($_POST['actime'])){
//	show_message(1);
//}
//$actoken = $_POST['actoken'];
//$actime = $_POST['actime'];
//if($actime){
//	if((time()-$actime)>$overtime){
//		show_message(4);
//	}
//}
//$token = md5($openid.$actime.$openkey);
//if($token != $actoken){
//	show_message(3);
//}
function show_message($ret,$data = null,$msg=null){
	$msgArr = array(
		'-1' => '调用失败',
		'0' => '成功',
		'1' => '请求的参数无效',
		'2' => '未查找到匹配数据',
		'3' => '令牌匹配错误',
		'4' => '密钥超时',
		'5' => '用户名或密码错误',
		'6' => '用户名不存在',
		'7' => '你的用户名或用户笔名过长，不允许注册',
		'8' => '你的用户名或密码过短，不允许注册',
		'9' => '你两次输入的密码不一致',
		'10' => '用户笔名或公司名称已经存在',
		'11' => 'Email格式不正确',
		'12' => '你使用的Email已经被另一帐号注册，请使其它帐号',
		'13' => '你指定的用户名 已存在，请使用别的用户名',
		'14' => '用户名不合法',
		'15'=>'注册失败，请检查资料是否有误或与管理员联系',
		'16'=>'无法收藏未知文档',
		'17'=>'找不到该用户',
		'18'=>'该文章您已经收藏过了',
		'19'=>'登陆失败',
		'20'=>'你上传的头像文件超过了系统限制大小',
		'21'=>'你输入的旧密码错误，不能进行修改操作',
		'22'=>'您已经对该文章点赞过，不能对同一文章重复点赞',
		'23'=>'对不起，没找到您要删除的点赞'
	);
	if(!isset($msgArr[$ret])){
		$ret = -1;
	}
	$msg = $msg ? $msg : $msgArr[$ret];
	$dataarr = array(
		'ret'  => $ret,
		'msg'  => urlencode($msg),
		'data' => $data,
	);
	echo urldecode(json_encode($dataarr));exit;
}

function daddslashes($string, $force = 1) {
	if(is_array($string)) {
		$keys = array_keys($string);
		foreach($keys as $key) {
			$val = $string[$key];
			unset($string[$key]);
			$string[addslashes($key)] = $this->daddslashes($val, $force);
		}
	} else {
		$string = addslashes($string);
	}
	return $string;
}
?>
