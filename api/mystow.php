<?php
/**
 * 提供给APP进行查看我的收藏的接口
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

#include_once (dirname(__FILE__)."/../include/common.inc.php");

include_once (dirname(__FILE__)."/../member/config.php");

$userid = $_POST['username'];
$mid = $_POST['mid'];
$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$end = $number;

$row = $dsql->GetOne("Select * From `#@__member` where mid='$mid'");
if(!is_array($row)){
	show_message(17);
}

$query = "Select * From `#@__member_stow` where mid='{$mid}' order by addtime desc limit $start, $end";
$dsql->SetQuery($query);
$dsql->Execute();
$rows = $dsql->GetTotalRow();
$sdata = $data = array();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $title = htmlspecialchars($row['title']);
	    $aid = intval($row['aid']);
		$sdata[$aid] = array(
			'aid'    => intval($row['aid']),
			'title'    => urlencode($title),
			'addtime'   => $row['addtime'],
		);
		$aidsarr[] = $aid;
	}
}
if(!empty($aidsarr)){
	$aids = implode(",", $aidsarr);
	$where = " WHERE id IN ($aids)";
	$query = "SELECT * FROM #@__archives $where";
	$dsql->SetQuery($query);
	$dsql->Execute();
	$rows = $dsql->GetTotalRow();
	if(!empty($rows)){
		while($row = $dsql->GetArray()){
		    $description = htmlspecialchars($row['description']);
			$litpic = $row['litpic'];
		    $source = $row['source'] ? $row['source'] : '肥皂网';
		    $writer = $row['writer'] ? $row['writer'] : '肥皂网';
		    $click = $row['click'];
		    $addtime = GetDateTimeMk($sdata[$row['id']]['addtime']);
		    $sdata[$row['id']]['id'] = intval($row['id']);
		    $sdata[$row['id']]['typeid'] = urlencode($row['typeid']);
		    $sdata[$row['id']]['description'] = urlencode($description);
		    $sdata[$row['id']]['writer'] = urlencode($writer);
		    $sdata[$row['id']]['source'] = urlencode($source);
		    $sdata[$row['id']]['litpic'] = 'http://www.vr2.tv'.urlencode($litpic);
		    $sdata[$row['id']]['pubdate'] = intval($row['id']);
		    $sdata[$row['id']]['click'] = intval($click);
		    $sdata[$row['id']]['addtime'] = $addtime;
		    $sdata[$row['id']]['id'] = intval($row['id']);
		}
		if(!empty($sdata)){
			foreach ($sdata as $val){
				$data[] = $val;
			}
		}
	}
}
function multi_array_sort($multi_array,$sort_key,$sort=SORT_DESC){
	if(is_array($multi_array)){
		foreach ($multi_array as $row_array){
			if(is_array($row_array)){
				$key_array[] = $row_array[$sort_key];
			}else{
				return false;
			}
		}
	}else{
		return false;
	}
	array_multisort($key_array,$sort,$multi_array);
	return $multi_array;
} 
show_message(0,$data);

