<?php
/**
 * 提供给APP进行版本更新的接口
 * @author      jch
 * @date        2017年12月25日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

$pagename = isset($_REQUEST['pagename']) ? $_REQUEST['pagename'] : 'com.vr2';
$version = isset($_REQUEST['version']) ? $_REQUEST['version'] : 1;
$type = $_REQUEST['type'] ? $_REQUEST['type'] : 'android';
if($type == 'android'){
$ver = array(
		'pagename'=>'com.vr2',
		'title'=>'VR兔',
		'pversion'=>12,
		'size'=>3731880,
		'versionname'=>'v1.2',
		'downurl'=>'http://www.vr2.tv/down/vr2_Umeng_V1.2_20150325_182910.apk',
		'litpic' => "",
		'info' => "1. 新增点赞，插入等互动功能；
2. 更简约更黄色的界面设计；
3. 阉割掉所有广告，干净无毒；
4. 修复V1.1版本部分bug；‍",
	);
}elseif($type == 'ios'){
	$ver = array(
		'pagename'=>'com.vr2',
		'title'=>'VR兔',
		'pversion'=>1,
		'size'=>1111254,
		'versionname'=>'v1.0',
		'downurl'=>'',
		'litpic' => "",
		'info' => "",
	);
}
if($ver && $pagename==$ver['pagename']){
	$pversion = $ver['pversion'];
	$status = $pversion>$version ? 1 : 0;
	$data = array(
		'pagename' => $ver['pagename'],
		'title'    => $ver['title'],
		'version' => $ver['pversion'],
		'versionname' => $ver['versionname'],
		'size' => $ver['size'],
		'pubdate' => time(),
		'downurl' => $ver['downurl'],
		'litpic' => $ver['litpic'],
		'info' => $ver['info'],
		'status' => $status,
	);
}
show_message(0,$data);

