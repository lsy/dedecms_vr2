<?php
/**
 * 获得指定用户的短消息
 * @author      jch
 * @date        2014年10月10日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");
$dopost = $_POST['action'];
$id = $_POST['id'];
$mid = intval($_POST['mid']);
$username = $_POST['username'];
$id = isset($id)? intval($id) : 0;
$row = $dsql->GetOne("Select * From `#@__member` where mid='$mid'");
if(!is_array($row)){
	show_message(17);
}
#api{{
if(defined('UC_API') && @include_once DEDEROOT.'/uc_client/client.php')
{
    if($data = uc_get_user($cfg_ml->M_LoginID)) uc_pm_location($data[0]);
}
#/aip}}

if(!isset($dopost))
{
    $dopost = '';
}
$state=(empty($state))? "" : $state;
/*--------------------
function __send(){  }
----------------------*/
if($dopost=='send')
{
    /** 好友记录 **/
    $sql = "SELECT * FROM `#@__member_friends` WHERE  mid='{$mid}' AND ftype!='-1'  ORDER BY addtime DESC LIMIT 20";
    $friends = array();
    $dsql->SetQuery($sql);
    $dsql->Execute();
    while ($row = $dsql->GetArray()) {
        $friends[] = $row;
    }

    include_once(dirname(__FILE__).'/templets/pm-send.htm');
    exit();
}
/*-----------------------
function __del(){  }
----------------------*/
else if($dopost=='del')
{
	$ids = $_POST['ids'];
    $ids = preg_replace("#[^0-9,]#", "", $ids);
    $query = "DELETE FROM `#@__member_pms` WHERE id in($ids) AND toid='{$mid}'";
    $dsql->ExecuteNoneQuery($query);
    show_message(0);
 #   ShowMsg("成功删除指定的消息!","pm.php?folder=".$folder);
    exit();
}
/*-----------------------
function __man(){  }
----------------------*/
else
{
    if(!isset($folder))
    {
        $folder = 'inbox';
    }
    require_once(DEDEINC."/datalistcp.class.php");
    $wsql = '';
    if($folder=='outbox')
    {
        $wsql = " `fromid`='{$mid}' AND folder LIKE 'outbox' ";
        $tname = "发件箱";
    }
    elseif($folder=='inbox')
    {
        $query = "SELECT * FROM `#@__member_pms` WHERE folder LIKE 'outbox' AND isadmin='1'";
        $dsql->SetQuery($query);
        $dsql->Execute();
        while($row = $dsql->GetArray())
        {
            $row2 = $dsql->GetOne("SELECT * FROM `#@__member_pms` WHERE fromid = '$row[id]' AND toid='{$mid}'");
            if(!is_array($row2))
            {
                $row3= "INSERT INTO
                `#@__member_pms` (`floginid`,`fromid`,`toid`,`tologinid`,`folder`,`subject`,`sendtime`,`writetime`,`hasview`,`isadmin`,`message`)
                VALUES ('admin','{$row['id']}','{$mid}','{$username}','inbox','{$row['subject']}','{$row['sendtime']}','{$row['writetime']}','{$row['hasview']}','{$row['isadmin']}','{$row['message']}')";
                $dsql->ExecuteNoneQuery($row3);
            }
        }
        if($state=="1"){
            $wsql= " toid='{$mid}' AND folder='inbox' AND writetime!='' and hasview=1";
            $tname = "收件箱";
        } else if ($state=="-1")
        {
            $wsql = "toid='{$mid}' AND folder='inbox' AND writetime!='' and hasview=0";
            $tname = "收件箱";
        } else {
            $wsql = " toid='{$mid}' AND folder='inbox' AND writetime!=''";
            $tname = "收件箱";
        }
    }
    else
    {
        $wsql = " `fromid` ='{$mid}' AND folder LIKE 'outbox'";
        $tname = "已发信息";
    }
    $data = array();
    $number = empty($_POST['count']) ? 20 : intval($_POST['count']);
	$start = empty($_POST['page']) ? 0 : intval($_POST['page'])*$number;
	$end = $number;
    $query = "SELECT * FROM `#@__member_pms` WHERE $wsql ORDER BY sendtime DESC LIMIT $start,$end";
	$dsql->SetQuery($query);
	$dsql->Execute();
	$rows = $dsql->GetTotalRow();
	if(!empty($rows)){
		while($row = $dsql->GetArray()){
		    $subject = htmlspecialchars($row['subject']);
		    $messgae = htmlspecialchars($row['message']);
		    $sendtime = $row['sendtime'];
		    $floginid = $row['floginid'];
		    $fromid = $row['fromid'];
		    $hasview = $row['hasview'];
		    $isadmin = $row['isadmin'];
			$data[] = array(
				'id'    => intval($row['id']),
				'subject'    => urlencode($subject),
				'username'   => $floginid,
				'message' => urlencode($messgae),
				'fromid'    => $fromid,
				'hasview'   => $hasview,
				'isadmin'   => $isadmin,
				'sendtime'  => $row['sendtime'],
			);
		}
	}
	$query = "UPDATE `#@__member_pms` SET `hasview` = 1 WHERE toid='{$mid}' LIMIT $number";
	$dsql->ExecuteNoneQuery($query);
	show_message(0,$data);
}

