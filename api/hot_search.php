<?php
/**
 * 根据指定的类别ID获得该ID的文章列表
 * 当ID为空时调用最新的文章列表
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

//热门搜索词
$hot_searchs = explode(",",$cfg_hot_searchs);
$data['hot_searchs'] = $hot_searchs;

show_message(0,$data);
