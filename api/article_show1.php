<?php
/**
 * 根据指定的文章ID获得该ID的文章内容
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");
require_once(DEDEINC.'/arc.archives.class.php');

$aid = intval($_POST['aid']);
if($aid == 0){
	show_message(1);
	$aid = 2;
}
$arc = new Archives($aid);

if($arc->IsError) {
	show_message(2);
}

$data = array();
$body = strip_tags($arc->addTableRow['body'],"<p>,<img>,<object>");
$body = preg_replace("/\r\n|\n|\r/e", '', $body);
#$body =  preg_replace('//s*/', '', $body);
$body =  str_replace(PHP_EOL, '<br>', $body);
$body =  str_replace("\r", '', $body);
$body =  str_replace("\t", '', $body);
$body =  str_replace("src=\"/", "src=\"http://www.vr2.tv/", $body);
$data['title'] = urlencode(strip_tags($arc->Fields['title']));
$data['keywords'] = urlencode(strip_tags($arc->Fields['keywords']));
$data['description'] = urlencode(strip_tags($arc->Fields['description']));
$data['body'] = urlencode((htmlspecialchars($body)));

show_message(0,$data);

