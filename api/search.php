<?php
/**
 *
 * 搜索页
 *
 * @version        $Id: search.php 1 15:38 2010年7月8日Z tianya $
 * @package        DedeCMS.Site
 * @copyright      Copyright (c) 2007 - 2010, DesDev, Inc.
 * @license        http://help.dedecms.com/usersguide/license.html
 * @link           http://www.dedecms.com
 */
header("Content-type: text/html; charset=utf-8");
error_reporting(E_ALL ^ E_NOTICE);
include_once ("./common.php");
require_once(dirname(__FILE__)."/../include/common.inc.php");
require_once(DEDEINC."/arc.searchview.class.php");
if ( file_exists(DEDEINC."/baidusitemap.func.php") ) require_once(DEDEINC."/baidusitemap.func.php");

$pagesize = (isset($pagesize) && is_numeric($pagesize)) ? $pagesize : 10;
$typeid = (isset($typeid) && is_numeric($typeid)) ? $typeid : 0;
$channeltype = (isset($channeltype) && is_numeric($channeltype)) ? $channeltype : 0;
$kwtype = (isset($kwtype) && is_numeric($kwtype)) ? $kwtype : 0;
$mid = (isset($mid) && is_numeric($mid)) ? $mid : 0;
$mobile = (isset($mobile) && is_numeric($mobile)) ? $mobile : 0;
$keyword = trim($_REQUEST['keyword']);
if ( $mobile==1 )
{
    define('DEDEMOB', 'Y');
}

if(!isset($orderby)) $orderby='';
else $orderby = preg_replace("#[^a-z]#i", '', $orderby);

if(!isset($searchtype)) $searchtype = 'titlekeyword';
else $searchtype = preg_replace("#[^a-z]#i", '', $searchtype);

if(!isset($keyword)){
    if(!isset($q)) $q = '';
    $keyword=$q;
}
$oldkeyword = $keyword = FilterSearch(stripslashes($keyword));

if ( function_exists('baidu_get_setting') )
{
    $site_id = baidu_get_setting('site_id');
    $lastuptime_all = baidu_get_setting('lastuptime_all');

    $addquery='';
    if ( $searchtype =='title' )
    {
        $addquery ='&stp=1';
    }
    // 需要提交全量索引后5个小时内才能够进行跳转
    if ( !empty($site_id) AND time()-$lastuptime_all>60*60*5)
    {
        $row = $dsql->GetOne("SELECT spwords FROM `#@__search_keywords` WHERE keyword='".addslashes($keyword)."'; ");
        if(!is_array($row))
        {
            $inquery = "INSERT INTO `#@__search_keywords`(`keyword`,`spwords`,`count`,`result`,`lasttime`)
          VALUES ('".addslashes($keyword)."', '".addslashes($keyword)."', '1', '0', '".time()."'); ";
            $dsql->ExecuteNoneQuery($inquery);
        }
        else
        {
            $dsql->ExecuteNoneQuery("UPDATE `#@__search_keywords` SET count=count+1,lasttime='".time()."' WHERE keyword='".addslashes($keyword)."'; ");
        }

        $keyword = urlencode($keyword);
        $baidu_search_url = "http://zhannei.baidu.com/cse/search?s={$site_id}&entry=1&q={$keyword}{$addquery}";
        header("Location:{$baidu_search_url}");
        exit;
    }
}

$keyword = addslashes(cn_substr($keyword,30));

if($cfg_notallowstr !='' && preg_match("#".$cfg_notallowstr."#i", $keyword))
{
    show_message(24);
    ShowMsg("你的搜索关键字中存在非法内容，被系统禁止！","-1");
    exit();
}

if(($keyword=='' || strlen($keyword)<2) && empty($typeid))
{
    show_message(25);
    ShowMsg('关键字不能小于2个字节！','-1');
    exit();
}

/*/检查搜索间隔时间
$lockfile = DEDEDATA.'/time.lock.inc';
$lasttime = file_get_contents($lockfile);
if(!empty($lasttime) && ($lasttime + $cfg_search_time) > time())
{
    show_message(26);
    ShowMsg('管理员设定搜索时间间隔为'.$cfg_search_time.'秒，请稍后再试！','-1');
    exit();
}//*/

//开始时间
if(empty($starttime)) $starttime = -1;
else
{
    $starttime = (is_numeric($starttime) ? $starttime : -1);
    if($starttime>0)
    {
       $dayst = GetMkTime("2008-1-2 0:0:0") - GetMkTime("2008-1-1 0:0:0");
       $starttime = time() - ($starttime * $dayst);
  }
}
$number = empty($_REQUEST['count']) ? 20 : intval($_REQUEST['count']);
$start = empty($_REQUEST['page']) ? 0 : intval($_REQUEST['page']-1)*$number;
$end = $number;
$typeid = intval($_REQUEST['typeid']);
//获得最新文章列表
$articleData = getArticle($typeid,'',0,$start,$number,0,1,$keyword,'new',0,1);
$newtotal = $articleData['total'];
$articleData = $articleData['list'];

//获得最热文章列表
$articleHotData = getArticle($typeid,'',0,$start,$number,0,1,$keyword,'hot',0);

$tagsData = searchTags($keyword,$start);

PutFile($lockfile, time());
show_message(0,array('new_article'=>$articleData,'hot_article'=>$articleHotData,'tagsData'=>$tagsData,'total'=>$newtotal));


/**
 * 搜索标签
 * @param $keyword
 * @param int $start
 * @param int $end
 * @return array
 */
function searchTags($keyword,$start = 0, $end = 3){
    global $dsql;
    $data = $newdata = array();
    $keyword1 = str_replace(' ','',$keyword);
    $where = " WHERE tag='".$keyword."' ";
    $row = $dsql->GetOne("Select * From `#@__tagindex` $where");
    if(!empty($row)){
        $data[$row['tag']] = ['tag'=>$row['tag'],'total'=>$row['total']];
    }
    if($keyword != $keyword1){
        $where = " WHERE tag='".$keyword1."' ";
        $row = $dsql->GetOne("Select * From `#@__tagindex` $where");
        $data[$row['tag']] = ['tag'=>$row['tag'],'total'=>$row['total']];
    }
    $where = " WHERE tag like '%$keyword%' ";
    if($keyword != $keyword1){
        $where .= " OR tag like '%$keyword1%' ";
    }
    $query = "SELECT * FROM #@__tagindex $where ORDER BY id DESC limit $start, $end";
    $dsql->SetQuery($query);
    $dsql->Execute();
    $rows = $dsql->GetTotalRow();
    if(!empty($rows)) {
        while ($row = $dsql->GetArray()) {
            $data[$row['tag']] = ['tag'=>$row['tag'],'total'=>$row['total']];
        }
    }
    if(!empty($data)){
        $num = 1;
        foreach($data as $val){
            if($num>3){
                break;
            }
            $newdata[] = $val;
            $num++;
        }
    }
    return $newdata;
}
//echo ExecTime() - $t1;