<?php
/**
 * 提供给APP进行发布和展示评论相关的接口
 * @author      jch
 * @date        2014年10月10日
 */

include_once ("./common.php");

#include_once (dirname(__FILE__)."/../include/common.inc.php");

include_once (dirname(__FILE__)."/../member/config.php");
$dopost = $_POST['action'];
if(!isset($dopost)){
    $dopost = 'list';
}
if($dopost == 'add'){
	$uname_utf = $_POST['uname'];
	$uname = iconv("utf-8", "gb2312", $uname_utf);
	$aid = intval($_POST['aid']);
	$mid = intval($_POST['mid']);
	$tomid = intval($_POST['tomid']);
	$msg_utf = $_POST['msg'];
	$feedbacktype = $_POST['feedbacktype'];
	if(empty($feedbacktype) || ($feedbacktype!='good' && $feedbacktype!='bad')){
	   $feedbacktype = 'feedback';
	}
	require_once(DEDEINC.'/arc.archives.class.php');
	$arc = new Archives($aid);
	if($arc->IsError) {
		show_message(2);
	}
	$arctitle = (strip_tags($arc->Fields['title']));
	$msg = iconv("utf-8", "gb2312", $msg_utf);
	$ip = GetIP();
	$dtime = time();
	$inquery = "INSERT INTO `#@__feedback`(`aid`,`typeid`,`username`,`arctitle`,`ip`,`ischeck`,`dtime`, `mid`,`tomid`,`bad`,`good`,`ftype`,`face`,`msg`)
	                   VALUES ('$aid','$typeid','$uname','$arctitle','$ip','1','$dtime', '{$mid}','{$tomid}','0','0','$feedbacktype','1','$msg'); ";
	$rs = $dsql->ExecuteNoneQuery($inquery);
	show_message(0);
}else{
	$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
	$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
	$end = $number;
	$aid = intval($_POST['aid']);
	$where = "";
	if($aid){
		$where = " WHERE aid={$aid} AND ischeck=1 ";
	}
	$query = "SELECT * FROM #@__feedback $where ORDER BY id DESC limit $start, $end";
	$dsql->SetQuery($query);
	$dsql->Execute();
	$mids = array();
	$rows = $dsql->GetTotalRow();
	if(!empty($rows)){
		while($row = $dsql->GetArray()){
		    $username = htmlspecialchars($row['username']);
		    $msg = htmlspecialchars($row['msg']);
			$mid = intval($row['mid']);
			$tomid = intval($row['tomid']);
			if($mid > 0){
				$mids[] = $mid;
			}
			if($tomid > 0){
				$mids[] = $tomid;
			}
			$data[] = array(
				'id'    => intval($row['id']),
				'msg' => urlencode($msg),
				'writer'    => urlencode($username),
				'mid'   => $mid,
				'tomid'   => $tomid,
				'dtime'  => $row['dtime'],
			);
		}
		if(!empty($mids)){
			$members = array();
			$query = "SELECT * FROM #@__member WHERE mid IN (".implode(',',$mids).")";
			$dsql->SetQuery($query);
			$dsql->Execute();
			$rows = $dsql->GetTotalRow();
			while($row = $dsql->GetArray()){
				$members[$row['mid']]['uname'] = $row['uname'];
				$members[$row['mid']]['face'] = $row['face'] ? 'http://www.vr2.tv'.$row['face'] : '';
				$members[$row['mid']]['sex'] = $row['sex'];
			}
			foreach ($data as $key=>$d){
				$data[$key]['face'] = $members[$d['mid']]['face'];
				$data[$key]['sex'] = urlencode($members[$d['mid']]['sex']);
				$data[$key]['tousername'] = $d['tomid'] ? urlencode($members[$d['tomid']]['uname']) : '';
				$data[$key]['toface'] = $d['tomid'] ? $members[$d['tomid']]['face'] : '';
				$data[$key]['tosex'] = $d['tomid'] ? urlencode($members[$d['tomid']]['sex']) : '';
			}
		}
		
	}
	show_message(0,$data);
}

