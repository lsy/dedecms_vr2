<?php
/**
 * 提供给APP的注册接口
 * @author      jch
 * @date        2014年8月17日
 */

include_once ("./common1.php");

include_once (dirname(__FILE__)."/../member/config.php");
require_once DEDEINC.'/membermodel.cls.php';
$userid_utf = $_POST['username'];
$userpwd = $_POST['pwd'];
$userpwdok = $_POST['pwdok'];
$uname_utf = $_POST['uname'];
$email = $_POST['email'];
$sextypes = array('男','女','');
$sex_utf = in_array($_POST['sex'],$sextypes) ? $_POST['sex'] : '';
$mtype_utf = $_POST['mtype'] == '企业' ? '企业' : '个人';
$sex = iconv("utf-8", "gb2312", $sex_utf);
$mtype = iconv("utf-8", "gb2312", $mtype_utf);
$userid = iconv("utf-8", "gb2312", $userid_utf);
$uname = iconv("utf-8", "gb2312", $uname_utf);
$userid = trim($userid);
        $pwd = trim($userpwd);
        $pwdc = trim($userpwdok);
        $rs = CheckUserID($userid, '用户名');
        if($rs != 'ok')
        {
            show_message(14);
        }
        if(strlen($userid) > 20 || strlen($uname) > 36)
        {
            show_message(7);
        }
        if(strlen($userid) < $cfg_mb_idmin || strlen($pwd) < $cfg_mb_pwdmin)
        {
           show_message(8);
        }
        if($pwdc != $pwd)
        {
 #           show_message(9);
        }
        
        $uname = HtmlReplace($uname, 1);
        //用户笔名重复检测
        if($cfg_mb_wnameone=='N' && 1==2)
        {
            $row = $dsql->GetOne("SELECT * FROM `#@__member` WHERE uname LIKE '$uname' ");
            if(is_array($row))
            {
                show_message(10);
            }
        }
        if(!CheckEmail($email))
        {
#            show_message(11);
        }
        
        if($cfg_md_mailtest=='Y' && 1==2)
        {
            $row = $dsql->GetOne("SELECT mid FROM `#@__member` WHERE email LIKE '$email' ");
            if(is_array($row))
            {
                show_message(12);
            }
        }
    
        //检测用户名是否存在
        $row = $dsql->GetOne("SELECT mid FROM `#@__member` WHERE userid LIKE '$userid' ");
        if(is_array($row))
        {
            show_message(13);
        }
        if(defined('UC_API') && @include_once DEDEROOT.'/uc_client/client.php'){
        	$res = uc_user_checkname($userid);
        	if($res > 0){
        		show_message(13);
        	}
        }
        if($safequestion==0)
        {
            $safeanswer = '';
        }
    
        //会员的默认金币
        $dfscores = 0;
        $dfmoney = 0;
        $dfrank = $dsql->GetOne("SELECT money,scores FROM `#@__arcrank` WHERE rank='10' ");
        if(is_array($dfrank))
        {
            $dfmoney = $dfrank['money'];
            $dfscores = $dfrank['scores'];
        }
        $jointime = time();
        $logintime = time();
        $joinip = GetIP();
        $loginip = GetIP();
        $pwd = md5($userpwd);
		$mtype = RemoveXSS(HtmlReplace($mtype,1));
		$safeanswer = HtmlReplace($safeanswer);
		$safequestion = HtmlReplace($safequestion);
        
        $spaceSta = 2;
        
        $inQuery = "INSERT INTO `#@__member` (`mtype` ,`userid` ,`pwd` ,`uname` ,`sex` ,`rank` ,`money` ,`email` ,`scores` ,
        `matt`, `spacesta` ,`face`,`safequestion`,`safeanswer` ,`jointime` ,`joinip` ,`logintime` ,`loginip` )
       VALUES ('$mtype','$userid','$pwd','$uname','$sex','10','$dfmoney','$email','$dfscores',
       '0','$spaceSta','','$safequestion','$safeanswer','$jointime','$joinip','$logintime','$loginip'); ";
        if($dsql->ExecuteNoneQuery($inQuery))
        {
            $mid = $dsql->GetLastID();
            if(defined('UC_API') && @include_once DEDEROOT.'/uc_client/client.php'){
            	$uid = uc_user_register($userid, $pwd, $email);
            }
            //写入默认会员详细资料
            if($mtype=='个人'){
                $space='person';
            }else if($mtype=='企业'){
                $space='company';
            }else{
                $space='person';
            }
            $spacename = iconv("utf-8", "gb2312", $uname.'的空间');
            $title = iconv("utf-8", "gb2312", 'VR兔');
            //写入默认统计数据
            $membertjquery = "INSERT INTO `#@__member_tj` (`mid`,`article`,`album`,`archives`,`homecount`,`pagecount`,`feedback`,`friend`,`stow`)
                   VALUES ('$mid','0','0','0','0','0','0','0','0'); ";
            $dsql->ExecuteNoneQuery($membertjquery);
    
            //写入默认空间配置数据
            $spacequery = "INSERT INTO `#@__member_space`(`mid` ,`pagesize` ,`matt` ,`spacename` ,`spacelogo` ,`spacestyle`, `sign` ,`spacenews`)
                    VALUES('{$mid}','10','0','{$spacename}','','$space','',''); ";
            $dsql->ExecuteNoneQuery($spacequery);
    
            //写入其它默认数据
            $dsql->ExecuteNoneQuery("INSERT INTO `#@__member_flink`(mid,title,url) VALUES('$mid','$title','http://www.www.vr2.tv'); ");
            
            $membermodel = new membermodel($mtype);
            $modid=$membermodel->modid;
            $modid = empty($modid)? 0 : intval(preg_replace("/[^\d]/",'', $modid));
            $modelform = $dsql->getOne("SELECT * FROM #@__member_model WHERE id='$modid' ");
            
            if(!is_array($modelform))
            {
#                showmsg('模型表单不存在', '-1');
#                exit();
            }else{
                $dsql->ExecuteNoneQuery("INSERT INTO `{$membermodel->table}` (`mid`) VALUES ('{$mid}');");
            }
            
            //----------------------------------------------
            //模拟登录
            //---------------------------
            $data = array('uname'=>$uname_utf,
            			  'mid'=>$mid,
            			  'sex'=>$sex_utf,
            			  'mtype'=>$mtype_utf,
            			  'email'=>$email,
            			  'userid'=>$userid_utf);
			show_message(0,$data);
            
            //邮件验证
            if($cfg_mb_spacesta==-10)
            {
                $userhash = md5($cfg_cookie_encode.'--'.$mid.'--'.$email);
                $url = $cfg_basehost.(empty($cfg_cmspath) ? '/' : $cfg_cmspath)."/member/index_do.php?fmdo=checkMail&mid={$mid}&userhash={$userhash}&do=1";
                $url = preg_replace("#http:\/\/#i", '', $url);
                $url = 'http://'.preg_replace("#\/\/#", '/', $url);
                $mailtitle = "{$cfg_webname}--会员邮件验证通知";
                $mailbody = '';
                $mailbody .= "尊敬的用户[{$uname}]，您好：\r\n";
                $mailbody .= "欢迎注册成为[{$cfg_webname}]的会员。\r\n";
                $mailbody .= "要通过注册，还必须进行最后一步操作，请点击或复制下面链接到地址栏访问这地址：\r\n\r\n";
                $mailbody .= "{$url}\r\n\r\n";
                $mailbody .= "Power by http://www.dedecms.com 织梦内容管理系统！\r\n";
          
                $headers = "From: ".$cfg_adminemail."\r\nReply-To: ".$cfg_adminemail;
                if($cfg_sendmail_bysmtp == 'Y' && !empty($cfg_smtp_server))
                {        
                    $mailtype = 'TXT';
                    require_once(DEDEINC.'/mail.class.php');
                    $smtp = new smtp($cfg_smtp_server,$cfg_smtp_port,true,$cfg_smtp_usermail,$cfg_smtp_password);
                    $smtp->debug = false;
                    $smtp->sendmail($email,$cfg_webname,$cfg_smtp_usermail, $mailtitle, $mailbody, $mailtype);
                }
                else
                {
                    @mail($email, $mailtitle, $mailbody, $headers);
                }
            }//End 邮件验证
            show_message(0);
            if($cfg_mb_reginfo == 'Y' && $spaceSta >=0)
            {
                ShowMsg("完成基本信息的注册，接下来完善详细资料...","index_do.php?fmdo=user&dopost=regnew&step=2",0,1000);
                exit();
            } else {
                require_once(DEDEMEMBER."/templets/reg-new3.htm");
                exit;
            } 
        } else {
        	show_message(15);
        }
    
