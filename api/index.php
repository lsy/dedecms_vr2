<?php
/**
 * api的索引入口
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'new';
$mod = isset($_REQUEST['mod']) ? $_REQUEST['mod'] : 'article_list';
$from = isset($_REQUEST['from']) ? $_REQUEST['from'] : 'android';
if(!in_array($from,array('android','ios'))){
    $from = 'android';
}
if($mod == 'list'){//查看文章列表
    $mod = 'article_list';
    $_REQUEST['typeid'] = isset($_REQUEST['typeid']) && $_REQUEST['typeid'] ? $_REQUEST['typeid'] : intval($action);
}elseif($mod == 'show'){//查看文章详情
    $mod = 'article_show';
    $_REQUEST['aid'] = isset($_REQUEST['aid']) && $_REQUEST['aid'] ? $_REQUEST['aid'] : intval($action);
}elseif($mod == 'search'){//搜索内容详情
    $mod = 'search';
    $_REQUEST['keyword'] = isset($_REQUEST['keyword']) && $_REQUEST['keyword'] ? $_REQUEST['keyword'] : trim($action);
}elseif($mod == 'tags'){//标签列表
    $mod = 'tags_list';
}elseif($mod == 'index'){//首页内容
    $mod = 'index_list';
}elseif($mod == 'digg'){//首页内容
    $mod = 'digg';
}elseif($mod == 'hot_search'){//热门搜索词
    $mod = 'hot_search';
}elseif($mod == 'update'){//版本更新
    $mod = 'update';
    $_REQUEST['type'] = isset($_REQUEST['type']) && $_REQUEST['type'] == 'ios' ? 'ios' : 'android';
}else{
    $mod = 'article_list';
}
if($action == 'lunbo'){//顶部轮播
    $_REQUEST['typeid'] = -1;
    $_REQUEST['flag'] = 'f';
}elseif($action == 'kuaixun'){//快讯
    $_REQUEST['typeid'] = -1;
    $_REQUEST['flag'] = 's';
}elseif($action == 'fenlei_shiping'){//分类导航视频
    $_REQUEST['typeid'] = [10,88,89,90,91,92,93,94,95,96,97];
}elseif($action == 'fenlei_youxi'){//分类导航游戏
    $_REQUEST['channel'] = 18;
}elseif($action == 'fenlei_yinjian'){//分类导航硬件
    $_REQUEST['typeid'] = 172;
}elseif($action == 'fenlei_bbs'){//分类导航社区
    $_REQUEST['typeid'] = 87;
}elseif($action == 'fenlei_shop'){//分类导航商城
    $_REQUEST['channel'] = 6;
}elseif($action == 'jingxuan'){//精选内容
    $_REQUEST['typeid'] = -1;
    $_REQUEST['flag'] = 'a';
}elseif($action == 'remenshebei'){//热门设备
    $_REQUEST['typeid'] = 172;
}elseif($action == 'week_hot'){//周热门
    $_REQUEST['typeid'] = [2,6,9,8,152,120];
    $_REQUEST['order'] = 'hot';
    $_REQUEST['begin'] = intval(time() - 60*60*24*7);
}elseif($action == 'month_hot'){//周热门
    $_REQUEST['typeid'] = [2,6,9,8,152,120];
    $_REQUEST['order'] = 'hot';
    $_REQUEST['begin'] = intval(time() - 60*60*24*30);
}elseif($action == 'hyzx'){//行业资讯
    $_REQUEST['typeid'] = 9;
}elseif($action == 'arzx'){//AR资讯
    $_REQUEST['typeid'] = 120;
}elseif($action == 'yxzx'){//游戏资讯
    $_REQUEST['typeid'] = 8;
}elseif($action == 'yjzx'){//硬件资讯
    $_REQUEST['typeid'] = 152;
}elseif($action == 'kuaibao' || $action == 'kjkx'){//24H快报
    $_REQUEST['typeid'] = 67;
}elseif($action == 'sdgd'){//深度观点
    $_REQUEST['typeid'] = 6;
}elseif($action == 'new'){//最新新闻
    $_REQUEST['typeid'] = [2,6,9,8,152,120];
}

require_once $mod.'.php';