<?php
/**
 * 根据指定的文章ID获得该ID的文章内容
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");
require_once(DEDEINC.'/arc.archives.class.php');

$aid = intval($_REQUEST['aid']);
$mid = intval($_POST['mid']);
if($aid == 0){
	show_message(1);
	$aid = 2;
}
$arc = new Archives($aid);

if($arc->IsError) {
	show_message(2);
}

$data = array();
$body = strip_tags($arc->addTableRow['body'],"<p>,<a>,<img>,<object>,<embed>,<b>,<font>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<div>,<iframe><br><span>");
$body = preg_replace("/\r\n|\n|\r/e", '', $body);
#$body =  preg_replace('//s*/', '', $body);
$body =  str_replace(PHP_EOL, '<br>', $body);
$body =  str_replace("\r", '', $body);
$body =  str_replace("\t", '', $body);
$body =  str_replace("src=\"/", "src=\"http://www.vr2.tv/", $body);
foreach($arc->Fields as $key => $val){
    $data[$key] = $val;
}
$data['tags'] = GetTags($aid,true);
$data['title'] = urlencode(strip_tags($arc->Fields['title']));
$data['keywords'] = urlencode(strip_tags($arc->Fields['keywords']));
$data['description'] = urlencode(strip_tags($arc->Fields['description']));
$data['body'] = urlencode((htmlspecialchars('<style>a {color:#0076c9;text-decoration:none;}</style>'.$body)));
$data['arcurl'] = $arc->GetTrueUrl();
//$data['arcurl'] = 'http://m.www.vr2.tv/wap.php?action=article&id='.$aid;
$data['typeid'] = $arc->Fields['typeid'];
$data['redirecturl'] = $arc->Fields['redirecturl'];
$query = "UPDATE `#@__archives` SET `click` = click+1 WHERE id='{$aid}' ";
$dsql->ExecuteNoneQuery($query);
if($mid){
	$row = $dsql->GetOne("Select * From `#@__member_stow` where aid='{$aid}' And mid='{$mid}'");
	if(is_array($row)){
		$data['stowed'] = 1;
    }else{
    	$data['stowed'] = 0;
    }
	$row = $dsql->GetOne("Select * From `#@__diggest` where aid='{$aid}' And mid='{$mid}'");
	if(is_array($row)){
		$data['digged'] = 1;
    }else{
    	$data['digged'] = 0;
    }
}
//增加显示文章栏目名称
$typeNames = getAllTypes();
$data['type_name'] = $typeNames[$data['typeid']];
//获得相关文章列表
$likeData = getArticle($data['typeid'],null,0,0,8,0,0,$data['tags'],null,$data['id']);
if(empty($likeData)){
    $likeData = getArticle($data['typeid'],null,0,0,8,0,0,null,null,$data['id']);
}
$addRow = getSpecialRow($aid,0);
if(isset($_GET['test2'])){
    print_r($addRow);
    $data = array_merge($data,$addRow);
    print_r($data);
    exit;
}
$data = array_merge($data,$addRow);
$data['litpic'] = getFullPath($data['litpic']);
if(!empty($data)){
    foreach ($data as $key => $val){
        if(substr($val,1,7) == 'uploads'){
//            echo '<br>'.substr($val,0,8);
            $data[$key] = getFullPath($val);
        }
    }
}
$data['like_aticles'] = $likeData;

//$row = $dsql->GetOne("Select count(*) num From `#@__feedback` where aid='{$aid}' AND ischeck=1");
//$data['back_count'] = $row['num'];
//$row = $dsql->GetOne("Select count(*) num From `#@__diggest` where aid='{$aid}'");
//$data['digg_count'] = $row['num'];
show_message(0,$data);

