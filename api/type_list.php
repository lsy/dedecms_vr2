<?php
/**
 * 根据指定的类别ID获得该ID的文章列表
 * 当ID为空时调用最新的文章列表
 * @author      jch
 * @date        2014年10月21日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

$query = "SELECT * FROM #@__arctype ORDER BY id DESC";
$dsql->SetQuery($query);
$dsql->Execute();


$rows = $dsql->GetTotalRow();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $typename = htmlspecialchars($row['typename']);
	    $ishidden = $row['ishidden'];
		$data[] = array(
			'id'    => intval($row['id']),
			'typename'    => urlencode($typename),
			'ishidden'  => $row['ishidden'],
		);
	}
}
show_message(0,$data);

