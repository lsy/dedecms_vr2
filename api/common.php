<?php
/**
 * 
 * 接口调用的公用验证方法和提示信息
 * @author      jch
 * @date        2014年8月16日
 */
define('INAPI', true);
define('UC_API', true);
define('CDNURL','http://www.vr2.tv');
/**
 * 超时时间
 */
$overtime = 60;
$openkey = 'f64898e4-4f47-4897-bbba-34a525fei';
$openid = 2014;
ini_set("display_errors", "On");
error_reporting(E_ALL | E_STRICT);
header("Content-Type: text/html; charset=UTF-8");

if(!isset($_POST['openid'])||!isset($_POST['actoken'])||!isset($_POST['actime'])){
//	show_message(1);
}
$actoken = isset($_POST['actoken']) ? $_POST['actoken'] : '';
$actime = isset($_POST['actime']) ? $_POST['actime'] : '';
if($actime){
	if((time()-$actime)>$overtime){
//		show_message(4);
	}
}
$token = md5($openid.$actime.$openkey);
if($token != $actoken){
//	show_message(3);
}
function show_message($ret,$data = null,$msg=null){
	global $cfg_search_time;
	$msgArr = array(
		'-1' => '调用失败',
		'0' => '成功',
		'1' => '请求的参数无效',
		'2' => '未查找到匹配数据',
		'3' => '令牌匹配错误',
		'4' => '密钥超时',
		'5' => '用户名或密码错误',
		'6' => '用户名不存在',
		'7' => '你的用户名或用户笔名过长，不允许注册',
		'8' => '你的用户名或密码过短，不允许注册',
		'9' => '你两次输入的密码不一致',
		'10' => '用户笔名或公司名称已经存在',
		'11' => 'Email格式不正确',
		'12' => '你使用的Email已经被另一帐号注册，请使其它帐号',
		'13' => '你指定的用户名 已存在，请使用别的用户名',
		'14' => '用户名不合法',
		'15'=>'注册失败，请检查资料是否有误或与管理员联系',
		'16'=>'无法收藏未知文档',
		'17'=>'找不到该用户',
		'18'=>'该文章您已经收藏过了',
		'19'=>'登陆失败',
		'20'=>'你上传的头像文件超过了系统限制大小',
		'21'=>'你输入的旧密码错误，不能进行修改操作',
		'22'=>'您已经对该文章点赞过，不能对同一文章重复点赞',
		'23'=>'对不起，没找到您要删除的点赞',
		'24'=>'你的搜索关键字中存在非法内容，被系统禁止！',
		'25'=>'关键字不能小于2个字节！',
		'26'=>'管理员设定搜索时间间隔为'.$cfg_search_time.'秒，请稍后再试！',
		'27'=>'读取档案基本信息出错',
		'28'=>'读取频道配置信息出错'
	);
	if(!isset($msgArr[$ret])){
		$ret = -1;
	}
	$msg = $msg ? $msg : $msgArr[$ret];
	$dataarr = array(
		'ret'  => $ret,
		'msg'  => urlencode($msg),
		'data' => $data,
	);
	echo urldecode(json_encode($dataarr));exit;
}

function daddslashes($string, $force = 1) {
	if(is_array($string)) {
		$keys = array_keys($string);
		foreach($keys as $key) {
			$val = $string[$key];
			unset($string[$key]);
			$string[addslashes($key)] = $this->daddslashes($val, $force);
		}
	} else {
		$string = addslashes($string);
	}
	return $string;
}

/**
 * 路径拼接者 （urlencode处理后结果）
 * @param string $url     待处理地址
 * @return string
 */
function urlMaker($url) {
    if (empty($url)) {
        return '';
    }

    $reg = '/^http/';
    if (preg_match($reg, $url)) {
        return urlencode($url);
    } else {
        return CDNURL . urlencode($url);
    }
}

/**
 * 获得图片全路径
 * @param $path
 * @return string
 */
function getFullPath($path){
	if(substr($path,0,4) == 'http'){
		return $path;
	}else{
		return CDNURL.$path;
	}
}

/**
 * 参数获取
 * @param string $param   参数名称
 * @return string
 */
function paramMaker($param) {
    $post = $_POST[$param];
    $get = $_GET[$param];
    if (!empty($post)) {
        return $post;
    } else {
        return $get;
    }
}

/**
 * 获得文章的扩展字段
 * @param $aid
 * @return array|string
 */
function getSpecialRow($aid,$noHtml = 1){
	global $dsql;
    $query = "SELECT ch.typename AS channelname,ar.membername AS rankname,arc.*
    FROM `#@__archives` arc
    LEFT JOIN `#@__channeltype` ch ON ch.id=arc.channel
    LEFT JOIN `#@__arcrank` ar ON ar.rank=arc.arcrank WHERE arc.id='$aid' ";
    $arcRow = $dsql->GetOne($query);
    if(!is_array($arcRow))
    {
        show_message(27);
    }
    $query = "SELECT * FROM `#@__channeltype` WHERE id='".$arcRow['channel']."'";
    $cInfos = $dsql->GetOne($query);
    if(!is_array($cInfos))
    {
        show_message(28);
    }
    $addtable = $cInfos['addtable'];
    $newRow = [];
    $addRow = $dsql->GetOne("SELECT * FROM `$addtable` WHERE aid='$aid'");

    if(!empty($addRow)){
        foreach($addRow as $key => $val){
            if($noHtml){
                $val = strip_tags($val);
            }else{
                $val = getNoHtml($val);
            }
            if(!$noHtml || strlen($val) < 255){
                $newRow[$key] = $val;
            }
            if(substr($val,0,9) == '/uploads/'){
                $newRow[$key] = getFullPath($val);
            }
        }
    }
    $newRow['app_screen'] = getPicUrl($addRow['app_screen']);
    return $newRow;
}

function getNoHtml($str){
    $body = strip_tags($str,"<p>,<a>,<img>,<object>,<embed>,<b>,<font>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<div>,<iframe>,<br>,<span>,<ul>,<li>");
    $body = preg_replace("/\r\n|\n|\r/e", '', $body);
#$body =  preg_replace('//s*/', '', $body);
    $body =  str_replace(PHP_EOL, '<br>', $body);
    $body =  str_replace("\r", '', $body);
    $body =  str_replace("\t", '', $body);
    $body =  str_replace('\"', '', $body);
    $body =  str_replace('\'', '', $body);
    $body =  str_replace("\n", '<br>', $body);
    $body =  str_replace("\r\n","<br>",$body);
    $body =  str_replace("src=\"/", "src=\"".CDNURL."/", $body);
    return $body;
}

function getPicUrl($str){
    $imgs = array();
    $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/";
    preg_match_all($pattern,$str,$match);
    if(isset($match[1]) && $match[1]){
        foreach ($match[1] as $img){
            $imgs[] = getFullPath($img);
        }
    }
    return implode(",",$imgs);
}

/**
 * @param int $typeid
 * @param string $flag
 * @param int $channel
 * @param int $start
 * @param int $end
 * @param int $noSpecial
 * @return array
 */
function getArticle($typeid = 0, $flag='', $channel = 0, $start = 0, $end = 10,$noSpecial=1,$noTags = 0,$keyword = '',$order = 'id',$noaid = 0, $total = 0){
    global $dsql;
    //获得所有栏目名称
    $typeNames = getAllTypes();
    $where = " WHERE arcrank=0 ";
    if($typeid){
        $where .= " AND typeid IN ({$typeid}) ";
    }
    if($flag){
        $where .= " AND flag like '%{$flag}%' ";
    }
    if($channel){
        $where .= " AND channel={$channel} ";
    }
    if($keyword){
        $like = array();
        $keywords = explode(',' , trim($keyword));
        $n = 1;
        foreach($keywords as $k)
        {
            if($n > 3)  break;
            if(trim($k)=='') continue;
            else $k = addslashes($k);
            $like[] = "title LIKE '%$k%'";
            $like[] = "keywords LIKE '%$k%'";
            $n++;
            $k1 = str_replace(' ','',$k);
            if($k1 != $k){
                $like[] = "title like '%$k1%'";
                $like[] = "keywords LIKE '%$k1%'";
            }
        }
        if(!empty($like)){
            $where .= ' AND ('.implode(" OR ",$like).')';
        }
    }

    if(intval($noaid)){
        $where .= ' AND id !='.intval($noaid);
    }
    if($order == 'hot'){
        $order = 'click';
    }else{
        $order = 'id';
    }
    $query = "SELECT * FROM #@__archives $where ORDER BY $order DESC limit $start, $end";
    $dsql->SetQuery($query);
    $dsql->Execute();
    $sdata = $data = array();
    $rows = $dsql->GetTotalRow();
    if(!empty($rows)){
        while($row = $dsql->GetArray()){
            $title = stripslashes(htmlspecialchars($row['title']));
            $description = stripslashes(htmlspecialchars($row['description']));
            $litpic = $row['litpic'] ? CDNURL.urlencode($row['litpic']) : '';
            $source = $row['source'] ? $row['source'] : 'VR兔';
            $writer = $row['writer'] ? $row['writer'] : 'VR兔';
            $click = $row['click'];
            $aid = intval($row['id']);
            $old = array(
                'id'    => $aid,
                'title'    => urlencode($title),
                'typeid'   => urlencode($row['typeid']),
                'channel'   => intval($row['channel']),
                'money'   => intval($row['money']),
                'goodpost'   => intval($row['goodpost']),
                'description' => urlencode($description),
                'writer'    => urlencode($writer),
                'source'   => urlencode($source),
                'litpic'   => $litpic,
                'flag'   => $row['flag'],
                'pubdate'  => $row['pubdate'],
                'click' => intval($click),
                'type_name' => $typeNames[$row['typeid']],
            );
            if(!$noTags){
                $tags = GetTags($aid,true);
                $old['tags'] = $tags;
            }
            $channel = intval($row['channel']);
            $addRow = array();
            if($channel > 1 && !$noSpecial){
                $addRow = getSpecialRow($aid);
            }elseif($channel == 1){
                $aidsarr[] = $aid;
            }
            $new = array_merge($old,$addRow);
            $sdata[$aid] = $new;
        }
        if(!empty($aidsarr)){
            $aids = implode(",", $aidsarr);
            $whereAids = " WHERE aid IN ($aids)";
            $query = "SELECT * FROM #@__addonarticle $whereAids";
            $dsql->SetQuery($query);
            $dsql->Execute();
            $articles = $dsql->GetTotalRow();
            if(!empty($articles)){
                while($row = $dsql->GetArray()){
                    if($row['redirecturl']){
                        $redirecturl = strstr($row['redirecturl'] ,'?') ? $row['redirecturl'].'&source=www.vr2.tv' : $row['redirecturl'].'?source=www.vr2.tv';
                    }else{
                        $redirecturl = '';
                    }
                    $sdata[$row['aid']]['redirecturl'] = $redirecturl;
                }
            }
        }
        if(!empty($sdata)){
            foreach ($sdata as $val){
                $data[] = $val;
            }
        }
        if($total){
            $data['list'] = $data;
            $sql = "SELECT count(*) total FROM #@__archives $where";
            $row = $dsql->GetOne($sql);
            $data['total'] = $row['total'];
        }
    }
    return $data;
}

/**
 * 获得所有的分类名称
 * @return array
 */
function getAllTypes(){
    global $dsql;
    $query = "SELECT id,typename FROM #@__arctype";
    $dsql->SetQuery($query);
    $dsql->Execute();
    $data = array();
    while($row = $dsql->GetArray()){
        $id = intval($row['id']);
        $data[$id] = $row['typename'];
    }
    return $data;
}

/**
 * 根据来源取android或ios的游戏
 * @param string $from
 * @param int $start
 * @param int $end
 * @return array
 */
function getApps($from = 'android',$start = 0,$end = 10){
    global $dsql,$typeNames;
    $query = "SELECT * FROM #@__addon18 WHERE app_system like '%{$from}%'  ORDER BY aid DESC limit $start, $end";
    $dsql->SetQuery($query);
    $dsql->Execute();
    $aids = $sdata = $data = array();
    $rows = $dsql->GetTotalRow();
    if(!empty($rows)){
        while($row = $dsql->GetArray()){
            $aid = $row['aid'];
            $sdata[$aid] = $row;
            $aids[] = $aid;
        }
        $query = "SELECT * FROM #@__archives WHERE id IN (".implode(',',$aids).")";
        $dsql->SetQuery($query);
        $dsql->Execute();
        $rows = $dsql->GetTotalRow();
        if(!empty($rows)){
            while($row = $dsql->GetArray()){
                $aid = intval($row['id']);
                $title = stripslashes(htmlspecialchars($row['title']));
                $description = stripslashes(htmlspecialchars($row['description']));
                $litpic = $row['litpic'] ? 'http://www.vr2.tv'.urlencode($row['litpic']) : '';
                $source = $row['source'] ? $row['source'] : 'VR兔';
                $writer = $row['writer'] ? $row['writer'] : 'VR兔';
                $click = $row['click'];
                $tags = GetTags($aid,true);
                $sdata[$aid] = array_merge($sdata[$aid],array(
                    'id'    => $aid,
                    'title'    => urlencode($title),
                    'typeid'   => urlencode($row['typeid']),
                    'channel'   => intval($row['channel']),
                    'money'   => intval($row['money']),
                    'goodpost'   => intval($row['goodpost']),
                    'description' => urlencode($description),
                    'writer'    => urlencode($writer),
                    'source'   => urlencode($source),
                    'litpic'   => $litpic,
                    'pubdate'  => $row['pubdate'],
                    'click' => intval($click),
                    'tags' => $tags,
                    'type_name' => $typeNames[$row['typeid']],
                ));
            }
        }
        if(isset($_GET['test2'])){
            print_r($sdata);
        }
        if(!empty($sdata)){
            foreach ($sdata as $val){
                $da = array();
                if(!empty($val)){
                    foreach($val as $key => $va){
                        $va = getNoHtml($va);
                        if(strlen($va) < 255){
                            $va = $va;
                        }
                        if(substr($va,0,9) == '/uploads/'){
                            $va = getFullPath($va);
                        }
                        $da[$key] = $va;
                    }
                }
                $da['app_screen'] = getPicUrl($da['app_screen']);
                $data[] = $da;
            }
        }
    }
    return $data;
}
?>
