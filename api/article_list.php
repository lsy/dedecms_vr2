<?php
/**
 * 根据指定的类别ID获得该ID的文章列表
 * 当ID为空时调用最新的文章列表
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

$number = empty($_REQUEST['count']) ? 20 : intval($_REQUEST['count']);
$page = intval($_REQUEST['page']);
$page = max($page,1);
//如果是取精选内容自动给页数加1
if($action == 'jingxuan'){
    $page = $page + 1;
}
$start = intval($page-1)*$number;
$order = trim($_REQUEST['order']);
if($order == 'hot'){
    $order = 'click DESC';
}else{
    $order = 'id DESC';
}
$end = $number;
$typeid = ($_REQUEST['typeid']);
$flag = $_REQUEST['flag'];
$channel = intval($_REQUEST['channel']);
if(in_array($flag,array('h','c','f','a','s','b','p'))){
	$flag = $_REQUEST['flag'];
}else{
	$flag = 'c';
}
$where = " WHERE arcrank >=0 ";
if($typeid){
#	show_message(1);
	if($typeid == -1){
		$where .= " AND flag like '%{$flag}%' ";
	}else{
	    if(is_array($typeid)){
            $typeid = implode(",",$typeid);
        }
		$where .= " AND typeid IN ({$typeid}) ";
	}
}
if($channel){
    $where .= " AND channel = {$channel} ";
}
$begin = isset($_REQUEST['begin']) && intval($_REQUEST['begin']) ? intval($_REQUEST['begin']) : 0;
if($begin){
    $where .= " AND pubdate >= {$begin} ";
}

//如果有传上次更新时间就取最新的记录总数
if(isset($_REQUEST['last_time']) && intval($_REQUEST['last_time'])){
    $lastTime = intval($_REQUEST['last_time']);
    $where .= " AND pubdate>".$lastTime;
    $query = "SELECT * FROM #@__archives $where";
    $dsql->SetQuery($query);
    $dsql->Execute();
    $total = $dsql->GetTotalRow();
    $data = ['total'=>$total];
    $message = $total.'条最新内容';
    if(!$total){
        $message = '暂时没有更新内容';
    }
    $data['showmessage'] = $message;
    show_message(0,$data);
}

//获得所有栏目名称
$typeNames = getAllTypes();

if($channel == 18){
    $data = getApps($from,$start,$end);
    show_message(0,$data);
}

$query = "SELECT * FROM #@__archives $where ORDER BY $order limit $start, $end";

$dsql->SetQuery($query);
$dsql->Execute();
$sdata = $data = array();

$rows = $dsql->GetTotalRow();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $title = stripslashes(htmlspecialchars($row['title']));
	    $description = stripslashes(htmlspecialchars($row['description']));
		$litpic = $row['litpic'] ? 'http://www.vr2.tv'.urlencode($row['litpic']) : '';
	    $source = $row['source'] ? $row['source'] : 'VR兔';
	    $writer = $row['writer'] ? $row['writer'] : 'VR兔';
	    $click = $row['click'];
	    $aid = intval($row['id']);
	#   $pubdate = GetDateTimeMk($row['pubdate']);
	    $tags = GetTags($aid,true);
	    $old = array(
            'id'    => $aid,
            'title'    => urlencode($title),
            'typeid'   => urlencode($row['typeid']),
            'channel'   => intval($row['channel']),
            'money'   => intval($row['money']),
            'goodpost'   => intval($row['goodpost']),
            'description' => urlencode($description),
            'writer'    => urlencode($writer),
            'source'   => urlencode($source),
            'litpic'   => $litpic,
            'pubdate'  => $row['pubdate'],
            'flag'  => $row['flag'],
            'click' => intval($click),
            'tags' => $tags,
            'type_name' => $typeNames[$row['typeid']],
        );
	    $channel = intval($row['channel']);
        $addRow = array();
        if($channel > 1){
            $addRow = getSpecialRow($aid);
        }elseif($channel == 1){
            $aidsarr[] = $aid;
        }
        $new = array_merge($old,$addRow);
		$sdata[$aid] = $new;
	}
	if(!empty($aidsarr)){
		$aids = implode(",", $aidsarr);
		$where = " WHERE aid IN ($aids)";
		$query = "SELECT * FROM #@__addonarticle $where";
		$dsql->SetQuery($query);
		$dsql->Execute();
		$rows = $dsql->GetTotalRow();
		$urls = array();
		if(!empty($rows)){
			while($row = $dsql->GetArray()){
			   if($row['redirecturl']){
			   		$redirecturl = strstr($row['redirecturl'] ,'?') ? $row['redirecturl'].'&source=www.vr2.tv' : $row['redirecturl'].'?source=www.vr2.tv';
			   }else{
			   		$redirecturl = '';
			   }
			   $sdata[$row['aid']]['redirecturl'] = $redirecturl;
			}
		}
	}
    if(!empty($sdata)){
        foreach ($sdata as $val){
            if($action == 'jingxuan' && isset($val['app_system']) && !stristr($val['app_system'],$from)){
                continue;
            }
            $data[] = $val;
        }
    }
}
show_message(0,$data);

