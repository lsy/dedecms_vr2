<?php
/**
 * 获取TAGS管理
 *
 * @version        $Id: tag_test_action.php 1 23:07 2010年7月20日Z tianya $
 * @package        DedeCMS.Administrator
 * @copyright      Copyright (c) 2007 - 2010, DesDev, Inc.
 * @license        http://help.dedecms.com/usersguide/license.html
 * @link           http://www.dedecms.com
 */
include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");
$tag = trim($_REQUEST['keyword']);
if(empty($tag)) $tag = '';

$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$end = $number;

$orderby = empty($orderby) ? 'id' : preg_replace("#[^a-z]#i", '', $orderby);
$orderway = isset($orderway) && $orderway == 'asc' ? 'asc' : 'desc';
if(!empty($tag)) $where = " where tag like '%$tag%'";
else $where = '';

$neworderway = ($orderway == 'desc' ? 'asc' : 'desc');
$query = "SELECT * FROM `#@__tagindex` $where ORDER BY $orderby $orderway limit $start, $end";
$dsql->SetQuery($query);
$dsql->Execute();
$sdata = $data = array();
$rows = $dsql->GetTotalRow();
if(!empty($rows)){
    while($row = $dsql->GetArray()){
        $data[] = $row;
    }
}
show_message(0,$data);
