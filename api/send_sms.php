<?php

/**
 * 提供给APP进行验证码发送请求
 * @author      yjk
 * @date        2016年01月11日
 */
include_once ("./common.php");
include_once ("./sms_helper.php");
require_once(dirname(__FILE__) . '/../include/common.inc.php');

//短信模板ID
$templateId = 62152;

$phoneNum = paramMaker('phonenum');
$vcode = rand(1000, 9999);
$flag = (paramMaker('flag') == '') ? true : false;
if (empty($phoneNum)) {
    show_message(-1, null, '手机号码不能为空');
}

$exist = $dsql->GetOne("SELECT count(1) num From `#@__member` WHERE userid = '$phoneNum' ");
if ($exist['num'] > 0) {
    show_message(-1, null, '该号码已注册！');
}

$phoneInfo = $dsql->GetOne("SELECT * FROM `#@__phone_vcode` WHERE phone = '$phoneNum' ");
if (!empty($phoneInfo) && isset($phoneInfo['last_time'])) {
    $lastTime = $phoneInfo['last_time'];
    if ($lastTime + 60 * 1.5 > time() && $flag) {
        show_message(-1, null, '请勿频繁请求短信验证码！');
    }

    $sendRes = sendTemplateSMS($phoneNum, array($vcode, 5), $templateId);
    if ($sendRes[0] == 1) {
        $query = "UPDATE `#@__phone_vcode` SET `vcode` = '$vcode', `last_time` = " . time() . " WHERE phone='{$phoneNum}' ";
        $dsql->ExecuteNoneQuery($query);
        show_message(0, null, '发送成功');
    } else {
        show_message(-1, null, '发送失败' . $sendRes[1]);
    }
}

if (empty($phoneInfo)) {
    $sendRes = sendTemplateSMS($phoneNum, array($vcode, 5), $templateId);
    if ($sendRes[0] == 1) {
        $query = "INSERT into `#@__phone_vcode`(`phone`, `vcode`, `last_time`) values ('$phoneNum', '$vcode', " . time() . ") ";
        $dsql->ExecuteNoneQuery($query);
        show_message(0, null, '发送成功');
    } else {
        show_message(-1, null, '发送失败' . $sendRes[1]);
    }
}
show_message(-1, null, '发送失败');


