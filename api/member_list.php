<?php
/**
 * 根据指定的类别ID获得该ID的文章列表
 * 当ID为空时调用最新的文章列表
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$end = $number;
$query = "SELECT * FROM #@__member ORDER BY mid DESC limit $start, $end";
$dsql->SetQuery($query);
$dsql->Execute();

$rows = $dsql->GetTotalRow();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $uname = stripslashes(htmlspecialchars($row['uname']));
	#   $pubdate = GetDateTimeMk($row['pubdate']);
		$data[] = array(
			'mid'    => intval($row['mid']),
			'uname'    => urlencode($uname),
			'face'   => urlencode($row['face']),
			'sex' => urlencode($row['sex']),
			'jointime'  => $row['jointime'],
		);
	}
}
show_message(0,$data);

