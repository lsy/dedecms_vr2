<?php
/**
 * 提供给APP进行登陆的接口
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");
require_once(dirname(__FILE__).'/../include/common.inc.php');
$mid = $_POST['mid'];
$face = $GLOBALS['face'];
$dopost = 'save';
if($dopost=='save')
{
	$member = $dsql->GetOne("SELECT * FROM `#@__member` WHERE mid = '$mid' ");
	$oldface = $member['face'];
    $maxlength = $cfg_max_face * 1024;
    $userdir = $cfg_user_dir.'/'.$mid;
   
    if(!preg_match("#^".$userdir."#", $oldface))
    {
        $oldface = '';
    }
    if(is_uploaded_file($face))
    {
        if(@filesize($_FILES['face']['tmp_name']) > $maxlength)
        {
 #           show_message(20);
        }
        //删除旧图片（防止文件扩展名不同，如：原来的是gif，后来的是jpg）
        if(preg_match("#\.(jpg|gif|png)$#i", $oldface) && file_exists($cfg_basedir.$oldface))
        {
            @unlink($cfg_basedir.$oldface);
        }
        //上传新工图片
        $face = MemberUploads4APP('face', $oldface, $mid, 'image', 'myface', 180, 180);
    }
    else
    {
        $face = $oldface;
    }
    $query = "UPDATE `#@__member` SET `face` = '$face' WHERE mid='{$mid}' ";
    $dsql->ExecuteNoneQuery($query);
    $data = array('mid'=>$mid,'face'=>'http://www.vr2.tv'.$face);
    show_message(0,$data);
}
else if($dopost=='delold')
{
    if(empty($oldface))
    {
        ShowMsg("没有可删除的头像！", "-1");
        exit();
    }
    $userdir = $cfg_user_dir.'/'.$cfg_ml->M_ID;
    if(!preg_match("#^".$userdir."#", $oldface) || preg_match('#\.\.#', $oldface))
    {
        $oldface = '';
    }
    if(preg_match("#\.(jpg|gif|png)$#i", $oldface) && file_exists($cfg_basedir.$oldface))
    {
        @unlink($cfg_basedir.$oldface);
    }
    $query = "UPDATE `#@__member` SET `face` = '' WHERE mid='{$cfg_ml->M_ID}' ";
    $dsql->ExecuteNoneQuery($query);
    // 清除缓存
    $cfg_ml->DelCache($cfg_ml->M_ID);
    ShowMsg('成功删除原来的头像！', $backurl);
    exit();
}

function MemberUploads4APP($upname,$handname,$userid=0,$utype='image',$exname='',$maxwidth=0,$maxheight=0,$water=false,$isadmin=false)
    {
        global $cfg_imgtype,$cfg_mb_addontype,$cfg_mediatype,$cfg_user_dir,$cfg_basedir,$cfg_dir_purview;
        
        //当为游客投稿的情况下，这个 id 为 0
        if(empty($userid) ) $userid = 0;
        if(!is_dir($cfg_basedir.$cfg_user_dir."/$userid"))
        {
                MkdirAll($cfg_basedir.$cfg_user_dir."/$userid", $cfg_dir_purview);
                CloseFtp();
        }
        //有上传文件
        $allAllowType = str_replace('||', '|', $cfg_imgtype.'|'.$cfg_mediatype.'|'.$cfg_mb_addontype);
        if(!empty($GLOBALS[$upname]) && is_uploaded_file($GLOBALS[$upname]))
        {
            $nowtme = time();

            $GLOBALS[$upname.'_name'] = trim(preg_replace("#[ \r\n\t\*\%\\\/\?><\|\":]{1,}#",'',$GLOBALS[$upname.'_name']));
            //源文件类型检查
            if($utype=='image')
            {
                if(!preg_match("/\.(".$cfg_imgtype.")$/", $GLOBALS[$upname.'_name']))
                {
#                    ShowMsg("你所上传的图片类型不在许可列表，请上传{$cfg_imgtype}类型！",'-1');
#                    exit();
                }
                $sparr = Array("image/pjpeg","image/jpeg","image/gif","image/png","image/xpng","image/wbmp");
                $imgfile_type = strtolower(trim($GLOBALS[$upname.'_type']));
                if(!in_array($imgfile_type, $sparr))
                {
#                    ShowMsg('上传的图片格式错误，请使用JPEG、GIF、PNG、WBMP格式的其中一种！', '-1');
#                    exit();
                }
            }
            else if($utype=='flash' && !preg_match("/\.swf$/", $GLOBALS[$upname.'_name']))
            {
#                ShowMsg('上传的文件必须为flash文件！', '-1');
#                exit();
            }
            else if($utype=='media' && !preg_match("/\.(".$cfg_mediatype.")$/",$GLOBALS[$upname.'_name']))
            {
#                ShowMsg('你所上传的文件类型必须为：'.$cfg_mediatype, '-1');
#                exit();
            }
            else if(!preg_match("/\.(".$allAllowType.")$/", $GLOBALS[$upname.'_name']))
            {
#                ShowMsg("你所上传的文件类型不被允许！",'-1');
#                exit();
            }
            //再次严格检测文件扩展名是否符合系统定义的类型
            $fs = explode('.', $GLOBALS[$upname.'_name']);
            $sname = $fs[count($fs)-1];
            $alltypes = explode('|', $allAllowType);
            if(!in_array(strtolower($sname), $alltypes))
            {
#                ShowMsg('你所上传的文件类型不被允许！', '-1');
#                exit();
            }
            //强制禁止的文件类型
            if(preg_match("/(asp|php|pl|cgi|shtm|js)$/", $sname))
            {
#                ShowMsg('你上传的文件为系统禁止的类型！', '-1');
#                exit();
            }
            if($exname=='')
            {
                $filename = $cfg_user_dir."/$userid/".dd2char($nowtme.'-'.mt_rand(1000,9999)).'.'.$sname;
            }
            else
            {
                $filename = $cfg_user_dir."/{$userid}/{$exname}.".$sname;
            }
            move_uploaded_file($GLOBALS[$upname], $cfg_basedir.$filename) or die("上传文件到 {$filename} 失败！");
            @unlink($GLOBALS[$upname]);
            
            if(@filesize($cfg_basedir.$filename) > $GLOBALS['cfg_mb_upload_size'] * 1024)
            {
                @unlink($cfg_basedir.$filename);
#                ShowMsg('你上传的文件超出系统大小限制！', '-1');
#                exit();
            }
            
            //加水印或缩小图片
            if($utype=='image')
            {
                include_once(DEDEINC.'/image.func.php');
                if($maxwidth>0 || $maxheight>0)
                {
                    ImageResize($cfg_basedir.$filename, $maxwidth, $maxheight);
                }
                else if($water)
                {
                    WaterImg($cfg_basedir.$filename);
                }
            }
            return $filename;
        }
        //没有上传文件
        else
        {
            //强制禁止的文件类型
            if($handname=='')
            {
                return $handname;
            }
            else if(preg_match("/\.(asp|php|pl|cgi|shtm|js)$/", $handname))
            {
                exit('Not allow filename for not safe!');
            }
            else if( !preg_match("/\.(".$allAllowType.")$/", $handname) )
            {
                exit('Not allow filename for filetype!');
            }
            // 2011-4-10 修复会员中心修改相册时候错误(by:jason123j)
            else if( !preg_match('#^http:#', $handname) && !preg_match('#^'.$cfg_user_dir.'/'.$userid."#", $handname) && !$isadmin )
            {
                exit('Not allow filename for not userdir!');
            }
            return $handname;
        }
    }
