<?php
/**
 * 根据指定的类别ID获得该ID的文章列表
 * 当ID为空时调用最新的文章列表
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

include_once (dirname(__FILE__)."/../include/common.inc.php");

$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$end = $number;
$typeid = intval($_POST['typeid']);
$where = "";
if($typeid){
#	show_message(1);
	$where = " WHERE typeid={$typeid} ";
	if($typeid == -1){
		$where = " WHERE flag like 'c%' ";
	}
}
$query = "SELECT * FROM #@__archives $where ORDER BY id DESC limit $start, $end";
$dsql->SetQuery($query);
$dsql->Execute();
$sdata = $data = array();

$rows = $dsql->GetTotalRow();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $title = stripslashes(htmlspecialchars($row['title']));
	    $description = stripslashes(htmlspecialchars($row['description']));
		$litpic = $row['litpic'] ? 'http://www.vr2.tv'.urlencode($row['litpic']) : '';
	    $source = $row['source'] ? $row['source'] : '肥皂网';
	    $writer = $row['writer'] ? $row['writer'] : '肥皂网';
	    $click = $row['click'];
	    $aid = intval($row['id']);
	#   $pubdate = GetDateTimeMk($row['pubdate']);
		$sdata[$aid] = array(
			'id'    => $aid,
			'title'    => urlencode($title),
			'typeid'   => urlencode($row['typeid']),
			'description' => urlencode($description),
			'writer'    => urlencode($writer),
			'source'   => urlencode($source),
			'litpic'   => $litpic,
			'pubdate'  => $row['pubdate'],
			'click' => intval($click),
		);
		$aidsarr[] = $aid;
	}
	if(!empty($aidsarr)){
		$aids = implode(",", $aidsarr);
		$where = " WHERE aid IN ($aids)";
		$query = "SELECT * FROM #@__addonarticle $where";
		$dsql->SetQuery($query);
		$dsql->Execute();
		$rows = $dsql->GetTotalRow();
		$urls = array();
		if(!empty($rows)){
			while($row = $dsql->GetArray()){
			   $sdata[$row['aid']]['redirecturl'] = $row['redirecturl'];
			}
			if(!empty($sdata)){
				foreach ($sdata as $val){
					$data[] = $val;
				}
			}
		}
	}
}
show_message(0,$data);

