<?php
/**
 * 提供给APP进行查看我的收藏的接口
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");

#include_once (dirname(__FILE__)."/../include/common.inc.php");

include_once (dirname(__FILE__)."/../member/config.php");

$userid = $_POST['username'];
$mid = $_POST['mid'];
$number = empty($_POST['count']) ? 20 : intval($_POST['count']);
$start = empty($_POST['page']) ? 0 : intval($_POST['page']-1)*$number;
$end = $number;

$row = $dsql->GetOne("Select * From `#@__member` where mid='$mid'");
if(!is_array($row)){
	show_message(17);
}

$query = "Select * From `#@__member_stow` where mid='{$mid}' limit $start, $end";
$dsql->SetQuery($query);
$dsql->Execute();
$rows = $dsql->GetTotalRow();
$row = $dsql->GetOne();
$sdata = array();
if(!empty($rows)){
	while($row = $dsql->GetArray()){
	    $title = htmlspecialchars($row['title']);
	    $addtime = GetDateTimeMk($row['addtime']);
	    $aid = intval($row['aid']);
		$sdata[$aid] = array(
			'aid'    => intval($row['aid']),
			'title'    => urlencode($title),
			'addtime'   => $addtime,
		);
		$aidsarr[] = $aid;
	}
}
if(!empty($aidsarr)){
	$aids = implode(",", $aidsarr);
	$where = " WHERE id IN ($aids)";
	$query = "SELECT * FROM #@__archives $where";
	$dsql->SetQuery($query);
	$dsql->Execute();
	$rows = $dsql->GetTotalRow();
	if(!empty($rows)){
		while($row = $dsql->GetArray()){
		    $title = htmlspecialchars($row['title']);
		    $description = htmlspecialchars($row['description']);
			$litpic = $row['litpic'];
		    $source = $row['source'] ? $row['source'] : '肥皂网';
		    $writer = $row['writer'] ? $row['writer'] : '肥皂网';
		    $click = $row['click'];
		#   $pubdate = GetDateTimeMk($row['pubdate']);
			$data[] = array(
				'id'    => intval($row['id']),
				'title'    => urlencode($title),
				'typeid'   => urlencode($row['typeid']),
				'description' => urlencode($description),
				'writer'    => urlencode($writer),
				'source'   => urlencode($source),
				'litpic'   => 'http://www.vr2.tv'.urlencode($litpic),
				'pubdate'  => $row['pubdate'],
				'click' => intval($click),
				'addtime'   => $sdata[$row['id']]['addtime'],
			);
		}
	}
}

show_message(0,$data);

