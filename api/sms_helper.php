<?php

include_once '../SDK/CCPRestSDK.php';

//主帐号
$accountSid = 'aaf98f89521b91a3015220064a2807c5';

//主帐号Token
$accountToken = '7bf1ab09978044eaa650c8c0b52a8cf8';

//应用Id
$appId = '8a48b551521b87bc01522509075c0f9c';

//请求地址，格式如下，不需要写https://
$serverIP = 'app.cloopen.com';

//请求端口
$serverPort = '8883';

//REST版本号
$softVersion = '2013-12-26';

/**
 * 发送模板短信
 * @param to 手机号码集合,用英文逗号分开
 * @param datas 内容数据 格式为数组 例如：array('Marry','Alon')，如不需替换请填 null
 * @param $tempId 模板Id
 */
function sendTemplateSMS($to, $datas, $tempId) {
    // 初始化REST SDK
    global $accountSid, $accountToken, $appId, $serverIP, $serverPort, $softVersion;
    $rest = new REST($serverIP, $serverPort, $softVersion);
    $rest->setAccount($accountSid, $accountToken);
    $rest->setAppId($appId);

    // 发送模板短信
    $result = $rest->sendTemplateSMS($to, $datas, $tempId);
    if ($result == NULL) {
        return array(0, 'not return result!');
    }
    if ($result->statusCode != 0) {
        return array(0, 'error code :' . $result->statusCode . ', error msg :' . $result->statusMsg);
    } else {
        // 获取返回信息
        $smsmessage = $result->TemplateSMS;
        return array(1, $smsmessage->dateCreated, $smsmessage->smsMessageSid);
    }
}
