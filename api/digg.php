<?php
/**
 * 提供给APP进行登陆的接口
 * @author      jch
 * @date        2014年8月16日
 */

include_once ("./common.php");
include_once (dirname(__FILE__)."/../include/common.inc.php");

$id = isset($_REQUEST['aid']) ? intval($_REQUEST['aid']) : 0;
$maintable = '#@__archives';

$action = isset($_REQUEST['action']) && $_REQUEST['action'] == 'bad' ? 'bad' : 'good';
if($id)
{
    $row = $dsql->GetOne("SELECT goodpost,badpost,scores FROM `$maintable` WHERE id='$id' ");
    if($row)
    {
        if($action == 'good')
        {
            $dsql->ExecuteNoneQuery("UPDATE `$maintable` SET goodpost=goodpost+1 WHERE id='$id'");
        }
        else if($action=='bad')
        {
            $dsql->ExecuteNoneQuery("UPDATE `$maintable` SET badpost=badpost+1 WHERE id='$id'");
        }
        show_message(0);
    }else{
        show_message(2);
    }
}else{
	show_message(1);
}

